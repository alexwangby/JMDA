/**
 * 
 */
function addData(){
	//var url = '/execute/form/ext/getPage?formId=e8f7da4149094c33ba7b1b77c03c0275&id=';
	//openFormSelector('新建', 800, 500, url);
	var row = $('#data_table').datagrid('getSelected');
	if(row==null){
		jQuery.messager.alert('提示', '请选择一行!');
		return;
	}
	if(row.type=='root'){
		var id = row.id;
		var text = row.text;
		var url = '/execute/form/ext/getPage?formId=b24ae607dd3b41acb9da94ea65a5d596&id=&companyId='+id+'&companyName='+text;
		openFormSelector('添加公司', 800, 500, url);
	}else if(row.type=='company'){
		var id = row.id;
		var text = row.text;
		var url = '/execute/form/ext/getPage?formId=348d4813be014a3ebc5428453cc7dd66&id=&companyId='+id+'&companyName='+text;
		openFormSelector('添加部门', 800, 500, url);
	}else if(row.type=='dept'){
		var id = row.id;
		var text = row.text;
		var companyId = row.companyId;
		var companyName = row.companyName;
		var url = '/execute/form/ext/getPage?formId=348d4813be014a3ebc5428453cc7dd66&id=&departmentPid='+id+'&departmentPName='+text+'&companyId='+companyId+'&companyName='+companyName;
		openFormSelector('添加部门', 800, 500, url);
	}else {
		jQuery.messager.alert('提示', '类型不符合，不能添加!');
	}
}
function updateData(){
	var row = $('#data_table').datagrid('getSelected');
	if(row==null){
		jQuery.messager.alert('提示', '请选择一行!');
		return;
	}
	if(row.type=='company'){
		var id = row.id;
		var url = '/execute/form/ext/getPage?formId=b24ae607dd3b41acb9da94ea65a5d596&id='+id;
		openFormSelector('修改公司', 800, 500, url);
	}else if(row.type=='dept'){
		var id = row.id;
		var url = '/execute/form/ext/getPage?formId=348d4813be014a3ebc5428453cc7dd66&id='+id;
		openFormSelector('修改部门', 800, 500, url);
	}
}

/**
 * 批量删除数据操作
 * 注：需要将 TABLE_ID 替换成需要查询标的主键字段名称
 */
function deleteDatas() {
	var row = $("#data_table").datagrid('getSelected');
	if (row!=null) {
		jQuery.messager.confirm("提示", "您确定要删除选择的数据吗？", function(res) {// 提示是否删除
			if (res) {
				var url = "";
				if(row.type=='root'){
					jQuery.messager.alert('提示', '根节点无法删除，请重新选择!');
					return false;
			    }else if(row.type=='company'){
			    	url = basePath + "/system/SysCompany/deleteDatas";
			    }else if(row.type=='dept'){
			    	url = basePath + "/system/SysDepartment/deleteDatas";
			    }else {
					jQuery.messager.alert('提示', '选择删除的行无法识别，请重新选择!');
					return false;
				}
				$.ajax({
					url : url,
					data : {
						ids : row.id
					},
					success : function(data) {
						LoadModel.hide(); 
						if (data == 'ok') {
							jQuery.messager.alert('提示', '删除成功!');
							//$('#data_table').treegrid('reload');
							window.location.reload();
						}else if(data == '1'){
							jQuery.messager.alert('提示', '当前节点下存在子节点，请先删除子节点!');
						}else if(data == '2'){
							jQuery.messager.alert('提示', '当前节点下存在人员，请先删除人员!');
						} else {
							jQuery.messager.alert('提示', '删除失败!');
						}
						
					}
				});

			}
		});
	} else {
		jQuery.messager.alert('提示', '请选择要删除的行!');
	}
}
function titleformatter(value, row, index) {
	var type = row.type;
	if (value != null&&row.type=='dept') {
		var html1 = "<a href=\"javascript:moveUp('"+value+"')\" class=\"btn btn-success btn-xs icon-only white\"><i class=\"fa  fa-arrow-up\"></i></a>";
		var html2 = "<a href=\"javascript:moveDown('"+value+"')\" class=\"btn btn-danger  btn-xs icon-only white\"><i class=\"fa fa-arrow-down\"></i></a>";
		return html1+"&nbsp;&nbsp;&nbsp;"+html2;
	} else {
		return "";
	}
}
function moveUp(id){
	LoadModel.show();
	$.ajax({
		url : basePath + "/system/SysDepartment/moveUp",
		data : {
			id : id
		},
		success : function(data) {
			LoadModel.hide(); 
			if (data == 'ok') {
				$('#data_table').treegrid('reload');
			} else {
				jQuery.messager.alert('提示', '操作失败!');
			}
			
		}
	});
}
function moveDown(id){
	LoadModel.show();
	$.ajax({
		url : basePath + "/system/SysDepartment/moveDown",
		data : {
			id : id
		},
		success : function(data) {
			LoadModel.hide(); 
			if (data == 'ok') {
				$('#data_table').treegrid('reload');
			}else {
				jQuery.messager.alert('提示', '操作失败!');
			}
			
		}
	});
}
function treeFormatter(value,row,index){
	var priClass="";
	var html="<input type='checkbox' onclick=\"checkTree('"+row.id+"')\"  class='tree-checkbox' value='"+row.id+"' id='cid_"+row.id+"'  /><div class='tree-checkbox-text'>"+value+"</div>";
	if(row.checked){
		html="<input type='checkbox' checked='checked' onclick=\"checkTree('"+row.id+"')\"  class='tree-checkbox' value='"+row.id+"' id='cid_"+row.id+"'  /><div class='tree-checkbox-text'>"+value+"</div>";
	}
	return html;

}