var currentNode ;
function clickWestTreeNode(node) {
    currentNode = node;
    //如果点击的是公司
    if (node.type == "company") {
    	dataGridParamsRefresh({companyId: node.id});
    } else if(node.type == "dept"){
        dataGridParamsRefresh({departmentId: node.id});
    }else {
    	
    }
}


/**
 * 表格字段格式化字段 参数形式可参照 jEasyUI1.3.6版API 中关于表格格式化函数
 * 注：需要将 TABLE_ID 替换成需要查询标的主键字段名称
 * @param value
 * @param row
 * @param index
 * @returns
 */
function titleformatter(value, row, index) {
	if (value != null) {
		return "<span style=\"color:#1331D7;cursor:pointer;\" onClick=\"openFormTitle('"
				+ row.TABLE_ID + "');return false;\">" + value + "</span>";
	} else {
		return value;
	}
}
/**
 * 新建或者添加按钮点击后弹出表单对话框
 */
function addData() {
	var url = '/execute/form/ext/getPage?formId=e8f7da4149094c33ba7b1b77c03c0275&id=';
	if(currentNode!=undefined){
		if(currentNode.type == "dept"){
			url = '/execute/form/ext/getPage?formId=e8f7da4149094c33ba7b1b77c03c0275&id='+'&departmentId='+currentNode.id;
		}
	}
	
	// 获取 新建表单URL 参数为表单唯一标识,新建表单主键值为空即可
	
	openFormSelector('添加人员', 900, 500, url);
}
/**
 * 单元格格式化事件后，显示为链接，点击后弹出表单对话框
 */
function openFormTitle(id, title) {
	// 获取 修改表单URL 参数为表单唯一标识和数据主键值
	var url = getFormUrl('e8f7da4149094c33ba7b1b77c03c0275', id);
	openFormSelector('修改', 900, 500, url);
}
/**
 * 修改按钮点击后弹出表单对话框
 * 注：需要将 TABLE_ID 替换成需要查询标的主键字段名称
 */
function updateData() {
	var rows = $("#data_table").datagrid('getSelections');
	if (rows.length == 1) {
		var id = rows[0].PERSON_ID;
		// 打开修改表单 参数为表单唯一标识和数据主键值
		var url = getFormUrl('e8f7da4149094c33ba7b1b77c03c0275', id);
		openFormSelector('修改人员', 900, 500, url);
	} else {
		jQuery.messager.alert('提示', '请选择要修改的数据且只能选择一行!');
	}
}
/**
 * 批量删除数据操作
 * 注：需要将 TABLE_ID 替换成需要查询标的主键字段名称
 */
function deleteDatas() {
	var rows = $("#data_table").datagrid('getSelections');
	if (rows.length > 0) {
		jQuery.messager.confirm("提示", "您确定要删除选择的数据吗？", function(res) {// 提示是否删除
			if (res) {
				LoadModel.show();
				var codes = [];
				for (var i = 0; i < rows.length; i++) {
					codes.push(rows[i].PERSON_ID);
				}
				var json = codes.join(',');
				$.ajax({
					url : basePath + "/system/SysPerson/deleteDatas",
					data : {
						ids : json
					},
					success : function(data) {
						LoadModel.hide();
						if (data == 'ok') {
							jQuery.messager.alert('提示', '删除成功!');
						} else {
							jQuery.messager.alert('提示', '删除失败!');
						}
						dataGridRefresh();
					}
				});

			}
		});
	} else {
		jQuery.messager.alert('提示', '请选择要删除的行!');
	}
}
function onDblClickGridRow(rowIndex, rowData){
	$('#data_table').datagrid("unselectAll");
	$('#data_table').datagrid("selectRow", rowIndex);
	var PERSON_ID = rowData.PERSON_ID;
	var param = "?personId=" + PERSON_ID;
	rightTreeRefresh(param);
}
