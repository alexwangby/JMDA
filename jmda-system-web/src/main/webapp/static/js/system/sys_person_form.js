$(document).ready(function() {
	var personId = $("input[name='personId']").val();
	if(personId==''){
		$("select[name='departmentId']").val(param_departmentId);
	}else{
		$("input[name='loginName']").attr("readonly","readonly");
	}
});
//初始化密码
function initPassWord(){

}
/**
 * 保存按钮 保存当前表单数据
 * 
 * @returns {Boolean}
 */
function saveData() {
	// TODO 表单校验跟进JMDA配置的表单属性 为非空验证
	var isValid = $('#data_form').form('validate');
	if (isValid) {
	} else {
		jQuery.messager.alert('提示', '验证不通过');
		return false;
	}
		// TODO 提交保存时将权限组表格中所有的数据另存到, Id为 [sys_rm_group_person]<hidden>标签中
	coverGridJsonDatas('sys_rm_group_person');
	// 将表单数据提交到后台进行处理
	LoadModel.show();
	$("#data_form").form('submit', {
		url : basePath + "/system/SysPerson/save",
		dataType : 'text',
		success : function(data) {
		    LoadModel.hide(); 
			// 保存失败
			if (data == "no") {
				jQuery.messager.alert('提示', '保存失败!');
			} else if (data == "ok") {
				// 保存成功
				jQuery.messager.alert('提示', '保存成功');
				// 刷新父窗体表格
				try {
					parent.dataGridRefresh();
				} catch (e) {
				}
				// 关闭当期表单窗口
				parent.DialogHelper.closeDialog();
			}

		}

	});

}

function add(){
    openFormSelector('sys_rm_group_selector.xml',sys_rm_group_selector, '选择权限组', 800, 400, '');
}
function sys_rm_group_selector(datas){
	var personId = $("input[name='personId']").val();
	for (var i = 0; i < datas.length; i++) {
		var bool = true;
		var data = datas[i];
		var index = $('#sys_rm_group_person_data_table').datagrid('getRows').length;
		var row = $('#sys_rm_group_person_data_table').datagrid('getRows');
		for (var c = 0; c < index; c++) {
			if (row[c].rmGroupId == data.RM_GROUP_ID) {
				bool = false;
			}
		}
		if(bool){
			$('#sys_rm_group_person_data_table').datagrid('insertRow', {
				index : index, // 索引从0开始
				row : {
					name:data.NAME,
					personId:personId,
					rmGroupId : data.RM_GROUP_ID
				}
			});
		}

	}
}
function deleteDatas(){
	var delRows = $("#sys_rm_group_person_data_table").datagrid('getSelections');
    if (delRows.length > 0) {
    	 jQuery.messager.confirm("提示", "您确定要删除选择的数据吗？", function (res) {// 提示是否删除
             if (res) {
             	var index = $('#sys_rm_group_person_data_table').datagrid('getRows').length;
            	var rows = $('#sys_rm_group_person_data_table').datagrid('getRows');
            	//删除
            	for (var l = 0; l < index; l++) {
            		for (var i = 0; i < delIndex; i++){
            			if (null !=rows[l] && null != delRows[i]){
            				if (rows[l].materialId == delRows[i].materialId){
            					$("#sys_rm_group_person_data_table").datagrid('deleteRow', l);
            				}
            			}
            		}
            	}
             }
    	 });
    }
}
