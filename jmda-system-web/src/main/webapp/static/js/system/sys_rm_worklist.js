/**
 * 表格字段格式化字段 参数形式可参照 jEasyUI1.3.6版API 中关于表格格式化函数
 * 注：需要将 TABLE_ID 替换成需要查询标的主键字段名称
 * @param value
 * @param row
 * @param index
 * @returns
 */
function titleformatter(value, row, index) {
	
	if (value != null) {
		var html1 = "<a href=\"javascript:moveUp('"+value+"')\" class=\"btn btn-success btn-xs icon-only white\"><i class=\"fa  fa-arrow-up\"></i></a>";
		var html2 = "<a href=\"javascript:moveDown('"+value+"')\" class=\"btn btn-danger  btn-xs icon-only white\"><i class=\"fa fa-arrow-down\"></i></a>";
		return html1+"&nbsp;&nbsp;&nbsp;"+html2;
	} else {
		return value;
	}
}
function moveUp(id){
	LoadModel.show();
	$.ajax({
		url : basePath + "/system/SysRm/moveUp",
		data : {
			rmId : id
		},
		success : function(data) {
			LoadModel.hide(); 
			if (data == 'ok') {
				$('#data_table').treegrid('reload');
			} else {
				jQuery.messager.alert('提示', '操作失败!');
			}
			
		}
	});
}
function moveDown(id){
	LoadModel.show();
	$.ajax({
		url : basePath + "/system/SysRm/moveDown",
		data : {
			rmId : id
		},
		success : function(data) {
			LoadModel.hide(); 
			if (data == 'ok') {
				$('#data_table').treegrid('reload');
			}else {
				jQuery.messager.alert('提示', '操作失败!');
			}
			
		}
	});
}
/**
 * 新建或者添加按钮点击后弹出表单对话框
 */
function addData() {
	var row = $('#data_table').datagrid('getSelected');
	if(row==null){
		var url = '/execute/form/ext/getPage?formId=6f1a8a5be0a84ceda6a59278f04569da&id=&rmPid=';
		openFormSelector('新建', 800, 500, url);
	}else{
		var pid = row.id;
		var url = '/execute/form/ext/getPage?formId=6f1a8a5be0a84ceda6a59278f04569da&id=&rmPid='+pid;
		openFormSelector('新建', 800, 500, url);
	}
	// 获取 新建表单URL 参数为表单唯一标识,新建表单主键值为空即可

}
/**
 * 单元格格式化事件后，显示为链接，点击后弹出表单对话框
 */
function openFormTitle(id, title) {
	// 获取 修改表单URL 参数为表单唯一标识和数据主键值
	var url = getFormUrl('6f1a8a5be0a84ceda6a59278f04569da', id);
	openFormSelector('修改', 800, 500, url);
}
/**
 * 修改按钮点击后弹出表单对话框
 * 注：需要将 TABLE_ID 替换成需要查询标的主键字段名称
 */
function updateData() {
	var row = $('#data_table').datagrid('getSelected');
	if(row==null){
		jQuery.messager.alert('提示', '请选择一行!');
		return;
	}
	var id = row.id;
	var url = getFormUrl('6f1a8a5be0a84ceda6a59278f04569da', id);
	openFormSelector('修改', 800, 500, url);
	
}
/**
 * 批量删除数据操作
 * 注：需要将 TABLE_ID 替换成需要查询标的主键字段名称
 */
function deleteDatas() {
	var row = $("#data_table").datagrid('getSelected');
	if (row!=null) {
		jQuery.messager.confirm("提示", "您确定要删除选择的数据吗？", function(res) {// 提示是否删除
			LoadModel.show();
			if (res) {
				$.ajax({
					url : basePath + "/system/SysRm/deleteDatas",
					data : {
						ids : row.id
					},
					success : function(data) {
						LoadModel.hide(); 
						if (data == 'ok') {
							jQuery.messager.alert('提示', '删除成功!');
							//$('#data_table').treegrid('reload');
							window.location.reload();
						}else if(data == '1'){
							jQuery.messager.alert('提示', '当前节点下存在子节点，请先删除子节点!');
						} else {
							jQuery.messager.alert('提示', '删除失败!');
						}
						
					}
				});

			}
		});
	} else {
		jQuery.messager.alert('提示', '请选择要删除的行!');
	}
}
