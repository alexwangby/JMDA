/**
 * 保存按钮 保存当前表单数据
 * 
 * @returns {Boolean}
 */
function saveData() {
	// TODO 表单校验跟进JMDA配置的表单属性 为非空验证
	var isValid = $('#data_form').form('validate');
	if (isValid) {
	} else {
		return false;
	}
		// 将表单数据提交到后台进行处理
	LoadModel.show();
	$("#data_form").form('submit', {
		url : basePath + "/system/SysRmGroup/save",
		dataType : 'text',
		success : function(data) {
		    LoadModel.hide(); 
			// 保存失败
			if (data == "no") {
				jQuery.messager.alert('提示', '保存失败!');
			} else if (data == "ok") {
				// 保存成功
				jQuery.messager.alert('提示', '保存成功');
				// 刷新父窗体表格
				try {
					parent.dataGridRefresh();
				} catch (e) {
				}
				// 关闭当期表单窗口
				parent.DialogHelper.closeDialog();
			}
		}
	});
}