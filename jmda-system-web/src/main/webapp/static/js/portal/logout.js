    jQuery.cookie = function(name, value, options) {   
        if (typeof value != 'undefined') { // name and value given, set cookie  
            options = options || {};  
            if (value === null) {  
                value = '';  
                options.expires = -1;  
            }  
            var expires = '';  
            if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {  
                var date;  
                if (typeof options.expires == 'number') {  
                    date = new Date();  
                    date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));  
                } else {  
                    date = options.expires;  
                }  
                expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE  
            }  
            // CAUTION: Needed to parenthesize options.path and options.domain  
            // in the following expressions, otherwise they evaluate to undefined  
            // in the packed version for some reason...  
            var path = options.path ? '; path=' + (options.path) : '';  
            var domain = options.domain ? '; domain=' + (options.domain) : '';  
            var secure = options.secure ? '; secure' : '';    
            document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');  
        } else { // only name given, get cookie  
            var cookieValue = null;  
            if (document.cookie && document.cookie != '') {  
                var cookies = document.cookie.split(';');  
                for (var i = 0; i < cookies.length; i++) {  
                    var cookie = jQuery.trim(cookies[i]);  
                    // Does this cookie string begin with the name we want?  
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {  
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));  
                        break;  
                    }  
                }  
            }  
            return cookieValue;  
        }  
    };  


function logoutBPM(form) {
	if (false == confirm("确认离开系统吗？")) {
		return false;
	}
	form.target = "_self";
	form.action = pbsBasePath + "/logout";
	form.submit();
	return false;
}
var timer;
jQuery(function(){
	/*var userid= jQuery('#user_id').val();
	//$.cookie("pbs_session_userid", user_id);//设置cookie 名字，值
	jQuery(window).bind("beforeunload", function() {
		jQuery.ajax({
			type:"POST",
			url : encodeURI(pbsBasePath + "logout/destroySession.wf?userid="+userid),
			async : false
		});
	});
	 window.setInterval('sessionCookieCheck()',2000);*/
});

function sessionCookieCheck(){
	/*var userid= jQuery('#user_id').val();
	var user_id= jQuery('#ID').val();
	//session 乱了 跳转到登录页面  清除session
	 var cookie_userid= jQuery.cookie('pbs_session_userid');
	if(user_id!=cookie_userid){
		jQuery.ajax({
			type:"POST",
			url : pbsBasePath + "logout/destroySession.wf?userid="+userid,
			async : false
		});
		location.href =pbsBasePath+'index.jsp';
		window.clearInterval();
	}*/
}
