var APP_ITEM_HEIGHT = 28;
var MIN_PNAEL_HEIGHT = 11 * APP_ITEM_HEIGHT;
var SCROLL_HEIGHT = 4 * APP_ITEM_HEIGHT;
var SCREEN_MAX_APP_NUM = 32;
// -- 模块对应ID固定 --
// var moduleInfo =
// {"email":1,"notify":4,"news":147,"vote":119,"workflow":5,"calendar":8,"diary":81,"attendance":7,"bbs":62};
var moduleInfoIndex = {
	"a1" : "email",
	"a4" : "notify",
	"a147" : "news",
	"a119" : "vote",
	"a5" : "workflow",
	"a8" : "calendar",
	"a81" : "diary",
	"a7" : "attendance",
	"a62" : "bbs"
};

// -- 可用菜单图标 --
var availappicon = [ '@crs', 'address', 'attendance', 'attendance_manage', 'bbs', 'calendar', 'comm', 'default', 'diary', 'email', 'erp', 'exam_manage', 'fax', 'file_folder',
		'hrms', 'info', 'meeting', 'mytable', 'netdisk', 'news', 'notify', 'notify_auditing', 'person_info', 'picture', 'project', 'reportshop', 'roll_manage', 'sale_manage',
		'score', 'sms', 'sys', 'system', 'todo', 'training', 'vehicle', 'vote', 'wf_destory', 'wf_entrust', 'wf_log', 'wf_mine', 'wf_new', 'wf_query', 'wf_stat', 'wiki',
		'work_plan', 'workflow', 'ewp_baidu', 'ewp_sina', 'ewp_58', 'ewp_weather', 'ewp_qiyi', 'ewp_gov', 'ewp_tianya', 'ewp_163', 'ewp_taobao', 'ewp_youku', 'ewp_bdtieba',
		'ewp_sinas', 'ewp_sinanews', 'ewp_bdmp3', 'ewp_xiami' ];
// -- 可用菜单图标 --
var pWindow = window.parent;
var fmenu = window.parent.first_array;
var smenu = window.parent.second_array;
var tmenu = window.parent.third_array;

var default_icon = 'default';
var s_default_icon = 'oa';
var rowAppNum = 8;

window.onactive = function() {
	jQuery(window).triggerHandler('resize');
	window.onactive = null;
};

// lp 设置界面Html 结构
var appboxHtml = '';
appboxHtml = '<div id="portalSetting">';
appboxHtml += '<div id="bar" class="ui-layout-north">';
appboxHtml += '<span id="btnAppSet"></span>';
appboxHtml += '<span id="btnScreenSet"></span>';
appboxHtml += '<span id="portalSettingMsg"></span>';
appboxHtml += '</div>';
appboxHtml += '<div id="appPageAll" class="ui-layout-center">';
appboxHtml += '<div id="appPageDom" class="appPage">';
appboxHtml += '<div id="app_cate_list" class="ui-layout-west">';
appboxHtml += '<div class="scroll-up"></div>';
appboxHtml += '<ul>';
appboxHtml += '<div class="clearfix"></div>';
appboxHtml += '</ul>';
appboxHtml += '<div class="scroll-down"></div>';
appboxHtml += '</div>';
appboxHtml += '<div id="app_list_box" class="ui-layout-center">';
appboxHtml += '<div id="app_list_record"></div>';
appboxHtml += '<ul></ul>';
appboxHtml += '<div class="clearfix"></div>';
appboxHtml += '</div>';
appboxHtml += '</div>';
appboxHtml += '<div id="screenPageDom">';
appboxHtml += '<div id="screen_list">';
appboxHtml += '<div class="clearfix"></div>';
appboxHtml += '<ul></ul>';
appboxHtml += '</div>';
appboxHtml += '</div>';
appboxHtml += '</div>';
appboxHtml += '</div>';

// 过滤重复js元素 return array;
function unique(d) {
	var o = {};
	jQuery.each(d, function(i, e) {
		o[e] = i;
	});
	var a = [];
	jQuery.each(o, function(i, e) {
		a.push(d[e]);
	});
	return a;
}
// 过滤重复js元素 返回 boolean
Array.prototype.S = String.fromCharCode(2);
Array.prototype.in_array = function(e) {
	var r = new RegExp(this.S + e + this.S);
	return (r.test(this.S + this.join(this.S) + this.S));
}

function isTouchDevice() {
	try {
		document.createEvent("TouchEvent");
		return true;
	} catch (e) {
		return false;
	}
}

// 添加桌面应用 e {"func_id": ,"id": ,"name":} index 为要添加应用的屏幕索引
/*
 * function addApp(e, index) { var s = slideBox.getScreen(index); if (s) { var
 * ul = s.find("ul"); if (!ul.length) { ul = jQuery("<ul></ul>");
 * s.append(ul); ul.sortable({ revert: true, //delay: 200, //distance: 10,
 * //延迟拖拽事件(鼠标移动十像素),便于操作性 tolerance: 'pointer', //通过鼠标的位置计算拖动的位置*重要属性*
 * connectWith: ".screen ul", scroll: false, stop: function(e, ui) {
 * setTimeout(function() { jQuery(".block.remove").remove();
 * jQuery("#trash").hide(); ui.item.click(openUrl); serializeSlide(); }, 0); },
 * start: function(e, ui) { jQuery("#trash").show(); ui.item.unbind("click"); }
 * }); } addModule(e, s.find("ul")); } }
 */
function getAppMargin() {
	var clientSize = jQuery(document.body).outerWidth(true);
	var appsize = 120 * rowAppNum;
	if (clientSize > appsize) {
		var _margin = Math.floor((clientSize - appsize - 70 * 2) / 16);
	} else {
		var _margin = 0;
	}
	return _margin;
}
function refixAppPos() {
	var _margin = getAppMargin() + "px";
	jQuery("#container .screen li.block").css({
		"margin-left" : _margin,
		"margin-right" : _margin
	})
}

function addModule(e, el) {

	el = jQuery(el);
	var _id = e.id;
	fixid = fixAppImage(_id);
	var li = jQuery("<li class=\"block\"></li>");
	var tipDiv = "";
	if (e.count > 0 && e.count <= 999) {
		tipDiv = "<div class=\"count\"><div class=\"numb\">" + e.count + "</div></div>";
	} else if (e.count > 999) {
		tipDiv = "<div class=\"count\"><div class=\"numb\">999+</div></div>";
	}
	var img = jQuery("<div class='img'><p class='pbs_p_img'><img src='" + pbsBasePath + "/static/js/portal/img/app_icons/" + fixid + ".png' /></p></div>");
	var divT = jQuery(tipDiv);
	li.attr("id", "block_" + e.func_id);
	li.attr("title", e.name);
	li.attr("index", e.func_id);
	li.attr("url", e.url);
	var _margin = getAppMargin() + "px";
	li.css({
		"margin-left" : _margin,
		"margin-right" : _margin
	});
	divT.attr("id", "count_" + e.func_id);
	/*
	 * if(e.count > 0){ divT.addClass("count" + e.count); }else{ //测试
	 * divT.addClass("count1"); }
	 */
	var a = jQuery("<a class=\"icon-text\" href=\"javascript: void(0)\"></a>");
	var span = jQuery("<span></span>").text(e.name);
	li.append(img.append(divT)).append(a.append(span));
	el.append(li);
}

function delModule(el) {
	var pObj = jQuery("#container .screen ul li.block");
	pObj.each(function() {
		var index = jQuery(this).attr("index");
		if (el == index) {
			jQuery(this).remove();
			var flag = serializeSlide();
		}
	});
}

// lp 检查应用图片是否存在
function fixAppImage(e) {
	var els = availappicon;
	if (jQuery.inArray(e, els) == -1) {
		return default_icon;
	} else {
		return e;
	}
}

// lp 获取当前屏幕应用的个数
function getAppNums(index) {
	var index = (index == "" || typeof (index) == "undefined") ? slideBox.getCursor() : index;
	var num = jQuery("#container .screen:eq(" + index + ") ul li.block").size();
	return num;
}

function initModules(modules, el) {
	window.slideBox = jQuery("#container").slideBox({
		count : modules.length,
		cancel : isTouchDevice() ? "" : ".block",
		obstacle : "200",
		speed : "slow",
		touchDevice : isTouchDevice(),
		title : modules,
		control : "#control .control-c",
		listeners : {
			afterScroll : function(i) {
			},
			beforeScroll : function(i) {
				jQuery(".background").stop().animate({
					//left : -i * 70
					left:0
				}, "normal");
			}
		}
	});
	el = jQuery(el);
	var count = 0;
	jQuery.each(modules || [], function(i, e) {
		var ul = jQuery("<ul></ul>");
		slideBox.getScreen(i).append(ul)
		jQuery.each(e.items || [], function(i, e) {
			// 增加第二级列表
			addModule(e, ul);
		});
		i++;
	});
}

jQuery.noConflict();
(function($) {
	function resizeContainer() {
		var wWidth = Math.floor(parseInt((window.innerWidth || (window.document.documentElement.clientWidth || window.document.body.clientWidth)) * 0.9));
		var blockWidth = $('#container > .block:first').outerWidth();
		if (blockWidth <= 0)
			return;

		var count = Math.min(4, Math.max(3, Math.floor(wWidth / blockWidth)));
		$('#container').width(blockWidth * count);
	}

	function openUrl() {
		var id = this.id.substr(6);
		/*
		 * if(id.indexOf("ewp") > -1){ var func_id = $(this).attr('index');
		 * if(typeof(webapparrayinfo) && typeof(webapparrayinfo[func_id])) {
		 * pWindow.createTab(func_id, webapparrayinfo[func_id][0],
		 * webapparrayinfo[func_id][1], webapparrayinfo[func_id][2]); } return; }
		 */

		// if($('#count_'+id).attr('class').indexOf(' ') > 0)
		// 原有业务逻辑当出现任务时
		if (false) {
			if ($('#dialog_' + id).length <= 0)
				CreateDialog(id, $(this).attr('title'), document.body);
			$('#dialog_content_' + id).html('<div class="loading">' + td_lang.inc.msg_30 + '</div>');// 正在加载，请稍候……
			$('#overlay').show();
			$('#dialog_' + id).show();
			// LoadContent(id);
		} else {
			if ($(this).attr('url')) {
					pWindow.openURL(id, $(this).attr('title'), pbsBasePath2+$(this).attr('url'));
			}

		}
	}
	function openMemu(){
		alert('打开');
	}
	// 点击事件
	function initBlock() {
		$('#container .screen ul li.block').live("click", openUrl);
	}

	function initDialog() {
		$('div.dialogContainer', document.body).live('_show', function() {
			var wWidth = (window.innerWidth || (window.document.documentElement.clientWidth || window.document.body.clientWidth));
			var hHeight = (window.innerHeight || (window.document.documentElement.clientHeight || window.document.body.clientHeight));

			var left = 100;
			var top = 20;
			var maxWidth = wWidth - 200;
			var maxHeight = hHeight - 100;
			var minHeight = 200;

			if (wWidth - $(this).outerWidth() > 200) {
				left = Math.floor((wWidth - $(this).outerWidth()) / 2);
			} else {
				$("div.msg-content", this).width(maxWidth - 18);
			}

			if ($(this).outerHeight() < minHeight) {
				$("div.msg-content", this).height(minHeight);
			} else if (hHeight - $(this).outerHeight() > 100) {
				top = Math.floor((hHeight - $(this).outerHeight()) / 2);
			} else {
				$("div.msg-content", this).height(maxHeight - 88);
			}

			var top = 0;
			var bst = document.body.scrollTop || document.documentElement.scrollTop;
			top = Math.round((hHeight - $(this).height()) / 2 + bst) + "px";

			$(this).css({
				left : left
			});
			$(this).css({
				top : top
			});
			$('#overlay').height(hHeight);

		});

		$('div.dialogContainer', document.body).live('_hide', function() {
			var index = $(this).attr('index');
			if (index != "appbox") {
				index = moduleInfoIndex["a" + index];
				// GetCounts(index);
			}
		});

		// 对话框关闭按钮
		var dialogClose = $('a.close', $('div.dialogContainer'));
		dialogClose.live('click', function() {
			var dialog = $('div.dialogContainer:visible', document.body).first();
			dialog.trigger('_hide');
			refixminScreenbtn();
			$('#overlay').hide();
			$("body").focus();
			dialog.hide();
		});
	}

	function CreateDialog(id, title, parent) {
		var html = '<div id="dialog_' + id + '" index="' + id + '" class="dialogContainer">';
		html += '<table class="dialog" align="center">';
		html += '   <tr class="head">';
		html += '      <td class="left"></td>';
		html += '      <td class="center">';
		html += '         <div class="title">' + title + '</div>';
		html += '         <a class="close" href="javascript:;"></a>';
		html += '      </td>';
		html += '      <td class="right"></td>';
		html += '   </tr>';
		html += '   <tr class="body">';
		html += '      <td class="left"></td>';
		html += '      <td class="center">';
		html += '         <div id="dialog_content_' + id + '" class="msg-content"></div>';
		html += '      </td>';
		html += '      <td class="right"></td>';
		html += '   </tr>';
		html += '   <tr class="foot">';
		html += '      <td class="left"></td>';
		html += '      <td class="center"></td>';
		html += '      <td class="right"></td>';
		html += '   </tr>';
		html += '</table>';
		html += '</div>';
		$(parent).append(html);
	}

	function initTrash() {
	}

	// 生成一级对应的二级菜单和三级菜单 @fappid 一级菜单ID return array;
	function returnSTmenu(fappid) {
		var myapp = getScreenAppIds();
		var arrMyapp = new Array();
		var arrfinalApp = new Array();
		var arrWebApp = new Array();
		arrMyapp = myapp.split(",");
		var arrSmenu = new Array();
		if (smenu[fappid]) {
			var arrSmenu = $.grep(smenu[fappid], function(n, i) {
				return ((!arrMyapp.in_array(n)) && !(tmenu["f" + n]));
			});
			$.merge(arrfinalApp, arrSmenu);
		}

		if (smenu[fappid] && smenu[fappid].length > 0) {
			var smenulen = smenu[fappid].length;
		}

		if (smenulen > 0) {
			var _smenu = smenu[fappid];
			var arrTmenu = new Array();
			for (i = 0; i < smenulen; i++) {
				if (!tmenu["f" + _smenu[i]])
					continue;
				var arrTmenuIterm = $.grep(tmenu["f" + _smenu[i]], function(n, k) {
					return !arrMyapp.in_array(n);
				});
				$.merge(arrfinalApp, arrTmenuIterm);
			}
		}
		return unique(arrfinalApp);
	}

	// 构造屏幕设置html结构 return str;
	function returnScreen() {
		var html = '';
		var _len = slideBox.getCount();
		for ( var i = 0; i < _len; i++) {
			html += '<li class="minscreenceil" index=' + i + '>' + (i + 1) + '</li>';
		}
		return html;
	}

	// 选中桌面已有的app，@para srceenid 屏幕自然索引
	function getScreenAppIds(srceenid) {
		var idstr = sep = '';
		if (srceenid) {
			obj = $("#container .screen").eq(srceenid).find("li.block")
		} else {
			obj = $("#container .screen li.block");
		}
		obj.each(function() {
			var appid = $(this).attr("index");
			idstr += sep + appid;
			sep = ',';
		});
		return idstr;
	}

	// 显示消息 @para msg 要显示的提示文字
	function portalMessage(msg) {
		if (!msg)
			return;
		msgObj = $("#portalSettingMsg");
		msgObj.html(msg).show();
		setTimeout(function() {
			msgObj.empty().hide()
		}, 5000);
	}

	// 修正点击按钮出现屏幕小按钮width为0的现象
	function refixminScreenbtn() {
		$('#control').width(window.document.documentElement.clientWidth);
	}

	// refixDialogPos
	function refixDialogPos() {
		var dialog = $('div.extDialog:visible', document.body).first();
		height = dialog.height();
		width = dialog.width();
		var wWidth = (window.innerWidth || (window.document.documentElement.clientWidth || window.document.body.clientWidth));
		var hHeight = (window.innerHeight || (window.document.documentElement.clientHeight || window.document.body.clientHeight));
		var top = left = 0;
		var bst = document.body.scrollTop || document.documentElement.scrollTop;
		top = Math.round((hHeight - height) / 2 + bst) + "px";
		mleft = "-" + Math.round(width / 2) + "px";
		top = top < 0 ? top = 0 : top;
		dialog.css({
			"top" : top,
			"left" : "50%",
			"margin-left" : mleft
		});
	}

	$(window).resize(function() {

		refixAppPos();

		$('#overlay').height(window.document.documentElement.scrollHeight);

		refixminScreenbtn();

		refixDialogPos();

	});

	// 菜单滚动箭头事件,id为app_cate_list
	function initAppScroll(id) {
		// 菜单向上滚动箭头事件
		$('#' + id + ' > .scroll-up:first').hover(function() {
			$(this).addClass('scroll-up-hover');
		}, function() {
			$(this).removeClass('scroll-up-hover');
		});

		// 点击向上箭头
		$('#' + id + ' > .scroll-up:first').click(function() {
			var ul = $('#' + id + ' > ul:first');
			ul.animate({
				'scrollTop' : (ul.scrollTop() - SCROLL_HEIGHT)
			}, 600);
		});

		// 向下滚动箭头事件
		$('#' + id + ' > .scroll-down:first').hover(function() {
			$(this).addClass('scroll-down-hover');
		}, function() {
			$(this).removeClass('scroll-down-hover');
		});

		// 点击向下箭头
		$('#' + id + ' > .scroll-down:first').click(function() {
			var ul = $('#' + id + ' > ul:first');
			ul.animate({
				'scrollTop' : (ul.scrollTop() + SCROLL_HEIGHT)
			}, 600);
		});
	}

	function initAppListScroll() {
		var su = $("#app_cate_list .scroll-up:first");
		var sd = $("#app_cate_list .scroll-down:first");
		var scrollHeight = $("#app_cate_list ul").attr('scrollHeight');
		var orgheight = $("#app_cate_list ul").height();
		if (orgheight < scrollHeight) {
			var height = scrollHeight > MIN_PNAEL_HEIGHT ? MIN_PNAEL_HEIGHT : scrollHeight;
			$("#app_cate_list ul").height(height);
		}

		if (orgheight >= scrollHeight) {
			su.hide();
			sd.hide();
		}
		initAppScroll('app_cate_list');
	}

	function reSortMinScreen() {
		$("#screenPageDom #screen_list ul li.minscreenceil").each(function(i) {
			$(this).text(i + 1);
			$(this).attr("index", i);
		});
	}

	$(document).ready(function($) {

		$("body").focus();

		$('#overlay').height(window.document.documentElement.scrollHeight);

		// 初始化显示列数
		// resizeContainer();

		// 初始化图标
		initModules(modules);

		// 初始化图标间距
		refixAppPos();

		// 模块点击事件
		initBlock();

		// 对话框事件
		// initDialog();

		// GetCounts(moduleIdStr);

		initTrash();

		// 初始化屏幕
		/*
		 * $(".screen ul").sortable({ revert: true, //delay: 200, //distance:
		 * 10, //延迟拖拽事件(鼠标移动十像素),便于操作性 tolerance: 'pointer',
		 * //通过鼠标的位置计算拖动的位置*重要属性* connectWith: ".screen ul", scroll: false,
		 * stop: function(e, ui) { setTimeout(function() {
		 * $(".block.remove").remove(); $("#trash").hide();
		 * ui.item.click(openUrl); serializeSlide(); }, 0); }, start:
		 * function(e, ui) { $("#trash").show(); refixminScreenbtn();
		 * ui.item.unbind("click"); } });
		 */

	});

})(jQuery);

var __sto = setTimeout;
window.setTimeout = function(callback, timeout, param) {
	var args = Array.prototype.slice.call(arguments, 2);
	var _cb = function() {
		callback.apply(null, args);
	}
	return __sto(_cb, timeout);
};

// 序列化桌面上的图标,并且更新
function serializeSlide() {
	var s = "";
	jQuery("#container .screen").each(function(i, e) {
		jQuery(this).find("li.block").each(function(j, el) {
			if (!jQuery(el).attr("index"))
				return true;
			s += jQuery(el).attr("index");
			s += ",";
		});
		s += "|";
	});
	if (s.length) {
		s = s.replace(/\|$/, "");
	}
	var flag = false;
	/*
	 * jQuery.ajax({ async: false, data: {"action":"general", "icon_id": s},
	 * url: '/general/slidebox.php', success: function(r) { if (r == "+ok") {
	 * flag = true; } } });
	 */
	return flag;
}
function refreshMainPannel() {
	/*jQuery.ajax({
		type: "POST",
		url: encodeURI( pbsBasePath +'portal/skins/getPortalJson.wf'),
		success : function(r) {
			if (r != "[]") {
				var obj = jQuery.parseJSON(r); 
				jQuery.each(obj, function(i, sys) {
					jQuery.each(sys.items, function(i, fun) {
						//count_54a7ea1cb134453e9fbef5c99c0f243d
						var countEl=jQuery("#count_"+fun.id);
						var parent=jQuery("#count_"+fun.id).parent();
						if(countEl){
							jQuery(countEl).remove();
							if(fun.count>0){
								parent.append('<div class="count" id="count_'+fun.id+'"><div class="numb">'+fun.count+'</div></div>');
							}
						}
			        });
			        });
			}
		}
	});*/
}