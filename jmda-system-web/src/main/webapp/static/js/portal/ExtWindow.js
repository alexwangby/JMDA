/*弹出窗口*/
var OperateWinObj;
var operateWinObjIframe='Operate_DlgPage';
function openExtWindow(winTitle,winWidth,winHeight,isShow,isShowMaxBtn,frameUrl,isClose){
	if(typeof(isClose)=="undefined"){
		isClose = true;
	}
	if(typeof(isShow)=="undefined"){
		isShow = true;
	}
	if(typeof(frameUrl)=="undefined"){
		frameUrl = '/wait.jsp';
	}
	if(typeof(isShowMaxBtn)=="undefined"){
		isShowMaxBtn = true;
	}
	Ext.QuickTips.init();
	var id = "TMP_Operate_Div";
	if (Ext.getCmp(id)) {
		Ext.getCmp(id).destroy();
	}
	var winContainerElId = Ext.id(); 
	Ext.getBody().createChild({
		tag :'div',
		id : winContainerElId
	});
	if(!OperateWinObj){
		OperateWinObj = new Ext.Window({
			contentEl : winContainerElId,
			layout : 'fit',
			constrain : true, // 强制将window控制在viewport中
			constrainHeader:true,
			title : winTitle,
			width : winWidth,
			height : winHeight,
			shadow : false,
			maximizable : isShowMaxBtn,
			plain : true,
			closeAction : 'hide',
			closable:isClose,
			modal: true,
			keys: [{
				key: 27,
				scope: this,
				fn: function(){OperateWinObj.hide();}
			}],
			items:[{
				html:'<iframe name="Operate_DlgPage" id="Operate_DlgPage" frameborder=0 width=100% height=100% src="'+frameUrl+'"></iframe>'
			}]
		});
	}
	OperateWinObj.addListener('hide', function(){
		try{Operate_DlgPage.clearInterval(Operate_DlgPage.timer);}catch(e){}
		OperateWinObj.destroy();
		return true;
	});
	OperateWinObj.addListener('maximize', function(){//设置最大化事件防止
		//OperateWinObj.setPosition(document.body.scrollLeft,document.body.scrollTop);
		return true;
	});

	OperateWinObj.addListener('destroy', function(){
		OperateWinObj = null;
		return true;
	});

	if(isShow) {
		OperateWinObj.show();
		//openExtWindowShow(winTitle,frameUrl);
	}
}

function openExtWindowShow(winTitle,frameUrl){
	OperateWinObj.show();
	OperateWinObj.center();
	OperateWinObj.setTitle(winTitle);
	if(frameUrl!=undefined&&frameUrl!=null){
		Operate_DlgPage.location=encodeURI(frameUrl);
	}
}

/*
		var voucherTip = Ext.id(); 
		Ext.getBody().createChild({
			tag :'div',
			id : voucherTip
		});
		var EXT_WIN_TIP;
 		// 窗口右下角按钮
 		var btn = new Ext.Button({
				text:'确定',
				handler: function(){
					//eval('VOUCHER_LIST').Bank_Account_FORM
					//var flat = eval('VOUCHER_LIST').document.getElementById('flat').value;
					var url = parent.pbsBasePath+'finance/accvouch/modifyVoucher.wf';
					if(busType==2){
						url = parent.pbsBasePath+'finance/accvouch/modifyLoanVoucher.wf';
					}
					// 修改凭证验证及生成业务调用
					Ext.Ajax.request({
						url : url,
						method : 'POST',
						params : {
							bind_id : '${bindid}'
						},
						failure : function(response, options) {
							var box = Ext.MessageBox.alert('警告框', '服务器在偷懒,请稍后...');
							setTimeout(function() {
								box.hide();
							}, 1000);
						},// end failure block
						success : function(response, options) {
							var resp = Ext.util.JSON.decode(response.responseText);
							if(resp.success){
								Ext.MessageBox.alert('提示',resp.info);
								closeLaborExtWin();
							}else{
								Ext.MessageBox.alert('提示',resp.info);
								return false;
							}
						}
					});
				},
				iconCls: 'x-btn-text-icon',
				type:'button',
				icon: '../../pbs_img/edit.png'
			});
			var cancel = new Ext.Button({
				text:'取消',
				handler: function(){
					closeLaborExtWin();
					return false;
				},
				iconCls: 'x-btn-text-icon',
				type:'button',
				icon: '../../pbs_img/cancel.png'
		});
		// 新建凭证预览窗口
 		function extWinList2(winTitle,winWidth,winHeight, domName, fn, bbar, frameName, x, y){
		
			if(!EXT_WIN_TIP){
				
				EXT_WIN_TIP = new Ext.Window({
					el:voucherTip,
					y:y,
					resizable : false, // 禁止改变窗口大小
					//constrain : true, // 强制将window控制在viewport中
					constrainHeader:true, // 将窗口控制在抬头中
					//draggable:false,
					layout : 'fit',
					title : winTitle,
					width : winWidth,
					height : winHeight,
					shadow : false, // 设置阴影
					plain : true,
					closeAction : 'hide',
					modal: true,
					bbar: ['->',btn,'-',cancel],
					iconCls: 'x-group-by-icon',
					//iconCls: 'x-ext-window-by-icon',
					autoDestroy :true,
					//closable:false,
					keys: [{
						key: 27,
						scope: this,
						fn: function(){
							EXT_WIN_TIP.hide();
							return false;
						}
					}],
					items:[{
						html:'<iframe name="' + frameName + '" id="' + frameName + '" frameborder=0 style="margin-top:0px;z-index:9999;" width=100% height=100% src="../../wait.htm"></iframe>'
					}]
				});
			}
			EXT_WIN_TIP.addListener('hide', function(){
				EXT_WIN_TIP.restore();
				eval(frameName).location = "../../wait.html";
			});
			EXT_WIN_TIP.addListener('show', function(){
				EXT_WIN_TIP.setTitle(winTitle);
				EXT_WIN_TIP.setWidth(winWidth);
				EXT_WIN_TIP.setHeight(winHeight);
				//EXT_WIN_TIP.setIconClass('x-ext-window-by-icon');
				Ext.getBody().unmask(true);
			});
			EXT_WIN_TIP.show();
			r=eval(fn);
		}
		function closeLaborExtWin(){
			EXT_WIN_TIP.hide();
		}
		function voucherView(tg,type){
			var url = encodeURI(parent.pbsBasePath+'finance/accvouchview/voucherView.wf?bind_id=${bindid}');
			if(type==2){
				url = encodeURI(parent.pbsBasePath+'finance/accvouchview/loanVoucherView.wf?bind_id=${bindid}');
			}
			eformPage.document.frmForm.action = url;
			eformPage.document.frmForm.target = tg;
			eformPage.document.frmForm.submit();
			return true;
		}
*/
