var MENU_ITEM_HEIGHT = 30;
var MIN_PNAEL_HEIGHT = 8 * MENU_ITEM_HEIGHT;
var MAX_PNAEL_HEIGHT = 20 * MENU_ITEM_HEIGHT;
var SCROLL_HEIGHT = 4 * MENU_ITEM_HEIGHT;

var nextTabId = 0;

// 门户切换变量
var max_portals = 7;
var imgPosition = 0;
var curPosition = 3;
var portalIsRunning = 'udefined';
var portalImgs = null;
var movec = null;
var hide = null;

jQuery.noConflict();

(function($) {
	$.fn.addTab = function(id, title, url, closable, selected) {
		$('.over-mask-layer').hide(); // 如果有门户切换、常用任务等面板打开，则隐藏之
		$('#overlay_panel').hide();
		if (!id)
			return;
		closable = (typeof (closable) == 'undefined') ? true : closable;
		selected = (typeof (selected) == 'undefined') ? true : selected;
		var height = isTouchDevice() ? 'auto' : '100%';
		$('#tabs_container').tabs(
				'add',
				{
					id : id,
					title : title,
					closable : closable,
					selected : selected,
					style : 'height:' + height + ';',
					content : '<iframe id="tabs_' + id + '_iframe" name="tabs_' + id + '_iframe" allowTransparency= "true" src="' + url + '" onload="IframeLoaded(\'' + id
							+ '\');" border="0" frameborder="0" framespacing="0" marginheight="0" marginwidth="0" style="width:100%;height:' + height + ';"></iframe>'
				});
	};
	$.fn.selectTab = function(id) {
		alert(id);
		$('#tabs_container').tabs('select', id);
	};
	$.fn.closeTab = function(id) {
		$('#tabs_container').tabs('close', id);
	};
	$.fn.getSelected = function() {
		return $('#tabs_container').tabs('selected');
	};

	// 门户切换函数开始
	function roll(direction) {
		var length = portalImgs.length;
		var start = imgPosition;
		var offset = Math.floor((max_portals - portalImgs.length) / 2);

		if ('r' == direction) {
			for ( var i = 0; i < length; i++) {
				start = start + 1;
				if (start > (length - 1))
					start = start - length;
				portalImgs[i].src = portalArray[start + offset].src;
			}

			imgPosition = imgPosition + 1;
			if (imgPosition > (length - 1)) {
				imgPosition = imgPosition - length;
			}
		}
		if ('l' == direction) {
			var a = true;
			for ( var i = 0; i < length; i++) {
				if (a) {
					start = start - 1;
					if (start < 0) {
						start = start + length;
						a = false;
					}
					if (start < (length - 1)) {
						a = false;
					}
				} else {
					start = start + 1;
					if (start > (length - 1)) {
						start = start - length;
						a = true;
					}
				}

				portalImgs[i].src = portalArray[start + offset].src;
				if (start == (length - 1)) {
					start = -1;
				}
			}
			imgPosition = imgPosition - 1;
			if (imgPosition < 0)
				imgPosition = imgPosition + length;
		}
	}

	function right() {
		i++;
		var offset = Math.floor((max_portals - portalImgs.length) / 2);

		var posArray = [];
		for ( var j = 0; j < portalImgs.length; j++) {
			var left = $(portalImgs[j]).offset().left - $(portalImgs[j].parentNode).offset().left;
			var top = $(portalImgs[j]).offset().top - $(portalImgs[j].parentNode).offset().top;
			posArray[j] = [ $(portalImgs[j]).width(), $(portalImgs[j]).height(), left, top ];
		}

		var diffArray = [ [], [ -2, -2, -8, 1 ], [ -2, -4, -9, 1.5 ], [ -2, -5, -10, 1.5 ], [ 2, 4, -11, -1.5 ], [ 2, 4, -11, -1.5 ], [ 2, 2, -10, -1 ] ];
		for ( var j = 1; j < portalImgs.length; j++) {
			$(portalImgs[j]).css({
				width : (posArray[j][0] + diffArray[j + offset][0]),
				height : (posArray[j][1] + diffArray[j + offset][1]),
				left : (posArray[j][2] + diffArray[j + offset][2]),
				top : (posArray[j][3] + diffArray[j + offset][3])
			});
		}

		if (i > 9) {
			clearInterval(hide);
			hide = null;
			resetPortalCss();
			roll('r');
			portalIsRunning = 'false';
		}
	}

	function left() {
		i++;
		var offset = Math.floor((max_portals - portalImgs.length) / 2);

		var posArray = [];
		for ( var j = 0; j < portalImgs.length; j++) {
			var left = $(portalImgs[j]).offset().left - $(portalImgs[j].parentNode).offset().left;
			var top = $(portalImgs[j]).offset().top - $(portalImgs[j].parentNode).offset().top;
			posArray[j] = [ $(portalImgs[j]).width(), $(portalImgs[j]).height(), left, top ];
		}

		var diffArray = [ [ 9, 4, 2, -1.5 ], [ 2, 4, 9, -1.5 ], [ 2, 5, 10, -1.5 ], [ -2, -5, 11, 1.5 ], [ -2, -4, 11, 1.5 ], [ -2, -2, 10, 1 ], [] ];
		for ( var j = 0; j < portalImgs.length - 1; j++) {
			$(portalImgs[j]).css({
				width : (posArray[j][0] + diffArray[j + offset][0]),
				height : (posArray[j][1] + diffArray[j + offset][1]),
				left : (posArray[j][2] + diffArray[j + offset][2]),
				top : (posArray[j][3] + diffArray[j + offset][3])
			});
		}

		if (i > 9) {
			clearInterval(hide);
			hide = null;
			resetPortalCss();
			roll('l');
			portalIsRunning = 'false';
		}
	}

	function move(direction) {
		if (portalIsRunning != 'udefined' && portalIsRunning == 'true') {
			return;
		}

		var frequency = $.browser.msie ? 30 : 20;
		var offset = Math.floor((max_portals - portalImgs.length) / 2);

		i = 0;
		var lastIndex = portalImgs.length - 1;
		var cssIndex = $(portalImgs[lastIndex]).attr('index')

		if (direction == 'r') {
			curPosition = curPosition + 2;
			$(portalImgs[lastIndex]).css({
				left : portalImgCss[cssIndex].left,
				top : portalImgCss[cssIndex].top
			});
			hide = window.setInterval(right, frequency);
			portalIsRunning = 'true';
		}

		if (direction == 'l') {
			curPosition = curPosition - 1;
			$(portalImgs[lastIndex]).css({
				left : ("-" + portalImgCss[cssIndex].left),
				top : portalImgCss[cssIndex].top
			});
			hide = window.setInterval(left, frequency);
			portalIsRunning = 'true';
		}

		if (curPosition > (portalImgs.length - 1))
			curPosition = 0;
		if (curPosition < 0)
			curPosition = portalImgs.length - 1;
	}

	function moveC(direction) {
		if (portalIsRunning != 'true') {
			move(direction);
			clearInterval(movec);
		}
	}

	function openPortal() {
		if (hide) {
			window.setTimeout(openPortal, 300);
		} else {
			$(portalImgs[Math.floor(portalImgs.length / 2)]).triggerHandler('click');
		}
	}

	var portalImgCss = [];
	portalImgCss["0"] = {
		width : "110px",
		height : "170px",
		left : "5px",
		top : "130px",
		zIndex : "2"
	};
	portalImgCss["1"] = {
		width : "130px",
		height : "190px",
		left : "85px",
		top : "120px",
		zIndex : "3"
	};
	portalImgCss["2"] = {
		width : "150px",
		height : "230px",
		left : "175px",
		top : "105px",
		zIndex : "4"
	};
	portalImgCss["3"] = {
		width : "170px",
		height : "280px",
		left : "275px",
		top : "90px",
		zIndex : "5"
	};
	portalImgCss["4"] = {
		width : "150px",
		height : "230px",
		left : "395px",
		top : "105px",
		zIndex : "4"
	};
	portalImgCss["5"] = {
		width : "130px",
		height : "190px",
		left : "505px",
		top : "120px",
		zIndex : "3"
	};
	portalImgCss["6"] = {
		width : "110px",
		height : "170px",
		left : "605px",
		top : "130px",
		zIndex : "2"
	};

	function resetPortalCss() {
		for ( var j = 0; j < portalImgs.length; j++) {
			$(portalImgs[j]).css(portalImgCss[$(portalImgs[j]).attr('index')]);
		}
	}
	// 门户切换函数结束

	function checkActive(id) {
		if ($('#' + id + '_panel:hidden').length > 0)
			$('#' + id).removeClass('active');
		else
			window.setTimeout(checkActive, 300, id);
	}
	;
	function resizeLayout() {
		// 主操作区域高度
		var wWidth = (window.document.documentElement.clientWidth || window.document.body.clientWidth || window.innerHeight);
		var wHeight = (window.document.documentElement.clientHeight || window.document.body.clientHeight || window.innerHeight);
		var nHeight = $('#north').is(':visible') ? $('#north').outerHeight() : 0;
		var fHeight = $('#funcbar').is(':visible') ? $('#funcbar').outerHeight() : 0;
		var cHeight = wHeight - nHeight - fHeight - $('#south').outerHeight() - $('#taskbar').outerHeight();
		$('#center').height(cHeight);

		$("#center iframe").css({
			height : cHeight
		});

		/*
		 * if(isTouchDevice()) { $('.tabs-panel:visible').height(cHeight);
		 * if($('.tabs-panel > iframe:visible').height() > cHeight)
		 * $('.tabs-panel:visible').height($('.tabs-panel >
		 * iframe:visible').height()); }
		 */
		// 一级标签宽度
		var width = wWidth - $('#taskbar_left').outerWidth() - $('#taskbar_right').outerWidth();
		$('#tabs_container').width(width - $('#tabs_left_scroll').outerWidth() - $('#tabs_right_scroll').outerWidth() - 2);
		$('#taskbar_center').width(width - 1); // -1是为了兼容iPad

		$('#tabs_container').triggerHandler('_resize');
	}
	;

	// 菜单滚动箭头事件,id为first_menu
	function initMenuScroll(id) {
		// 菜单向上滚动箭头事件
		$('#' + id + ' > .scroll-up:first').hover(function() {
			$(this).addClass('scroll-up-hover');
			if (id == 'first_panel') {
				$("#first_menu > li > a.active").removeClass('active'); // 恢复一级active的菜单为正常
			}
		}, function() {
			$(this).removeClass('scroll-up-hover');
		});

		// 点击向上箭头
		$('#' + id + ' > .scroll-up:first').click(function() {
			var ul = $('#' + id + ' > ul:first');
			ul.animate({
				'scrollTop' : (ul.scrollTop() - SCROLL_HEIGHT)
			}, 600);
		});

		// 向下滚动箭头事件
		$('#' + id + ' > .scroll-down:first').hover(function() {
			$(this).addClass('scroll-down-hover');
			if (id == 'first_panel') {
				$("#first_menu > li > a.active").removeClass('active'); // 恢复一级级active的菜单为正常
			}
		}, function() {
			$(this).removeClass('scroll-down-hover');
		});

		// 点击向下箭头
		$('#' + id + ' > .scroll-down:first').click(function() {
			var ul = $('#' + id + ' > ul:first');
			ul.animate({
				'scrollTop' : (ul.scrollTop() + SCROLL_HEIGHT)
			}, 600);
		});
	}
	;
	function initTabs() {
		// 设置标签栏属性
		$('#tabs_container').tabs({
			tabsLeftScroll : 'tabs_left_scroll',
			tabsRightScroll : 'tabs_right_scroll',
			panelsContainer : 'center',
			secondTabsContainer : 'funcbar_left'
		});

		// 关闭所有标签后，显示门户切换
		$('#tabs_container').bind('_close', function() {
			if ($('a.tab', this).length <= 0 && $('#portal_panel:visible').length <= 0){
				$('#portal').trigger('click');
				tabs_portal_0_iframe.refreshMainPannel();
			}
		});
	}


	function initHome() {
		$('#start_menu').bind(
				'click',
				function() {
					for ( var i = 0; i < portalLoadArray.length; i++) {
						$().addTab('portal_' + portalLoadArray[i], portalArray[portalLoadArray[i]].title, portalArray[portalLoadArray[i]].url,
								portalArray[portalLoadArray[i]].closable, (i == 0));
						$('#tabs_portal_0').remove();
					}
					return false;
				});
	}
	function initHideTopbar() {
		// 隐藏topbar事件
		$('#hide_topbar').bind('click', function() {
			$('#north').slideToggle(300, function() {
				resizeLayout();
			});
			$(this).toggleClass('up');

			var hidden = $(this).attr('class').indexOf('up') >= 0;
			$.cookie('hideTopbar', (hidden ? '1' : null), {
				expires : 1000,
				path : '/'
			});
		});

		if ($.cookie('hideTopbar') == '1')
			$('#hide_topbar').triggerHandler('click');
	}
	// 窗口resize事件
	// if(!isTouchDevice())
	$(window).resize(function() {
		resizeLayout();
	});
	// else
	// $(window).bind('orientationchange', function(){resizeLayout();});

	$(document).ready(
			function($) {
				$('#loading').remove();

				// iPad 等触屏设备
				if (isTouchDevice()) {
					$('body').addClass('mobile-body');
					$('#center').addClass('mobile-center');
				}

				// 调整窗口大小
				resizeLayout();

				// 标签栏
				initTabs();

				// 注销事件
				//initLogout();

				// 隐藏topbar事件
				initHideTopbar();
				// home首页事件
				initHome();
				// 加载门户
				for ( var i = 0; i < portalLoadArray.length; i++) {
					$().addTab('portal_' + portalLoadArray[i], portalArray[portalLoadArray[i]].title, portalArray[portalLoadArray[i]].url,
							portalArray[portalLoadArray[i]].closable, (i == 0));
					$('#tabs_portal_0').remove();
				}

				// 点击overlay_panel时自动收回门户或者切换主题的面板
				$("#overlay_panel").click(function() {
					if ($("#portal_panel:visible").length) {
						$("#portal").trigger("click");
					}
					if ($("#theme_panel:visible").length) {
						$("#theme").trigger("click");
					}
				});
				$("#start_menu").click(function() {
					tabs_portal_0_iframe.refreshMainPannel();
				});
				
			});
})(jQuery);

function openMemu(id,name,url){
	closeTab(id) ;
	createTab(id, name, url) ;
}
// 修复setTimeout bug，使用window.setTimeout调用
if (!+'\v1') {
	(function(f) {
		window.setTimeout = f(window.setTimeout);
		window.setInterval = f(window.setInterval);
	})(function(f) {
		return function(c, t) {
			var a = [].slice.call(arguments, 2);
			return f(function() {
				c.apply(this, a)
			}, t)
		}
	});
}

var $ = function(id) {
	return document.getElementById(id);
};

function HTML2Text(html) {
	var div = document.createElement('div');
	div.innerHTML = html;
	return div.innerText;
}

function Text2Object(data) {
	try {
		var func = new Function("return " + data);
		return func();
	} catch (ex) {
		return '<b>' + ex.description + '</b><br /><br />' + HTML2Text(data) + '';
	}
}

function createTab(id, name, code, open_window) {

	jQuery('#overlay_startmenu').triggerHandler('click');
	jQuery('#funcbar_left > div.second-tabs-container').hide();
	if (code.indexOf('http://') == 0 || code.indexOf('https://') == 0 || code.indexOf('ftp://') == 0) {
		openURL(id, name, code, open_window);
		return;
	} else if (code.indexOf('file://') == 0) {
		winexe(name, code.substr(7));
		return;
	}
	var url = "";
	if (id >= 10000 && id <= 14999)
		url = '/fis/' + code
	else if (id >= 15000 && id <= 15499)
		url = '/hr/' + code
	else if (id >= 650 && id <= 1000 || code.length > 4 && code.substr(code.length - 4).toLowerCase() == ".jsp")
		url = '/app/' + code
	else
		//url = '/general/' + code
		url =  code

		// 新窗口打开或非OA模块
	if (open_window == "1" || url.indexOf('/general/') != 0) {
		openURL(id, name, url, open_window);
		return;
	}

	var url2 = 'http://www.podsoft.com.cn' + url;
	var parse = url2.match(/^(([a-z]+):\/\/)?([^\/\?#]+)\/*([^\?#]*)\??([^#]*)#?(\w*)$/i); // */
	var path = parse[4];
	var query = parse[5];

	// 菜单地址直接定义为具体文件或路径传递参数的模块
	var pos = path.lastIndexOf('/');
	if (pos > 0 && path.substr(pos + 1).indexOf('.') > 0 || query != "") {
		openURL(id, name, url, open_window);
		return;
	}
	jQuery(document).trigger('click');
}

function closeTab(id) {
	id = (typeof (id) != 'string') ? jQuery().getSelected() : id;
	tabs_portal_0_iframe.refreshMainPannel();
	jQuery().closeTab(id);
}

function IframeLoaded(id) {
	var iframe = window.frames['tabs_' + id + '_iframe'];
	if (iframe && $('tabs_link_' + id) && $('tabs_link_' + id).innerText == '') {
		$('tabs_link_' + id).innerText = !iframe.document.title ? td_lang.inc.msg_27 : iframe.document.title;// "无标题"
	}

	var iframeid = document.getElementById('tabs_' + id + '_iframe'); // iframe
																		// id
	if (document.getElementById) {
		if (iframeid && !window.opera) {
			if (iframeid.contentDocument && iframeid.contentDocument.body.offsetHeight) {
				iframeid.height = iframeid.contentDocument.body.offsetHeight;
			} else if (iframeid.Document && iframeid.Document.body.scrollHeight) {
				iframeid.height = iframeid.Document.body.scrollHeight;
			}
		}
	}
	/*
	 * if(isTouchDevice()) { jQuery(window).triggerHandler('orientationchange'); }
	 */
}

function openURL(id, name, url, open_window, width, height, left, top) {
	id = !id ? ('w' + (nextTabId++)) : id;
	if (open_window != "1") {
		window.setTimeout(function() {
			jQuery().addTab(id, name, url, true)
		}, 1);
	} else {
		width = typeof (width) == "undefined" ? 780 : width;
		height = typeof (height) == "undefined" ? 550 : height;
		left = typeof (left) == "undefined" ? (screen.availWidth - width) / 2 : left;
		top = typeof (top) == "undefined" ? (screen.availHeight - height) / 2 - 30 : top;
		window
				.open(url, id, "height=" + height + ",width=" + width + ",status=0,toolbar=no,menubar=yes,location=no,scrollbars=yes,top=" + top + ",left=" + left
						+ ",resizable=yes");
	}
	jQuery(document).trigger('click');
}

function gotoURL(id, url) {
	$('tabs_' + id + "_iframe").src = url;
}

function BlinkTabs(id) {
}

function getEvent() // 同时兼容ie和ff的写法
{
	if (document.all)
		return window.event;
	func = getEvent.caller;
	while (func != null) {
		var arg0 = func.arguments[0];
		if (arg0) {
			if ((arg0.constructor == Event || arg0.constructor == MouseEvent) || (typeof (arg0) == "object" && arg0.preventDefault && arg0.stopPropagation)) {
				return arg0;
			}
		}
		func = func.caller;
	}
	return null;
}
var relogin = 0;
/*function logout() {
	if (false == confirm("确认离开系统吗？")) {
		return false;
	}
	document.frmMain.target = "_self";
	document.frmMain.action = pbsBasePath + "logout.wf";
	document.frmMain.submit();
}*/
function winexe(NAME, PROG) {
	var URL = pbsBasePath + "general/winexe?PROG=" + PROG + "&NAME=" + NAME;
	window.open(URL, "winexe", "height=100,width=350,status=0,toolbar=no,menubar=no,location=no,scrollbars=yes,top=0,left=0,resizable=no");
}
function personConfig() {
    layer.open({
        title: ['个人设置', 'font-size:18px;'],
        area: ['500px', '400px'],
        type: 2,
        content: pbsBasePath+'/system/SysPerson/getInfoPage' //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
    });
    // layer.tab({
    //     area: ['800px', '500px'],
    //     tab: [{
    //         title: '个人设置',
    //         content: '<input name="title" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input" type="text">'
    //     }, {
    //         title: '密码管理',
    //         content: '内容2'
    //     }]
    // });
}