	var hotLinkTree;
	var root;
	var hotLinkTabs;
	var boNode;
	var workNode;
	var type;
	var text;
function showSetWin(cid,hotlinkName,rowIndex){
	hotLinkTabs = new Ext.TabPanel({
		region:'center',
		deferredRender:false,
		activeTab:0,
		border: true
	});
	var curHotLinkName=hotlinkName;
	root = new Ext.tree.AsyncTreeNode({
		id : 'hootLinkRoot',
		type:'hootLinkRoot',
		text : '热链', 
		expanded : true	,
		loader:new Ext.tree.TreeLoader({
			dataUrl:encodeURI(basePath+"repository/worklist/columns/hotlink/getTreeJson"),
			baseParams:{requestType:'hootLinkRoot'}
		})
	});
	hotLinkTree=new Ext.tree.TreePanel({
		title : "热链",
		id:"formHotLinkId",
		region: 'west',
		loader: new Ext.tree.TreeLoader(),
		width : 230,
		border: true, //面板边框
		useArrows : false, // 箭头节点图标
		root : root, // 根节点
		autoScroll : true, // 内容溢出时产生滚动条
		animate : false,
		rootVisible:false
	});
	hotLinkTree.on("beforeload", function(node) {
		var nodeLoader = node.attributes.loader;
			nodeLoader.baseParams.requestType=node.attributes.type;
	});
	var hotlinkRecord = grid.getStore().getAt(rowIndex);
	var hotlinkParam=hotlinkRecord.data.hotlinkParam;
	hotlinkParam = typeof (hotlinkParam) == "undefined" || hotlinkParam == '' ? "" : hotlinkParam;
	var iframeUrl=basePath+"repository/worklist/columns/hotlink/getSettingPage?id="+id +"&columId="+cid+"&name="+hotlinkName+"&params="+hotlinkParam;
	createViewTab("hotLink",hotlinkName,"",iframeUrl,"hotLinkIfram",true);
	hotLinkTree.on('click', function(node){
		type = node.attributes.type;
		text = node.attributes.text;
		var nodeId=node.attributes.id;
		 if(nodeId.indexOf('GROUP_')==-1){
			var newiframeUrl=basePath+"repository/worklist/columns/hotlink/getSettingPage?id="+id +"&columId="+cid+"&name="+text+'&params=';
			curHotLinkName=text;	
			createViewTab("hotLink",text,"",newiframeUrl,"hotLinkIfram",true);
		 }});
	hotLinkWindow=new Ext.Window({
         id:"hotLinkWindowId",
         width:920,
         height:350,
         constrainHeader:true,
         modal:true,
         plain:true,
         layout: 'border',
         items: [hotLinkTree,hotLinkTabs]
     });
	hotLinkTree.expandAll();
	hotLinkWindow.show();
	 function createViewTab(tabId,tabTitle,iconClassName,htmlSrc,frameName,isRefresh){
			tabId = 'tabs_'+tabId;
			var html = "<iframe id='"+frameName+"' name='"+frameName+"' src='"+htmlSrc+"' frameborder='0' scrolling=\"false\" width='100%' height='100%'  ></iframe>";
			var tab = hotLinkTabs.getItem(tabId);
			if(tab==null){
				hotLinkTabs.add({
					id: tabId,
					title: tabTitle,
					tabTip: tabTitle,
					autoDestroy:true,
					closable: false,
					autoScroll: true,
					iconCls: iconClassName,
					tbar :[ {
						text : '确定',
						iconCls : 'iconSave',
						handler : function() {
							setColumnHotLinkParams(curHotLinkName,cid, rowIndex);
						}
					}, '-', {
						text : '删除',
						iconCls : 'iconDelete',
						id : "deleteC",
						handler : function() {
							delColumnHotLinkParams(curHotLinkName, cid, rowIndex);
						}
					}],
					html: html
				}).show();
			}else{
				if(isRefresh){
					tab.setTitle(tabTitle);
					tab.addListener('show', function(){
						document.getElementById(frameName).src=htmlSrc;
					});
				}
				tab.show();
			}
	 }
}
function setColumnHotLinkParams(curHotLinkName,cid,rowIndex){
	curHotLinkName=curHotLinkName=='设置'?'表单模型':curHotLinkName;
	var params=hotLinkIfram.getSettingParams();
	 var record = grid.getStore().getAt(rowIndex);
	record.set('hotlinkParam',params);
	record.set('hotlinkName',curHotLinkName);
	hotLinkWindow.close();
}
function delColumnHotLinkParams(curHotLinkName,cid,rowIndex){
	var params=hotLinkIfram.getSettingParams();
	 var record = grid.getStore().getAt(rowIndex);
	record.set('hotlinkParam','');
	record.set('hotlinkName','无');
	hotLinkWindow.close();
}


