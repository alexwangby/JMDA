﻿var viewport;
Ext.onReady(function() {
	Ext.BLANK_IMAGE_URL = application_STATIC_SERVER + 'jmda-static/js/extjs3/images/default/s.gif';
	Ext.QuickTips.init();
	var tabs = new Ext.TabPanel({
		activeTab : 0,
		plain : true,
		tabPosition : "bottom",
		defaults : {
			autoScroll : true
		}
	});

	for (var i = 0; i < tabArray.length; i++) {
		var tabNameAndType = tabArray[i].split("|");
		var tabName = tabNameAndType[0];
		var pageType = tabNameAndType[1];
		var cmd = tabNameAndType[2];
		tabs.add({
			title : tabName,
			listeners : {
				activate : handleActivate
			},
			formlb : cmd,
			html : '<iframe id="FormData_Page_' + pageType + '" name="FormData_Page_' + pageType + '" frameborder=0 width=100% height=100% src="' + basePath + 'wait.jsp"></iframe>',
			pageType : pageType
		});
	}

	function handleActivate(tab) {
		if (tab.formlb == 'JBXX') {
			eval("FormData_Page_" + tab.pageType).location = encodeURI(basePath + "repository/worklist/getWorkListBasePage?worklistid=" + id);
		} else if (tab.formlb == 'XSL') {
			eval("FormData_Page_" + tab.pageType).location = encodeURI(basePath + "repository/worklist/columns/getWorkListColumnsPage?id=" + id);
		} else if (tab.formlb == 'GJT') {
			eval("FormData_Page_" + tab.pageType).location = encodeURI(basePath + "repository/worklist/buttons/getWorkListButtonsPage?id=" + id);
		} else if (tab.formlb == 'CXTJ') {
			eval("FormData_Page_" + tab.pageType).location = encodeURI(basePath + "repository/worklist/searchs/getWorkListSearchsPage?id=" + id);
		} else if (tab.formlb == 'LBDC') {
			eval("FormData_Page_" + tab.pageType).location = encodeURI(basePath + "repository/worklist/execel/getWorkListExecelPage?id=" + id);
		} else {
		}
	}
	var monibutton = '<a style="float:right;margin-right:10px;" href="#" onclick="testList()" >模拟运行</a>';
	viewport = new Ext.Viewport({
		layout : 'border',
		items : [ {
			region : 'north',
			html : '<h1 class="x-panel-header">当前路径：' + '' + applicationName + '》'+name + monibutton + '</h1>',
			autoHeight : false,
			border : false,
			margins : '0 0 2 0'
		}, {
			region : 'center',
			layout : 'fit',
			border : false,
			items : tabs
		} ]
	});
});
function testList() {
	var url = encodeURI(basePath + 'execute/worklist/ext/getPage?worklistId=' + id + '');
	parent.createViewTab('testBoList', '[测试数据列表]', '', url, true);
}
function getviewportHeight() {
	return viewport.el.dom.clientHeight;
}
function refreshParent(nodeId) {
	try {
		if (parent.FORM_Frame) {
			parent.FORM_Frame.store.load();
		}
		parent.regreshNodeById(nodeId);
	} catch (e) {
	}
}