﻿Ext.onReady(function() {
	Ext.QuickTips.init();
	var tb = new Ext.Toolbar();
	tb.setHeight(30);
	var btnAdd = new Ext.Action({
		text : '保存',
		tooltip : '保存修改的表单数据',
		handler : function() {
			if (templateName != null && templateName != '') {
				var formHtml = CKEDITOR.instances.editor_id.getData();
				Ext.Ajax.request({
					url : basePath + 'repository/form/templete/saveFormTemplete',
					method : 'POST',
					params : {
						formId : formId,
						templateName : templateName,
						formHtml : formHtml
					},
					failure : function(response, options) {
						Ext.MessageBox.alert('操作失败', '操作失败！可能是网络超时！');
						return false;
					},
					success : function(response, options) {
						var resText = response.responseText;
						if (resText == 'ok') {
							Ext.MessageBox.alert('操作提示', '保存表单模板成功！');
							return false;
						} else if (resText == 'noexist') {
							Ext.MessageBox.alert('操作提示', '当前表单模板不存在,请先套用模板！');
							return false;
						} else {
							Ext.MessageBox.alert('操作失败', '保存表单模板失败！');
							return false;
						}
					}
				});
			} else {
				Ext.MessageBox.alert('操作提示', '当前表单模板不存在,请先套用模板！');
				return false;
			}

		},
		iconCls : 'iconSave1'
	});
	var btnTemplate = new Ext.Action({
		text : '套用模板',
		tooltip : '采用默认格式模板样式',
		handler : function() {
			var tdStyleValue = document.getElementById('tdStyleSelect').value; 
			Ext.Ajax.request({
				url : basePath + 'repository/form/templete/createDefaultTemplete',
				method : 'POST',
				params : {
					formId : formId,
					templateName : templateName,
					tdStyleValue:tdStyleValue
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('操作失败', '操作失败！可能是网络超时！');
					return false;
				},
				success : function(response, options) {
					if (response.responseText == 'ok') {
						window.location.replace(window.location);
						return false;
					} else if (response.responseText == 'noexist') {
						Ext.MessageBox.alert('操作失败', '读取系统模板失败,请确定系统模板是否已删除！');
						return false;
					} else {
						Ext.MessageBox.alert('操作失败', '创建模板失败！');
						return false;
					}
				}
			});
		},
		iconCls : 'iconFormTemplate'
	});
	tb.add(btnAdd);
	tb.add('-');
	tb.add(tdJson);
	tb.add('-');
	tb.add(btnTemplate);
	tb.add('->');
	tb.add(templatePage);
	var tbdiv = new Ext.Toolbar();
	tbdiv.setHeight(23);
	tbdiv.add('提示：创建完毕数据源后，首次进入[表单模板],请先点击套用模板创建表单文件.');
	tb.render('toolbar');
	tbdiv.render('bottomDiv');
});

function applyTemplate() {
	Ext.MessageBox.confirm('提示', '确定套用选中的模板吗?', function(btn) {
		if (btn == 'yes') {
			var newTemplatePage = document.getElementById('templatePage').value;
			Ext.Ajax.request({
				url : basePath + 'repository/form/templete/applyFormTemplate',
				method : 'POST',
				params : {
					formId : formId,
					newTemplatePage : newTemplatePage
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('操作失败', '操作失败！可能是网络超时！');
					return false;
				},
				success : function(response, options) {
					if (response.responseText == 'ok') {
						window.location.replace(window.location);
						return false;
					} else {
						Ext.MessageBox.alert('操作失败', '保存表单模板失败！');
						return false;
					}
				}
			});
		}
	});
}