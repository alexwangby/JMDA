function openForm(formId, id) {
    var formUrl = basePath + '/execute/form/ext/getPage?formId=' + formId + '&id=' + id;
    openFormUrl(formUrl, id);
}
/**
 *
 * @param formId 表单唯一标识
 * @param id 表唯一主键值
 * @returns {String}
 */
function getFormUrl(formId, id) {
    return '/execute/form/ext/getPage?formId=' + formId + '&id=' + id;
}
/**
 *
 * 获取表单URL方法
 * @param formId
 * @param params 参数拼接
 * @returns {String}
 */
function getFormCustomParamsUrl(formId, params) {
    return '/execute/form/ext/getPage?formId=' + formId + params;
}
function openFormUrl(url, winName) {
    if (typeof (winName) == "undefined") {
        winName = 'newwindow';
    }
    winName = winName.replace(/\-/g, "");
    var xpwidth = window.screen.width - 10;
    var xpheight = window.screen.height - 35;
    window.open(url, winName, 'height=' + xpheight + ', width=' + xpwidth + ', top=0, left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=yes,location=no, status=no');
    return false;
}
function resetForm(fm) {
    //document.getElementById(fm).reset();
    $('#' + fm).form('clear');	//使用easyui的form清空方法，以清空form中使用了easyui控件的表单项
    try {
        eval('oasisResetFormCallBack()');
    } catch (e) {
    }

}


/**
 * 新版查询方法，添加对form中combobox取值的支持
 * add by wangbo
 * @param table
 * @param sfm
 */
function workListSearch(table, sfm) {
	var show=eval(true);
    try {
        show=eval('beforeHbdiySearchFormCallBack()');
    } catch (e) {
    	show = true;
    }
    if(show){
    	if (arguments.length > 2) {
    		$("#" + table).datagrid('options').queryParams = eval('(' + arguments[2] + ')');
    	} else {
    		$("#" + table).datagrid('options').queryParams = paramSrarchForm(sfm);
    	}
    	
    	$('#' + table).datagrid('loadData', {
    		total: 0,
    		rows: []
    	});
    	$('#' + table).datagrid({
    		queryParams: $("#" + table).datagrid('options').queryParams
    	});
    	try {
    		eval('afterHbdiySearchFormCallBack()');
    	} catch (e) {
    		
    	}    	
    }
}
/**
 * 新版拼接参数列表，添加对combobox取值的支持
 * add by wangbo
 * @param sfm
 * @returns
 */
function paramSrarchForm(sfm) {
    var b = $("#" + sfm + " input");
    var param = '';
    for (var i = 0; i < b.length; i++) {
        var bname = b[i].id;
        if (bname == null || bname == '') {
            bname = b[i].name;
        }
        if (bname != null && bname != '') {
            var sb = bname.split('search_');
            if (sb.length <= 1) {
                continue;
            }
            if (b[i].style.display == 'none') {
                var classStr = $(b[i]).attr('class');
                var value = null;

                classStr = classStr ? classStr : "";

                if (classStr.indexOf('easyui-combobox') != -1) {
                    value = $(b[i]).combobox('getValue');
                    param += ',\"' + bname + '\":\"' + value + '\"';
                } else if (classStr.indexOf('easyui-datebox') != -1) {
                    value = $(b[i]).datebox('getValue');
                    if (value == null || value == '') {
                        continue;
                    } else {
                        value = $(b[i]).datebox('calendar').calendar('options').current.getTime();
                        param += ',\"' + bname + '\":\"' + value + '\"';
                    }
                } else {
                    try {
                        var arr = b[i].parentNode.childNodes[2].childNodes[2];
                        if (arr.type == 'hidden') {
                            param += ',\"' + bname + '\":\"' + arr.value + '\"';
                        }
                    } catch (e) {
                        conlose.log(e.message);
                    }
                }
            } else if (b[i].type == 'text' || b[i].type == 'hidden') {
                param += ',\"' + bname + '\":\"' + b[i].value + '\"';
            } else if (b[i].type == 'radio' || b[i].type == 'checkbox') {
                if (b[i].checked)
                    param += ',\"' + bname + '\":\"' + b[i].value + '\"';
            }
        }

    }
    var s = $("#" + sfm + " select");
    for (var i = 0; i < s.length; i++) {
        var sname = s[i].id;
        if (sname == null || sname == '') {
            sname = s[i].name;
        }
        if (sname != null && sname != '') {
            var ssb = sname.split('search_');
            if (ssb.length > 1) {
            	var val = $(s[i]).val();
            	if(val=="null"||val==null){
            		val='';
            	}
                param += ',\"' + sname + '\":\"' + val + '\"';
            }
        }
    }

    return eval('({' + param.substring(1, param.length) + '})');
}

/**
 * 双击方法
 * @param rowIndex点击的行的索引值，该索引值从0开始。
 * @param rowData 对应于点击行的记录。
 */
function onDblClickRow(rowIndex, rowData) {
    try {
        onDblClickGridRow(rowIndex, rowData);
    } catch (e) {
    }
}
/**
 * 单击方法
 * @param rowIndex点击的行的索引值，该索引值从0开始。
 * @param rowData 对应于点击行的记录。
 */
function onClickRow(rowIndex, rowData) {
    try {
        onClickGirdRow(rowIndex, rowData);
    } catch (e) {
    }
}

/**
 * 刷新grid
 */
function dataGridRefresh() {
    $('#data_table').datagrid('reload');
}
function dataGridParamsRefresh(paramsJson) {
    //模式
    /*{
     name: 'easyui',
     subject: 'datagrid'
     }*/
    $('#data_table').datagrid({
        queryParams: paramsJson
    });
}
/**
 * 点击左侧树事件
 * @param node
 */
function clickWestNode(node) {
    //	alert(node.text);  // 在用户点击的时候提示
    try {
        clickWestTreeNode(node);
    } catch (e) {
    }
}
/**
 * 点击右侧树事件
 * @param node
 */
function clickEastNode(node) {
    //	alert(node.text);  // 在用户点击的时候提示
    try {
        clickEastTreeNode(node);
    } catch (e) {
    }
}
/**
 * 右侧树事件
 * @param node
 */
function rightTreeRefresh(params) {
    if (params == undefined || params == "") {
        $("#worklist_east_tree").tree('reload');
    } else {
        $('#worklist_east_tree').tree({
            url: basePath + rightUrl + params
        });
    }

}


function openFormSelector(title, width, height, url) {
    var settings = {
        'title': title,
        'width': width,
        'height': height,
        'url': url
    };
    DialogHelper.openSelector(settings);
}
function openSearchSelector(selectorId, callBackFunction, title, width, height, urlParams) {
    try {
        openFormSelectorBefore(selectorId);
    } catch (e) {
    }
    var curUrlParams = urlParams;
    try {
        urlParams = eval('openFormSelectorParamsDiy("' + selectorId + '","' + urlParams + '")');
    } catch (e) {
        urlParams = curUrlParams;
    }
    console.log(urlParams);
    var settings = {
        'id': selectorId,
        'callBack': callBackFunction,
        'title': title,
        'width': width,
        'height': height,
        'urlparams': urlParams
    };
    SelectorHelper.openSelector(settings);
}
function formatDateTime(value, row, index) {
    if (value != null) {
        var newValue = getFormatDateTimeByLong(value);
        return newValue;
    } else {
        return value;
    }
}
function formatDate(value, row, index) {
    if (value != null) {
        var newValue = getFormatDateByLong(value);
        return newValue;
    } else {
        return value;
    }
}
function materialIdLinkToTree(value, row, index) {
    if (value != null) {
        return "<span style=\"color:#1331D7;cursor:pointer;\" onClick=\"rightTreeRefresh('?materialId="
            + value + "');return false;\">" + value + "</span>";
    } else {
        return value;
    }
}
/**
 *转换long值为日期字符串
 * @param l long值
 * @param pattern 格式字符串,例如：yyyy-MM-dd hh:mm:ss
 * @return 符合要求的日期字符串
 */
function getFormatDateTimeByLong(l) {
    return getFormatDate(new Date(l), "yyyy-MM-dd hh:mm:ss");
}
/**
 *转换long值为日期字符串
 * @param l long值
 * @param pattern 格式字符串,例如：yyyy-MM-dd
 * @return 符合要求的日期字符串
 */
function getFormatDateByLong(l) {
    return getFormatDate(new Date(l), "yyyy-MM-dd");
}
/**
 *转换日期对象为日期字符串
 * @param l long值
 * @param pattern 格式字符串,例如：yyyy-MM-dd hh:mm:ss
 * @return 符合要求的日期字符串
 */
function getFormatDate(date, pattern) {
    if (date == undefined) {
        date = new Date();
    }
    if (pattern == undefined) {
        pattern = "yyyy-MM-dd hh:mm:ss";
    }
    return date.format(pattern);
}
Date.prototype.format = function (f) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(),    //day
        "h+": this.getHours(),   //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
        "S": this.getMilliseconds() //millisecond
    }
    if (/(y+)/.test(f))f = f.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(f))f = f.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    return f
}


function resetForm(formId, tableId) {
    $('#' + formId).form('clear');
    var search_object = form2object($('#' + formId));
    $('#' + tableId).datagrid('reload', search_object);
}

function form2object($form) {
    var obj = {};

    var formItemStr = $form.serialize();
    formItemStr = decodeURIComponent(formItemStr, true);
    var arr = formItemStr.split('&');
    for (var i = 0; i < arr.length; i++) {
        obj[arr[i].split('=')[0]] = arr[i].split('=')[1];
    }

    return obj;
}
/**
 * 过滤掉查询条件执行回车事件
 */
$(document).ready(function () {
    $('#search_form').find(':text').each(function () {
        $(this).bind('keyup', function (e) {
            var key = e.which;
            if (key == 13) {
                workListSearch('data_table', 'search_form');
            }
        });
    });
    
    try{
      $(".chzn-select").chosen();
    }catch(e){}
});

/**
 * 渲染成图片
 * @param value
 * @param row
 * @param index
 * @returns
 */
function imageUrlformatte(value, row, index) {
    if (value != null) {
        var url = HbdiyHelper.getImageUrlById(value);
        return '<div class="datagrid-cell datagrid-cell-c5-productItemImgUrl" style="height:auto;"><img src="' + url + '"></div>';
    } else {
        return value;
    }
}
function exportExecel(){
	var show=eval(true);
    if(show){
    	var params_;
    	if (arguments.length > 2) {
    		params_ = eval('(' + arguments[2] + ')');
    	} else {
    		params_ = paramSrarchForm('search_form');
    	}
		/*  $("#data_form").form('submit', {
			  	url : basePath + "/repository/worklist/execel/getAllDatas?worklistId="+worklistId+dataparams,
				success : function(data) {
					alert(data);
				}
		  });*/
    	var ex_params = $.param(params_);
    	if(ex_params!=''&&ex_params.length>1){
    		ex_params = "&"+ex_params;
    	}
    	data_form_excel.action = basePath + "/repository/worklist/execel/getAllDatas?worklistId="+worklistId+dataparams+ex_params;
    	data_form_excel.submit();
    }
}

function CurentTime()
{ 
    var now = new Date();
    var year = now.getFullYear();       //年
    var month = now.getMonth() + 1;     //月
    var day = now.getDate();            //日
   
    var hh = now.getHours();            //时
    var mm = now.getMinutes();          //分
   
    var clock = year + "年";
   
    if(month < 10)
        clock += "0";
   
    clock += month + "月";
   
    if(day < 10)
        clock += "0";
       
    clock += day + "日";
   
    if(hh < 10)
        clock += "0";
       
    clock += hh + "时";
    if (mm < 10) clock += '0'; 
    clock += mm+"分"; 
    return(clock); 
} 
