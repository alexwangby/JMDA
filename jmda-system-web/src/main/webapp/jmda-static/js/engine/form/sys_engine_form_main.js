function openFormSelector(selectorId, callBackFunction, title, width, height, urlParams) {
    try {
        openFormSelectorBefore(selectorId);
    } catch (e) {
    }
    var curUrlParams = urlParams;
    try {
        urlParams = eval('openFormSelectorParamsDiy("' + selectorId + '","' + urlParams + '")');
    } catch (e) {
        urlParams = curUrlParams;
    }
    console.log(urlParams);
    var settings = {
        'id': selectorId,
        'callBack': callBackFunction,
        'title': title,
        'width': width,
        'height': height,
        'urlparams': urlParams
    };
    SelectorHelper.openSelector(settings);
}
function openFormDialogHelper(title, width, height, url) {
    var settings = {
        'title': title,
        'width': width,
        'height': height,
        'url': url
    };
    DialogHelper.openSelector(settings);
}
function coverGridJsonDatas(gridCode) {
    var gridId = gridCode + '_data_table';
    var rows = $('#' + gridId).datagrid('getRows');
    var json = [];
    for (var r = 0; r < rows.length; r++) {
        var row = rows[r];
        var rowIndex = $('#' + gridId).datagrid('getRowIndex', rows[r]);
        var param = '';
        var fields = $('#' + gridId).datagrid('getColumnFields');
        for (var i = 0; i < fields.length; i++) {
            var filed_name = fields[i];
            if (filed_name != 'check') {
                //判断是否存在 自定义输入事件
                if ($('#jmda_grid_text_' + rowIndex + '_' + filed_name).length > 0) {
                    if ($('#jmda_grid_text_' + rowIndex + '_' + filed_name).val() == '') {
                        param += ',\"' + filed_name + '\":\"' + $('#jmda_grid_text_' + rowIndex + '_' + filed_name).html() + '\"';
                    } else {
                        param += ',\"' + filed_name + '\":\"' + $('#jmda_grid_text_' + rowIndex + '_' + filed_name).val() + '\"';
                    }
                } else {
                    var value = String(row[filed_name]).replace(new RegExp('"', 'g'), '”');
                    param += ',\"' + filed_name + '\":\"' + value + '\"';
                }
            }
        }
        json.push(eval('({' + param.substring(1, param.length) + '})'));
    }
    $('#' + gridCode).val(JSON.stringify(json));
}

function coverSectionGridJsonDatas(gridCode, sectionFields) {
    var gridId = gridCode + '_data_table';
    var rows = $('#' + gridId).datagrid('getRows');
    var json = [];
    for (var r = 0; r < rows.length; r++) {
        var row = rows[r];
        var rowIndex = $('#' + gridId).datagrid('getRowIndex', rows[r]);
        var param = '';
        var fields = $('#' + gridId).datagrid('getColumnFields');
        for (var i = 0; i < fields.length; i++) {
            var filed_name = fields[i];
            if (sectionFields[filed_name]) {
                if (filed_name != 'check') {
                    //判断是否存在 自定义输入事件
                    if ($('#jmda_grid_text_' + rowIndex + '_' + filed_name).length > 0) {
                        if ($('#jmda_grid_text_' + rowIndex + '_' + filed_name).val() == '') {
                            param += ',\"' + filed_name + '\":\"' + $('#jmda_grid_text_' + rowIndex + '_' + filed_name).html() + '\"';
                        } else {
                            param += ',\"' + filed_name + '\":\"' + $('#jmda_grid_text_' + rowIndex + '_' + filed_name).val() + '\"';
                        }
                    } else {
                        var value = String(row[filed_name]).replace(new RegExp('"', 'g'), '”');
                        param += ',\"' + filed_name + '\":\"' + value + '\"';
                    }
                }
            }
        }
        json.push(eval('({' + param.substring(1, param.length) + '})'));
    }
    $('#' + gridCode).val(JSON.stringify(json));
}
function getGridJsonObject(gridCode) {
    var gridId = gridCode + '_data_table';
    var rows = $('#' + gridId).datagrid('getRows');
    var json = [];
    for (var r = 0; r < rows.length; r++) {
        var row = rows[r];
        var rowIndex = $('#' + gridId).datagrid('getRowIndex', rows[r]);
        var param = '';
        var fields = $('#' + gridId).datagrid('getColumnFields');
        for (var i = 0; i < fields.length; i++) {
            var filed_name = fields[i];
            if (filed_name != 'check') {
                //判断是否存在 自定义输入事件
                if ($('#jmda_grid_text_' + rowIndex + '_' + filed_name).length > 0) {
                    param += ',\"' + filed_name + '\":\"' + $('#jmda_grid_text_' + rowIndex + '_' + filed_name).val() + '\"';
                } else {
                    var value = String(row[filed_name]).replace(new RegExp('"', 'g'), '”');
                    param += ',\"' + filed_name + '\":\"' + value + '\"';
                }
            }
        }
        json.push(eval('({' + param.substring(1, param.length) + '})'));
    }
    return json;
}

function jmdaInputFormat(coumnName, value, row, index) {
    return '<input id="jmda_grid_text_' + index + '_' + coumnName + '" type="text" name="jmda_grid_text_'+coumnName+'" class="easyui-validatebox easyui-numberbox" data-options="required:true" style="width: 93%;" value="' + value + '"  />';
}
function redirectGridUrl(gridCode, params) {
    var gridId = gridCode + '_data_table';
    $('#' + gridId).datagrid('options').url = $('#' + gridId).datagrid('options').url + "&" + params;
    $('#' + gridId).datagrid('reload');
}
/**
 *转换long值为日期字符串
 * @param l long值
 * @param pattern 格式字符串,例如：yyyy-MM-dd hh:mm:ss
 * @return 符合要求的日期字符串
 */
function getFormatDateTimeByLong(l) {
    return getFormatDate(new Date(l), "yyyy-MM-dd hh:mm:ss");
}
/**
 *转换long值为日期字符串
 * @param l long值
 * @param pattern 格式字符串,例如：yyyy-MM-dd
 * @return 符合要求的日期字符串
 */
function getFormatDateByLong(l) {
    return getFormatDate(new Date(l), "yyyy-MM-dd");
}
/**
 *转换日期对象为日期字符串
 * @param l long值
 * @param pattern 格式字符串,例如：yyyy-MM-dd hh:mm:ss
 * @return 符合要求的日期字符串
 */
function getFormatDate(date, pattern) {
    if (date == undefined) {
        date = new Date();
    }
    if (pattern == undefined) {
        pattern = "yyyy-MM-dd hh:mm:ss";
    }
    return date.format(pattern);
}
Date.prototype.format = function (f) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(),    //day
        "h+": this.getHours(),   //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
        "S": this.getMilliseconds() //millisecond
    }
    if (/(y+)/.test(f))f = f.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(f))f = f.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    return f
}


/**
 * 渲染成图片
 * @param value
 * @param row
 * @param index
 * @returns
 */
function imageUrlformatter(value, row, index) {
    if (value != null) {
        var url = HbdiyHelper.getImageUrlById(value);
        return '<div class="datagrid-cell datagrid-cell-c5-productItemImgUrl" style="height:auto;"><img src="' + url + '"></div>';
    } else {
        return value;
    }
}

function showHideFormPart(obj) {
    var $this = $(obj);
    var $i = $this.find("i");
    var $rows = $this.parent().next(".row");

    if ($i.hasClass("fa-minus")) {//进行隐藏
        $i.removeClass("fa-minus");
        $i.addClass("fa-plus");
        hideRows($rows);
    } else {
        $i.removeClass("fa-plus");
        $i.addClass("fa-minus");
        showRows($rows);
    }
}
function hideRows(obj) {
    obj.addClass("hidden");
    if (!obj.next().hasClass("form-title") && !obj.next().hasClass("buts-row-bottom")) {
        hideRows(obj.next());
    }
}
function showRows(obj) {
    obj.removeClass("hidden");
    if (!obj.next().hasClass("form-title") && !obj.next().hasClass("buts-row-bottom")) {
        showRows(obj.next());
    }
}

//通用选择器按键事件绑定
$(document).on('keyup', 'input[hbdiyType="selector"]', function (e) {
    var key = e.which;
    if (key == 13) {
        var curSelectorInput = $(this);
        var id = curSelectorInput.attr("selectorName");
        var urlParams = '';
        try {
            urlParams = openFormSelectorParamsDiy(id, '');
        } catch (e) {
            urlParams = '';
        }
        if (urlParams != '') {
            urlParams = '&' + urlParams;
        }
        var name = curSelectorInput.attr("name");
        var searchValue = $(this).val();
        var param_ = '&' + name + '=' + searchValue;
        $.ajax({
            type: "post",// 使用get方法访问后台
            dataType: "json",// 返回json格式的数据
            url: basePath + '/selectorController/queryOne?id=' + id + urlParams + param_,// 要访问的后台地址
            data: {},
            success: function (data) {// msg为返回的数据，在这里做数据绑定
                $(curSelectorInput).removeAttr('disabled');
                if (data.rs == 1) {
                    var backFunciton = curSelectorInput.attr("backFunction");
                   // callBackjs.call(backFunciton, data.datas, '');
                   // eval(backFunciton + "(" + data.datas + ")");
                   // backFunciton("");
                    window[backFunciton](data.datas); 
                } else if (data.rs == 0) {
                    jQuery.messager.alert('提示:', '未查询到数据，请重新录入查询条件！', 'info');
                } else if (data.rs == 2) {
                    curSelectorInput.next().click();
                } else {
                    alert('请通知管理员，查询错误！');
                }
            },
            error: function () {
                $(curSelectorInput).removeAttr('disabled');
                alert('请通知管理员，查询错误！');
            }
        });
    }
});
/**
 * 
 * @param gridCode 子表编码
 * @param quantity 子表统计数量字段
 * @param countVarietyCode 统计行数ID（即品类）
 * @param countSumCode 统计数量ID
 */
function sumCalculation(gridCode,quantity,countVarietyCode,countSumCode){
	var json = getGridJsonObject(gridCode);
	var countVariety=0;
	var countSum =0;
	if(json.length>0){
		for(var i =0 ;i<json.length;i++){
			countVariety= countVariety+1;
			countSum=Number(countSum)+Number(eval('json[i].'+quantity));
		}
	}
	$("#"+countVarietyCode).html(countVariety);
	$("#"+countSumCode).html(countSum);
}

function three_sumCalculation_sjp(gridCode,quantity,countSumCode,amount,sumAmountCode,countVarietyCode){
	var json = getGridJsonObject(gridCode);
	var countVariety=0;
	var countSum =0;
	var sumAmount = 0;
	if(json.length>0){
		for(var i =0 ;i<json.length;i++){
			countVariety= countVariety+1;
			//var je = $("#jmda_grid_text_"+i+"_"+amount).val();
			countSum=Number(countSum)+Number(eval('json[i].'+quantity));
			sumAmount=Number(sumAmount)+Number(eval('json[i].'+amount));
		}
	}
	$("#"+countVarietyCode).html(countVariety);
	$("#"+countSumCode).html(countSum);
	$("#"+sumAmountCode).html(sumAmount);
}