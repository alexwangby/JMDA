var SelectorHelper = {
    openSelectorStatus: false,
    openSelector: function (options) {
        if (SelectorHelper.openSelectorStatus == false) {
            SelectorHelper.openSelectorStatus = true;
        } else {
            return;
        }
        var defaults = {'width': 700, 'height': 370, 'callBack': $.noop(), 'urlparams': ''};
        var settings = $.extend({}, defaults, options);

        var id = settings['id'];
        var title = settings['title'];
        var dg_width = settings['width'];
        var dg_height = settings['height'];
        var callBackjs = settings['callBack'];
        var urlparams = settings['urlparams'];
        var callBackParams = settings['callBackParams'];
        if (urlparams != '') {
            urlparams = '&' + urlparams;
        }

        $('#openDictionaryId').parent().remove();
        var dictionaryDiv = '<div id="openDictionaryId" class="easyui-dialog" closed="true" modal="true" ><iframe scrolling="no" id="openDictionaryFrameId" name="openDictionaryFrameId" frameborder="0"  src="' + BASE_PATH + '/selectorController/getPage?id=' + id + urlparams + '" style="width:100%;height:98%;"></iframe></div>';
        $(document.body).append(dictionaryDiv);
        $('#openDictionaryId').dialog({
            collapsible: false,
            minimizable: false,
            maximizable: false,
            resizable: false,
            draggable: true,
            inline: true,
            shadow: false,
            title: title,
            width: dg_width,
            height: dg_height,
            top: 10,
            modal: true,
            onClose: function () {
                SelectorHelper.openSelectorStatus = false;
                try {
                    eval('cancelCallBack()');
                } catch (e) {

                }
            },
            buttons: [{
                id: 'confirm',
                text: '确定',
                handler: function () {
                    var jsonData = openDictionaryFrameId.getDictionarySelections();
                    openDictionaryFrameId.clearDataGridSelected();
                    if (jsonData != '') {
                        callBackjs.call(callBackjs, jsonData, callBackParams);
                    }
                }
            }, {
                text: '关闭',
                handler: function () {
                    $('#openDictionaryId').dialog('close');
                    SelectorHelper.openSelectorStatus = false;
                    try {
                        eval('cancelCallBack()');
                    } catch (e) {

                    }
                }
            }]
        });
        $('#openDictionaryId').dialog('open');
    },

    closeSelector: function () {
        SelectorHelper.openSelectorStatus = false;
        $('#openDictionaryId').dialog('close');
    }
};
var DialogHelper = {
    openSelectorStatus: false,
    openSelector: function (options) {
        if (DialogHelper.openSelectorStatus == false) {
            DialogHelper.openSelectorStatus = true;
        } else {
            return;
        }
        var defaults = {'title': '', 'width': 700, 'height': 370, 'url': '',top:10};
        var settings = $.extend({}, defaults, options);
        var title = settings['title'];
        var dg_width = settings['width'];
        var dg_height = settings['height'];
        var url = settings['url'];
        var top = settings['top'];
        $('#openDialogId').parent().remove();
        var dialogDiv = '<div id="openDialogId" class="easyui-dialog" closed="true" modal="true" ><iframe scrolling="yes" id="openDialogFrameId" name="openDialogFrameId" frameborder="0"  src="' + BASE_PATH + url + '" style="width:100%;height:98%;"></iframe></div>';
        $(document.body).append(dialogDiv);
        $('#openDialogId').dialog({
            collapsible: false,
            minimizable: false,
            maximizable: false,
            resizable: false,
            draggable: true,
            inline: true,
            shadow: false,
            title: title,
            width: dg_width,
            height: dg_height,
            top: top,
            modal: true,
            onClose: function () {
                DialogHelper.openSelectorStatus = false;
                try {
                    eval('cancelCallBack()');
                } catch (e) {

                }
            }
        });
        $('#openDialogId').dialog('open');
    },
    closeDialog: function () {
        DialogHelper.openSelectorStatus = false;
        $('#openDialogId').dialog('close');
    }
};
function gridDataQuery(tableId, e) {
    var ev = window.event || e;
    if (ev.keyCode == 13) {
        $("#" + tableId).datagrid('options').queryParams = eval('({' + $("#gridDataQueryInput").attr("name") + ':\'' + $("#gridDataQueryInput").val() + '\'})');
        $('#' + tableId).datagrid({
            queryParams: $("#" + tableId).datagrid('options').queryParams
        });
    }
}
function getThumbnailUrl(materialId) {
    $.ajax({
        type: "post",
        dataType: "text",
        url: BASE_PATH + "/../hbdiy-erp-dubbo/erpImageController/getThumbnailUrl",
        data: "materialId=" + materialId,

        success: function (msg) {
            return msg;
        }
    })
}