/*弹出窗口*/
var OperateWinObj;
var operateWinObjIframe='Operate_DlgPage';
function openExtWindow(winTitle,winWidth,winHeight,isShow,isShowMaxBtn,frameUrl,isClose){
	if(typeof(isClose)=="undefined"){
		isClose = true;
	}
	if(typeof(isShow)=="undefined"){
		isShow = true;
	}
	if(typeof(frameUrl)=="undefined"){
		frameUrl = '/wait.jsp';
	}
	if(typeof(isShowMaxBtn)=="undefined"){
		isShowMaxBtn = true;
	}
	Ext.QuickTips.init();
	var id = "TMP_Operate_Div";
	if (Ext.getCmp(id)) {
		Ext.getCmp(id).destroy();
	}
	var winContainerElId = Ext.id(); 
	Ext.getBody().createChild({
		tag :'div',
		id : winContainerElId
	});
	if(!OperateWinObj){
		OperateWinObj = new Ext.Window({
			contentEl : winContainerElId,
			layout : 'fit',
			constrain : true, // 强制将window控制在viewport中
			constrainHeader:true,
			title : winTitle,
			width : winWidth,
			height : winHeight,
			shadow : false,
			maximizable : isShowMaxBtn,
			plain : true,
			closeAction : 'hide',
			closable:isClose,
			modal: true,
			keys: [{
				key: 27,
				scope: this,
				fn: function(){OperateWinObj.hide();}
			}],
			items:[{
				html:'<iframe name="Operate_DlgPage" id="Operate_DlgPage" frameborder=0 width=100% height=100% src="'+frameUrl+'"></iframe>'
			}]
		});
	}
	OperateWinObj.addListener('hide', function(){
		try{Operate_DlgPage.clearInterval(Operate_DlgPage.timer);}catch(e){}
		OperateWinObj.destroy();
		return true;
	});
	OperateWinObj.addListener('maximize', function(){//设置最大化事件防止
		//OperateWinObj.setPosition(document.body.scrollLeft,document.body.scrollTop);
		return true;
	});

	OperateWinObj.addListener('destroy', function(){
		OperateWinObj = null;
		return true;
	});

	if(isShow) {
		OperateWinObj.show();
		//openExtWindowShow(winTitle,frameUrl);
	}
}

function openExtWindowShow(winTitle,frameUrl){
	OperateWinObj.show();
	OperateWinObj.center();
	OperateWinObj.setTitle(winTitle);
	if(frameUrl!=undefined&&frameUrl!=null){
		Operate_DlgPage.location=encodeURI(frameUrl);
	}
}