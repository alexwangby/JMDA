<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="favicon.ico"  rel="bookmark" type="image/x-icon" />
    <link rel="shortcut icon"  href="${staticPath}/static/img/favicon.ico">
    <title>鸦雀漾水上防污染系统[登陆]</title>
	<script src="${staticPath}/jmda-static/assets/js/jquery.min.js"></script>
    <script src="${staticPath}/jmda-static/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${staticPath}/jmda-static/assets/js/jquery-easyui-1.4.3/jquery.easyui.min.js"></script>
    <script src="${staticPath}/jmda-static/assets/js/md5.js"></script>
    <link rel="stylesheet" href="${staticPath}/static/css/login/login.css" >
    
    <style>
   .logintitle{
		
		font-family : 微软雅黑,宋体;
		
		font-size : 2.5em;
		
		color : #FFFFFF;
		
		}
    
    </style>
</head>

<body>
<section class="hb_main">
		<div class="logindiv" >
			<div><span class="logintitle">鸦雀漾水上防污染系统</span></div>
			<div id="shurubox" style="margin: 3% 0 0 0;">
			    <div class="error-msg error" style="color: red;">
                  	<#if error!="">
                  		登录超时，请重新登录！
                  	</#if>
                  </div>
				<div id="usernames">
					<form id="login_form" class="" role="form"   method="post" >
						<div class="inpdivs" style="">
							<input value="" maxLength="128" name="loginName" id="loginName" autocomplete="off" type="text" class="youhuming">
						</div>

						<div class="ipbgss" >
                            <input type="password" style="display:none;width:0;height:0;">
							<input  value="" maxLength="24" type="password"  name="loginPassword" id="loginPassword" autocomplete="new-password"  class="mami">
							<input type="hidden" id="md5Password" name="loginPassword"  />
						</div>
						<input href="javascript:void(0)" onclick="login();" name="Submit1" type="button" value="登  录" class="denglu"  />
					</form>
					
				</div>
			</div>
			<div style="position:relative;bottom:-40px;color:#f2f2f2">Copyright © 2012-2017 清珺科技（北京）有限公司      </div>

		</div>
		<div class="logobbg"><img src="${staticPath}/static/img/login/1login.jpg" height="100%" width="100%"> 

		</div>
	
		</section>
</body>
</html>

<script>

$("#loginName").focus();

$('body').keydown(function(e){
	if(e.keyCode==13){
	   login();
	}
});



function login(){
	var loginName=$("#loginName").val();
	var password=$("#loginPassword").val();
	var reg = /\S/;
	if(reg.test(loginName)&&reg.test(password)){
	
		var md5Password=hex_md5(password);
		$("#md5Password").attr("value",md5Password);
		
		$("#login_form").form('submit', {
			url : '${staticPath}/loginin',
			dataType : 'text',
			success : function(data) {
				if(data=='success'){
	    			window.location.href="${staticPath}/skins";
	    		}else{
	    			$(".error-msg").html("账户名与密码不匹配，请重新输入!");
	    			//$(".input-group").addClass("has-error");
	    			$("#loginName").focus();
	    		}
			}
		});
	}else{
		$(".error-msg").html("账户名与密码不能为空，请重新输入!");
		//$(".input-group").addClass("has-error");
		$("#loginName").focus();
		return false;
	}
}

</script>


