<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<link rel="stylesheet" type="text/css" href="${staticPath}/static/js/portal/portal_index.css" />
<link rel="stylesheet" type="text/css" href="${staticPath}/static/js/portal/style.css" />
<script type="text/javascript" src="${staticPath}/static/js/portal/js_lang.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery.plugins.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery.ux.borderlayout.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery.ux.slidebox.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/portal_index.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery.ux.simulatemouse.js"></script>
<script type="text/javascript">
var pbsBasePath = "${staticPath}";
var pbsBasePath2 = "${staticPath}";


var modules=${modules};//[{"id":"bc2f130775a94f9a9122db166ce83689","title":"业务功能","items":[{"id":"icon6","count":0,"name":"导入计划","func_id":"ce45c2a5833545f6a2b755c1555a30e6","url":"/business/plan/getPage.wf"},{"id":"icon12","count":0,"name":"导入合同","func_id":"9bfa22ca716342bb82c33ae8312c3d7d","url":"/business/contract/getPage.wf"},{"id":"icon8","count":0,"name":"合同查询","func_id":"177d40ed828244ba938e42ff22efcaf7","url":"/business/mainframe/getPage.wf"},{"id":"icon17","count":0,"name":"计划合同对应","func_id":"00ac469f336f40759907ccbe0085c95e","url":"/business/plancontract/getPage.wf"},{"id":"icon16","count":0,"name":"用款计划","func_id":"4ad43d4ef6ba4ef8a3b99b2bfab3e1bd","url":"/business/expensesplan/getPage.wf"},{"id":"icon7","count":0,"name":"调整用款计划","func_id":"dfedb8c37fe74c159359454aab3f4bcd","url":"/business/tzexpensesplan/getTZPage.wf"},{"id":"icon16","count":0,"name":"付款登记","func_id":"33f7a1bf0b8c4cb09aef8e4d295cc8e1","url":"/business/settlement/getPage.wf"}]},{"id":"6262234e6f1d4046adcc5c56f43f677f","title":"基础数据","items":[{"id":"icon3","count":0,"name":"订货单位","func_id":"1d0328345c094178a80afbd323e69f0a","url":"/basicdata/orderunit/getPage.wf"},{"id":"icon5","count":0,"name":"承建单位","func_id":"bf3292b01a744b10ac870f25bc306cfc","url":"/basicdata/constructionunit/getPage.wf"},{"id":"icon16","count":0,"name":"主管业务部门","func_id":"1c2c672fafbb49e6b54c268d6f245117","url":"/basicdata/management/getPage.wf"}]},{"id":"f641c730267a463b9c54411b46749d62","title":"接收上报","items":[{"id":"icon16","count":0,"name":"预算接收","func_id":"da033ee210454f3aa85a3b61ed209e41","url":"/receivereport/receive/getBudgetReceivePage.wf"},{"id":"icon16","count":0,"name":"上报用款计划","func_id":"f71693bb16e94b7683d23d9a6ba0b064","url":"/receivereport/report/getEPReportPage.wf"},{"id":"icon16","count":0,"name":"接收用款计划","func_id":"3ab3d02f23b64fb9ba811add4f5f25c7","url":"/receivereport/receive/getEPReceivePage.wf"},{"id":"icon16","count":0,"name":"接收免税单位","func_id":"2bccfeec55f94bc2a722d3d5c3ca0cb7","url":"/receivereport/receive/getMSDWReceivePage.wf"},{"id":"icon16","count":0,"name":"接收订货项目","func_id":"7972e91cd7c74c488b5ba850b71b6e4a","url":"/receivereport/receive/getDHXMReceivePage.wf"},{"id":"icon16","count":0,"name":"上报免税清单","func_id":"5c09eabb678e4a5abf2c3efa0e003935","url":"/receivereport/report/getMSQDReportPage.wf"}]},{"id":"c08dd968c39b4837bd18d58a777f6505","title":"统计报表","items":[{"id":"/","count":0,"name":"计划执行情况明细表","func_id":"40754fe658c14679ac99c3fd359f334d","url":"/equipment/report/getPage.wf?resID=32237"},{"id":"/","count":0,"name":"用款计划调整上报表","func_id":"877cd447673f404ca9bc023c8942476d","url":"/equipment/report/getPage.wf?resID=32239"}]},{"id":"a36eec46e7544f7aba4888e7dd64e33d","title":"系统管理","items":[{"id":"icon14","count":0,"name":"菜单权限","func_id":"e8464832e21141ba9b15be4122443dd0","url":"/security.wf"},{"id":"icon7","count":0,"name":"菜单设置","func_id":"a95300c13c794cdc832a4f379500351d","url":"/nav/main.wf"},{"id":"icon2","count":0,"name":"部门人员","func_id":"ec7780a27b1f45129bf6c12dee453d0c","url":"/org/portal.wf"},{"id":"icon3","count":0,"name":"数据备份","func_id":"0df1c20ac24644598211357836832486","url":"/system/databackup/getPage.wf"},{"id":"icon4","count":0,"name":"数据还原","func_id":"00fdc5033dec49f0ac1b518d3442a89c","url":"/system/datarestore/getPage.wf"},{"id":"icon15","count":0,"name":"系统日志","func_id":"bff01c372f3c4d02a9267f9a62713659","url":"/system/systemlog/getPage.wf"},{"id":"icon6","count":0,"name":"数据下发","func_id":"4891470134c14cc0b7993c3dd33c9ac8","url":"/system/datasent/getPage.wf"}]}];
</script>
<style>
A.applink:hover {border: 2px dotted #DCE6F4;padding:2px;background-color:#ffff00;color:green;text-decoration:none}
A.applink       {border: 2px dotted #DCE6F4;padding:2px;color:#2F5BFF;background:transparent;text-decoration:none}
A.info          {color:#2F5BFF;background:transparent;text-decoration:none}
A.info:hover    {color:green;background:transparent;text-decoration:underline}
</style>
<title>导航菜单</title>
</head>
<body>
   <div id="control"> 
      <table align="center">
         <tr>
            <td class="control-l"></td>
            <td class="control-c"></td>
           <td class="control-r"> </td>
         </tr>
      </table>
   </div>
	<div class="slidebox" style="margin-top:-50px;">
	 	<div id="trash"></div>
	  	<div id="container"></div>
	</div>
	<div id="overlay"></div>
	<div class="background" style="width: 100%;height: 100%"></div>
</body>
</html>