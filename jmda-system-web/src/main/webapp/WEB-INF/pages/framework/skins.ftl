<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${staticPath}/static/js/portal/index.css" />
    <link rel="stylesheet" href="${staticPath}/plugs/layui/css/layui.css">
<script type="text/javascript" src="${staticPath}/static/js/portal/js_lang.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery.ui.autocomplete.min.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery.effects.bounce.min.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/jquery/jquery.plugins.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/utility.js"></script>
<script type="text/javascript" src="${staticPath}/plugs/layui/lay/dest/layui.all.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/index.js"></script>
<script type="text/javascript" src="${staticPath}/static/js/portal/logout.js" ></script>
<script type="text/javascript">
var pbsBasePath = "${staticPath}";
var pbsBasePath2 = "${staticPath}";
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight);
self.focus();
var portalArray = [];

portalArray["0"] = {src:"",url:pbsBasePath+"/index", title:"", closable:"true"};
var portalLoadArray = ["0"];
//var monInterval = 1;
//var moduleIdStr = 'work_plan,vehicle,info,meeting,workflow,zhidao,office_Product,picture,address,office_Product,workflow,workflow,news,info,info,roll_manage,bbs,bbs2,file_folder,workflow,vehicle,person_info,ewp_qiyi,ewp_sinanews,ewp_xiami,book,meeting,email,calendar,workflow,notify,vote,attendance,sms,project,diary,address,mobile_sms,file_folder,ewp_baidu,ewp_sina,';
//var modules = [{title: "",id: "",items:[{id: "work_plan",name: "工作计划查询",func_id: "97",count: 0,url:"/nav/main.wf"},{id: "vehicle",name: "车辆使用申请",func_id: "91",count: 0,url:"http://www.baidu.com"},{id: "info",name: "部门信息查询",func_id: "18",count: 0,url:"http://www.baidu.com"}]}];

</script>
<style>
A.applink:hover {border: 2px dotted #DCE6F4;padding:2px;background-color:#ffff00;color:green;text-decoration:none}
A.applink       {border: 2px dotted #DCE6F4;padding:2px;color:#2F5BFF;background:transparent;text-decoration:none}
A.info          {color:#2F5BFF;background:transparent;text-decoration:none}
A.info:hover    {color:green;background:transparent;text-decoration:underline}
.personConfig{
	color:#fff;
	margin-left:10px;
	text-decoration:none;
	}
.personConfig:hover{
	text-decoration:underline;
	}	
	.logintitle{
		
		font-family : 微软雅黑,宋体;
		
		font-size : 2.5em;
		
		color : #FFFFFF;
		
		}
</style>
<title>鸦雀漾水上防污染系统</title>
</head>
<body>
<form action="" name="frmMain" id="frmMain" method="post" >
   <div id="loading" class="loading" style="width:100%;height:100%;margin-top:100px;padding-top:100px;text-align:center;color:#1547b8;font-size:32px;font-family:微软雅黑,宋体;"></div>

   <div id="north" >
      <div id="north_left">
         <table><tr><td class="logintitle">鸦雀漾水上防污染系统</td></tr></table>
      </div>
      <div id="north_right">
          <b style="margin-right:10px;">${name}</b>|<a class="personConfig" href="javascript:void(0);" onClick="personConfig();">个人设置</a>
      </div>
   </div>
   <div id="taskbar">
      <div id="taskbar_left">
         <a href="javascript:;" id="start_menu" hidefocus="hidefocus"></a>
      </div>
      <div id="taskbar_center">
         <div id="tabs_left_scroll"></div>
         <div id="tabs_container"></div>
         <div id="tabs_right_scroll"></div>
      </div>
      <div id="taskbar_right">
         <a id="logout" href="javascript:void(0);" onClick="logoutBPM(frmMain);"  title="注销登录"></a>
         <a id="hide_topbar" href="javascript:;" hidefocus="hidefocus"></a>
      </div>
   </div>
   <div id="center" >
      <div id="overlay_panel"></div>
   </div>

   <div id="overlay_startmenu"></div>   
   <div id="overlay"></div>
   <div style="display:none;">
   </div>
   	<input type="hidden" name="user_id" id="user_id" value=""/>
   	<input type="hidden" name="ID" id="ID" value="" />
   </form>
   </body></html>