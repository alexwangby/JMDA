<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="${basePath}/plugs/layui/css/layui.css">
<script type="text/javascript" src="${basePath}/plugs/layui/lay/dest/layui.all.js"></script>
<script src="${basePath}/jmda-static/assets/js/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="/jmda-static/assets/js/jquery-easyui-1.4.3/themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="/jmda-static/assets/js/jquery-easyui-1.4.3/themes/icon.css">
<script type="text/javascript" src="/jmda-static/assets/js/jquery-easyui-1.4.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/jmda-static/assets/js/jquery-easyui-1.4.3/locale/easyui-lang-zh_CN.js"></script>
<head>
    <meta charset="utf-8" />
    <title</title>
</head>
<body>
<div class="layui-tab" style="margin-left: 2px">
    <ul class="layui-tab-title">
        <li class="layui-this">基本信息</li>
        <li>密码管理</li>
    </ul>
    <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">
            <form class="layui-form" id="base_form" action="">
                <div class="layui-form-item">
                    <label class="layui-form-label">登录名</label>
                    <div class="layui-input-inline">
                        <input name="loginName"  readonly="readonly" id="loginName" value="${loginName}" required="" lay-verify="required" autocomplete="off" class="layui-input" type="text">
                    </div>
                    <#--<div class="layui-form-mid layui-word-aux">只读</div>-->
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">名称</label>
                    <div class="layui-input-inline">
                        <input name="name" id="name" value="${name}" lay-verify="required"  autocomplete="off" class="layui-input" type="text">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">手机</label>
                    <div class="layui-input-inline">
                        <input name="mobile" id="mobile" value="${mobile}" placeholder="请输入手机" value="" lay-verify="required"  autocomplete="off" class="layui-input" type="text">
                    </div>

                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" type="button" onclick="updateData()" lay-filter="formDemo">保 存</button>
                        <#--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
                    </div>
                </div>
            </form>
        </div>
        <div class="layui-tab-item">            <form class="layui-form" action="" id="password_form" name="password_form">
            <div class="layui-form-item">
                <label class="layui-form-label">原始密码</label>
                <div class="layui-input-inline">
                    <input name="oldPassword"   id="oldPassword" placeholder="请输入原始密码" value="" required="" lay-verify="required" autocomplete="off" class="layui-input" type="password">
                </div>

            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">新密码</label>
                <div class="layui-input-inline">
                    <input name="newPassword" id="newPassword" placeholder="请输入新密码" value="" lay-verify="required"  autocomplete="off" class="layui-input" type="password">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">确认密码</label>
                <div class="layui-input-inline">
                    <input name="confirmPassword" id="confirmPassword" placeholder="请输入确认密码" value="" lay-verify="required"  autocomplete="off" class="layui-input" type="password">
                </div>

            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" type="button" onclick="updatePassWord();" lay-filter="formDemo">确认修改</button>
                <#--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
                </div>
            </div>
        </form>
        </div>

    </div>
</div>

<script>
    var basePath="${basePath}";
    //注意：选项卡 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element();

        //…
    });
    function updatePassWord() {
        var oldPassword = $("#oldPassword").val();
        var newPassword = $("#newPassword").val();
        var confirmPassword = $("#confirmPassword").val();
        if(oldPassword==''){
            layer.open({
                type: 0,
                content: '原始密码不能为空'
            });
            return false;
        }
        if(newPassword==''){
            layer.open({
                type: 0,
                content: '新密码不能为空'
            });
            return false;
        }
        if(confirmPassword==''){
            layer.open({
                type: 0,
                content: '确认密码不能为空'
            });
            return false;
        }
        if(newPassword!=confirmPassword){
            layer.open({
                type: 0,
                content: '新密码与确认密码不符'
            });
            return false;
        }
        $("#password_form").form('submit', {
            url : basePath + "/system/SysPerson/updatePassword",
            dataType : 'text',
            success : function(data) {
               if(data=='1'){
                   layer.open({
                       type: 0,
                       content: '原始密码错误'
                   });
               }else if(data=='2'){
                   layer.open({
                       type: 0,
                       content: '新密码与确认密码不符'
                   });
               }else{
                   layer.open({
                       type: 0,
                       content: '修改密码成功'
                   });
               }
            }

        });
    }
    function updateData() {
        $("#base_form").form('submit', {
            url : basePath + "/system/SysPerson/saveBase",
            dataType : 'text',
            success : function(data) {
                layer.open({
                    type: 0,
                    content: '修改个人基本信息成功' //这里content是一个普通的String
                });
            }

        });
    }
</script>
<body>
</html>