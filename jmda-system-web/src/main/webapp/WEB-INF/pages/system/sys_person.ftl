<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>人员</title>
    <!--公共资源配置开始-->
     <#include "../../comm/eformMeta.ftl" />
    <!--公共资源配置结束-->
    
    ${extMeta}
    <script>
     var param_departmentId ='${param_departmentId}';
    </script>
</head>
<body>
     <form id="data_form" method="post" action="" class="page-form">
      <div class="buts-row-top row ">
         	<div class="col-sm-12 text-align-left buts-col" >
         	 <button type="button" class="btn btn-success" onClick="saveData();"><i class="fa fa-check right"></i>保存</button>&nbsp;
              <button type="button" class="btn btn-success" onClick="initPassWord();" style="display: none;"><i class="fa fa-check right"></i>初始化密码</button>&nbsp;
        	</div>
        </div>
        <div class="but-row-top-shadow">
      </div>
        <div class="form-title">
            人员
            <a class="icon-only float-right blue btn-min" href="javascript:void(0);" onclick="showHideFormPart(this)">
            <i class="fa fa-minus"></i>
            </a>
        </div>
			
			<div class="row">
            <div class="hb-group col-sm-4 no-padding">
                
                	<label class="col-sm-3 hb-label" style="margin-right: 10px;">部门</label>
                	<div class="col-sm-5 hb-control" >
                         ${departmentId}
                    </div>
                   
            </div>
            <div class="hb-group col-sm-4 no-padding">
                
                	<label class="col-sm-3 hb-label" style="margin-right: 10px;">登录名</label>
                	<div class="col-sm-5 hb-control" >
                         ${loginName}
                    </div>
                   
            </div>
            <div class="hb-group col-sm-4 no-padding">
                
                	<label class="col-sm-3 hb-label" style="margin-right: 10px;">名称</label>
                	<div class="col-sm-5 hb-control" >
                      ${name}
                    </div>
                   
            </div>
            </div>
			<div class="row">
            <div class="hb-group col-sm-4 no-padding">
                
                	<label class="col-sm-3 hb-label" style="margin-right: 10px;">编码</label>
                	<div class="col-sm-5 hb-control" >
                      ${code}
                    </div>
                   
            </div>
            <div class="hb-group col-sm-4 no-padding">
                
                	<label class="col-sm-3 hb-label" style="margin-right: 10px;">email</label>
                	<div class="col-sm-5 hb-control" >
                      ${email}
                    </div>
                   
            </div>
            <div class="hb-group col-sm-4 no-padding">
                
                	<label class="col-sm-3 hb-label" style="margin-right: 10px;">电话</label>
                	<div class="col-sm-5 hb-control" >
                      ${mobile}
                    </div>
                   
            </div>
            </div>
			<div class="row">
            <div class="hb-group col-sm-4 no-padding">
                
                	<label class="col-sm-3 hb-label" style="margin-right: 10px;">手机</label>
                	<div class="col-sm-5 hb-control" >
                      ${telephone}
                    </div>
                   
            </div>
			   </div>
			  ${personId}
			 ${sys_rm_group_person_GRID}
    </form>
</body>
</html>
