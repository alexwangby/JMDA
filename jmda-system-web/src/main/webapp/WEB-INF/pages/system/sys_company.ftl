<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>公司</title>
    <!--公共资源配置开始-->
     <#include "../../comm/eformMeta.ftl" />
    <!--公共资源配置结束-->
    
    ${extMeta}
</head>
<body>
     <form id="data_form" method="post" action="" class="page-form">
      <div class="buts-row-top row ">
         	<div class="col-sm-12 text-align-left buts-col" >
         	 <button type="button" class="btn btn-success" onClick="saveData();"><i class="fa fa-check right"></i>保存</button>&nbsp;
        	</div>
        </div>
        <div class="but-row-top-shadow">
      </div>
        <div class="form-title">
            公司
            <a class="icon-only float-right blue btn-min" href="javascript:void(0);" onclick="showHideFormPart(this)">
            <i class="fa fa-minus"></i>
            </a>
        </div>
			  ${companyId}
			<div class="row">
            <div class="hb-group col-sm-6 no-padding">
                
                	<label class="col-sm-2 hb-label">编码</label>
                	<div class="col-sm-8 hb-control">
                      ${code}
                    </div>
                   
            </div>
            <div class="hb-group col-sm-6 no-padding">
                
                	<label class="col-sm-2 hb-label">名称</label>
                	<div class="col-sm-8 hb-control">
                      ${name}
                    </div>
                   
            </div>
            </div>
    </form>
</body>
</html>
