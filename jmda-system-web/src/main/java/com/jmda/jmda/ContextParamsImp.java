package com.jmda.jmda;


import com.jmda.common.context.FmsContext;
import com.jmda.common.model.PersonBean;
import com.jmda.platform.plugs.context.JmdaContextInterface;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Service
public class ContextParamsImp implements JmdaContextInterface {
    @Override
    public Map<String, Object> getContextParams() {
        // TODO Auto-generated method stub
        Map<String, Object> params = new HashMap<>();
        PersonBean bean = FmsContext.getPersonContext() == null ? new PersonBean() : FmsContext.getPersonContext().getPersonBean();
        params.put("_personId", bean.getPersonId());
        params.put("_personAccount", bean.getLoginName());
        params.put("_personName", bean.getPersonName());
        params.put("_companyId", bean.getCompanyId());
        params.put("_companyName", bean.getCompanyName());
        params.put("_departmentId", bean.getDepartmentId());
        params.put("_departmentName", bean.getDepartmentName());
        params.put("_time", String.valueOf(System.currentTimeMillis()));
        Calendar cal = Calendar.getInstance();
        Integer year = cal.get(Calendar.YEAR);
        params.put("_year", year.toString());
        return params;
    }
}
