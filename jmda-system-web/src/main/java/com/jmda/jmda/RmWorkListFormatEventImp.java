package com.jmda.jmda;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jmda.common.dao.table.mapper.SysRmPOMapper;
import com.jmda.common.model.po.SysRmPO;
import com.jmda.common.model.po.SysRmPOExample;
import com.jmda.platform.engine.worklist.event.WorkListFormatEvent;
import com.jmda.platform.repository.worklist.model.ExtColumnModel;
/**
 * 权限树
 * @author chens
 *
 */
@Service
public class RmWorkListFormatEventImp implements WorkListFormatEvent {
	@Resource
	SysRmPOMapper sysRmPOMapper;

	@Override
	public void formatColumnModel(List<ExtColumnModel> list) {
		// TODO Auto-generated method stub

	}

	@Override
	public String formatRecords(List<ExtColumnModel> columnList, List<Map<String, Object>> records) {
		// TODO Auto-generated method stub
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		SysRmPOExample sysRmPOExample = new SysRmPOExample();
		sysRmPOExample.createCriteria().andIsDeleteEqualTo(false);
		sysRmPOExample.setOrderByClause("priority ASC");
		List<SysRmPO> sysRmPOList = sysRmPOMapper.selectByExample(sysRmPOExample);
        try {
			buildTree(sysRmPOList, null, arrayNode, null, mapper);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String json="";
	    try {
			json=mapper.writeValueAsString(arrayNode);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	    return json;
	}

	private void buildTree(List<SysRmPO> sysRmPOList, ObjectNode pNode, ArrayNode pChildrenNode,
			String pid, ObjectMapper mapper) throws Exception {
		List<SysRmPO> list = this.getChildrenList(sysRmPOList, pid);
		if (list.size() > 0) {
			ArrayNode childrenNode = mapper.createArrayNode();
			for (SysRmPO childrenPO : list) {
				ObjectNode node = mapper.createObjectNode();
				node.put("id",  childrenPO.getRmId());
				node.put("text", childrenPO.getName());
				node.put("state", "open");
				node.put("programCode", childrenPO.getProgramCode());
				node.put("rmPid", childrenPO.getRmPid()==null?"":childrenPO.getRmPid());
				this.buildTree(sysRmPOList, node, childrenNode, childrenPO.getRmId(), mapper);
				if(pNode==null){
					pChildrenNode.add(node);
				}else{
					pNode.put("children", childrenNode);
				}
			}
		}
		if(pNode!=null){
			pChildrenNode.add(pNode);
		}
	}

	private List<SysRmPO> getChildrenList(List<SysRmPO> sysRmPOList, String pid) {
		List<SysRmPO> ls = new ArrayList<>();
		for (SysRmPO sysDepartmentPO : sysRmPOList) {
			if (((null == sysDepartmentPO.getRmPid()||"".equals(sysDepartmentPO.getRmPid())) && null == pid)
					|| (null != sysDepartmentPO.getRmPid() && sysDepartmentPO.getRmPid().equals(pid))) {
				ls.add(sysDepartmentPO);
			}
		}
		return ls;

	}

}
