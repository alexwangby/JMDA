package com.jmda.cu.service;


import com.jmda.common.model.po.WfWorkflowPO;


public interface WfWorkflowService {

	// TODO 写入一条数据
	public void insert(WfWorkflowPO record);

	// TODO 通过主键ID批量删除表中数据
	public void deleteDatas(String ids) ;

	// TODO 通过主键修改 表中数据
	public void update(WfWorkflowPO record) ;

	// TODO 通过主键读取表中数据
	public WfWorkflowPO getWfWorkflowPO(String id);
	
}
