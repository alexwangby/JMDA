package com.jmda.cu.service.imp;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.common.dao.table.mapper.WfWorkflowPOMapper;
import com.jmda.common.model.po.WfWorkflowPO;
import com.jmda.cu.service.WfWorkflowService;

@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
@Service
public class WfWorkflowServiceImp extends SqlSessionDaoSupport implements WfWorkflowService {
	// 由系统工具生成的数据库表的DAO对象文件 可参照metadata工程相关代码
	@Autowired
	WfWorkflowPOMapper mapper;

	// TODO 表中写入一条数据
	@Override
	public void insert(WfWorkflowPO record) {
		mapper.insertSelective(record);
	}

	// TODO 通过主键ID批量删除mm_goods 表中数据
	@Override
	public void deleteDatas(String ids) {
		String[] idArray = ids.split(",");
		for (int i = 0; i < idArray.length; i++) {
			String id = idArray[i];
			mapper.deleteByPrimaryKey(id);
		}
	}

	// TODO 通过主键修改表中数据
	@Override
	public void update(WfWorkflowPO record) {
		mapper.updateByPrimaryKeySelective(record);
	}

	// TODO 通过主键读取表中数据
	@Override
	public WfWorkflowPO getWfWorkflowPO(String id) {
		return mapper.selectByPrimaryKey(id);
	}
}
