package com.jmda.cu.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jmda.common.model.po.WfWorkflowPO;
import com.jmda.cu.service.WfWorkflowService;

@Controller
@RequestMapping("/cu/WfWorkflow")
public class WfWorkflowController {

	@Autowired
	WfWorkflowService service;
	
	@RequestMapping(value = "/save", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String save(WfWorkflowPO record,HttpServletRequest request) {
			//TODO 如果有子表数据时可通过如下例子进行json数据转换为 po 对象
			/**
			    ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			 try {
				List<WmStockInListPO> list  = mapper.readValue(jsondatas, new TypeReference<ArrayList<WmStockInListPO>>() {});
				System.out.println(list.size());
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 */
			 
		return "ok";
	}

	@RequestMapping(value = "/deleteDatas", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String deleteDatas(String ids) {
		service.deleteDatas(ids);
		return "ok";
	}
}
