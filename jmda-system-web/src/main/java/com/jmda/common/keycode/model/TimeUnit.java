package com.jmda.common.keycode.model;

/**
 * 用于枚举codes/code节点的reset_time属性的单位<br/>
 * 该属性不存在或者为空,则表示默认为-1<br/>
 * 该属性未使用单位,则表示单位为毫秒<br/>
 * 该属性若使用单位,则自动计算毫秒数<br/>
 * 示例: 1d,2M,3h<br/>
 * 单位前面的数字不能为小数,比如1.5d是不允许的,请使用36h代替<br/>
 * 单位区分大小写<br/>
 * 单位简写分别为:y,M,d,w,h,m,s
 *
 * @author chens
 */
public enum TimeUnit {
    YEAR("y", 365 * 24 * 60 * 60 * 1000L), MONTH("M", 30 * 24 * 60 * 60 * 1000L), DAY("d", 24 * 60 * 60 * 1000L),
    WEEK("w", 7 * 24 * 60 * 60 * 1000L), HOUR("h", 60 * 60 * 1000L), MINUTE("m", 60 * 1000L), SECOND("s", 1000L);
    // 成员变量
    private String index;
    private Long ms;

    // 构造方法
    private TimeUnit(String index, Long ms) {
        this.index = index;
        this.ms = ms;
    }

    public Long getMs() {
        return ms;
    }

    public void setMs(Long ms) {
        this.ms = ms;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    /**
     * 字符串转毫秒数
     *
     * @param index
     * @return
     */
    public static Long str2Millisecond(String index) {
        for (TimeUnit tu : TimeUnit.values()) {
            if (index.equals(tu.getIndex())) {
                return tu.ms;
            }
        }
        return -1L;
    }
}
