package com.jmda.common.keycode.service;


import org.apache.commons.lang.StringUtils;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Service;

import com.jmda.common.constances.SysConstances;
import com.jmda.common.keycode.model.KeyCodeEnum;
import com.jmda.common.keycode.model.KeyCodeRuleBean;
import com.jmda.common.model.po.CuGeneratorKeyCodePO;
import com.jmda.common.model.po.CuGeneratorOrderCodePO;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author chens
 */
@Service("keyCodeGenerator")
public class KeyCodeServiceImpl extends SqlSessionDaoSupport implements KeyCodeService {
    private String KEY_CODE_TYPE;
    private String nameSpace = "com.jmda.common.dao.KeyCodeMapper.";
    private String cuGeneratorKeyCodePOMapperNameSpace = "com.jmda.common.dao.table.mapper.CuGeneratorKeyCodePOMapper.";
    private String cuGeneratorOrderCodePOMapperNameSpace = "com.jmda.common.dao.table.mapper.CuGeneratorOrderCodePOMapper.";

    /**
     * 初始化
     */
    private void init() {
    	 this.KEY_CODE_TYPE = SysConstances.KEYCODE_CFG_MAP.get("keycode.config.ctype");
    }

    @Override
    public String nextCode(String codeName, Map<KeyCodeEnum, String> params) {
        this.init();
        String paramsKey = "";
        if (null != params) {
            for (KeyCodeEnum value : KeyCodeEnum.values()) {
                for (KeyCodeEnum key : params.keySet()) {
                    if (value.name().equals(key.toString())) {
                        paramsKey += params.get(key);
                    }
                }
            }
        }
        String keyCodeId = codeName;//主键编码
        if (StringUtils.isNotEmpty(paramsKey)) {
            keyCodeId += paramsKey;
        }
        KeyCodeRuleBean codeRuleBean = (KeyCodeRuleBean) SysConstances.KEY_CODE_RULE_MAP.get(codeName);//获得主键规则bean
        Map<String, Object> dateMap = codeRuleBean.getDateMap();
        Map<String, Object> sequenceMap = codeRuleBean.getSequenceMap();
        Map<String, Object> randomMap = codeRuleBean.getRandomMap();

        String name = codeRuleBean.getLabel();
        String expression = codeRuleBean.getExpression();
        String currentKey = expression;
        String dateCode = null;
        if (null != dateMap) {
            String format = (String) dateMap.get("format");
            dateCode = new SimpleDateFormat(format).format(new Date());
        }
        int digits = 10;//进制 默认10
        int length = 5;//长度 默认5
        String type = "append";
        String digitsValue = "00000";
        long sequenceCurrent = 0L;
        if (null != sequenceMap) {
            digits = (int) sequenceMap.get("digits");
            length = (int) sequenceMap.get("length");
            type = (String) sequenceMap.get("type");
            digitsValue = (String) sequenceMap.get("digitsValue");
            sequenceCurrent = Long.valueOf(digitsValue, digits);
        }
        String sequence = null;
        CuGeneratorKeyCodePO cuGeneratorKeyCodePO = null;
        long dbSequenceCurrent = 0l;

        if (KEY_CODE_TYPE.equals("NO_CACHE")) {//不做缓存数据
            Map<String, Object> callParams = new HashMap<>();
            callParams.put("keyCodeId", keyCodeId);
            callParams.put("name", name);
            callParams.put("expression", expression);
            callParams.put("dateCode", dateCode);
            callParams.put("type", type);
            callParams.put("digits", digits);
            callParams.put("sequenceCurrent", sequenceCurrent);
            cuGeneratorKeyCodePO =   super.getSqlSession().selectOne(nameSpace + "callCuPrcKeyCodeNext", callParams);
            dbSequenceCurrent = cuGeneratorKeyCodePO.getSequenceCurrent();
            digitsValue = Long.toString(dbSequenceCurrent, digits).toUpperCase();
            sequence = String.format("%" + length + "s", digitsValue).replace(" ", "0");
        }

        String H_KEY = "KEY_CODE_" + codeName;

        String random = null;
        if (null != randomMap) {
            Random rm = new Random();
            int randomLength = (int) randomMap.get("length");
            String max = "";
            for (int i = 0; i < randomLength; i++) {
                max += "9";
            }
            random = String.format("%" + randomLength + "d", rm.nextInt(Integer.valueOf(max))).replace(" ", "0");//longToStr(randomLength, rm.nextInt(Integer.valueOf(max)));
        }

        if (StringUtils.isNotEmpty(dateCode)) {
            currentKey = currentKey.replace("[date]", dateCode);
        } else {
            currentKey = currentKey.replace("[date]", "");
        }
        if (StringUtils.isNotEmpty(sequence)) {
            currentKey = currentKey.replace("[sequence]", sequence);
        } else {
            currentKey = currentKey.replace("[sequence]", "");
        }
        if (StringUtils.isNotEmpty(random)) {
            currentKey = currentKey.replace("[random]", random);
        } else {
            currentKey = currentKey.replace("[random]", "");
        }
        if (null != params) {
            for (KeyCodeEnum key : params.keySet()) {
                currentKey = currentKey.replace("[" + key.toString() + "]", params.get(key));
            }
        }

        if (KEY_CODE_TYPE.equals("NO_CACHE")) {//不做缓存数据
            cuGeneratorKeyCodePO.setCurrentKey(currentKey);
            cuGeneratorKeyCodePO.setDigitsValue(digitsValue);
            super.getSqlSession().update(cuGeneratorKeyCodePOMapperNameSpace+ "updateByPrimaryKeySelective", cuGeneratorKeyCodePO);
        }
        return currentKey;
    }
    /*
	 * <p>Title: nextOrderCode</p>
	 * <p>Description: </p>
	 * @param codeName
	 * @param order
	 * @return
	 * @see com.hbdiy.common.plugs.keycode.service.KeyCodeService#nextOrderCode(java.lang.String, java.lang.String)
	 */
	@Override
	public String nextOrderCode(String codeName, String order) {
		this.init();
		
		String orderCodeId=codeName;//主键编码
		orderCodeId=codeName+order;
		
		KeyCodeRuleBean codeRuleBean=(KeyCodeRuleBean) SysConstances.KEY_CODE_RULE_MAP.get(codeName);//获得主键规则bean
		Map<String,Object> sequenceMap=codeRuleBean.getSequenceMap();
		
		String name=codeRuleBean.getLabel();
		String expression=codeRuleBean.getExpression();
		String currentCode=expression;

		int digits=10;//进制 默认10
		String digitsValue="00000";
		long sequenceCurrent=0L;
		if(null!=sequenceMap){
			digits=(int) sequenceMap.get("digits");
			digitsValue=(String)sequenceMap.get("digitsValue");
			sequenceCurrent=Long.valueOf(digitsValue, digits);
		}
		String sequence=null;
		CuGeneratorOrderCodePO cuGeneratorOrderCodePO=null;
		long dbSequenceCurrent=1l;
		
		Map<String,Object> callParams=new HashMap<>();
		callParams.put("orderCodeId", orderCodeId);
		callParams.put("name", name);
		callParams.put("expression", expression);
		callParams.put("digits", digits);
		callParams.put("sequenceCurrent", sequenceCurrent);
		cuGeneratorOrderCodePO = super.getSqlSession().selectOne(nameSpace + "callCuPrcOrderCodeNext", callParams);
		dbSequenceCurrent=cuGeneratorOrderCodePO.getSequenceCurrent();
		digitsValue=Long.toString(dbSequenceCurrent, digits).toUpperCase();
		sequence=String.format("%1s",digitsValue).replace(" ", "0");
	
		if(StringUtils.isNotEmpty(sequence)){
			currentCode=currentCode.replace("[sequence]", sequence);
		}else{
			currentCode=currentCode.replace("[sequence]", "");
		}
		currentCode=currentCode.replace("[order]",order);

		cuGeneratorOrderCodePO.setCurrentCode(currentCode);
		super.getSqlSession().update(cuGeneratorOrderCodePOMapperNameSpace+ "updateByPrimaryKeySelective", cuGeneratorOrderCodePO);
		return currentCode;
	}


}
