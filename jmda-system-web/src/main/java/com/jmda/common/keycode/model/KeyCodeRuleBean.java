package com.jmda.common.keycode.model;

import java.io.Serializable;
import java.util.Map;

public class KeyCodeRuleBean implements Serializable {

	private static final long serialVersionUID = 4892202900390405176L;
	public static final String OBJECT_KEY = "CODE_RULE";
    public String name;
    public String label;
    public Map<String, Object> sequenceMap;
    public Map<String, Object> randomMap;
    public Map<String, Object> dateMap;
    public String expression;

    public KeyCodeRuleBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getSequenceMap() {
        return sequenceMap;
    }

    public void setSequenceMap(Map<String, Object> sequenceMap) {
        this.sequenceMap = sequenceMap;
    }

    public Map<String, Object> getRandomMap() {
        return randomMap;
    }

    public void setRandomMap(Map<String, Object> randomMap) {
        this.randomMap = randomMap;
    }

    public Map<String, Object> getDateMap() {
        return dateMap;
    }

    public void setDateMap(Map<String, Object> dateMap) {
        this.dateMap = dateMap;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public static String getObjectKey() {
        return OBJECT_KEY;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
