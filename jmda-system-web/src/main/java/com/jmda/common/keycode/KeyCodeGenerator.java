package com.jmda.common.keycode;

import java.util.UUID;

import com.jmda.common.keycode.service.KeyCodeService;
import com.jmda.common.utils.other.SpringBeanUtil;

/**
 * @author chensen
 */
public class KeyCodeGenerator {
    /**
     * 获得主键编码
     * 存储中的序列自增1，当保存表的时候调用
     *
     * @param codeName
     * @return
     */
    public static synchronized String nextCode(String codeName) {
        KeyCodeService keyCodeService = (KeyCodeService) SpringBeanUtil.getBean("keyCodeGenerator");
        String seqKey = keyCodeService.nextCode(codeName,null);
        return seqKey;
    }

	public static synchronized String nextOrderCode(String codeName,String order) {
		KeyCodeService keyCodeService = (KeyCodeService) SpringBeanUtil.getBean("keyCodeGenerator");
		String seqKey = keyCodeService.nextOrderCode(codeName, order);
		return seqKey;
	}
    /**
     * 获得32位UUID
     */
    public static String random32UUID() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        String temp = str.substring(0, 8) + str.substring(9, 13)
                + str.substring(14, 18) + str.substring(19, 23)
                + str.substring(24);
        return temp.toUpperCase();
    }

}
