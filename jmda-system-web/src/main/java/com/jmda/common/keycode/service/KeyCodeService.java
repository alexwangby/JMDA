package com.jmda.common.keycode.service;

import java.util.Map;

import com.jmda.common.keycode.model.KeyCodeEnum;


public interface KeyCodeService {
    /**
     * nextCode(获得下一个编码)
     *
     * @param @param codeName 主键生成器编码
     * @param @param params 参数
     * @return String
     * @throws
     */
    String nextCode(String codeName, Map<KeyCodeEnum, String> params);
    /**
	 * 
	  * nextOrderCode(获得下一个单据编码)
	  *
	  * @Title: nextOrderCode
	  * @Description: TODO
	  * @param @param codeName
	  * @param @param order
	  * @param @return    
	  * @return String    
	  * @throws
	 */
	public String nextOrderCode(String codeName,String order);
}
