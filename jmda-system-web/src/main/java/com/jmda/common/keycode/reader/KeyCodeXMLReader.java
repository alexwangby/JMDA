package com.jmda.common.keycode.reader;

import com.google.common.collect.Lists;
import com.jmda.common.constances.SysConstances;
import com.jmda.common.keycode.model.KeyCodeRuleBean;
import com.jmda.common.keycode.model.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class KeyCodeXMLReader {
    @SuppressWarnings("unchecked")
    public static List<KeyCodeRuleBean> read(String filepath) {
        SAXReader sr = new SAXReader();// 获取读取xml的对象。
        try {

            List<KeyCodeRuleBean> list = new ArrayList<KeyCodeRuleBean>();
            Document doc = sr.read(new ClassPathResource(filepath).getInputStream());
            Element elRoot = doc.getRootElement();
            List<Element> elCodes = elRoot.elements("code");
            for (Element elCode : elCodes) {
                KeyCodeRuleBean codeRuleBean = new KeyCodeRuleBean();
                String name = elCode.attributeValue("name");
                String label = elCode.attributeValue("label");
                codeRuleBean.setName(name);
                codeRuleBean.setLabel(label);
                Element sequenceEl = elCode.element("sequence");
                if (null != sequenceEl) {
                    int digits = 10;//默认十进制
                    if (StringUtils.isNotEmpty(sequenceEl.attributeValue("digits"))) {
                        digits = Integer.valueOf(sequenceEl.attributeValue("digits"));
                    }
                    int length = 5;//默认5位
                    if (StringUtils.isNotEmpty(sequenceEl.attributeValue("length"))) {
                        length = Integer.valueOf(sequenceEl.attributeValue("length"));
                    }
                    String type = "append";//默认追加的方式
                    if (StringUtils.isNotEmpty(sequenceEl.attributeValue("type"))) {
                        type = sequenceEl.attributeValue("type");
                    }
                    String digitsValue = "00000";//默认值是0
                    if (StringUtils.isNotEmpty(sequenceEl.getText())) {
                        digitsValue = sequenceEl.getText();
                    }

                    Map<String, Object> sequenceMap = new HashMap<String, Object>();
                    sequenceMap.put("digits", digits);
                    sequenceMap.put("length", length);
                    sequenceMap.put("type", type);
                    sequenceMap.put("digitsValue", digitsValue);
                    codeRuleBean.setSequenceMap(sequenceMap);
                }

                Element randomEl = elCode.element("random");
                if (null != randomEl) {
                    int length = Integer.valueOf(randomEl.attributeValue("length"));
                    Map<String, Object> randomMap = new HashMap<String, Object>();
                    randomMap.put("length", length);
                    codeRuleBean.setRandomMap(randomMap);
                }
                Element dateEl = elCode.element("date");
                if (null != dateEl) {
                    String format = dateEl.attributeValue("format");
                    Map<String, Object> dateMap = new HashMap<String, Object>();
                    dateMap.put("format", format);
                    codeRuleBean.setDateMap(dateMap);
                }

                Element expressionEl = elCode.element("expression");
                if (null != expressionEl) {
                    String expression = expressionEl.getText();
                    codeRuleBean.setExpression(expression);
                }
                list.add(codeRuleBean);
            }
            return list;
        } catch (Exception e) {
            //System.err.println(e.getMessage());
            System.err.println(filepath + " 文件不存在！无法配置");
        }
        return null;
    }

    /**
     * 获得主键配置
     */
    public static KeyCodeRuleBean getCodeRuleBean(String codeName) {

        List<KeyCodeRuleBean> list = KeyCodeXMLReader.read("config/keycode.xml");
        for (KeyCodeRuleBean codeRuleBean : list) {
            if (codeRuleBean.getName().equals(codeName)) {
                return codeRuleBean;
            }
        }
        return null;
    }

    /**
     * 将带单位的过期时间转化成毫秒数,不带单位的数字直接输出<br/>
     * 单位:y-年(365日),M-月(30日),d-日,w-周,h-小时,m-分,s-秒<br/>
     * 基本上用不到年月的单位
     *
     * @param timeout
     * @return
     */
    private static Long str2long(final String timeout) {
        Long result = 0L;
        if (StringUtils.isEmpty(timeout)) {
            return -1L;
        }
        try {
            result = Long.valueOf(timeout);
        } catch (Exception e) {
            String unit = timeout.substring(timeout.length() - 1);
            Long num = Long.valueOf(timeout.substring(0, timeout.length() - 1));
            if (num <= 0) {
                result = -1L;
            } else {
                result = num * TimeUnit.str2Millisecond(unit);
            }
        }
        return result;
    }

    public static void initKeyCodeConfig() {
        //读取默认keycode.xml
        List<KeyCodeRuleBean> list = KeyCodeXMLReader.read("config/keycode.xml");
        if (list == null) {
            list = Lists.newArrayList();
        }
        //读取扩展keycode_app.xml等扩展xml
        IOFileFilter fileFilter = FileFilterUtils.prefixFileFilter("keycode");
        IOFileFilter dirFilter = FileFilterUtils.directoryFileFilter();
        try {
            File keycodeCfgFolder = new ClassPathResource("config/").getFile();
            Collection<File> keycodeFiles = FileUtils.listFiles(keycodeCfgFolder, fileFilter, dirFilter);
            for (File keycodeFile : keycodeFiles) {
                System.out.println(keycodeFile.getPath());
                List<KeyCodeRuleBean> listItem = KeyCodeXMLReader.read("config/" + keycodeFile.getName());
                if (listItem != null) {
                    list.addAll(listItem);
                }
            }
        } catch (IOException e) {
            System.err.println(e);
        }


        Map<String, Object> codeRuleMap = new HashMap<String, Object>();
        if (null != list) {
            for (KeyCodeRuleBean codeRuleBean : list) {
                codeRuleMap.put(codeRuleBean.getName(), codeRuleBean);
            }
        }

        KeyCodeRuleBean codeRuleBean = new KeyCodeRuleBean();
        codeRuleBean.setName(SysConstances.KEYCODE_CFG_MAP.get("keycode.code.name"));
        codeRuleBean.setLabel(SysConstances.KEYCODE_CFG_MAP.get("keycode.code.label"));
        Map<String, Object> sequenceMap = new HashMap<String, Object>();
        sequenceMap.put("length", Integer.parseInt(SysConstances.KEYCODE_CFG_MAP.get("keycode.code.sequence.length")));
        sequenceMap.put("type", SysConstances.KEYCODE_CFG_MAP.get("keycode.code.sequence.type"));
        sequenceMap.put("digits", Integer.parseInt(SysConstances.KEYCODE_CFG_MAP.get("keycode.code.sequence.digits")));
        sequenceMap.put("digitsValue", SysConstances.KEYCODE_CFG_MAP.get("keycode.code.sequence.digitsValue"));

        codeRuleBean.setSequenceMap(sequenceMap);
        codeRuleBean.setExpression(SysConstances.KEYCODE_CFG_MAP.get("keycode.code.expression"));
        if (null != codeRuleMap.get(codeRuleBean.getName())) {
            codeRuleMap.remove(codeRuleBean.getName());
        }
        codeRuleMap.put(codeRuleBean.getName(), codeRuleBean);
        SysConstances.KEY_CODE_RULE_MAP = codeRuleMap;
    }
}