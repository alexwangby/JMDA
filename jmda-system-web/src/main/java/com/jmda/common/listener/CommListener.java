package com.jmda.common.listener;



import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.jmda.common.keycode.reader.KeyCodeXMLReader;
import com.jmda.common.reader.PropertiesReader;

public class CommListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent event) {
    	// --keycode_cfg.properties
        PropertiesReader.cacheKeyCodeCfgProperties();
        // 主键生成器
        KeyCodeXMLReader.initKeyCodeConfig();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
