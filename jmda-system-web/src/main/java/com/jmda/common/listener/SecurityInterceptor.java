package com.jmda.common.listener;


import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jmda.common.context.FmsContext;
import com.jmda.common.context.PersonContext;
import com.jmda.common.context.PersonContextImp;
import com.jmda.common.utils.other.CookieUtil;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;


/**
 * 登录拦截器
 */
public class SecurityInterceptor extends HandlerInterceptorAdapter {
	@Resource(name="personContextImp")
	PersonContextImp personContextImp;
    private int timeout = 30 * 60;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String topath = request.getRequestURI();
        String[] strings = topath.split("/");

        if (strings==null||strings.length==0||strings.length==1){
            return true;
        }
        String string = strings[1];
        switch (string) {
            case "timetoken":
                return true;
            case "login":
                return true;
            case "appLogin":
                return true;
            case "loginin":
                return true;
            case "toWelcome":
                return true;
            case "static":
                return true;
            case "assets":
                return true;
            case "jmda-static":
                return true;
            case "api":
                return true;

        }
        String deviceType = request.getParameter("deviceType");//CookieUtil.getCookieByName(request, "deviceType");
        if ("mobile".equalsIgnoreCase(deviceType)) {
            String usertokenValue = request.getParameter("cookie");
            PersonContext uc = personContextImp.newPersonContext(usertokenValue);
            if(uc==null||uc.getPersonId()==null){
                return false;
            }
            FmsContext.setPersonContext(uc);
            return true;
        }else {
            Cookie cookie = CookieUtil.getCookieByName(request, "FMS_USERTOKEN");
            String usertokenValue = cookie == null ? "" : cookie.getValue();
            if (cookie == null || "".equals(usertokenValue)) {// 未登录
                PrintWriter out = response.getWriter();
                StringBuilder builder = new StringBuilder();
                builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
                builder.append("alert(\"ERROR01,未登录系统，请登录系统!\");");
                builder.append("window.location.href=\""+request.getContextPath()+"/login\";</script>");
                out.print(builder.toString());
                out.close();
                return false;
            }
            // 成功操作用户刷新session信息
            // ERPJedisKeyValueAPI.expire(usertokenValue, timeout);
            //将登录信息 添加到线程变量
            PersonContext uc = personContextImp.newPersonContext(usertokenValue);
            if(uc==null||uc.getPersonId()==null){
                PrintWriter out = response.getWriter();
                StringBuilder builder = new StringBuilder();
                builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
                builder.append("alert(\"ERROR01,未登录系统，请登录系统!\");");
                builder.append("window.top.tologin();</script>");
                out.print(builder.toString());
                out.close();
                return false;
            }
            FmsContext.setPersonContext(uc);
            return true;
        }

    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

}
