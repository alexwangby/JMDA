package com.jmda.common.context;

import com.jmda.common.model.PersonBean;

public interface PersonContext {
//	public CompanyBean getCompanyBean() ;
//	public DepartmentBean getDepartmentBean();
	public PersonBean getPersonBean() ;
//	public WarehouseBean getWarehouseBean();
	public String getPersonId() ;
//	public String getPersonName() ;
	public String getCompanyId();
	public String getCompanyName();
	public String getDepartmentId() ;
	public String getDepartmentName() ;
	public String getLoginName() ;
}
