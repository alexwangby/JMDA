package com.jmda.common.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jmda.common.dao.table.mapper.SysCompanyPOMapper;
import com.jmda.common.dao.table.mapper.SysDepartmentPOMapper;
import com.jmda.common.dao.table.mapper.SysPersonPOMapper;
import com.jmda.common.model.CompanyBean;
import com.jmda.common.model.DepartmentBean;
import com.jmda.common.model.PersonBean;
import com.jmda.common.model.po.SysCompanyPO;
import com.jmda.common.model.po.SysDepartmentPO;
import com.jmda.common.model.po.SysPersonPO;


@Service("personContextImp")
public class PersonContextImp implements PersonContext {
    @Autowired
    SysPersonPOMapper sysPersonPOMapper;
    @Autowired
    SysDepartmentPOMapper sysDepartmentPOMapper;
    @Autowired
    SysCompanyPOMapper sysCompanyPOMapper;
    private CompanyBean companyBean;
    private DepartmentBean departmentBean;
    private PersonBean personBean;

    public PersonContext newPersonContext(String usertokenValue) {
        personBean = builtPersonBean(usertokenValue);
        return this;
    }

    public PersonBean builtPersonBean(String usertokenValue) {
    	SysPersonPO sysPersonPO = sysPersonPOMapper.selectByPrimaryKey(usertokenValue);
    	if(sysPersonPO!=null){
        	PersonBean personBean = new PersonBean();
        	personBean.setLoginName(sysPersonPO.getLoginName());
            personBean.setPersonId(sysPersonPO.getPersonId());
            personBean.setPersonName(sysPersonPO.getName());
            personBean.setMobile(sysPersonPO.getMobile());
            personBean.setEmail(sysPersonPO.getEmail());
            personBean.setTelephone(sysPersonPO.getTelephone());
            personBean.setLoginPassword(sysPersonPO.getLoginPassword());
            SysDepartmentPO sysDepartmentPO = sysDepartmentPOMapper.selectByPrimaryKey(sysPersonPO.getDepartmentId());
            personBean.setDepartmentId(sysPersonPO.getDepartmentId());
            personBean.setDepartmentName(sysDepartmentPO.getName());
            SysCompanyPO sysCompanyPO = sysCompanyPOMapper.selectByPrimaryKey(sysDepartmentPO.getCompanyId());
            personBean.setCompanyId(sysCompanyPO.getCompanyId());
            personBean.setCompanyName(sysCompanyPO.getName());
        	return personBean;
    	}else{
    		return null;
    	}

    }

    public CompanyBean getCompanyBean() {
        return companyBean;
    }

    public DepartmentBean getDepartmentBean() {
        return departmentBean;
    }

    public PersonBean getPersonBean() {
        return personBean;
    }


    public String getPersonId() {
        return personBean.getPersonId();
    }

    public String getPersonName() {
        return personBean.getPersonName();
    }

    public String getCompanyId() {
        return personBean.getCompanyId();
    }

    public String getCompanyName() {
        return personBean.getCompanyName();
    }

    public String getDepartmentId() {
        return personBean.getDepartmentId();
    }

    public String getDepartmentName() {
        return personBean.getDepartmentName();
    }

    public String getLoginName() {
        return personBean.getLoginName();
    }

    public String getLoginPassword() {
        return personBean.getLoginPassword();
    }
}
