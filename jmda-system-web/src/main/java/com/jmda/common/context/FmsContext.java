package com.jmda.common.context;

import java.util.Stack;

public class FmsContext {
	protected static ThreadLocal<Stack<PersonContext>> userContextThreadLocal = new ThreadLocal<Stack<PersonContext>>();

	protected static <T> Stack<T> getStack(ThreadLocal<Stack<T>> threadLocal) {
		Stack<T> stack = threadLocal.get();
		if (stack == null) {
			stack = new Stack<T>();
			threadLocal.set(stack);
		}
		return stack;
	}

	public static PersonContext getPersonContext() {
		Stack<PersonContext> stack = getStack(userContextThreadLocal);
		if (stack.isEmpty()) {
			return null;
		}
		return stack.peek();
	}

	public static void setPersonContext(PersonContext userContext) {
		getStack(userContextThreadLocal).push(userContext);
	}
}
