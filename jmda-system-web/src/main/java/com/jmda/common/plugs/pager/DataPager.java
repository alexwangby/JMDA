package com.jmda.common.plugs.pager;

import com.beust.jcommander.internal.Lists;
import com.github.pagehelper.Page;

import java.util.List;

public class DataPager<T> {
    private List<T> rows; // 当页记录
    private long page = 1l; // 页号
    private long size = 20l;// 每页记录数
    private long total = 0l; // 总记录数
    private long max;
    private String pageHtml;// ERP分页html
    private String ecPageHtml;// EC分页html
    private String ecPersonPageHtml;// EC个人中心分页html

    public DataPager() {

    }

    public DataPager(Pager pager) {
        this.page = pager.getPage();
        this.size = pager.getRows();
    }

    public DataPager(DataPager dp) {
        this.rows = dp.getRows();
        this.page = dp.getPage();
        this.size = dp.getSize();
        this.total = dp.getTotal();
        this.max = dp.getMax();
        this.pageHtml = dp.getPageHtml();
        this.ecPageHtml = dp.getEcPageHtml();

    }

    /**
     * 当hbdiy-common/src/main/resources/mybatis.xml的<br/>
     * plugins/plugin/property的name="reasonable"并且value="true"时,<br/>
     * 当传入当前页码大于最大页数,不会返回空记录,而是返回最后一页的记录,为返回空记录需要特殊处理
     *
     * @param dataPager 分页数据
     * @param pager     请求分页的数据,主要使用pager.getPage()
     * @return
     */
    public static <T> DataPager<T> getDataPager(DataPager<T> dataPager, Pager pager) {
        return getDataPager(dataPager, pager.getPage());
    }

    /**
     * 当hbdiy-common/src/main/resources/mybatis.xml的<br/>
     * plugins/plugin/property的name="reasonable"并且value="true"时,<br/>
     * 当传入当前页码大于最大页数,不会返回空记录,而是返回最后一页的记录,为返回空记录需要特殊处理
     *
     * @param dataPager 分页数据
     * @param pageNum   当前页数
     * @return
     */
    public static <T> DataPager<T> getDataPager(DataPager<T> dataPager, int pageNum) {
        Page<T> page = (Page<T>) dataPager.getRows();
        if (dataPager.getRows() != null && dataPager.getRows().size() > 0 & page.getPages() < pageNum) {
            dataPager.setRows(Lists.newArrayList());
        }
        return dataPager;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {

        this.total = total;
    }

    public String getPageHtml() {
        return pageHtml;
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }

    public String getEcPageHtml() {
        return ecPageHtml;
    }

    public void setEcPageHtml(String ecPageHtml) {
        this.ecPageHtml = ecPageHtml;
    }

    public void setPageHtml(String pageHtml) {
        this.pageHtml = pageHtml;
    }

    public String getEcPersonPageHtml() {
        return ecPersonPageHtml;
    }

    public void setEcPersonPageHtml(String ecPersonPageHtml) {
        this.ecPersonPageHtml = ecPersonPageHtml;
    }

}
