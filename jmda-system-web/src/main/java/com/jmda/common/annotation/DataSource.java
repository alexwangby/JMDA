package com.jmda.common.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSource {
    //枚举类型
    enum DataSourceEnum {
        dataSource,yuanDataSource
    }

    DataSourceEnum value() default DataSourceEnum.dataSource;
}
