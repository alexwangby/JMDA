/**   
* TODO 
* @author zhanghui   
* @date 2016年4月14日 下午4:11:36  
*/
package com.jmda.common.utils.other;

import java.net.InetAddress;

import javax.servlet.http.HttpServletRequest;


public class IpUtils {
	public static String getIpAddr(HttpServletRequest request) {  
        String ip = request.getHeader("X-Forwarded-For");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
            //这里主要是获取本机的ip,可有可无  
	         if (ip.equals("127.0.0.1")|| ip.endsWith("0:0:0:0:0:0:1")) {  
	             // 根据网卡取本机配置的IP  
	             InetAddress inet = null;  
	             try {  
	                 inet = InetAddress.getLocalHost();  
	             } catch (Exception e) {  
	                 e.printStackTrace();  
	             }  
	             ip = inet.getHostAddress();  
	         }
        }  
        return ip;  
    }
}
