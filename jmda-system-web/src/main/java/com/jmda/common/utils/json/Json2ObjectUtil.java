package com.jmda.common.utils.json;



import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jmda.common.utils.reflect.BeanMapUtil;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;



public class Json2ObjectUtil {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(Json2ObjectUtil.class);

    @SuppressWarnings("unchecked")
    public static <T> List<T> jsonToPoList(Class<T> clazz, String json) {

        List<T> result = new ArrayList<T>();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        try {
            mapList = objectMapper.readValue(json, List.class);
        } catch (Exception e) {
            logger.error("jsonToPoList(clazz:" + clazz + ", json:" + json + ")" + "--解析josn出错", e);
            e.printStackTrace();
        }
        if (null != mapList && mapList.size() > 0) {
            for (int i = 0; i < mapList.size(); i++) {
                try {
                    T t = (T) BeanMapUtil.convertMap2Bean(clazz, mapList.get(i));
                    result.add(t);
                } catch (Exception e) {
                    logger.error("jsonToPoList(clazz:" + clazz + ", json:" + json + ")" + "--转换为类出错", e);
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

}
