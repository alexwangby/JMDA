package com.jmda.common.utils.other;

import org.apache.commons.beanutils.BeanUtilsBean;

import java.math.BigDecimal;

/**
 * Created by john on 2016/5/17.
 */
public class BeanUtils {
    public static BeanUtilsBean getBeanUtilsBean() {
        BeanUtilsBean beanUtilsBean = new BeanUtilsBean();
        beanUtilsBean.getConvertUtils().register(new org.apache.commons.beanutils.converters.BigDecimalConverter(null), BigDecimal.class);
        beanUtilsBean.getConvertUtils().register(new org.apache.commons.beanutils.converters.DateConverter(null), java.util.Date.class);
        beanUtilsBean.getConvertUtils().register(new org.apache.commons.beanutils.converters.SqlTimestampConverter(null), java.sql.Timestamp.class);
        beanUtilsBean.getConvertUtils().register(new org.apache.commons.beanutils.converters.SqlDateConverter(null), java.sql.Date.class);
        beanUtilsBean.getConvertUtils().register(new org.apache.commons.beanutils.converters.SqlTimeConverter(null), java.sql.Time.class);
        return beanUtilsBean;
    }
}
