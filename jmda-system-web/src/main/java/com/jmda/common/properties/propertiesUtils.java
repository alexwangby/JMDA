
package com.jmda.common.properties;

import java.io.*;
import java.util.Properties;

/**
 * <b>类名称：</b>propertiesUtils <br/>
 * <b>类描述：</b><br/>
 * <b>创建人：</b>chens<br/>
 * <b>修改人：</b><br/>
 * <b>修改备注：</b><br/>
 * <b>版本信息：</b>v1.0.0<br/>
 */

public class propertiesUtils {

    /**
     * 读取Properties配置文件内容
     *
     * @param filePath
     * @return Properties
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static Properties readProperties(String filePath) throws FileNotFoundException, IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(new File(filePath)));
        return properties;
    }

    /**
     * 写key-value到properties文件 相同的key会被覆盖 追加不同的key-value
     *
     * @param key      键
     * @param value    值
     * @param filePath 文件路径
     * @param comment  key-value的注释
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void writeProperties(String key, String value, String comment, String filePath) throws FileNotFoundException, IOException {
        Properties properties = new Properties();

        File file = new File(filePath);
        if (file.exists()) {
            FileInputStream fis = new FileInputStream(file);
            properties.load(fis);
            fis.close();
        }
        properties.setProperty(key, value);
        properties.store(new FileOutputStream(new File(filePath)), comment);
    }

    /*public static String getValue(String key, String filePath) {
        String classpath = Thread.currentThread().getContextClassLoader().getResource("").getFile();
        try {
            Properties properties = readProperties(classpath + "/" + filePath);
            return properties.getProperty(key);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        String value = "";

        ClassPathResource cp = new ClassPathResource(filePath);
        Properties properties = new OrderedProperties();
        try {
            properties.load(cp.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(filePath);
        }
        for (Iterator its = properties.keySet().iterator(); its.hasNext();) {
            String zkey = (String) its.next();
            if (zkey.equals(key)) {
                value = properties.getProperty(zkey).trim();
                break;
            }
        }
        return value;
    }
    */
    public static String getValue(String key, String filaPath) {
        InputStream in = propertiesUtils.class.getClassLoader().getResourceAsStream(filaPath);
        Properties p = new Properties();
        try {
            p.load(in);
            return p.getProperty(key);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
