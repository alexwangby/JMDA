package com.jmda.common.constances;


import java.util.HashMap;
import java.util.Map;

public class SysConstances {
    /**
     * keycode.xml
     */
    public static Map<String, Object> KEY_CODE_RULE_MAP = new HashMap<>();
	/**
	 * keycode_cfg.properties
	 */
	public static Map<String,String> KEYCODE_CFG_MAP=new HashMap<>();

    public static int SESSION_TIME_OUT = 7 * 24 * 60 * 60;

    /**
     * 数据源
     */
    public static String DATASOURCE= "dataSource";

}
