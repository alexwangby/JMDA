package com.jmda.common.reader;

import org.springframework.core.io.ClassPathResource;

import com.jmda.common.constances.SysConstances;
import com.jmda.common.properties.OrderedProperties;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

public class PropertiesReader {
    /**
     * 缓存keycode_cfg.properties
     */
    public static void cacheKeyCodeCfgProperties() {
        Map<String, String> map = new HashMap<>();
        ClassPathResource cp = new ClassPathResource("keycode_cfg.properties");
        Properties properties = new OrderedProperties();
        try {
            properties.load(cp.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException("load keycode_cfg.properties error!");
        }
        for (Iterator its = properties.keySet().iterator(); its.hasNext(); ) {
            String zkey = (String) its.next();
            map.put(zkey, properties.getProperty(zkey).trim());
        }
        SysConstances.KEYCODE_CFG_MAP = map;
    }
}
