package com.jmda;

import java.util.Map;
/**
 * 异常类，可以通过Map对象进行传参判断
 * @author chens
 *
 */
public class FmsException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String,Object> map;

	public FmsException(String message, Throwable cause,Map<String,Object> map) {
		super(message, cause);
		this.map=map;
	}
	public FmsException(String message,Map<String,Object> map) {
		super(message);
		this.map=map;
	}
	public FmsException(Throwable cause,Map<String,Object> map) {
		super(cause);
		this.map=map;
	}

	public FmsException(Map<String,Object> map) {
		super();
		this.map=map;
	}
	public Map<String,Object> getMap() {
		return map;
	}

	public void setMap(Map<String,Object> map) {
		this.map = map;
	}

}
