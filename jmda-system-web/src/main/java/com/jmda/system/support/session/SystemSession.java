/**   
* TODO 
* @author zhanghui   
* @date 2016年4月14日 下午4:27:37  
*/
package com.jmda.system.support.session;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.jmda.common.model.po.SysPersonPO;
import com.jmda.system.constants.SystemFinal;


public class SystemSession {
	/**
	 * 获得当前登录用户
	 * @param request
	 * @return
	 */
	public static SysPersonPO currentUser(HttpServletRequest request){
		Map<String,Object> cookieMap=cookies2Map(request);

		String token= (String) cookieMap.get(SystemFinal.FMS_USERTOKEN);
		//System.out.println("1>>>>>>>>>>>"+token);
		if(null!=token){
			//UpmsUserVO upmsUserVO=(UpmsUserVO) RedisHelper.get(UpmsFinal.KEYHEAD_UPMS_SESSION+token,UpmsFinal.FIELD_UPMS_USERVO);
			return null;
		}else{
			return null;
		}
		
	}
	
	private static Map<String,Object> cookies2Map(HttpServletRequest request){
		Map<String,Object> cookieMap=new HashMap<String, Object>();
		Cookie[] cookies = request.getCookies();
		if(null!=cookies){
			 for(Cookie c :cookies ){
		        cookieMap.put(c.getName(), c.getValue());
		     }
		}
       
		return cookieMap;
	}
}
