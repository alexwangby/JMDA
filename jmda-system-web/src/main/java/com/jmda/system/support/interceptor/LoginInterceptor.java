package com.jmda.system.support.interceptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.jmda.system.constants.SystemFinal;

public class LoginInterceptor implements HandlerInterceptor{
	private static List<String> URL_PASS_LIST=new ArrayList<String>();;
	
	public LoginInterceptor(){
		init();
	}
	
	//不被拦截的URL
	private void init(){
		URL_PASS_LIST.add("/login");
		URL_PASS_LIST.add("/loginin");
		URL_PASS_LIST.add("/exit");
		URL_PASS_LIST.add("/loginerror");
		URL_PASS_LIST.add("/timestamp");
		URL_PASS_LIST.add("/heartbeat");
		URL_PASS_LIST.add("/system-loginerror");
		URL_PASS_LIST.add("/static/**");
		
		
		Properties prop = new Properties();
		ClassPathResource cp = new ClassPathResource("url_pass.properties");
		try {
			prop.load(cp.getInputStream());
			Set keyset = prop.keySet();
			for (Object key : keyset) {
				URL_PASS_LIST.add(key.toString());
			}
		} catch (IOException e) {
			System.err.println("can not load url_pass.properties");
		}   
		
	}
	
	
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler) throws Exception {
		String contextPath=request.getContextPath();
		String url=request.getRequestURI().toString();
		if(isPassUrl(url)||StringUtils.isEmpty(url)){
			return true;
		}
		Map<String,Object> cookieMap=this.cookies2Map(request);
		String token= (String) cookieMap.get(SystemFinal.FMS_USERTOKEN);
		
		if(null!=token){
			return true;
		}else{
			response.sendRedirect(contextPath+"/system-loginerror?error=timeout");//超时
			return false;
		}
		
		
	}
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,Object handler, ModelAndView modelAndView) throws Exception {
		
		
	}
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
	}
	/**
	 * 是否URL直接通过
	 * @param url
	 * @return
	 */
	private boolean isPassUrl(String url){
		boolean isMtch=false;
		for (String furl : URL_PASS_LIST) {
			String[] furlArray=furl.split("/");
			String[] urlArray=url.split("/");
			int sum=0;
			List<Integer> matcher=new ArrayList<>();
			for (int i = 0; i < furlArray.length; i++) {
				String node=furlArray[i];
				if(!node.equals("**")&&StringUtils.isNotEmpty(node)){
					sum++;
					for (int j = 0; j < urlArray.length; j++) {
						String urlNode=urlArray[j];
						if(urlNode.equals(node)){
							matcher.add(i);
						}
					}
				}
			}
			if(matcher.size()>=sum){
				isMtch=true;
				for (int i = 0; i <matcher.size(); i++) {
					int indexI=matcher.get(i);
					for (int j = i+1; j < matcher.size(); j++) {
						int indexJ=matcher.get(j);
						if(indexJ<indexI){
							return false;
						}
					}
				}
			}
		}
		return isMtch;
	}
	
	

	private Map<String,Object> cookies2Map(HttpServletRequest request){
		Map<String,Object> cookieMap=new HashMap<String, Object>();
		Cookie[] cookies = request.getCookies();
		if(null!=cookies){
			 for(Cookie c :cookies ){
		        cookieMap.put(c.getName(), c.getValue());
		     }
		}
       
		return cookieMap;
	}


}
