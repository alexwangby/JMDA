
package com.jmda.system.constants;

import java.util.HashMap;
import java.util.Map;

public class SystemFinal {
	
	public static Map<String,String > URL_PASS_MAP=new HashMap<>();
	public static final int SESSION_TIME_OUT=3600*18;//18 hours
	
	public static final int ALIVE_TIME_OUT=60*15;//15分钟
	
	public static final String FMS_USERTOKEN="FMS_USERTOKEN";

}
