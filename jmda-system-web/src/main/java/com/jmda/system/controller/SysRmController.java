package com.jmda.system.controller;



import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jmda.FmsException;
import com.jmda.common.dao.table.mapper.SysRmGroupRmPOMapper;
import com.jmda.common.dao.table.mapper.SysRmPOMapper;
import com.jmda.common.model.po.SysRmPO;
import com.jmda.system.service.SysRmService;

@Controller
@RequestMapping("/system/SysRm")
public class SysRmController {
	@Autowired
	SysRmService service;
	@Autowired
	SysRmPOMapper sysRmPOMapper;
	@Autowired
	SysRmGroupRmPOMapper sysRmGroupRmPOMapper;
	@RequestMapping(value = "/save", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String save(SysRmPO record,HttpServletRequest request) {
		service.saveData(record);
		return "ok";
	}

	@RequestMapping(value = "/deleteDatas", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String deleteDatas(String ids) {
		try {
			service.deleteDatas(ids);
		} catch (FmsException e) {
			// TODO Auto-generated catch block
			return e.getMessage();
		}
		return "ok";
	}
	@RequestMapping("/moveUp")
	@ResponseBody
	public String moveUp(String rmId,HttpServletRequest request){
		service.moveUp(rmId);
		return "ok";
	}
	@RequestMapping("/moveDown")
	@ResponseBody
	public String moveDown(String rmId,HttpServletRequest request){
		service.moveDown(rmId);
		return "ok";
	}
}
