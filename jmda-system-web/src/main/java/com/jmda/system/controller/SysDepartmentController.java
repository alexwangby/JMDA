package com.jmda.system.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jmda.FmsException;
import com.jmda.common.dao.table.mapper.SysCompanyPOMapper;
import com.jmda.common.dao.table.mapper.SysDepartmentPOMapper;
import com.jmda.common.model.po.SysCompanyPO;
import com.jmda.common.model.po.SysCompanyPOExample;
import com.jmda.common.model.po.SysDepartmentPO;
import com.jmda.common.model.po.SysDepartmentPOExample;
import com.jmda.system.service.SysDepartmentService;

@Controller
@RequestMapping("/system/SysDepartment")
public class SysDepartmentController {

	@Autowired
	SysDepartmentService service;
	@Autowired
	SysCompanyPOMapper sysCompanyPOMapper;
	@Autowired
	SysDepartmentPOMapper sysDepartmentPOMapper;

	@RequestMapping(value = "/save", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String save(SysDepartmentPO record,HttpServletRequest request) {
			//TODO 如果有子表数据时可通过如下例子进行json数据转换为 po 对象
			
		service.saveData(record);
		return "ok";
	}

	@RequestMapping(value = "/deleteDatas", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String deleteDatas(String ids) {
		try {
			service.deleteDatas(ids);
		} catch (FmsException e) {
			// TODO Auto-generated catch block
			String rs = e.getMessage();
			return rs;
		}
		return "ok";
	}
	
	@RequestMapping("/moveUp")
	@ResponseBody
	public String moveUp(String id,HttpServletRequest request){
		service.moveUp(id);
		return "ok";
	}
	@RequestMapping("/moveDown")
	@ResponseBody
	public String moveDown(String id,HttpServletRequest request){
		service.moveDown(id);
		return "ok";
	}
	
	@RequestMapping(value = "/getTree", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getTree() {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
        rootNode.put("id", "root");
        rootNode.put("text", "组织机构");
        rootNode.put("iconCls", "");
        rootNode.put("type", "root");
		SysCompanyPOExample sysCompanyPOExample = new SysCompanyPOExample();
		sysCompanyPOExample.createCriteria().andIsDeleteEqualTo(false);
		List<SysCompanyPO> companyList = sysCompanyPOMapper.selectByExample(sysCompanyPOExample);
		if (companyList != null && companyList.size() > 0) {
			ArrayNode rootArrayNode = mapper.createArrayNode();
			for (SysCompanyPO sysCompanyPO : companyList) {
				ObjectNode cmpNode = mapper.createObjectNode();
				cmpNode.put("id", sysCompanyPO.getCompanyId());
				cmpNode.put("iconCls", "");
				cmpNode.put("text", sysCompanyPO.getName());
				cmpNode.put("state", "open");
				cmpNode.put("type", "company");
				ArrayNode cmpArrayNode = mapper.createArrayNode();
				SysDepartmentPOExample sysDepartmentPOExample = new SysDepartmentPOExample();
				sysDepartmentPOExample.createCriteria().andCompanyIdEqualTo(sysCompanyPO.getCompanyId()).andIsDeleteEqualTo(false);
				sysDepartmentPOExample.setOrderByClause(" CREATE_TIME ASC ");
				List<SysDepartmentPO> departmentModels = sysDepartmentPOMapper.selectByExample(sysDepartmentPOExample);
		        try {
					buildDepTree(sysCompanyPO,departmentModels, cmpNode, cmpArrayNode, null, mapper);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        rootArrayNode.add(cmpNode);
			}
			rootNode.put("children", rootArrayNode);
	        ArrayNode baseArrayNode = mapper.createArrayNode();
	        baseArrayNode.add(rootNode);
	        String json = "";
	        if (null != baseArrayNode.get(0).get("children")) {
	            try {
					json = mapper.writeValueAsString(baseArrayNode);
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	        return json;
		}
		return null;
	}
	
	private void buildDepTree(SysCompanyPO sysCompanyPO,List<SysDepartmentPO> adDepartmentPOList, ObjectNode pNode, ArrayNode pChildrenNode,
			String pid, ObjectMapper mapper) throws Exception {
		List<SysDepartmentPO> list = this.getChildrenList(adDepartmentPOList, pid);
		if (list.size() > 0) {
			ArrayNode childrenNode = mapper.createArrayNode();
			for (SysDepartmentPO childrenPO : list) {
				ObjectNode node = mapper.createObjectNode();
				node.put("id",  childrenPO.getDepartmentId());
				node.put("text", childrenPO.getName());
				node.put("iconCls", "");
				node.put("state", "open");
				node.put("type", "dept");
				node.put("companyId", sysCompanyPO.getCompanyId());
				node.put("companyName", sysCompanyPO.getName());
				this.buildDepTree(sysCompanyPO,adDepartmentPOList, node, childrenNode, childrenPO.getDepartmentId(), mapper);
				pNode.put("children", childrenNode);
			}
		}
		pChildrenNode.add(pNode);
	}

	private List<SysDepartmentPO> getChildrenList(List<SysDepartmentPO> adDepartmentPOList, String pid) {
		List<SysDepartmentPO> ls = new ArrayList<>();
		for (SysDepartmentPO adDepartmentPO : adDepartmentPOList) {
			if (((null == adDepartmentPO.getDepartmentPid()||"".equals(adDepartmentPO.getDepartmentPid())) && null == pid)
					|| (null != adDepartmentPO.getDepartmentPid() && adDepartmentPO.getDepartmentPid().equals(pid))) {
				ls.add(adDepartmentPO);
			}
		}
		return ls;

	}
}
