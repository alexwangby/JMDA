package com.jmda.system.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jmda.common.dao.table.mapper.SysRmGroupRmPOMapper;
import com.jmda.common.dao.table.mapper.SysRmPOMapper;
import com.jmda.common.model.po.SysRmGroupPO;
import com.jmda.common.model.po.SysRmGroupRmPOExample;
import com.jmda.common.model.po.SysRmGroupRmPOKey;
import com.jmda.common.model.po.SysRmPO;
import com.jmda.common.model.po.SysRmPOExample;
import com.jmda.system.service.SysRmGroupService;

@Controller
@RequestMapping("/system/SysRmGroup")
public class SysRmGroupController {

	@Autowired
	SysRmGroupService service;
	@Autowired
	SysRmPOMapper sysRmPOMapper;
	@Autowired
	SysRmGroupRmPOMapper sysRmGroupRmPOMapper;
	@RequestMapping(value = "/save", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String save(SysRmGroupPO record,HttpServletRequest request) {
			//TODO 如果有子表数据时可通过如下例子进行json数据转换为 po 对象
		service.saveData(record);
		return "ok";
	}

	@RequestMapping(value = "/deleteDatas", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String deleteDatas(String ids) {
		service.deleteDatas(ids);
		return "ok";
	}
	@RequestMapping(value = "/saveGroupRm", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String saveGroupRm(String ids,String rmGroupId,HttpServletRequest request) {
			//TODO 如果有子表数据时可通过如下例子进行json数据转换为 po 对象
		service.saveGroupRm(ids, rmGroupId);
		return "ok";
	}
	@RequestMapping(value = "/getTree", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getTree(String id) {
		id = id==null?"":id;
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		SysRmPOExample sysRmPOExample = new SysRmPOExample();
		sysRmPOExample.createCriteria().andIsDeleteEqualTo(false);
		sysRmPOExample.setOrderByClause(" CREATE_TIME ASC ");
		List<SysRmPO> sysRmPOList = null;
		if(!id.equals("")){
			sysRmPOList = sysRmPOMapper.selectByExample(sysRmPOExample);
		}
		SysRmGroupRmPOExample sysRmGroupRmPOExample = new SysRmGroupRmPOExample();
		sysRmGroupRmPOExample.createCriteria().andRmGroupIdEqualTo(id);
		List<SysRmGroupRmPOKey> list = null;
		if(!id.equals("")){
			list = sysRmGroupRmPOMapper.selectByExample(sysRmGroupRmPOExample);
		}
        try {
			buildTree(sysRmPOList, null, arrayNode, null, mapper,list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String json="";
	    try {
			json=mapper.writeValueAsString(arrayNode);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	    return json;
	}
	private void buildTree(List<SysRmPO> sysRmPOList, ObjectNode pNode, ArrayNode pChildrenNode,
			String pid, ObjectMapper mapper,List<SysRmGroupRmPOKey> sysRmGroupRmList) throws Exception {
		List<SysRmPO> list = this.getChildrenList(sysRmPOList, pid);
		if (list.size() > 0) {
			ArrayNode childrenNode = mapper.createArrayNode();
			for (SysRmPO childrenPO : list) {
				ObjectNode node = mapper.createObjectNode();
				node.put("id",  childrenPO.getRmId());
				node.put("text", childrenPO.getName());
				node.put("state", "open");
				node.put("programCode", childrenPO.getProgramCode());
				node.put("rmPid", childrenPO.getRmPid()==null?"":childrenPO.getRmPid());
				boolean isChecked=false; 
				if(sysRmGroupRmList!=null&&sysRmGroupRmList.size()>0){
					for (SysRmGroupRmPOKey sysRmGroupRmPOKey : sysRmGroupRmList) {
						String rmId = sysRmGroupRmPOKey.getRmId();
						if(rmId.equals(childrenPO.getRmId())){
							isChecked = true;
							break;
						}
					}
				}
				node.put("checked", isChecked);
				this.buildTree(sysRmPOList, node, childrenNode, childrenPO.getRmId(), mapper,sysRmGroupRmList);
				if(pNode==null){
					pChildrenNode.add(node);
				}else{
					pNode.put("children", childrenNode);
				}
			}
		}
		if(pNode!=null){
			pChildrenNode.add(pNode);
		}
	}

	private List<SysRmPO> getChildrenList(List<SysRmPO> sysRmPOList, String pid) {
		List<SysRmPO> ls = new ArrayList<>();
		if(sysRmPOList!=null){
			for (SysRmPO sysDepartmentPO : sysRmPOList) {
				if (((null == sysDepartmentPO.getRmPid()||"".equals(sysDepartmentPO.getRmPid())) && null == pid)
						|| (null != sysDepartmentPO.getRmPid() && sysDepartmentPO.getRmPid().equals(pid))) {
					ls.add(sysDepartmentPO);
				}
			}
		}

		return ls;

	}
}
