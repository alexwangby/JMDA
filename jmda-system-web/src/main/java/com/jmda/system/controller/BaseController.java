package com.jmda.system.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Maps;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;

public class BaseController {
    protected String SUCCESS = "success";

    /**
     * 获得当前登录用户
     *
     * @param request
     * @return
     */
    // public UserBean currentUser(HttpServletRequest request){
    // Map<String,Object> cookieMap=this.cookies2Map(request);
    // String token= (String) cookieMap.get(SysFinal.USERTOKEN);
    // if(null!=token){
    // UserBean userBean=(UserBean)
    // RedisUtil.get(SysFinal.REDIS_LOGIN_HEAD+token);
    // return userBean;
    // }else{
    // return null;
    // }
    //
    // }
    // public String token(HttpServletRequest request){
    // Map<String,Object> cookieMap=this.cookies2Map(request);
    // String token= (String) cookieMap.get(SysFinal.USERTOKEN);
    // return token;
    // }
    //
    protected Map<String, Object> cookies2Map(HttpServletRequest request) {
        Map<String, Object> cookieMap = Maps.newHashMap();
        Cookie[] cookies = request.getCookies();
        if (null != cookies) {
            for (Cookie c : cookies) {
                cookieMap.put(c.getName(), c.getValue());
            }
        }

        return cookieMap;
    }

    protected Map<String, Object> paramsToMap(HttpServletRequest request) {
        Map<String, Object> param = Maps.newHashMap();
        Enumeration e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String name = (String) e.nextElement();
            if (null != request.getParameter(name) && !"".equals(request.getParameter(name).trim())) {
                try {
                    param.put(StringUtils.trim(name),
                            java.net.URLDecoder.decode(request.getParameter(name).toString(), "UTF-8").trim());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        return param;
    }

    /**
     * 处理参数列表为Map<String, String>
     *
     * @param request
     * @return
     */
    protected Map<String, String> params2Map(HttpServletRequest request) {
        Map<String, String> param = Maps.newHashMap();
        Enumeration e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String name = (String) e.nextElement();
            if (null != request.getParameter(name) && !"".equals(request.getParameter(name).trim())) {
                try {
                    param.put(StringUtils.trim(name),
                            java.net.URLDecoder.decode(request.getParameter(name).toString(), "UTF-8").trim());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        return param;
    }

    protected void paramsToModel(HttpServletRequest request, Model model, String[] paramsKey) {
        Map<String, Object> params = paramsToMap(request);
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            boolean flag = false;
            if (null != paramsKey) {
                for (String key : paramsKey) {
                    if (key.equals(entry.getKey())) {
                        flag = true;
                        break;
                    }
                }
            } else {
                flag = true;
            }

            if (flag) {
                model.addAttribute(entry.getKey(), entry.getValue());
            }
        }
    }



    /**
     * ajaxSuccess(异步操作完成)
     */
    protected String ajaxSuccess(Object param, String successMsg, String errorMsg) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();

        if (param instanceof Integer) {
            int value = ((Integer) param).intValue();
            if (value > 0) {
                node.put("success", true);
                node.put("count", value);
                node.put("msg", successMsg);
            } else {
                node.put("success", false);
                node.put("count", value);
                node.put("msg", errorMsg);
            }

        } else if (param instanceof String) {
            String s = (String) param;
        } else if (param instanceof Double) {
            double d = ((Double) param).doubleValue();
        } else if (param instanceof Float) {
            float f = ((Float) param).floatValue();
        } else if (param instanceof Long) {
            long l = ((Long) param).longValue();
        } else if (param instanceof Boolean) {
            boolean b = ((Boolean) param).booleanValue();
            if (b) {
                node.put("success", true);
                node.put("msg", successMsg);
            } else {
                node.put("success", false);
                node.put("msg", errorMsg);
            }
        } else if (param instanceof Date) {
            Date d = (Date) param;
        }

        String json = "";
        try {
            json = mapper.writeValueAsString(node);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * ajaxSuccess(异步操作完成)
     */
    protected String ajaxSuccess(Object param) {
        return this.ajaxSuccess(param, "操作成功！", "操作失败！");
    }
}
