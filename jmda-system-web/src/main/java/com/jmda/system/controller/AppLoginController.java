package com.jmda.system.controller;

import com.alibaba.fastjson.JSON;
import com.jmda.common.model.po.SysPersonPO;
import com.jmda.common.utils.other.Md5Util;
import com.jmda.system.service.IndexService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;


@Controller
public class AppLoginController extends BaseController{
	@Autowired
	IndexService indexService;


	@RequestMapping(value={"/appLogin"})
	@ResponseBody
	public String appLogin(String loginName, String password){
		Map<String,Object> result=new HashMap();
		if(null!=loginName&&null!=password){
			password = Md5Util.MD5(password);
			SysPersonPO sysPersonPO=indexService.loginIn(loginName, password);
			if(null!=sysPersonPO){
				result.put("isSuccess",true);
				result.put("cookie",sysPersonPO.getPersonId());
				result.put("name",sysPersonPO.getName());
				return JSON.toJSONString(result);
			}
			result.put("isSuccess",false);
			result.put("name","");
			result.put("cookie","");
			return JSON.toJSONString(result);
		}else{
			result.put("isSuccess",false);
			result.put("cookie","");
			result.put("name","");
			return JSON.toJSONString(result);
		}
		
	}
}
