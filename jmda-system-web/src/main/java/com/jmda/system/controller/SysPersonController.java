package com.jmda.system.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jmda.common.context.FmsContext;
import com.jmda.common.dao.table.mapper.SysRmGroupPersonPOMapper;
import com.jmda.common.dao.table.mapper.SysRmPOMapper;
import com.jmda.common.model.po.SysPersonPO;
import com.jmda.common.utils.other.Md5Util;
import com.jmda.system.service.SysPersonService;

@Controller
@RequestMapping("/system/SysPerson")
public class SysPersonController {

	@Autowired
	SysPersonService service;
	@Autowired
	SysRmPOMapper sysRmPOMapper;
	@Autowired
	SysRmGroupPersonPOMapper sysRmGroupPersonPOMapper;
	@RequestMapping(value = "/save", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String save(SysPersonPO record,HttpServletRequest request) {
			//TODO 如果有子表数据时可通过如下例子进行json数据转换为 po 对象
		String sys_rm_group_person = request.getParameter("sys_rm_group_person");
		service.saveData(record,sys_rm_group_person);
		return "ok";
	}
	@RequestMapping(value = "/getInfoPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getInfoPage(HttpServletRequest request, Model model) {
		//TODO 如果有子表数据时可通过如下例子进行json数据转换为 po 对象
		String loginName = FmsContext.getPersonContext().getPersonBean().getLoginName();
		String name = FmsContext.getPersonContext().getPersonBean().getPersonName();
		String mobile = FmsContext.getPersonContext().getPersonBean().getMobile();
		String personId = FmsContext.getPersonContext().getPersonBean().getPersonId();
		model.addAttribute("personId",personId);
		model.addAttribute("loginName",loginName);
		model.addAttribute("name",name);
		model.addAttribute("mobile",mobile);
		model.addAttribute("basePath",request.getContextPath());
		return "/system/sys_person_tab";
	}

	@RequestMapping(value = "/saveBase", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String saveBase(SysPersonPO record ,HttpServletRequest request, Model model) {
		record.setPersonId(FmsContext.getPersonContext().getPersonId());
		service.saveBase(record);
		return "ok";
	}
	@RequestMapping(value = "/updatePassword", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String updatePassword(String oldPassword,String newPassword,String confirmPassword,HttpServletRequest request, Model model) {
		String md5Password = Md5Util.MD5(oldPassword);
		String password = FmsContext.getPersonContext().getPersonBean().getLoginPassword();
		SysPersonPO nowSysPersonPO = service.getSysPersonPO(FmsContext.getPersonContext().getPersonId());
		if(!md5Password.equals(nowSysPersonPO.getLoginPassword())){
			//密码不对
			return "1";
		}
		if(!newPassword.equals(confirmPassword)){
			//新密码与确认密码不符
			return "2";
		}
		SysPersonPO record = new SysPersonPO();
		record.setPersonId(FmsContext.getPersonContext().getPersonId());
		record.setLoginPassword(newPassword);
		service.updatePassword(FmsContext.getPersonContext().getPersonId(),newPassword);
		return "ok";
	}
	@RequestMapping(value = "/deleteDatas", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String deleteDatas(String ids) {
		service.deleteDatas(ids);
		return "ok";
	}
	
	@RequestMapping(value = "/getTree", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getTree(String personId) {
		return service.getTree(personId);
	}

}
