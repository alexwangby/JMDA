package com.jmda.system.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jmda.FmsException;
import com.jmda.common.model.po.SysCompanyPO;
import com.jmda.system.service.SysCompanyService;

@Controller
@RequestMapping("/system/SysCompany")
public class SysCompanyController {

	@Resource
	SysCompanyService service;
	
	@RequestMapping(value = "/save", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String save(SysCompanyPO record,HttpServletRequest request) {
			//TODO 如果有子表数据时可通过如下例子进行json数据转换为 po 对象
		service.saveData(record);
		return "ok";
	}

	@RequestMapping(value = "/deleteDatas", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String deleteDatas(String ids) {
		try {
			service.deleteDatas(ids);
		} catch (FmsException e) {
			// TODO Auto-generated catch block
			return e.getMessage();
		}
		return "ok";
	}
}
