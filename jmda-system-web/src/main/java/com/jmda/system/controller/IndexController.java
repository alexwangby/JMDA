package com.jmda.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.common.context.FmsContext;
import com.jmda.common.dao.table.mapper.SysPersonPOMapper;
import com.jmda.common.dao.table.mapper.SysRmPOMapper;
import com.jmda.common.model.po.SysPersonPO;
import com.jmda.common.model.po.SysRmPO;
import com.jmda.common.model.po.SysRmPOExample;
import com.jmda.common.utils.other.IpUtils;
import com.jmda.common.utils.other.Md5Util;
import com.jmda.system.constants.SystemFinal;
import com.jmda.system.service.IndexService;
import com.jmda.system.support.session.SystemSession;

import eu.bitwalker.useragentutils.UserAgent;

@Controller
public class IndexController extends BaseController{
	@Autowired
	IndexService indexService;
	@Autowired
	SysPersonPOMapper sysPersonPOMapper;
	@RequestMapping(value={"/","/login"})
	public String login(HttpServletRequest request,Model model){
		model.addAttribute("staticPath", request.getContextPath());
		super.paramsToModel(request,model,new String[]{"error"});
		return "/framework/login";
	}
	@RequestMapping(value={"/logout"})
	public String logout(HttpServletRequest request,HttpServletResponse response,Model model){
		Cookie cookie = new Cookie(SystemFinal.FMS_USERTOKEN, null);
		cookie.setMaxAge(0);
		cookie.setPath("/");
		response.addCookie(cookie);
		HttpSession session=request.getSession();
		session.invalidate();
		return "/framework/login";
	}
	@RequestMapping(value={"/loginin"})
	@ResponseBody
	public String loginin(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> params=super.paramsToMap(request);
		if(null!=params.get("loginName")&&null!=params.get("loginPassword")){
			String loginName=params.get("loginName").toString();
			String loginPassword=params.get("loginPassword").toString();
			loginPassword = Md5Util.MD5(loginPassword);
			SysPersonPO sysPersonPO=indexService.loginIn(loginName, loginPassword);
			if(null!=sysPersonPO){
				//String token=Md5Util.MD5(sysPersonPO.getPersonId());
				Cookie cookie = new Cookie(SystemFinal.FMS_USERTOKEN,sysPersonPO.getPersonId());
				cookie.setMaxAge(SystemFinal.SESSION_TIME_OUT);
				cookie.setPath("/");
				response.addCookie(cookie);
				UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
				indexService.saveUserSession(sysPersonPO, userAgent, sysPersonPO.getPersonId(), IpUtils.getIpAddr(request));
				return "success";
			}
			
			return "error";
		}else{
			return "error";
		}
		
	}
	@Autowired
	SysRmPOMapper sysRmPOMapper;
	@RequestMapping(value={"/index"})
	public String index(HttpServletRequest request,Model model){
		/**
		 * 导航菜单
		 */
		//查找根目录
		SysRmPOExample sysRmPOExample = new SysRmPOExample();
		sysRmPOExample.createCriteria().andRmPidEqualTo("");
		sysRmPOExample.setOrderByClause("PRIORITY ASC");
		List<SysRmPO> list = sysRmPOMapper.selectByExample(sysRmPOExample);
		List<Map<String, Object>> oneList = new ArrayList<Map<String, Object>>();
		if(list!=null&&list.size()>0){
			String personId = "";
			Map<String,Object> cookieMap=super.cookies2Map(request);
			if(null!=cookieMap.get(SystemFinal.FMS_USERTOKEN)){
				personId=cookieMap.get(SystemFinal.FMS_USERTOKEN).toString();
			}
			
			for (SysRmPO sysRmPO : list) {
				//检查是否拥有权限
				//Boolean bool = true;
				Boolean bool = indexService.checkIsAccess(personId,sysRmPO.getRmId());
				if(bool){
					Map<String, Object> oneMap = new HashMap<String, Object>();
					oneMap.put("title", sysRmPO.getName());
					oneMap.put("id", sysRmPO.getRmId());
					
					SysRmPOExample sysRmPOExample_ = new SysRmPOExample();
					sysRmPOExample_.createCriteria().andRmPidEqualTo(sysRmPO.getRmId());
					sysRmPOExample_.setOrderByClause("PRIORITY ASC");
					List<SysRmPO> list_ = sysRmPOMapper.selectByExample(sysRmPOExample_);
					List<Map<String, Object>> twoList = new ArrayList<Map<String, Object>>();
					if(list_!=null&&list_.size()>0){
						for (SysRmPO sysRmPO2 : list_) {
							Boolean bool_ = indexService.checkIsAccess(personId,sysRmPO2.getRmId());
							if(bool_){
								Map<String, Object> twoMap = new HashMap<String, Object>();
								twoMap.put("id", sysRmPO2.getRmId());
								twoMap.put("name",sysRmPO2.getName());
								twoMap.put("func_id", sysRmPO2.getRmId());
								twoMap.put("url", sysRmPO2.getProgramCode());
								twoMap.put("count", 0);
								twoList.add(twoMap);
							}
						}
						
					}
					oneMap.put("items", twoList);
					oneList.add(oneMap);
				}
			}
		}
		String modules =  "[]";
		ObjectMapper mapper = new ObjectMapper();
		try {
			modules =  mapper.writeValueAsString(oneList);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("modules", modules);
		model.addAttribute("staticPath", request.getContextPath());
		return "/framework/index";
	}
	@RequestMapping(value={"/skins"})
	public String skins(HttpServletRequest request,Model model){
		model.addAttribute("staticPath", request.getContextPath());
		String personId = null;
		Map<String,Object> cookieMap=super.cookies2Map(request);
		if(null!=cookieMap.get(SystemFinal.FMS_USERTOKEN)){
			personId=cookieMap.get(SystemFinal.FMS_USERTOKEN).toString();
		}
		SysPersonPO sysPersonPO= SystemSession.currentUser(request);
		if(null!=personId){
			//SysPersonPO sysPersonPO = sysPersonPOMapper.selectByPrimaryKey(personId);
			model.addAttribute("staticPath", request.getContextPath());
			model.addAttribute("name", FmsContext.getPersonContext().getPersonBean().getPersonName());
			return "/framework/skins";
		}else{
			model.addAttribute("staticPath", request.getContextPath());
			//return "/framework/skins";
			return "redirect:/login?error=timeout";
		}
	}
/*	public static void main(String[] args) {

		System.out.println(Md5Util.MD5("123456"));
	}*/
}
