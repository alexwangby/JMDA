package com.jmda.system.service.imp;

import java.util.List;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.FmsException;
import com.jmda.common.dao.table.mapper.SysRmGroupRmPOMapper;
import com.jmda.common.dao.table.mapper.SysRmPOMapper;
import com.jmda.common.keycode.KeyCodeGenerator;
import com.jmda.common.model.po.SysRmGroupRmPOExample;
import com.jmda.common.model.po.SysRmPO;
import com.jmda.common.model.po.SysRmPOExample;
import com.jmda.system.service.SysRmService;

@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
@Service
public class SysRmServiceImp extends SqlSessionDaoSupport implements SysRmService {
	// 由系统工具生成的数据库表的DAO对象文件 可参照metadata工程相关代码
	@Autowired
	SysRmPOMapper mapper;
	@Autowired
	SysRmGroupRmPOMapper sysRmGroupRmPOMapper;
	// TODO 表中写入一条数据
	@Override
	public void insert(SysRmPO record) {
		mapper.insertSelective(record);
	}

	// TODO 通过主键ID批量删除mm_goods 表中数据
	@Override
	public void deleteDatas(String ids) throws FmsException {
		String[] idArray = ids.split(",");
		for (int i = 0; i < idArray.length; i++) {
			String id = idArray[i];
			SysRmPOExample sysRmPOExample = new SysRmPOExample();
			sysRmPOExample.createCriteria().andIsDeleteEqualTo(false).andRmPidEqualTo(id);
			List<SysRmPO> list = mapper.selectByExample(sysRmPOExample);
			if(list!=null&&list.size()>0){
				throw new FmsException("1", null);
			}
			SysRmGroupRmPOExample sysRmGroupRmPOExample = new SysRmGroupRmPOExample();
			sysRmGroupRmPOExample.createCriteria().andRmIdEqualTo(id);
			sysRmGroupRmPOMapper.deleteByExample(sysRmGroupRmPOExample);
			SysRmPO sysRmPO = new SysRmPO();
			sysRmPO.setRmId(id);
			sysRmPO.setIsDelete(true);
			mapper.updateByPrimaryKeySelective(sysRmPO);
		}
	}

	// TODO 通过主键修改表中数据
	@Override
	public void update(SysRmPO record) {
		mapper.updateByPrimaryKeySelective(record);
	}

	// TODO 通过主键读取表中数据
	@Override
	public SysRmPO getSysRmPO(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public void saveData(SysRmPO record) {
		// TODO Auto-generated method stub
		if(record.getRmId()==null||record.getRmId().equals("")){
			int priority=0;
			SysRmPOExample example = new SysRmPOExample();
			example.createCriteria().andIsDeleteEqualTo(false).andRmPidEqualTo(record.getRmPid());
			example.setOrderByClause("priority DESC");
			List<SysRmPO> list = mapper.selectByExample(example);
			if(list!=null&&list.size()>0){
				priority = list.get(0).getPriority()+1;
			}
			record.setPriority(priority);
			record.setCreateTime(System.currentTimeMillis());
			record.setIsDelete(false);
			record.setRmId(KeyCodeGenerator.random32UUID());
			insert(record);
		}else{
			record.setUpdateTime(System.currentTimeMillis());
			update(record);
		}
	}

	@Override
	public void moveUp(String rmId) {
		// TODO Auto-generated method stub
		SysRmPO sysRmPO = mapper.selectByPrimaryKey(rmId);
		if(sysRmPO!=null){
			SysRmPOExample example = new SysRmPOExample();
			example.createCriteria().andIsDeleteEqualTo(false).andRmPidEqualTo(sysRmPO.getRmPid());
			example.setOrderByClause("priority ASC");
			List<SysRmPO> list = mapper.selectByExample(example);
			if(list!=null&&list.size()>1){
				int index=0;
				for (int i = 0; i < list.size(); i++) {
					if(list.get(i).getRmId().equals(rmId)){
						if(i==0){
							index=0;
						}else{
							index=i-1;
						}
						break;
					}
				}
				SysRmPO rm = list.get(index);
				int priority = rm.getPriority();
				rm.setPriority(sysRmPO.getPriority());
				sysRmPO.setPriority(priority);
				mapper.updateByPrimaryKeySelective(sysRmPO);
				mapper.updateByPrimaryKeySelective(rm);
			}
		}

	}

	@Override
	public void moveDown(String rmId) {
		// TODO Auto-generated method stub
		SysRmPO sysRmPO = mapper.selectByPrimaryKey(rmId);
		if(sysRmPO!=null){
			SysRmPOExample example = new SysRmPOExample();
			example.createCriteria().andIsDeleteEqualTo(false).andRmPidEqualTo(sysRmPO.getRmPid());
			example.setOrderByClause("priority ASC");
			List<SysRmPO> list = mapper.selectByExample(example);
			if(list!=null&&list.size()>1){
				int index=0;
				for (int i = 0; i < list.size(); i++) {
					if(list.get(i).getRmId().equals(rmId)){
						if(i==(list.size()-1)){
							index=list.size()-1;
						}else{
							index=i+1;
						}
						break;
					}
				}
				SysRmPO rm = list.get(index);
				int priority = rm.getPriority();
				rm.setPriority(sysRmPO.getPriority());
				sysRmPO.setPriority(priority);
				mapper.updateByPrimaryKeySelective(sysRmPO);
				mapper.updateByPrimaryKeySelective(rm);
			}
		}
	}
}
