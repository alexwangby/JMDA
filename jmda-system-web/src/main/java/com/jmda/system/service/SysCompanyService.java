package com.jmda.system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.FmsException;
import com.jmda.common.model.po.SysCompanyPO;


public interface SysCompanyService {

	// TODO 写入一条数据
	public void insert(SysCompanyPO record);

	// TODO 通过主键ID批量删除表中数据
	public void deleteDatas(String ids) throws FmsException;

	// TODO 通过主键修改 表中数据
	public void update(SysCompanyPO record) ;

	// TODO 通过主键读取表中数据
	public SysCompanyPO getSysCompanyPO(String id);
	
	// TODO 写入一条数据
	public void saveData(SysCompanyPO record);
	
}
