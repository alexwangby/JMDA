package com.jmda.system.service;


import com.jmda.FmsException;
import com.jmda.common.model.po.SysRmPO;

public interface SysRmService {

	// TODO 写入一条数据
	public void insert(SysRmPO record);

	// TODO 通过主键ID批量删除表中数据
	public void deleteDatas(String ids)throws FmsException ;

	// TODO 通过主键修改 表中数据
	public void update(SysRmPO record) ;

	// TODO 通过主键读取表中数据
	public SysRmPO getSysRmPO(String id);
	
	public void saveData(SysRmPO record);
	public void moveUp(String rmId);
	public void moveDown(String rmId);
	
}
