package com.jmda.system.service.imp;

import java.util.List;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.FmsException;
import com.jmda.common.dao.table.mapper.SysDepartmentPOMapper;
import com.jmda.common.dao.table.mapper.SysPersonPOMapper;
import com.jmda.common.keycode.KeyCodeGenerator;
import com.jmda.common.model.po.SysDepartmentPO;
import com.jmda.common.model.po.SysDepartmentPOExample;
import com.jmda.common.model.po.SysPersonPO;
import com.jmda.common.model.po.SysPersonPOExample;
import com.jmda.system.service.SysDepartmentService;

@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
@Service
public class SysDepartmentServiceImp extends SqlSessionDaoSupport implements SysDepartmentService {
	// 由系统工具生成的数据库表的DAO对象文件 可参照metadata工程相关代码
	@Autowired
	SysDepartmentPOMapper mapper;
	@Autowired
	SysPersonPOMapper sysPersonPOMapper;
	// TODO 表中写入一条数据
	public void insert(SysDepartmentPO record) {
		mapper.insertSelective(record);
	}

	// TODO 通过主键ID批量删除mm_goods 表中数据
	public void deleteDatas(String ids) throws FmsException {
		String[] idArray = ids.split(",");
		for (int i = 0; i < idArray.length; i++) {
			String id = idArray[i];
			SysDepartmentPOExample example = new SysDepartmentPOExample();
			example.createCriteria().andDepartmentPidEqualTo(id).andIsDeleteEqualTo(false);
			List<SysDepartmentPO> list = mapper.selectByExample(example);
			if(list!=null&&list.size()>0){
				throw new FmsException("1", null);
			}
			SysPersonPOExample sysPersonPOExample = new SysPersonPOExample();
			sysPersonPOExample.createCriteria().andIsDeleteEqualTo(false).andDepartmentIdEqualTo(id);
			List<SysPersonPO> list_ = sysPersonPOMapper.selectByExample(sysPersonPOExample);
			if(list_!=null&&list_.size()>0){
				throw new FmsException("2", null);
			}
			SysDepartmentPO sysDepartmentPO = new SysDepartmentPO();
			sysDepartmentPO.setDepartmentId(id);
			sysDepartmentPO.setIsDelete(true);
			mapper.updateByPrimaryKeySelective(sysDepartmentPO);
		}
	}

	// TODO 通过主键修改表中数据
	public void update(SysDepartmentPO record) {
		mapper.updateByPrimaryKeySelective(record);
	}

	// TODO 通过主键读取表中数据
	public SysDepartmentPO getSysDepartmentPO(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public void saveData(SysDepartmentPO record) {
		// TODO Auto-generated method stub
		if(record.getDepartmentId()==null||record.getDepartmentId().equals("")){
			int priority=0;
			SysDepartmentPOExample example = new SysDepartmentPOExample();
			example.createCriteria().andIsDeleteEqualTo(false).andDepartmentPidEqualTo(record.getDepartmentPid());
			example.setOrderByClause("priority DESC");
			List<SysDepartmentPO> list = mapper.selectByExample(example);
			if(list!=null&&list.size()>0){
				priority = list.get(0).getPriority()+1;
			}
			record.setPriority(priority);
			record.setCreateTime(System.currentTimeMillis());
			record.setIsDelete(false);
			record.setDepartmentId(KeyCodeGenerator.random32UUID());
			insert(record);
		}else{
			record.setUpdateTime(System.currentTimeMillis());
			update(record);
		}
	}

	@Override
	public void moveUp(String id) {
		// TODO Auto-generated method stub
		SysDepartmentPO sysDepartmentPO = mapper.selectByPrimaryKey(id);
		if(sysDepartmentPO!=null){
			SysDepartmentPOExample example = new SysDepartmentPOExample();
			example.createCriteria().andIsDeleteEqualTo(false).andDepartmentPidEqualTo(sysDepartmentPO.getDepartmentPid());
			example.setOrderByClause("priority ASC");
			List<SysDepartmentPO> list = mapper.selectByExample(example);
			if(list!=null&&list.size()>1){
				int index=0;
				for (int i = 0; i < list.size(); i++) {
					if(list.get(i).getDepartmentId().equals(id)){
						if(i==0){
							index=0;
						}else{
							index=i-1;
						}
						break;
					}
				}
				SysDepartmentPO rm = list.get(index);
				int priority = rm.getPriority();
				rm.setPriority(sysDepartmentPO.getPriority());
				sysDepartmentPO.setPriority(priority);
				mapper.updateByPrimaryKeySelective(sysDepartmentPO);
				mapper.updateByPrimaryKeySelective(rm);
			}
		}
	}

	@Override
	public void moveDown(String id) {
		// TODO Auto-generated method stub
		SysDepartmentPO sysDepartmentPO = mapper.selectByPrimaryKey(id);
		if(sysDepartmentPO!=null){
			SysDepartmentPOExample example = new SysDepartmentPOExample();
			example.createCriteria().andIsDeleteEqualTo(false).andDepartmentPidEqualTo(sysDepartmentPO.getDepartmentPid());
			example.setOrderByClause("priority ASC");
			List<SysDepartmentPO> list = mapper.selectByExample(example);
			if(list!=null&&list.size()>1){
				int index=0;
				for (int i = 0; i < list.size(); i++) {
					if(list.get(i).getDepartmentId().equals(id)){
						if(i==(list.size()-1)){
							index=list.size()-1;
						}else{
							index=i+1;
						}
						break;
					}
				}
				SysDepartmentPO rm = list.get(index);
				int priority = rm.getPriority();
				rm.setPriority(sysDepartmentPO.getPriority());
				sysDepartmentPO.setPriority(priority);
				mapper.updateByPrimaryKeySelective(sysDepartmentPO);
				mapper.updateByPrimaryKeySelective(rm);
			}
		}
	}
}
