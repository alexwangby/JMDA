package com.jmda.system.service.imp;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.common.dao.table.mapper.SysRmGroupPOMapper;
import com.jmda.common.dao.table.mapper.SysRmGroupRmPOMapper;
import com.jmda.common.keycode.KeyCodeGenerator;
import com.jmda.common.model.po.SysRmGroupPO;
import com.jmda.common.model.po.SysRmGroupRmPOExample;
import com.jmda.common.model.po.SysRmGroupRmPOKey;
import com.jmda.system.service.SysRmGroupService;

@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
@Service
public class SysRmGroupServiceImp extends SqlSessionDaoSupport implements SysRmGroupService {
	// 由系统工具生成的数据库表的DAO对象文件 可参照metadata工程相关代码
	@Autowired
	SysRmGroupPOMapper mapper;
	@Autowired
	SysRmGroupRmPOMapper sysRmGroupRmPOMapper;
	// TODO 表中写入一条数据
	@Override
	public void insert(SysRmGroupPO record) {
		mapper.insertSelective(record);
	}

	// TODO 通过主键ID批量删除mm_goods 表中数据
	@Override
	public void deleteDatas(String ids) {
		String[] idArray = ids.split(",");
		for (int i = 0; i < idArray.length; i++) {
			String id = idArray[i];
			SysRmGroupPO record = new SysRmGroupPO();
			record.setRmGroupId(id);
			record.setIsDelete(true);
			update(record);
			//mapper.deleteByPrimaryKey(id);
		}
	}

	// TODO 通过主键修改表中数据
	@Override
	public void update(SysRmGroupPO record) {
		mapper.updateByPrimaryKeySelective(record);
	}

	// TODO 通过主键读取表中数据
	@Override
	public SysRmGroupPO getSysRmGroupPO(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public void saveData(SysRmGroupPO record) {
		// TODO Auto-generated method stub
		if(record.getRmGroupId()==null||record.getRmGroupId().equals("")){
			record.setCreateTime(System.currentTimeMillis());
			record.setIsDelete(false);
			record.setRmGroupId(KeyCodeGenerator.random32UUID());
			insert(record);
		}else{
			record.setUpdateTime(System.currentTimeMillis());
			update(record);
		}
	}

	@Override
	public void saveGroupRm(String ids, String rmGroupId) {
		// TODO Auto-generated method stub
		if(ids!=null&&!ids.equals("")){
			SysRmGroupRmPOExample example = new SysRmGroupRmPOExample();
			example.createCriteria().andRmGroupIdEqualTo(rmGroupId);
			sysRmGroupRmPOMapper.deleteByExample(example);
			String[] ids_ = ids.split(",");
			for (String id : ids_) {
				SysRmGroupRmPOKey record = new SysRmGroupRmPOKey();
				record.setRmGroupId(rmGroupId);
				record.setRmId(id);
				sysRmGroupRmPOMapper.insert(record);
			}
		}
	}
}
