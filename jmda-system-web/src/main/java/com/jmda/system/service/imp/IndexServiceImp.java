package com.jmda.system.service.imp;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jmda.common.dao.table.mapper.SysPersonPOMapper;
import com.jmda.common.dao.table.mapper.SysSessionLogPOMapper;
import com.jmda.common.keycode.KeyCodeGenerator;
import com.jmda.common.model.po.SysPersonPO;
import com.jmda.common.model.po.SysPersonPOExample;
import com.jmda.common.model.po.SysSessionLogPO;
import com.jmda.system.service.IndexService;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
@Service
public class IndexServiceImp extends SqlSessionDaoSupport implements IndexService {
	@Autowired
	SysPersonPOMapper sysPersonPOMapper;
	@Autowired
	SysSessionLogPOMapper sysSessionLogPOMapper;
	private static String nameSpace = "com.fms.system.dao.SysRmDAO";
	@Override
	public SysPersonPO loginIn(String loginName, String loginPassword) {
		// TODO Auto-generated method stub
		SysPersonPOExample example=new SysPersonPOExample();
		example.createCriteria().andLoginNameEqualTo(loginName).andLoginPasswordEqualTo(loginPassword);
		List<SysPersonPO> sysPersonPOList=sysPersonPOMapper.selectByExample(example);
		
		if(null!=sysPersonPOList&&sysPersonPOList.size()!=0){
			return sysPersonPOList.get(0);
			
		}
		return null;
	}

	@Override
	public SysPersonPO getUpmsUserVOByStaffId(String staffId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveUserSession(SysPersonPO sysPersonPO, UserAgent userAgent, String token, String ip) {
		// TODO Auto-generated method stub
		SysSessionLogPO log = new SysSessionLogPO();
		Browser browser = userAgent.getBrowser();
		OperatingSystem os = userAgent.getOperatingSystem();
		log.setSessionLogId(KeyCodeGenerator.random32UUID());
		log.setPersonName(sysPersonPO.getName());
		log.setLoginName(sysPersonPO.getLoginName());
		log.setLoginTime(new Date().getTime());
		log.setLoginIp(ip);
		log.setLoginOs(os.getName());
		log.setLoginBrowser(browser.getName());
		log.setBrowserVersion(userAgent.getBrowserVersion().getVersion());
		log.setPersonId(sysPersonPO.getPersonId());
		log.setDeptId(sysPersonPO.getDepartmentId());
		sysSessionLogPOMapper.insertSelective(log);
	}

	@Override
	public void deleteUserSession(String token) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isTokenAlive(String token) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void userAlive() {
		// TODO Auto-generated method stub

	}

	@Override
	public Boolean checkIsAccess(String personId,String rmId) {
		// TODO Auto-generated method stub
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("personId",  personId);
		map.put("rmId", rmId);
      int count = super.getSqlSession().selectOne(nameSpace + ".countByPersonIdId", map);
		if(count==0){
			return false;
		}else{
			return true;
		}


		
	}
	

}
