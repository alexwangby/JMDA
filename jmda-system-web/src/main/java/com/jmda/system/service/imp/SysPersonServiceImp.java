package com.jmda.system.service.imp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jmda.common.dao.table.mapper.SysPersonPOMapper;
import com.jmda.common.dao.table.mapper.SysRmGroupPersonPOMapper;
import com.jmda.common.keycode.KeyCodeGenerator;
import com.jmda.common.model.po.SysPersonPO;
import com.jmda.common.model.po.SysRmGroupPersonPOExample;
import com.jmda.common.model.po.SysRmGroupPersonPOKey;
import com.jmda.common.model.po.SysRmPO;
import com.jmda.common.utils.other.Md5Util;
import com.jmda.system.service.SysPersonService;


@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
@Service
public class SysPersonServiceImp extends SqlSessionDaoSupport implements SysPersonService {
	// 由系统工具生成的数据库表的DAO对象文件 可参照metadata工程相关代码
	@Autowired
	SysPersonPOMapper mapper;
	@Autowired
	SysRmGroupPersonPOMapper sysRmGroupPersonPOMapper;
	private static String nameSpace = "com.fms.system.dao.SysRmVOMapper";
	// TODO 表中写入一条数据
	public void insert(SysPersonPO record) {
		mapper.insertSelective(record);
	}

	// TODO 通过主键ID批量删除mm_goods 表中数据
	public void deleteDatas(String ids) {
		String[] idArray = ids.split(",");
		for (int i = 0; i < idArray.length; i++) {
			String id = idArray[i];
			//删除权限组
			SysRmGroupPersonPOExample sysRmGroupPersonPOExample = new SysRmGroupPersonPOExample();
			sysRmGroupPersonPOExample.createCriteria().andPersonIdEqualTo(id);
			sysRmGroupPersonPOMapper.deleteByExample(sysRmGroupPersonPOExample);
			//删除人员
			SysPersonPO record = new SysPersonPO();
			record.setPersonId(id);
			record.setIsDelete(true);
			update(record);
		}
	}

	// TODO 通过主键修改表中数据
	public void update(SysPersonPO record) {
		mapper.updateByPrimaryKeySelective(record);
	}

	// TODO 通过主键读取表中数据
	public SysPersonPO getSysPersonPO(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public void saveData(SysPersonPO record,String sys_rm_group_person) {
		// TODO Auto-generated method stub
		if(record.getPersonId()==null||record.getPersonId().equals("")){
			record.setCreateTime(System.currentTimeMillis());
			record.setIsDelete(false);
			String personId = KeyCodeGenerator.random32UUID();
			record.setPersonId(personId);
			record.setLoginPassword(Md5Util.MD5("123456"));
			insert(record);
			/**
			 * 绑定权限组
			 */
			List<SysRmGroupPersonPOKey> list =  JSON.parseArray(sys_rm_group_person, SysRmGroupPersonPOKey.class);
			for (SysRmGroupPersonPOKey sysRmGroupPersonPOKey : list) {
				sysRmGroupPersonPOKey.setPersonId(personId);
				sysRmGroupPersonPOMapper.insert(sysRmGroupPersonPOKey);
			}
		}else{
			
			record.setUpdateTime(System.currentTimeMillis());
			update(record);
			/**
			 * 绑定权限组
			 */
			SysRmGroupPersonPOExample sysRmGroupPersonPOExample = new SysRmGroupPersonPOExample();
			sysRmGroupPersonPOExample.createCriteria().andPersonIdEqualTo(record.getPersonId());
			sysRmGroupPersonPOMapper.deleteByExample(sysRmGroupPersonPOExample);
			List<SysRmGroupPersonPOKey> list =  JSON.parseArray(sys_rm_group_person, SysRmGroupPersonPOKey.class);
			for (SysRmGroupPersonPOKey sysRmGroupPersonPOKey : list) {
				sysRmGroupPersonPOMapper.insert(sysRmGroupPersonPOKey);
			}
		}
	}

	@Override
	public String getTree(String personId) {
		// TODO Auto-generated method stub
		personId = personId==null?"":personId;
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		
		List<SysRmPO> sysRmPOList = null;
		if(!personId.equals("")){
			SysRmGroupPersonPOExample sysRmGroupPersonPOExample = new SysRmGroupPersonPOExample();
			sysRmGroupPersonPOExample.createCriteria().andPersonIdEqualTo(personId);
			List<SysRmGroupPersonPOKey> sysRmGroupPersonPOKeyList = sysRmGroupPersonPOMapper.selectByExample(sysRmGroupPersonPOExample);
			if(sysRmGroupPersonPOKeyList!=null&&sysRmGroupPersonPOKeyList.size()>0){
				Map<String, Object> map = new HashMap<>();
				List<String> l = new ArrayList<>();
				for (SysRmGroupPersonPOKey sysRmGroupPersonPOKey : sysRmGroupPersonPOKeyList) {
					String rmGroupId = sysRmGroupPersonPOKey.getRmGroupId();
					l.add(rmGroupId);
				}
				map.put("rmGroupIds", l);
				sysRmPOList = super.getSqlSession().selectList(nameSpace + ".selectByExample", map);
			}
		}
	
        try {
			buildTree(sysRmPOList, null, arrayNode, null, mapper);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String json="";
	    try {
			json=mapper.writeValueAsString(arrayNode);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	    return json;
	}

	@Override
	public String saveBase(SysPersonPO record) {
		record.setLoginName(null);
		mapper.updateByPrimaryKeySelective(record);
		return "ok";
	}

	@Override
	public String updatePassword(String userId, String password) {
		SysPersonPO record = new SysPersonPO();
		record.setPersonId(userId);
		record.setLoginPassword(Md5Util.MD5(password));
		mapper.updateByPrimaryKeySelective(record);
		return "ok";
	}

	private void buildTree(List<SysRmPO> sysRmPOList, ObjectNode pNode, ArrayNode pChildrenNode,
			String pid, ObjectMapper mapper) throws Exception {
		List<SysRmPO> list = this.getChildrenList(sysRmPOList, pid);
		if (list.size() > 0) {
			ArrayNode childrenNode = mapper.createArrayNode();
			for (SysRmPO childrenPO : list) {
				ObjectNode node = mapper.createObjectNode();
				node.put("id",  childrenPO.getRmId());
				node.put("text", childrenPO.getName());
				node.put("state", "open");
				node.put("programCode", childrenPO.getProgramCode());
				node.put("rmPid", childrenPO.getRmPid()==null?"":childrenPO.getRmPid());
				this.buildTree(sysRmPOList, node, childrenNode, childrenPO.getRmId(), mapper);
				if(pNode==null){
					pChildrenNode.add(node);
				}else{
					pNode.put("children", childrenNode);
				}
			}
		}
		if(pNode!=null){
			pChildrenNode.add(pNode);
		}
	}

	private List<SysRmPO> getChildrenList(List<SysRmPO> sysRmPOList, String pid) {
		List<SysRmPO> ls = new ArrayList<>();
		if(sysRmPOList!=null){
			for (SysRmPO sysDepartmentPO : sysRmPOList) {
				if (((null == sysDepartmentPO.getRmPid()||"".equals(sysDepartmentPO.getRmPid())) && null == pid)
						|| (null != sysDepartmentPO.getRmPid() && sysDepartmentPO.getRmPid().equals(pid))) {
					ls.add(sysDepartmentPO);
				}
			}
		}

		return ls;

	}
}
