package com.jmda.system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.common.model.po.SysRmGroupPO;


public interface SysRmGroupService {

	// TODO 写入一条数据
	public void insert(SysRmGroupPO record);

	// TODO 通过主键ID批量删除表中数据
	public void deleteDatas(String ids) ;

	// TODO 通过主键修改 表中数据
	public void update(SysRmGroupPO record) ;

	// TODO 通过主键读取表中数据
	public SysRmGroupPO getSysRmGroupPO(String id);
	public void saveData(SysRmGroupPO record);
	public void saveGroupRm(String ids,String rmGroupId);
	
}
