package com.jmda.system.service.imp;

import java.util.List;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.FmsException;
import com.jmda.common.dao.table.mapper.SysCompanyPOMapper;
import com.jmda.common.dao.table.mapper.SysDepartmentPOMapper;
import com.jmda.common.keycode.KeyCodeGenerator;
import com.jmda.common.model.po.SysCompanyPO;
import com.jmda.common.model.po.SysDepartmentPO;
import com.jmda.common.model.po.SysDepartmentPOExample;
import com.jmda.system.service.SysCompanyService;

@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
@Service
public class SysCompanyServiceImp extends SqlSessionDaoSupport implements SysCompanyService {
	// 由系统工具生成的数据库表的DAO对象文件 可参照metadata工程相关代码
	@Autowired
	SysCompanyPOMapper mapper;
	@Autowired
	SysDepartmentPOMapper sysDepartmentPOMapper;
	// TODO 表中写入一条数据
	public void insert(SysCompanyPO record) {
		mapper.insertSelective(record);
	}

	// TODO 通过主键ID批量删除mm_goods 表中数据
	public void deleteDatas(String ids) throws FmsException {
		String[] idArray = ids.split(",");
		for (int i = 0; i < idArray.length; i++) {
			String id = idArray[i];
			//mapper.deleteByPrimaryKey(id);
			SysDepartmentPOExample example = new SysDepartmentPOExample();
			example.createCriteria().andCompanyIdEqualTo(id).andIsDeleteEqualTo(false);
			List<SysDepartmentPO> list = sysDepartmentPOMapper.selectByExample(example);
			if(list.size()>0){
				throw new FmsException("1", null);
			}
			SysCompanyPO record = new SysCompanyPO();
			record.setCompanyId(id);
			record.setIsDelete(true);
			mapper.updateByPrimaryKeySelective(record);
		}
	}

	// TODO 通过主键修改表中数据
	public void update(SysCompanyPO record) {
		mapper.updateByPrimaryKeySelective(record);
	}

	// TODO 通过主键读取表中数据
	public SysCompanyPO getSysCompanyPO(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public void saveData(SysCompanyPO record) {
		// TODO Auto-generated method stub
		if(record.getCompanyId()==null||record.getCompanyId().equals("")){
			record.setCreateTime(System.currentTimeMillis());
			record.setIsDelete(false);
			record.setCompanyId(KeyCodeGenerator.random32UUID());
			insert(record);
		}else{
			record.setUpdateTime(System.currentTimeMillis());
			update(record);
		}
	}
}
