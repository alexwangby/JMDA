package com.jmda.system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.common.model.po.SysPersonPO;

public interface SysPersonService {

	// TODO 写入一条数据
	public void insert(SysPersonPO record);

	// TODO 通过主键ID批量删除表中数据
	public void deleteDatas(String ids) ;

	// TODO 通过主键修改 表中数据
	public void update(SysPersonPO record) ;

	// TODO 通过主键读取表中数据
	public SysPersonPO getSysPersonPO(String id);
	public void saveData(SysPersonPO record,String sys_rm_group_person);
	public String getTree(String personId);
	public String saveBase(SysPersonPO record);
	public String updatePassword(String userId,String password);
	
}
