package com.jmda.system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.FmsException;
import com.jmda.common.model.po.SysDepartmentPO;


public interface SysDepartmentService {

	// TODO 写入一条数据
	public void insert(SysDepartmentPO record);

	// TODO 通过主键ID批量删除表中数据
	public void deleteDatas(String ids) throws FmsException;

	// TODO 通过主键修改 表中数据
	public void update(SysDepartmentPO record) ;

	// TODO 通过主键读取表中数据
	public SysDepartmentPO getSysDepartmentPO(String id);
	public void saveData(SysDepartmentPO record);
	public void moveUp(String id);
	public void moveDown(String id);
}
