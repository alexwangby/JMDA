/**   
* TODO 
* @author zhanghui   
* @date 2016年4月14日 上午11:45:49  
*/
package com.jmda.system.service;

import com.jmda.common.model.po.SysPersonPO;

import eu.bitwalker.useragentutils.UserAgent;

/**
 * 
 * @author chens
 *
 */
public interface IndexService {
	
	public SysPersonPO loginIn(String loginName,String loginPassword); 
	
	public SysPersonPO getUpmsUserVOByStaffId(String staffId);
	
	public void saveUserSession(SysPersonPO upmsUserVO, UserAgent userAgent,String token,String ip);
	
	public void deleteUserSession(String token);
	
	public boolean isTokenAlive(String token);
	
	public void userAlive();
	public Boolean checkIsAccess(String personId,String rmId);
}
