function titleformatter(value, row, index) {
	if (value!=null) {
		var newValue = getFormatDateTimeByLong(value);
		return  newValue;
	} else {
		return value;
	}
}
function titleformatterRs(value, row, index){
	if (value != null) {
		if(value==false){
			return "否";
		}else{
			return "是";
		}
	} else {
		return value;
	}
}


function openFormSelector(selectorId, callBackFunction, title, width, height, urlParams) {
	var settings = {
		'id' : selectorId,
		'callBack' : callBackFunction,
		'title' : title,
		'width' : width,
		'height' :height,
		'urlparams' : urlParams
	};
	SelectorHelper.openSelector(settings);
}
function openFormDialogHelper(title, width, height, url) {
	var settings = {
		'title' : title,
		'width' : width,
		'height' :height,
		'url' : url
	};
	DialogHelper.openSelector(settings);
}


/**
*转换long值为日期字符串
* @param l long值
* @param pattern 格式字符串,例如：yyyy-MM-dd hh:mm:ss
* @return 符合要求的日期字符串
*/ 
function getFormatDateTimeByLong(l) {
	return getFormatDate(new Date(l), "yyyy-MM-dd hh:mm:ss");
} 
/**
*转换日期对象为日期字符串
* @param l long值
* @param pattern 格式字符串,例如：yyyy-MM-dd hh:mm:ss
* @return 符合要求的日期字符串
*/
function getFormatDate(date, pattern) {
if (date == undefined) {
date = new Date();
}
if (pattern == undefined) {
pattern = "yyyy-MM-dd hh:mm:ss";
}
return date.format(pattern);
} 
Date.prototype.format = function(f){
    var o ={
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(),    //day
        "h+" : this.getHours(),   //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
        "S" : this.getMilliseconds() //millisecond
    }
    if(/(y+)/.test(f))f=f.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(f))f = f.replace(RegExp.$1,RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));return f
}