function titleformatter(value, row, index) {
    if (value != null) {
        if (value == 0) {
            return "<img src='" + basePath + "/assets/img/ban.gif' />";
        } else {
            return "<img src='" + basePath + "/assets/img/run.png' />";
        }
    } else {
        return value;
    }
}
function titleformatterOperate(value, row, index) {
    return "<span style=\"color:#1331D7;cursor:pointer;\" onClick=\"openRecord('"
        + row.impClassPath + "');return false;\">日志</span>";
}
/**
 * 打开记录日志
 * @param impClassPath
 */
function openRecord(impClassPath) {
    var url = "/CuJobLogsWorklistController/getPage?impClassPath=" + impClassPath;
    openFormDialogHelper('日志', 800, 550, url);
}
//新增定时器
function addJob() {
    var url = "/job/getForm";
    openFormDialogHelper('新建', 800, 550, url);
}

//修改定时器
function editJob() {
    var rows = $("#data_table").datagrid('getSelections');
    if (rows.length == 1) {
        var jobId = rows[0].jobId;
        var url = "/job/getForm" + "?jobId=" + jobId;
//		openFormSelector(selectorId, callBackFunction, title, width, height, urlParams)
        openFormDialogHelper('修改', 800, 550, url);
    } else {
        jQuery.messager.alert('提示', '请选择一行进行操作!');
    }
}

//启动定时器
function startJob() {
    var rows = $("#data_table").datagrid('getSelections');
    if (rows.length == 1) {
        jQuery.messager.confirm("提示", "您确定要启动此定时器吗？", function (res) {//提示是否启动
            if (res) {
                var jobId = rows[0].jobId;
                $.ajax({
                    url: basePath + "/job/startJob",
                    data: {
                        jobId: jobId
                    },
                    success: function (data) {
                        $.messager.progress('close');
                        if (data == 'success') {
                            jQuery.messager.alert('提示', '启动成功!');
                            location.reload();
                        } else {
                            jQuery.messager.alert('提示', '启动失败!');
                        }
                    }
                });
            }
        });
    } else {
        jQuery.messager.alert('提示', '请选择一行进行操作!');
    }
}

//停止定时器
function stopJob() {
    var rows = $("#data_table").datagrid('getSelections');
    if (rows.length == 1) {
        jQuery.messager.confirm("提示", "您确定要停止此定时器吗？", function (res) {//提示是否停止
            if (res) {
                var jobId = rows[0].jobId;
                $.ajax({
                    url: basePath + "/job/stopJob",
                    data: {
                        jobId: jobId
                    },
                    success: function (data) {
                        $.messager.progress('close');
                        if (data == 'success') {
                            jQuery.messager.alert('提示', '停止成功!');
                            location.reload();
                        } else {
                            jQuery.messager.alert('提示', '停止失败!');
                        }
                    }
                });
            }
        });
    } else {
        jQuery.messager.alert('提示', '请选择一行进行操作!');
    }
}

function executeJob() {
    var rows = $("#data_table").datagrid('getSelections');
    if (rows.length == 1) {
        jQuery.messager.confirm("提示", "您确定要立即执行一次此定时器吗？", function (res) {//提示是否删除
            if (res) {
                var jobId = rows[0].jobId;
                $.ajax({
                    url: basePath + "/job/executeJob",
                    data: {
                        jobId: jobId
                    },
                    success: function (data) {
                        $.messager.progress('close');
                        if (data == 'success') {
                            jQuery.messager.alert('提示', '执行成功!');
                            location.reload();
                        } else {
                            jQuery.messager.alert('提示', '执行失败!');
                        }
                    }
                });
            }
        });
    } else {
        jQuery.messager.alert('提示', '请选择一行进行操作!');
    }
}

function deleteJob() {
    var rows = $("#data_table").datagrid('getSelections');
    if (rows.length == 1) {
        jQuery.messager.confirm("提示", "您确定要删除此定时器吗？", function (res) {//提示是否删除
            if (res) {
                var jobId = rows[0].jobId;
                $.ajax({
                    url: basePath + "/job/deleteJob",
                    data: {
                        jobId: jobId
                    },
                    success: function (data) {
                        $.messager.progress('close');
                        if (data == 'success') {
                            jQuery.messager.alert('提示', '删除成功!');
                            location.reload();
                        } else {
                            jQuery.messager.alert('提示', '删除失败!');
                        }
                    }
                });
            }
        });
    } else {
        jQuery.messager.alert('提示', '请选择一行进行操作!');
    }
}

function openFormSelector(selectorId, callBackFunction, title, width, height, urlParams) {
    var settings = {
        'id': selectorId,
        'callBack': callBackFunction,
        'title': title,
        'width': width,
        'height': height,
        'urlparams': urlParams
    };
    SelectorHelper.openSelector(settings);
}
function openFormDialogHelper(title, width, height, url) {
    var settings = {
        'title': title,
        'width': width,
        'height': height,
        'url': url
    };
    DialogHelper.openSelector(settings);
}