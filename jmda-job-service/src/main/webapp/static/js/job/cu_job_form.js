function saveJob() {
	var impClassPath = $("input[name='impClassPath']").val();
	var name = $("input[name='name']").val();
	var startTime = $("input[name='startTime']").val();
	if (impClassPath == '' || name == '' || startTime == '') {
		jQuery.messager.alert('提示', '信息填写不完整！');
	} else {
		if ($("#statusCheck").is(':checked')) {
			$("#jobStatus").val(1);
		} else {
			$("#jobStatus").val(0);
		}
		$("#data_form").form('submit', {
			url : basePath + "/job/save",
			dataType : 'text',
			success : function(data) {
				// 新增成功
				if (data == "no") {
					alert('操作失败');
				} else if (data == "save") {
					alert('新增成功');
					// 刷新父窗体表格
					parent.location.reload();
					// 关闭当期表单窗口
					parent.DialogHelper.closeDialog();
				} else if (data == "update") {
					// 修改成功
					alert('修改成功');
					parent.location.reload();
					parent.DialogHelper.closeDialog();
				} else if (data == 'fail') {
					jQuery.messager.alert('提示', '类路径不存在');
				}
			}
		});
	}
}
