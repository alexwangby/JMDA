<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>JOB页面</title>
<#include "../../comm/eformMeta.ftl" />
<script type="text/javascript" src="${basePath}/static/js/job/cu_job_form.js"></script>
</head>
<body>
     <form id="data_form" method="post" action="" class="page-form">
      <div class="buts-row-top row ">
         	<div class="col-sm-12 text-align-left buts-col" >
      
         	 <button class="btn btn-success" onclick="saveJob();" type="button">保存</button>&nbsp;
       
        	</div>
        </div>
        <div class="but-row-top-shadow">
      </div>
        <div class="form-title">
           	 定时器
        </div>
        <div class="row">

		
		<input type='hidden' class='form-control'   name="jobId" value='${jobId}'>
		
		
		<div class="col-sm-10">
		<div class="form-group"><label>名称</label> <span class="input-icon icon-right">  <input type='text' class='form-control easyui-validatebox' data-options=validType:["length[0,64]"]  name="name" value='${name}'> </span></div>
		</div>

		<div class="col-sm-10">
		<div class="form-group"><label>类路径</label> <span class="input-icon icon-right"> <input type='text' class='form-control easyui-validatebox' data-options=validType:["length[0,255]"]  name="impClassPath" value='${impClassPath}'> </span></div>
		</div>
		
		<div class="col-sm-10">
		<div class="form-group">
			<label>是否运行</label> 
			<span class="input-icon icon-right"> 
			<div class="checkbox">
			<label>
			<input type="checkbox" name="statusCheck" id="statusCheck" ${statusCheck} >
			<span class="text"></span>
			</label>
			</div>
            </span>
            </div>
		</div>
		
		<div class="col-sm-10">
		<div class="form-group"><label>执行时间配置</label> 
		<span class="input-icon icon-right"> <input type='text' class='form-control easyui-validatebox' data-options=validType:["length[0,32]"]  name="timeExpression" value='${timeExpression}'> </span></div>
		</div>
		<!--
		<div class="col-sm-10">
		<div class="form-group"><label><b>间隔时间</b></label> 
		<span class="input-icon icon-right"> <input type='text' class='form-control easyui-validatebox' data-options=validType:["length[0,32]"]  name="jiange" value=''> </span></div>
		</div>
		
		<div class="col-sm-2">
		<div class="form-group"><label>年</label> <span class="input-icon icon-right"> 	<select  class='form-control  easyui-validatebox'   ' name="nian" >
			<option value='' >请选择</option>
			<option value='*' >每年</option>
			</select> </span> </div>
		</div>
		<div class="col-sm-2">
		<div class="form-group"><label>月</label> <span class="input-icon icon-right"> 	
			<select  class='form-control  easyui-validatebox'   ' name="yue" >
			<option value='' >请选择</option>
			<option value='*' >每月</option>
			</select>
		 </span> </div>
		</div>
		<div class="col-sm-2">
		<div class="form-group"><label>星期</label> <span class="input-icon icon-right"> 	
			<select  class='form-control  easyui-validatebox'   ' name="xingqi" >
			<option value='' >请选择</option>
			<option value="?">无</option>
			<option value="MON">周一</option>
			<option value="TUE">周二</option>
			<option value="WED">周三</option>
			<option value="THU">周四</option>
			<option value="FRI">周五</option>
			<option value="SAT">周六</option>
			<option value="SUN">周日</option>
			</select>
		 </span></div>
		</div>
		<div class="col-sm-2">
		<div class="form-group"><label>天</label> <span class="input-icon icon-right"> 	
			<select  class='form-control  easyui-validatebox'   ' name="ri" >
			<option value='' >请选择</option>
			<option value="?">无</option>
			<option value='*' >每天</option>
			</select> 
		 </span></div>
		</div>
		<div class="col-sm-2">
		<div class="form-group"><label>时</label> 
			<span class="input-icon icon-right"> 
			<select  class='form-control  easyui-validatebox'   ' name="shi" >
			<option value='' >请选择</option>
			<option value='*' >每小时</option>
			</select> </span>
		</div>
		</div>
		<div class="col-sm-2">
		<div class="form-group"><label>分</label> 
			<span class="input-icon icon-right"> 
			<select  class='form-control  easyui-validatebox'   ' name="fen" >
			<option value='' >请选择</option>
			<option value='*' >每分钟</option>
			</select> </span>
		</div>
		</div>
		-->
        </div>
        <input type="hidden" id="jobStatus" name="jobStatus" value="${jobStatus}"/>
        <div><a href="${basePath}/static/document/定时器操作.doc">定时器操作帮助手册</a> </div>
    </form>
</body>
</html>
