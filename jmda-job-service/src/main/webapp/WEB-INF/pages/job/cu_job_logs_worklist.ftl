﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>数据列表</title>
<#include "../../comm/worklistMeta.ftl" />
<script type="text/javascript" src="${basePath}/static/js/job/cu_job_logs_worklist.js"></script>
</head>
<body  class="easyui-layout" fit=true >

<div data-options="region:'center'">
	<!-- id="datagrid_toolbar" 必填  -->
	<div id="datagrid_toolbar">
		<form id="search_form" class="search-form" role="form">
		<div class="row">
		</div>
		</form>
	</div>

	<!-- id="data_table" 必填  -->
	<table id="data_table" class="easyui-datagrid" pagination="true" pageSize='20' border="0" rownumbers="true" fitColumns="true" fit="true"
		data-options="singleSelect:false,
            collapsible:true,url:'${basePath}/CuJobLogsWorklistController/getDatas?impClassPath=${impClassPath}',
            method:'get',
            striped: true,
            remoteSort:true,
            toolbar:'#datagrid_toolbar'">
		<thead>
			<tr>
                <th data-options="field:'startTime',width:width(0.1),formatter:titleformatter">执行开始时间</th>
                <th data-options="field:'endTime',width:width(0.1),formatter:titleformatter">执行结束时间</th>
                <th data-options="field:'isSuccess',align:'center',formatter:titleformatterRs">是否成功</th>
			</tr>
		</thead>
	</table>
	</div>

</body>
</html>
