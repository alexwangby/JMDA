﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <title>数据列表</title>
	<#include "../../comm/worklistMeta.ftl" />
    <script type="text/javascript" src="${basePath}/static/js/job/cu_job_worklist.js"></script>
</head>
<body class="easyui-layout" fit=true>

<div data-options="region:'center'">
    <!-- id="datagrid_toolbar" 必填  -->
    <div id="datagrid_toolbar">
        <div class="buttons-preview" style="padding: 10px 10px 0px 10px">
            <a class="btn btn-primary btn-sm" href="#" onClick="addJob();;"> <i class="fa fa-plus"></i>新增定时器</a>
            <a class="btn btn-primary btn-sm" href="#" onClick="editJob();;"> <i class="fa fa-plus"></i>修改定时器</a>
            <a class="btn btn-primary btn-sm" href="#" onClick="startJob();;"> <i class="fa fa-edit"></i>启动定时器</a>
            <a class="btn btn-primary btn-sm" href="#" onClick="stopJob();;"> <i class="fa fa-times"></i>停止定时器</a>
            <a class="btn btn-primary btn-sm" href="#" onClick="deleteJob();;"> <i class="fa fa-times"></i>删除定时器</a>
            <a class="btn btn-primary btn-sm" href="#" onClick="executeJob();;"> <i class="fa fa-times"></i>立即执行</a>

            <!-- 设置数据表格  -->
            <a class="btn btn-default btn-xs shiny icon-only  float-right red " style="margin: 3px 0px 0px 0px;" href="javascript:settingDatagrid()"> <i class="fa fa-cog"></i></a>
        </div>
    </div>

    <!-- id="data_table" 必填  -->
    <table id="data_table" class="easyui-datagrid" pagination="true" pageSize='20' border="0" rownumbers="true" fitColumns="true" fit="true"
           data-options="singleSelect:false,
            collapsible:true,url:'${basePath}/job/getDatas',
            method:'get',
            striped: true,
            remoteSort:true,
            toolbar:'#datagrid_toolbar'">
        <thead>
        <tr>
            <th data-options="field:'check',checkbox:true">选择</th>
            <th data-options="field:'name',width:width(0.1)">定时器名称</th>
            <th data-options="field:'jobId',width:width(0.1),hidden:true">ID</th>
            <th data-options="field:'impClassPath',width:width(0.1)">类路径</th>
            <th data-options="field:'jobStatus',align:'center',formatter:titleformatter">状态</th>
            <th data-options="field:'operate',align:'center',formatter:titleformatterOperate">执行记录</th>
        </tr>
        </thead>
    </table>
</div>

</body>
</html>
