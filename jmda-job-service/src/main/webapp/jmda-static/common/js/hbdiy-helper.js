var HbdiyHelper = {
		getNameById: function (id,keys) {
	        var result = null;
	        if (id == null || id == '' || (typeof id) == 'undefined') {
	        	 console.log('id 不能为空');
	            return '';
	        }
	        if (keys == null || keys == '' || (typeof keys) == 'undefined') {
	        	 console.log('keys 不能为空');
	            return '';
	        }
	        keys=keys.replace(new RegExp('\\.','gm'),':');
	        commonJSONPLoader({},
	        		BASE_PATH + '/cu/getName/' + id+'/'+keys,
	            function (data) {
	                result = data;
	            },
	            function () {
	                console.log('根据ID查询NAME失败');
	            }
	        );

	        return (result != null && result.name) ? result.name : '';
	    },
	    getImageUrlById: function (id,width) {
	        var result = null;
	        if (id == null || id == '' || (typeof id) == 'undefined') {
	        	 console.log('id 不能为空');
	            return '';
	        }
	        if (width == null || width == '' || (typeof width) == 'undefined') {
	        	width = 30;
	        }
	        $.ajax({
	            url:  basePath+ '/cu/getImageUrl?itemId='+id+'&width='+width,
	            dataType : 'text',
	            async: false,
	            success: function (data) {
	            	result= data;
	            },
	            error: function () {
	                console.log("查询失败，目标地址目前可能不可用");
	            }
	        });
	        return result;
	    }
};
/**
 * 通用调用,主要用于自定义标签中的查询
 * */
function commonJSONPLoader(opts, url, success, error) {
    $.ajax({
        url: url,
        dataType: 'json',
        data: opts.params,
        async: false,
        success: function (data) {
            success(data);
        },
        error: function () {
            console.log("查询失败，目标地址目前可能不可用");
            error.apply(this, arguments);
        }
    });
}