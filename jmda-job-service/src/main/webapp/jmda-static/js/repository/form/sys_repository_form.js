﻿var viewport;
Ext.onReady(function() {
	Ext.BLANK_IMAGE_URL = application_STATIC_SERVER + 'jmda-static/js/extjs3/images/default/s.gif';
	Ext.QuickTips.init();
	var tabs = new Ext.TabPanel({
		activeTab : 1,
		plain : true,
		tabPosition : "bottom",
		defaults : {
			autoScroll : true
		}
	});

	for (var i = 0; i < tabArray.length; i++) {
		var tabNameAndType = tabArray[i].split("|");
		var tabName = tabNameAndType[0];
		var pageType = tabNameAndType[1];
		var cmd = tabNameAndType[2];
		tabs.add({
			title : tabName,
			listeners : {
				activate : handleActivate,
				hide : handleHide
			},
			formlb : cmd,
			html : '<iframe id="FormData_Page_' + pageType + '" name="FormData_Page_' + pageType + '" frameborder=0 width=100% height=100% src="' + basePath + 'wait.jsp"></iframe>',
			pageType : pageType
		});
	}

	function handleActivate(tab) {
		// var frmMain = Ext.getDom('boForm');
		if (tab.formlb == 'JB') {
			eval("FormData_Page_" + tab.pageType).location = encodeURI(basePath + "repository/form/getFormBasePage?form_id=" + formId);
		} else if (tab.formlb == 'SJY') {
			eval("FormData_Page_" + tab.pageType).location = encodeURI(basePath + "repository/form/metadata/getDataSourceForm?formId=" + formId);
		} else if (tab.formlb == 'BDMB') {
			eval("FormData_Page_" + tab.pageType).location = encodeURI(basePath + "repository/form/templete/getTempletePage?formId=" + formId);
		} else {
		}

	}
	function handleHide(tab) {
		eval("FormData_Page_" + tab.pageType).location = basePath + "wait.jsp";
	}

	viewport = new Ext.Viewport({
		layout : 'border',
		items : [ {
			region : 'north',
			html : '<h1 class="x-panel-header">当前路径：' + applicationName + '》' + formName + '</h1>',
			autoHeight : true,
			border : false,
			margins : '0 0 2 0'
		}, {
			region : 'center',
			layout : 'fit',
			border : false,
			items : tabs
		} ]
	});
});
function getviewportHeight() {
	return viewport.el.dom.clientHeight;
}
function refreshParent(nodeId) {
	try {
		if (parent.FORM_Frame) {
			parent.FORM_Frame.store.load();
		}
		parent.regreshNodeById(nodeId);
	} catch (e) {
	}
}