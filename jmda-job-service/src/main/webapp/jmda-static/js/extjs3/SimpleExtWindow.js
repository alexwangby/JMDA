/**
 * 调用使用范例
 * parent.gridStore = store;
 * --从对象中移除所有的监听器
 * activeDialog.purgeListeners();
 * activeDialog.addListener('show', handleShow);
 * activeDialog.addListener('beforehide', handleHide);
 * activeDialog.show();
 * 
 *			
 */
var dialogLocationURL=basePath+"wait.jsp";
var activeDialog;
var gridStore;
function openSimpleExtWindow(winTitle,winWidth,winHeight,frameUrl){
	dialogLocationURL=frameUrl;
	Ext.QuickTips.init();
	var id = "Simple_Operate_Div";
	if (Ext.getCmp(id)) {
		Ext.getCmp(id).destroy();
	}
	var winContainerElId = Ext.id(); 
	Ext.getBody().createChild({
		tag :'div',
		id : winContainerElId
	});
	if(!activeDialog){
	     activeDialog =new  Ext.Window({
	      el:winContainerElId,
				layout:'fit',
				title: '',
				plain: true,
				closable: true,
				closeAction :'hide',
				resizable: false,
				modal: true,
				//buttons:[],
				buttons:[{text:'确认',handler: function() {
					activeDlgPage.save();
				}},{text:'关闭',handler:function() {
					activeDialog.hide();
				}}],
				  
				items:[{
					html:'<iframe name=activeDlgPage id=activeDlgPage frameborder=0 width=100% height=100% src="'+basePath+"wait.jsp"+'"></iframe>'
				}]
	    });
	  }
	  activeDialog.width=winWidth;
	  activeDialog.height=winHeight;
	  activeDialog.setTitle(winTitle);
	}

function handleShow(o){
	   activeDlgPage.location=encodeURI(dialogLocationURL);
	   return;
	 }
function handleHide(o){
	   activeDialog.buttons = [];
	   activeDlgPage.location=basePath+"wait.jsp";
	   return;
	 }