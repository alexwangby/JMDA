function resetForm(fm) {
    //document.getElementById(fm).reset();
    $('#' + fm).form('clear');	//使用easyui的form清空方法，以清空form中使用了easyui控件的表单项
    try {
        eval('oasisResetFormCallBack()');
    } catch (e) {
    }

}
/**
 * 新版查询方法，添加对form中combobox取值的支持
 * add by wangbo
 * @param table
 * @param sfm
 */
function workListSearch(table, sfm) {
    try {
        eval('beforeHbdiySearchFormCallBack()');
    } catch (e) {

    }

    if (arguments.length > 2) {
        $("#" + table).datagrid('options').queryParams = eval('(' + arguments[2] + ')');
    } else {
        $("#" + table).datagrid('options').queryParams = paramSrarchForm(sfm);
    }

    $('#' + table).datagrid('loadData', {
        total: 0,
        rows: []
    });
    $('#' + table).datagrid({
        queryParams: $("#" + table).datagrid('options').queryParams
    });
    try {
        eval('afterHbdiySearchFormCallBack()');
    } catch (e) {

    }
}
/**
 * 新版拼接参数列表，添加对combobox取值的支持
 * add by wangbo
 * @param sfm
 * @returns
 */
function paramSrarchForm(sfm) {
    var b = $("#" + sfm + " input");
    var param = '';
    for (var i = 0; i < b.length; i++) {
        var bname = b[i].id;
        if (bname == null || bname == '') {
            bname = b[i].name;
        }
        if (bname != null && bname != '') {
            var sb = bname.split('search_');
            if (sb.length <= 1) {
                continue;
            }
            if (b[i].style.display == 'none') {
                var classStr = $(b[i]).attr('class');
                var value = null;

                classStr = classStr ? classStr : "";

                if (classStr.indexOf('easyui-combobox') != -1) {
                    value = $(b[i]).combobox('getValue');
                    param += ',\"' + sb[1] + '\":\"' + value + '\"';
                } else if (classStr.indexOf('easyui-datebox') != -1) {
                    value = $(b[i]).datebox('getValue');
                    if (value == null || value == '') {
                        continue;
                    } else {
                        value = $(b[i]).datebox('calendar').calendar('options').current.getTime();
                        param += ',\"' + sb[1] + '\":\"' + value + '\"';
                    }
                } else {
                    try {
                        var arr = b[i].parentNode.childNodes[2].childNodes[2];
                        if (arr.type == 'hidden') {
                            param += ',\"' + sb[1] + '\":\"' + arr.value + '\"';
                        }
                    } catch (e) {
                        conlose.log(e.message);
                    }
                }
            } else if (b[i].type == 'text' || b[i].type == 'hidden') {
                param += ',\"' + sb[1] + '\":\"' + b[i].value + '\"';
            } else if (b[i].type == 'radio' || b[i].type == 'checkbox') {
                if (b[i].checked)
                    param += ',\"' + sb[1] + '\":\"' + b[i].value + '\"';
            }
        }

    }
    var s = $("#" + sfm + ">select");
    for (var i = 0; i < s.length; i++) {
        var sname = s[i].id;
        if (sname == null || sname == '') {
            sname = s[i].name;
        }
        if (sname != null && sname != '') {
            var ssb = sname.split('search_');
            if (ssb.length > 1) {
                param += ',\"' + ssb[1] + '\":\"' + s[i].value + '\"';
            }
        }
    }

    return eval('({' + param.substring(1, param.length) + '})');
}
