package com.jmda.job.repository.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jmda.erp.common.dao.table.mapper.CuJobLogsPOMapper;
import com.jmda.erp.common.dao.table.mapper.CuJobPOMapper;
import com.jmda.erp.common.model.po.CuJobLogsPO;
import com.jmda.erp.common.model.po.CuJobLogsPOExample;
import com.jmda.erp.common.model.po.CuJobPO;
import com.jmda.erp.common.model.po.CuJobPOExample;
import com.jmda.job.engine.InitJob;

@Service
public class CuJobService extends SqlSessionDaoSupport {
	Logger logger = org.slf4j.LoggerFactory.getLogger(CuJobService.class);
	String nameSpace_vo = "com.jmda.job.repository.dao.CuJobVOMapper.";
	@Resource
	CuJobLogsPOMapper cuJobLogsPOMapper;
	@Resource
	CuJobPOMapper cuJobPOMapper;

	public List<CuJobLogsPO> getByJobStatus() {
		CuJobLogsPOExample cuJobLogsPOExample = new CuJobLogsPOExample();
		cuJobLogsPOExample.createCriteria().andJobStatusEqualTo("2");
		return cuJobLogsPOMapper.selectByExample(cuJobLogsPOExample);
	}

	public int updateByPrimaryKey(CuJobLogsPO cuJobLogsPO) {
		return cuJobLogsPOMapper.updateByPrimaryKey(cuJobLogsPO);
	}

	@PostConstruct
	public void initialize() {
		CuJobPOExample cuJobPOExample = new CuJobPOExample();
		cuJobPOExample.createCriteria().andRunStatusEqualTo("2");
		CuJobPO paramCuJobPO = new CuJobPO();
		paramCuJobPO.setRunStatus("0");
		cuJobPOMapper.updateByExampleSelective(paramCuJobPO, cuJobPOExample);
		List<CuJobPO> list = cuJobPOMapper.selectByExample(null);
		if (list != null && list.size() > 0) {
			for (CuJobPO cuJobPO : list) {
				String isRun = cuJobPO.getJobStatus();
				String dataSource = cuJobPO.getDataSource();
				if (dataSource == null || dataSource.equals("")) {
					dataSource = "";
				}
				if (isRun.equals("1") && dataSource.equals("")) {
					try {
						// logger.info("时间 "+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"启动任务:"+cuJobPO.getName()+"............");
						Class.forName(cuJobPO.getImpClassPath());
						InitJob.GetTimerScheduler(cuJobPO.getImpClassPath(), cuJobPO.getJobId(), cuJobPO.getTimeExpression()).start();
						logger.info("时间 " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "启动任务:" + cuJobPO.getName() + "成功...");
					} catch (SchedulerException e) {
						e.printStackTrace();
						logger.info("时间 " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "启动任务:" + cuJobPO.getName() + "失败...");
					} catch (ClassNotFoundException e) {
						System.err.println(cuJobPO.getImpClassPath() + "不存在");
						logger.info("时间 " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + ":获取类" + cuJobPO.getImpClassPath() + "失败...");
					}
				}
			}
		}
	}

	public int selectCount() {
		return cuJobPOMapper.countByExample(null);
	}

	public List<CuJobPO> selectAllTimer(int start, int limit) {
		return super.getSqlSession().selectList(nameSpace_vo + "selectAllTimer", null, new RowBounds(start, limit));
	}

	/**
	 * 新增操作
	 * 
	 * @param cuJobPO
	 * @return
	 */
	@Transactional
	public int insert(CuJobPO cuJobPO) {
		return cuJobPOMapper.insertSelective(cuJobPO);
	}

	/**
	 * 通过id使其status进行变更，为Start状态
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public int updateByIdStart(String id) {
		return super.getSqlSession().update(nameSpace_vo + "updateByIdStart", id);
	}

	/**
	 * 通过id使其status进行变更，为Stop状态
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public int updateByIdStop(String id) {
		return super.getSqlSession().update(nameSpace_vo + "updateByIdStop", id);
	}

	/**
	 * 通过主键进行删除
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public int deleteByPrimaryKey(String id) {
		return cuJobPOMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 通过主键进行查询
	 * 
	 * @param id
	 * @return
	 */
	public CuJobPO selectByPrimaryKey(String id) {
		return cuJobPOMapper.selectByPrimaryKey(id);
	}

	/**
	 * 更新
	 * 
	 * @param cuJobPO
	 * @return
	 */
	@Transactional
	public int updateByPrimaryKeySelective(CuJobPO cuJobPO) {
		return cuJobPOMapper.updateByPrimaryKeySelective(cuJobPO);
	}

}
