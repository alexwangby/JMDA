package com.jmda.job.repository.service;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Service;

import com.jmda.erp.common.model.po.CuJobLogsPO;



@Service
public class CuJobLogsService extends SqlSessionDaoSupport{
	private static String nameSpace_po="com.jmda.erp.common.dao.table.mapper.CuJobLogsPOMapper.";
	private static String nameSpace_vo="com.jmda.job.repository.dao.CuJobLogsVOMapper.";
    /**
     * 分页查询
     * @param impClassPath
     * @param start
     * @param limit
     * @return
     */
    public List<CuJobLogsPO> selectByImpClassPath(String impClassPath,int start,int limit){
    	return super.getSqlSession().selectList(nameSpace_vo +"selectByImpClassPath", impClassPath, new RowBounds(start, limit));
    }
    public int selectCount(String impClassPath){
    	return super.getSqlSession().selectOne(nameSpace_vo +"selectCountByImpClassPath",impClassPath);
    }
    /**
     * 新增
     * @param cuJobLogsPO
     * @return
     */
    public int insert(CuJobLogsPO cuJobLogsPO){
    	return super.getSqlSession().insert(nameSpace_po+"insertSelective", cuJobLogsPO);
    }
    public int insertRun(CuJobLogsPO cuJobLogsPO){
    	return super.getSqlSession().insert(nameSpace_vo+"insertRun", cuJobLogsPO);
    }
    public int selectRunCountByImpClassPath(String impClassPath){
    	return super.getSqlSession().selectOne(nameSpace_vo +"selectRunCountByImpClassPath",impClassPath);
    }
    public int updateByPrimaryKeySelective(CuJobLogsPO cuJobLogsPO){
    	return super.getSqlSession().update(nameSpace_po +"updateByPrimaryKeySelective",cuJobLogsPO);
    }
}
