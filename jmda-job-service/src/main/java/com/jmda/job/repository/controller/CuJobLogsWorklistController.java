package com.jmda.job.repository.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.erp.common.model.po.CuJobLogsPO;
import com.jmda.job.repository.service.CuJobLogsService;

@Controller
@RequestMapping("/CuJobLogsWorklistController")
public class CuJobLogsWorklistController {
	private static Logger logger = Logger.getLogger(CuJobLogsWorklistController.class);
	@Autowired
	private CuJobLogsService cuJobLogsService;
	@RequestMapping(value = "/getPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getPage(String impClassPath,HttpServletRequest request, ModelMap map) {
		map.put("impClassPath", impClassPath);
		return "/job/cu_job_logs_worklist";
	}

	@RequestMapping(value = "/getDatas", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getDatas(String impClassPath, int page, int rows, HttpServletRequest request) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		List<CuJobLogsPO> list = cuJobLogsService.selectByImpClassPath(impClassPath, page, rows);
		Map<String, Object> json = new HashMap<String, Object>();
		int total = cuJobLogsService.selectCount(impClassPath);
		if (list == null) {
			json.put("rows", "[]");
			json.put("total", 0);
		} else {
			json.put("rows", list);
			json.put("total", total);
		}
		return mapper.writeValueAsString(json);
	}

}