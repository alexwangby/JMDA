package com.jmda.job.repository.controller;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.erp.common.model.po.CuJobPO;
import com.jmda.job.engine.InitJob;
import com.jmda.job.repository.service.CuJobService;
import com.jmda.platform.commom.SequenceService;


@Controller
@RequestMapping("/job")
public class CuJobWorklistController {
    private static Logger logger = Logger.getLogger(CuJobWorklistController.class);
    @Autowired
    private CuJobService cuJobService;

    @Resource
    SequenceService seq;
    @RequestMapping(value = "/getPage", method = {RequestMethod.POST, RequestMethod.GET})
    public String getPage(HttpServletRequest request, ModelMap map) {
        return "/job/cu_job_worklist";
    }

    @RequestMapping(value = "/getForm", method = {RequestMethod.POST, RequestMethod.GET})
    public String getForm(HttpServletRequest request, ModelMap map) {
        String jobId = (String) request.getParameter("jobId");
        if (jobId != null) {
            CuJobPO cuJobPO = cuJobService.selectByPrimaryKey(jobId);
            map.put("jobId", jobId);
            map.put("name", cuJobPO.getName());
            map.put("jobStatus", cuJobPO.getJobStatus());
            map.put("impClassPath", cuJobPO.getImpClassPath());
            map.put("timeExpression", cuJobPO.getTimeExpression());
            if (cuJobPO.getJobStatus().equals("1")) {
                map.put("statusCheck", "checked=\"checked\"");
            }
        }
        return "/job/cu_job_edit_form";
    }

    @RequestMapping(value = "/getDatas", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String getDatas(int page, int rows, HttpServletRequest request) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        List<CuJobPO> list = cuJobService.selectAllTimer(page, rows);
        Map<String, Object> json = new HashMap<String, Object>();
        int total = cuJobService.selectCount();
        if (list == null) {
            json.put("rows", "[]");
            json.put("total", 0);
        } else {
            json.put("rows", list);
            json.put("total", total);
        }
        return mapper.writeValueAsString(json);
    }

    /**
     * 新增定时器
     */
    @RequestMapping(value = "/save", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String saveJob(CuJobPO cuJobPO, HttpServletRequest request) throws SchedulerException {
        String jobId = cuJobPO.getJobId();
        String impClassPath = cuJobPO.getImpClassPath();
        try {
            //如果类路径存在则往下执行
            Class.forName(impClassPath);
            // 如果id为null则执行新增操作
            if (jobId == null || jobId.equals("")) {
                jobId = seq.getUUID();
                cuJobPO.setJobId(jobId);
                cuJobPO.setRunStatus("1");
                int rs = cuJobService.insert(cuJobPO);
                if (rs == 1) {// 新增成功
                    String timeExp = cuJobPO.getTimeExpression();
                    if (cuJobPO.getJobStatus() == "1") {// 立即运行
                        String implClassPath = cuJobPO.getImpClassPath();
                        InitJob.GetTimerScheduler(implClassPath, jobId, timeExp).start();
                    }
                    return "save";
                } else {
                    return "no";
                }
            } else {//执行更新操作
                int rs = cuJobService.updateByPrimaryKeySelective(cuJobPO);
                if (rs == 1) {// 如果对定时器更新成功
                    if (cuJobPO.getJobStatus() == "1") {// 如果用户选择为立即运行
                        String timeExp = cuJobPO.getTimeExpression();
                        JobKey jobKey = new JobKey(jobId, "hbdiy_jobtimer");
                        String implClassPath = cuJobPO.getImpClassPath();
                        InitJob.getSchedulerFactory().getScheduler().deleteJob(jobKey);
                        InitJob.GetTimerScheduler(implClassPath, jobId, timeExp).start();
                    }
                    return "update";
                } else {
                    return "no";
                }
            }
        } catch (ClassNotFoundException e) {
            //类路径不存在，返回fail，进行提示
            return "fail";
        }
    }

    /**
     * 立即执行定时器（执行逻辑）
     */
    @RequestMapping(value = "/executeJob", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String executeJob(String jobId) throws SchedulerException {
        CuJobPO cuJobPO = cuJobService.selectByPrimaryKey(jobId);
        try {
            Class c = Class.forName(cuJobPO.getImpClassPath());
            Object object = c.newInstance();
            Method method = c.getMethod("doIt", null);
            method.invoke(object, null);
            System.out.println("执行了。。。。。。。。。。。");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }

    /**
     * 重新启动定时器
     */
    @RequestMapping(value = "/startJob", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String startJob(String jobId) throws SchedulerException {
        int rs = cuJobService.updateByIdStart(jobId);
        CuJobPO cuJobPO = cuJobService.selectByPrimaryKey(jobId);
        String implClassPath = cuJobPO.getImpClassPath();
        String timeExp = cuJobPO.getTimeExpression();
        // 启动定时器
        InitJob.GetTimerScheduler(implClassPath, jobId, timeExp).start();
        if (rs == 1) {
            return "success";
        } else {
            return "no";
        }
    }

    /**
     * 停止定时器
     *
     * @param jobId
     * @return
     * @throws SchedulerException
     */
    @RequestMapping(value = "/stopJob", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String stopJob(String jobId) throws SchedulerException {

        JobKey jobKey = new JobKey(jobId, "hbdiy_jobtimer");
        // 将定时任务删除，以便于重新启动时可以执行start
        InitJob.getSchedulerFactory().getScheduler().deleteJob(jobKey);
        int rs = cuJobService.updateByIdStop(jobId);
        if (rs == 1) {
            return "success";
        } else {
            return "no";
        }
    }

    /**
     * 删除定时器，先删除定时任务，再对定时器进行删除
     *
     * @param jobId
     * @return
     * @throws SchedulerException
     */
    @RequestMapping(value = "/deleteJob", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String deleteJob(String jobId) throws SchedulerException {
        JobKey jobKey = new JobKey(jobId, "hbdiy_jobtimer");
        InitJob.getSchedulerFactory().getScheduler().deleteJob(jobKey);
        int rs = cuJobService.deleteByPrimaryKey(jobId);
        if (rs == 1) {
            return "success";
        } else {
            return "no";
        }
    }

}