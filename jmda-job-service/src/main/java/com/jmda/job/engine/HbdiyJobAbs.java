package com.jmda.job.engine;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;

import com.jmda.erp.common.dao.table.mapper.CuJobLogsPOMapper;
import com.jmda.erp.common.dao.table.mapper.CuJobPOMapper;
import com.jmda.erp.common.model.po.CuJobLogsPO;
import com.jmda.erp.common.model.po.CuJobPO;
import com.jmda.erp.common.model.po.CuJobPOExample;
import com.jmda.job.repository.service.CuJobLogsService;
import com.jmda.platform.commom.SequenceService;
import com.jmda.platform.commom.SpringContextUtil;

public abstract class HbdiyJobAbs implements Job{
	Logger logger = org.slf4j.LoggerFactory.getLogger(HbdiyJobAbs.class);
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		//写入数据库 开始执行什么
		SequenceService seq=SpringContextUtil.getBean(SequenceService.class);
		CuJobPOMapper cuJobPOMapper =SpringContextUtil.getBean(CuJobPOMapper.class);
		try {
			CuJobPOExample example = new CuJobPOExample();
			example.createCriteria().andImpClassPathEqualTo(super.getClass().getName());
			List<CuJobPO> list = cuJobPOMapper.selectByExample(example);
			if(list!=null&&list.size()==1){
				if(!"2".equals(list.get(0).getRunStatus())){
					
					CuJobLogsPOMapper cuJobLogsPOMapper =SpringContextUtil.getBean(CuJobLogsPOMapper.class);
					CuJobPO record = new CuJobPO();
					record.setRunStatus("2");
					CuJobPOExample cuJobPOExample = new CuJobPOExample();
					cuJobPOExample.createCriteria().andImpClassPathEqualTo(super.getClass().getName());
					cuJobPOMapper.updateByExampleSelective(record, cuJobPOExample);
					
					
					//cuJobLogsService.insert(cuJobLogsPO);
					//logger.info("时间  {} 订单状态设置为已完成程序开始", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					logger.info("时间"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"开始执行定时任务："+super.getClass().getName());
					boolean isOk=executeHbdiyJob(context);
					String key =  seq.getUUID();
					CuJobLogsPO cuJobLogsPO = new CuJobLogsPO();
					Long start = System.currentTimeMillis();
					Long end = System.currentTimeMillis();
					cuJobLogsPO.setStartTime(start);
					cuJobLogsPO.setEndTime(end);
					cuJobLogsPO.setJobLogsId(key);
					cuJobLogsPO.setImpClassPath(super.getClass().getName());
					if(isOk){
						cuJobLogsPO.setJobStatus("1");
					}else{
						cuJobLogsPO.setJobStatus("0");
					}
					cuJobLogsPOMapper.insertSelective(cuJobLogsPO);
					record.setRunStatus("0");
					CuJobPOExample cuJobPOExample_ = new CuJobPOExample();
					cuJobPOExample_.createCriteria().andImpClassPathEqualTo(super.getClass().getName());
					cuJobPOMapper.updateByExampleSelective(record, cuJobPOExample_);
					logger.info("时间"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"结束执行定时任务："+super.getClass().getName());
				}

			}
		} catch (HbdiyJobException e) {
			// TODO Auto-generated catch block
			CuJobLogsPO cuJobLogsPO = new CuJobLogsPO();
			Long start = System.currentTimeMillis();
			Long end = System.currentTimeMillis();
			cuJobLogsPO.setStartTime(start);
			cuJobLogsPO.setEndTime(end);
			cuJobLogsPO.setJobLogsId(seq.getUUID());
			cuJobLogsPO.setImpClassPath(super.getClass().getName());
			cuJobLogsPO.setJobStatus("0");
			CuJobLogsService cuJobLogsService =SpringContextUtil.getBean(CuJobLogsService.class);
			cuJobLogsService.insert(cuJobLogsPO);
			logger.info("时间"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"结束执行定时任务："+super.getClass().getName()+"异常：HbdiyJobException");
			JobExecutionException e2 =    new JobExecutionException(e);  
		    e2.setUnscheduleAllTriggers(true);  
		    throw e2;  
		}catch(RuntimeException e){
			CuJobLogsPO cuJobLogsPO = new CuJobLogsPO();
			Long start = System.currentTimeMillis();
			Long end = System.currentTimeMillis();
			cuJobLogsPO.setStartTime(start);
			cuJobLogsPO.setEndTime(end);
			cuJobLogsPO.setJobLogsId(seq.getUUID());
			cuJobLogsPO.setImpClassPath(super.getClass().getName());
			cuJobLogsPO.setJobStatus("0");
			CuJobLogsService cuJobLogsService =SpringContextUtil.getBean(CuJobLogsService.class);
			cuJobLogsService.insert(cuJobLogsPO);
			logger.info("时间"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"结束执行定时任务："+super.getClass().getName()+"异常：RuntimeException");
			JobExecutionException e2 =    new JobExecutionException(e);  
		    e2.setUnscheduleAllTriggers(true);  
		    throw e2;  
		}
		//写入数据库 执行是否成功
	}
	public abstract boolean executeHbdiyJob(JobExecutionContext context)throws HbdiyJobException;
}
