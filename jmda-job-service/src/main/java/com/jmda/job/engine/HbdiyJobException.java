package com.jmda.job.engine;

public class HbdiyJobException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HbdiyJobException(String message, Throwable cause) {
		super(message, cause);
	}

	public HbdiyJobException(Throwable cause) {
		super(cause);
	}

	public HbdiyJobException(String message) {
		super(message);
	}
}
