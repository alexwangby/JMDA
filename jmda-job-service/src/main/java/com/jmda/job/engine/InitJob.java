package com.jmda.job.engine;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

public class InitJob {

	public InitJob() {
	};

	private static SchedulerFactory sf = null;

	public static synchronized SchedulerFactory getSchedulerFactory() {
		if (sf == null) {
			sf = new StdSchedulerFactory();
		}
		return sf;
	}

	// Scheduler：代表一个Quartz的独立运行容器，Trigger和JobDetail可以注册到Scheduler中
	// 两者在Scheduler中拥有各自的组及名称，组及名称是Scheduler查找定位容器中某一对象的依据
	// Scheduler定义了多个接口方法，允许外部通过组及名称访问和空值容器中Trigger和JobDetail
	// 由SchedulerFactory创建
	public static Scheduler GetTimerScheduler(String className, String id, String cron) {
		Scheduler sched = null;
		// Job是一个接口有一个execute(JobExecutionContext context)
		// 开发者实现该接口定义运行任务，JobExecutionContext类提供了调度上下文的各种信息

		// JobDetail：Quartz在每次执行Job时，都会重新创建一个Job实例，所以他不直接接受一个Job实例，相反它接受一个Job实现类
		// 已便运行时，通过newInstance()的反射机制实例化Job，因此需要通过一个类来描述Job的实现类及其他相关的静态信息
		JobDetail job = null;
		try {
			sched = getSchedulerFactory().getScheduler();
			try {

				job = newJob(Class.forName(className).asSubclass(org.quartz.Job.class)).withIdentity(id, "jmda_jobtimer").build();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			CronTrigger trigger = newTrigger().withIdentity(id, "jmda_jobtimer").withSchedule(cronSchedule(cron)).build();
			sched.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}

		return sched;
	}
}
