package com.jmda.cache.redis.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jmda.cache.redis.service.CuCacheService;
import com.jmda.erp.common.model.po.CuCachePO;
import com.jmda.platform.commom.redis.JedisCacheException;

@Controller
@RequestMapping("/redis/CuCache")
public class CuCacheController {
    @Resource
    CuCacheService service;

    @RequestMapping(value = "/save", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String save(CuCachePO record, HttpServletRequest request) throws JedisCacheException {
        //  如果有子表数据时可通过如下例子进行json数据转换为 po 对象
        if (service.getCuCachePO(record.getCacheId()) != null) {
            service.update(record);
        } else {
            service.insert(record);
        }
        service.reloadAll(record.getCacheId());
        return "ok";
    }

    @RequestMapping(value = "/deleteDatas", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String deleteDatas(String ids) {
        service.deleteDatas(ids);
        String[] idArray = ids.split(",");
        for (int i = 0; i < idArray.length; i++) {
            service.deleteAll(idArray[i]);
        }
        return "ok";
    }

    @RequestMapping(value = "/reloadAll", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String reloadAll(String cacheId) {
        try {
            service.reloadAll(cacheId);
        } catch (JedisCacheException e) {
            e.printStackTrace();
            return "error";
        }
        return "ok";
    }

    @RequestMapping(value = "/reloadAllCache", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String reloadAllCache() {
        try {
            service.reloadAllCache();
        } catch (JedisCacheException e) {
            e.printStackTrace();
            return "error";
        }
        return "ok";
    }

    @RequestMapping(value = "/deleteAll", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String deleteAll(String cacheId) {
        service.deleteAll(cacheId);
        return "ok";
    }
}
