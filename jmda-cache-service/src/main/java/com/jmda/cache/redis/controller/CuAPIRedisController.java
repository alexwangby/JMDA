package com.jmda.cache.redis.controller;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jmda.cache.redis.service.CuCacheService;
import com.jmda.platform.commom.redis.JedisKeyValueAPI;
import com.jmda.platform.commom.redis.JedisCacheException;

@Controller
@RequestMapping("/api/redis")
public class CuAPIRedisController {
    @Resource
    CuCacheService service;
    private long sleepTimes = 5000;

    @RequestMapping(value = "/reloadAll", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String reloadAll(String cacheId) {
        try {
            Thread.sleep(sleepTimes);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        try {
            service.reloadAll(cacheId);
        } catch (JedisCacheException e) {
            e.printStackTrace();
            return "error";
        }
        return "ok";
    }

    @RequestMapping(value = "/reloadAllCache", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String reloadAllCache() {
        try {
            service.reloadAllCache();
        } catch (JedisCacheException e) {
            e.printStackTrace();
            return "error";
        }
        return "ok";
    }

    @RequestMapping(value = "/reloadOne", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String reloadOne(String cacheId, String id) {
        try {
            Thread.sleep(sleepTimes);
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        String value = JedisKeyValueAPI.hget(cacheId, id);
        if (value != null) {
            try {
                service.updateOne(cacheId, id);
            } catch (JedisCacheException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "error";
            }
        } else {
            try {
                service.addOne(cacheId, id);
            } catch (JedisCacheException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "error";
            }
        }
        return "ok";
    }

    @RequestMapping(value = "/deleteAll", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String deleteAll(String cacheId) {
        try {
            Thread.sleep(sleepTimes);
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        service.deleteAll(cacheId);
        return "ok";
    }

    @RequestMapping(value = "/deleteOne", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String deleteOne(String cacheId, String id) {
        try {
            Thread.sleep(sleepTimes);
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            service.deleteOne(cacheId, id);
        } catch (JedisCacheException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "error";
        }
        return "ok";
    }
}
