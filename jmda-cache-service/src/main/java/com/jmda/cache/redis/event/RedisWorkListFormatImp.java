package com.jmda.cache.redis.event;


import com.jmda.cache.redis.service.CuCacheService;
import com.jmda.platform.engine.worklist.event.WorkListFormatEvent;
import com.jmda.platform.repository.worklist.model.ExtColumnModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service
public class RedisWorkListFormatImp implements WorkListFormatEvent {
    @Resource
    CuCacheService cachaService;

    @Override
    public void formatColumnModel(List<ExtColumnModel> list) {
        // TODO Auto-generated method stub
        ExtColumnModel colCacheCount = new ExtColumnModel();
        colCacheCount.setColumn_align("center");
        colCacheCount.setColumnName("cacheDatas");
        colCacheCount.setHidden(false);
        colCacheCount.setTitle("缓存数据");
        colCacheCount.setFormatter("cacheCountFormat");
        colCacheCount.setWidth(new BigDecimal(0.05));
        list.add(colCacheCount);
        ExtColumnModel colCacheEdit = new ExtColumnModel();
        colCacheEdit.setColumn_align("center");
        colCacheEdit.setColumnName("cacheOperator");
        colCacheEdit.setHidden(false);
        colCacheEdit.setTitle("操作");
        colCacheEdit.setWidth(new BigDecimal(0.12));
        colCacheEdit.setFormatter("cacheOperatorFormat");
        list.add(colCacheEdit);
    }

    @Override
    public void formatRecords(List<ExtColumnModel> columnList, List<Map<String, Object>> records) {
        // TODO Auto-generated method stub
        for (Map<String, Object> record : records) {
            String cacheId = (String) record.get("CACHE_ID");
            if (cacheId != null && cacheId.trim().length() > 0) {
                int cachaQuantity = cachaService.getCacheQuantity(cacheId);
                record.put("cacheDatas", "共[" + cachaQuantity + "]条数据");
                record.put("cacheOperator", "cacheOperator");
            }
        }
    }
}
