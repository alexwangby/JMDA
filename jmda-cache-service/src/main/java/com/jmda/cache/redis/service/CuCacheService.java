package com.jmda.cache.redis.service;


import com.jmda.erp.common.model.po.CuCachePO;
import com.jmda.platform.commom.redis.JedisCacheException;

import org.springframework.stereotype.Service;

@Service
public interface CuCacheService {

    //   写入一条数据
    void insert(CuCachePO record);

    //   通过主键ID批量删除表中数据
    void deleteDatas(String ids);

    //   通过主键修改 表中数据
    void update(CuCachePO record) throws JedisCacheException;

    //   通过主键读取表中数据
    CuCachePO getCuCachePO(String id) throws JedisCacheException;

    void reloadAll(String cacheId) throws JedisCacheException;

    boolean reloadAllCache() throws JedisCacheException;

    void addOne(String cacheId, String id) throws JedisCacheException;

    void deleteOne(String cacheId, String id) throws JedisCacheException;

    void updateOne(String cacheId, String id) throws JedisCacheException;

    void deleteAll(String cacheId);

    int getCacheQuantity(String cacheId);
}
