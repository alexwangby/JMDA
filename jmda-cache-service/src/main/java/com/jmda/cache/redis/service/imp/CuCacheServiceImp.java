package com.jmda.cache.redis.service.imp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.cache.redis.service.CuCacheService;
import com.jmda.erp.common.model.po.CuCachePO;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.commom.redis.JedisKeyValueAPI;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.commom.redis.JedisCacheException;
import com.jmda.platform.commom.redis.JedisHashMapBean;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ServletContextAware;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CuCacheServiceImp extends SqlSessionDaoSupport implements CuCacheService {
	private static String nameSpace = "com.jmda.erp.common.dao.table.mapper.CuCachePOMapper.";
	private static Map<String, Integer> _syncMap = new HashMap<String, Integer>();
	private static Object _sync = new Object();
	@Resource
	DataBaseService db;

	@Transactional
	// 表中写入一条数据
	public void insert(CuCachePO record) {
		super.getSqlSession().insert(nameSpace + "insertSelective", record);
	}

	@Transactional
	// 通过主键ID批量删除mm_goods 表中数据
	public void deleteDatas(String ids) {
		String[] idArray = ids.split(",");
		for (int i = 0; i < idArray.length; i++) {
			String id = idArray[i];
			super.getSqlSession().delete(nameSpace + "deleteByPrimaryKey", id);
		}
	}

	@Transactional
	// 通过主键修改表中数据
	public void update(CuCachePO record) {
		super.getSqlSession().update(nameSpace + "updateByPrimaryKeySelective", record);
		try {
			reloadAll(record.getCacheId());
		} catch (JedisCacheException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}

	// 通过主键读取表中数据
	public CuCachePO getCuCachePO(String id) {
		return super.getSqlSession().selectOne(nameSpace + "selectByPrimaryKey", id);
	}

	public int getCacheQuantity(String cacheId) {
		return _syncMap.get(cacheId);
	}

	@PostConstruct
	public void init() throws JedisCacheException {
		_syncMap = new HashMap<>();
		JedisKeyValueAPI.delete("cu_host", "redis.api.host");
		JedisKeyValueAPI.hset("cu_host", "redis.api.host", LocalCacheManager.jmdaPropertiesMap.get("redis.api.host"));
		List<CuCachePO> list = super.getSqlSession().selectList(nameSpace + "selectByExample");
		synchronized (_sync) {
			for (CuCachePO cuCachePO : list) {
				reloadAll(cuCachePO);
			}
		}
	}

	public boolean reloadAllCache() throws JedisCacheException {
		init();
		return true;
	}

	public void reloadAll(String cacheId) throws JedisCacheException {
		CuCachePO cuCachePO = getCuCachePO(cacheId);
		reloadAll(cuCachePO);
	}

	public void reloadAll(CuCachePO cuCachePO) throws JedisCacheException {
		if (cuCachePO != null && cuCachePO.getCacheSql() != null) {
			JedisKeyValueAPI.delete(cuCachePO.getCacheId());
			String sql = "select count(*) as c  from (" + cuCachePO.getCacheSql() + ") as c";
			
			try {
				int sum = db.getValue(sql, "cc", null).toInt();
			int n = 15000;
			int start = 0;
			int m = sum / n;
			long startTime = System.currentTimeMillis();
			System.out.println("【START】开始加载Redis缓存 [" + cuCachePO.getName() + "],待加载数据共" + sum + "条");
			for (int k = 0; k < m + 1; k++) {
				List<JedisHashMapBean> redisHashMapBeanList = new ArrayList<>();
				int count = start;
				String querySql = cuCachePO.getCacheSql() + " limit " + start + " , " + n;
				List<Map<String, Object>> records = db.queryList(querySql, null);
				if (records != null) {
					for (Map<String, Object> record : records) {
						String field = String.valueOf(record.get(cuCachePO.getFieldColumn()));
						if (field != null && field.trim().length() > 0) {
							try {
								ObjectMapper mapper = new ObjectMapper();
								String value = mapper.writeValueAsString(record);
								JedisHashMapBean redisHashMapBean = new JedisHashMapBean(cuCachePO.getCacheId(), field,
										value, 0);
								redisHashMapBeanList.add(redisHashMapBean);
								count++;
							} catch (JsonProcessingException e) {
								e.printStackTrace();
								System.out.println("ID为" + cuCachePO.getCacheId() + "的查询sql值转换JSON失败！");
							}
						}
					}
					_syncMap.put(cuCachePO.getCacheId(), count);
				}
			
				long cEndTime = System.currentTimeMillis();
				System.out.println("        已完成" + count + "条,已耗时" + (cEndTime - startTime) / 1000 + "秒");
				JedisKeyValueAPI.hsetByPipeline(redisHashMapBeanList);
				start = (k + 1) * n;
				long endTime = System.currentTimeMillis();
				System.out.println("【END】加载Redis缓存 [" + cuCachePO.getName() + "]完成,共加载["
						+ _syncMap.get(cuCachePO.getCacheId()) + "]条数据" + ",共耗时" + (endTime - startTime) / 1000 + "秒");
			}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}

	public void addOne(String cacheId, String id) throws JedisCacheException {
		CuCachePO cuCachePO = getCuCachePO(cacheId);
		ObjectMapper mapper = new ObjectMapper();
		System.out.println("正在执行添加缓存,key[" + cacheId + "],id[" + id + "]...");
		if (cuCachePO != null && cuCachePO.getCacheSql() != null) {
			String sql = "select * from (" + cuCachePO.getCacheSql() + ") t where t." + cuCachePO.getFieldColumn() + " = ?";
			try {
				Map<String, Object> record = db.query(sql, new Object[] { id });
				if (record != null) {
					String field = String.valueOf(record.get(cuCachePO.getFieldColumn()));
					if (field != null && field.trim().length() > 0) {
						synchronized (_sync) {
							try {
								String value = mapper.writeValueAsString(record);
								JedisKeyValueAPI.hset(cuCachePO.getCacheId(), field, value);
								int count = _syncMap.get(cuCachePO.getCacheId());
								_syncMap.put(cuCachePO.getCacheId(), (count + 1));
								System.out.println("完成添加缓存,key[" + cacheId + "],id,[" + id + "]...");
							} catch (JsonProcessingException e) {
								e.printStackTrace();
							}
						}
					}
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void deleteAll(String cacheId) {
		System.out.println("正在执行删除缓存,key[" + cacheId + "]...");
		synchronized (_sync) {
			System.out.println("完成删除缓存,key[" + cacheId + "]...");
			JedisKeyValueAPI.delete(cacheId);
			_syncMap.remove(cacheId);
		}
	}

	public void deleteOne(String cacheId, String id) throws JedisCacheException {
		System.out.println("正在执行删除缓存,key[" + cacheId + "],id[" + id + "]...");
		synchronized (_sync) {
			JedisKeyValueAPI.delete(cacheId, id);
			int count = _syncMap.get(cacheId);
			_syncMap.put(cacheId, (count - 1));
			System.out.println("完成删除缓存,key[" + cacheId + "],id[" + id + "]...");
		}
	}

	public void updateOne(String cacheId, String id) throws JedisCacheException {
		CuCachePO cuCachePO = getCuCachePO(cacheId);
		ObjectMapper mapper = new ObjectMapper();
		System.out.println("正在执行更新缓存,key[" + cacheId + "],id[" + id + "]...");
		if (cuCachePO != null && cuCachePO.getCacheSql() != null) {
			String sql = "select * from (" + cuCachePO.getCacheSql() + ") t where t." + cuCachePO.getFieldColumn() + " = ?";
			Map<String, Object> record=null;
			try {
				record = db.query(sql, new Object[] { id });
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (record != null) {
				String field = String.valueOf(record.get(cuCachePO.getFieldColumn()));
				if (field != null && field.trim().length() > 0) {
					synchronized (_sync) {
						try {
							String value = mapper.writeValueAsString(record);
							JedisKeyValueAPI.delete(cuCachePO.getCacheId(), field);
							JedisKeyValueAPI.hset(cuCachePO.getCacheId(), field, value);
							System.out.println("完成更新缓存,key[" + cacheId + "],id[" + id + "]...");
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}


}
