// grid和布局的全局变量
var sm;
var cm;
var store;
var grid;
var viewport;

Ext.onReady(function() {
	Ext.QuickTips.init();

	/* 表格 */
	var sm = new Ext.grid.CheckboxSelectionModel();

	cm = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(), sm, {
		header : '列表名称',
		dataIndex : 'NAME',
		width : 100
	}, {
		header : '操作',
		dataIndex : 'OPERATOR',
		align : 'center',
		dataIndex : 'OPERATOR',
		width : 30
	}, {
		header : 'ID',
		dataIndex : 'ID',
		width : 100
	} ]);
	var Plant = Ext.data.Record.create([ {
		name : 'NAME',
		type : 'string'
	}, {
		name : 'OPERATOR',
		type : 'string'
	}, {
		name : 'ID',
		type : 'string'
	} ]);

	store = new Ext.data.Store({
		baseParams : {
			applicationId : applicationId
		},
		proxy : new Ext.data.HttpProxy({
			url : basePath + 'repository/worklist/getWorkListGridJson'
		}),
		reader : new Ext.data.JsonReader({
			totalProperty : "total",
			root : "root",
			id : "ID"
		}, [ 'NAME', 'OPERATOR', 'ID' ], Plant)
	});
	grid = new Ext.grid.GridPanel({
		region : 'grid',
		title : '列表模型',
		store : store,
		region : 'center',
		cm : cm,
		sm : sm,
		trackMouseOver : false,
		loadMask : {
			msg : '正在加载数据，请稍候...'
		},
		border : false,
		viewConfig : {
			forceFit : true
		},
		tbar : [ {
			text : '刷新',
			tooltip : '刷新',
			handler : function() {
				store.reload();
			},
			iconCls : 'iconRefresh'
		}, '-', {
			text : '新建',
			iconCls : 'iconNew',
			handler : function() {
				add();
			}
		}, '-', {
			text : '删除',
			iconCls : 'iconDelete',
			id : "deleteC",
			disabled : true,
			handler : function() {
				remove();
			}
		} ]
	});

	viewport = new Ext.Viewport({
		layout : 'border',
		items : [ grid ]
	});
	store.load();
	grid.getSelectionModel().on('selectionchange', function(t) {
		Ext.getCmp("deleteC").setDisabled(t.getSelections().length === 0);
	});
});

function add() {
	parent.openSimpleExtWindow('新建列表', 500, 270, basePath + "repository/application/getCreatePage?applicationId=" + applicationId + "&typeName=数据列表");
	if (parent.activeDialog) {
		parent.gridStore = store;
		parent.activeDialog.purgeListeners();
		parent.activeDialog.addListener('show', parent.handleShow);
		parent.activeDialog.addListener('beforehide', parent.handleHide);
		parent.activeDialog.show();
	}
}
function remove() {
	var selectedKeys = grid.selModel.selections.keys;
	if (selectedKeys.length > 0) {
		Ext.MessageBox.confirm('提示', '确定删除选定的列表模型?', function(btn) {
			if (btn == 'yes') {
				Ext.Ajax.request({
					url : basePath + 'repository/worklist/remove',
					method : 'POST',
					params : {
						ids : selectedKeys
					},
					failure : function(response, options) {
						Ext.MessageBox.alert('操作失败', '操作失败！可能是网络超时！');
						return false;
					},
					success : function(response, options) {
						if (response.responseText == 'ok') {
							Ext.MessageBox.alert('提示', " 删 除 成 功! ");
							store.reload();
							parent.regreshNodeById('LIST_' + applicationId);
						}
					}
				});
			}

		});
	} else {
		Ext.MessageBox.alert('提示', '请选择要删除的模型库');
	}
}
function openWorklist(title, id) {
	parent.createViewTab('second_floor_form', title, '', basePath + 'repository/worklist/openWorkList?worklistid=' + id, true);
}
