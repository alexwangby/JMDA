// grid和布局的全局变量
var sm;
var cm;
var store;
var grid;
var viewport;

Ext.onReady(function() {
	Ext.QuickTips.init();

	/* 表格 */
	var sm = new Ext.grid.CheckboxSelectionModel();

	cm = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(), sm, {
		header : '名称',
		dataIndex : 'NAME',
		menuDisabled : true,
		width : 250
	}, {
		header : '操作',
		dataIndex : 'OPERATOR',
		fixed : true,
		menuDisabled : true,
		width : 150
	}, {
		header : 'ORDER_INDEX',
		dataIndex : 'ORDER_INDEX',
		hidden:true,
		width : 150
	} ]);
	var Plant = Ext.data.Record.create([ {
		name : 'NAME',
		type : 'string'
	}, {
		name : 'OPERATOR',
		type : 'string'
	}, {
		name : 'ORDER_INDEX',
		type : 'int'
	}  ]);

	store = new Ext.data.Store(
			{
				// load using HTTP
				proxy : new Ext.data.HttpProxy({
					url : basePath+'repository/application/getApplicationGridJson'
				}),
				reader : new Ext.data.JsonReader({	
					 totalProperty: "total",   
   				 root: "root",               
  				   id: "ID" 
				},
				['NAME','OPERATOR','ORDER_INDEX'], Plant)
			});
	grid = new Ext.grid.GridPanel({
		region : 'grid',
		title : '模型库',
		store : store,
		region : 'center',
		cm : cm,
		sm : sm,
		trackMouseOver : false,
		loadMask : {
			msg : '正在加载数据，请稍候...'
		},
		border : false,
		viewConfig : {
			forceFit : true
		},
		tbar : [ {
			text : '刷新',
			tooltip : '刷新',
			handler : function() {
				store.reload();
			},
			iconCls : 'iconRefresh'
		},'-', {
			text : '新建',
			iconCls : 'iconNew',
			handler : function() {
				add();
			}
		}, '-',{
			text : '删除',
			iconCls : 'iconDelete',
			id:"deleteC",
			disabled:true,
			handler : function() {
				remove();
			}
		} ]
	});

	viewport = new Ext.Viewport({
		layout : 'border',
		items : [ grid ]
	});
	grid.getSelectionModel().on('selectionchange', function(t) {
		Ext.getCmp("deleteC").setDisabled(t.getSelections().length === 0);
	});
	store.load();
});

function eidt(uuid){
	parent.openSimpleExtWindow('编辑模型分类', 500, 270, basePath + "repository/application/getCreatePage?applicationId="+uuid+"&typeName=模型分类");
	if (parent.activeDialog) {
		parent.activeDialog.purgeListeners();
		parent.activeDialog.addListener('show', parent.handleShow);
		parent.activeDialog.addListener('beforehide', parent.handleHide);
		parent.activeDialog.show();
	}
}

function generatorCode(uuid){
	parent.openSimpleExtWindow('代码生成器', 700, 520, basePath + "repository/generator?applicationId="+uuid);
	if (parent.activeDialog) {
		parent.activeDialog.purgeListeners();
		parent.activeDialog.addListener('show', parent.handleShow);
		parent.activeDialog.addListener('beforehide', parent.handleHide);
		parent.activeDialog.show();
	}
}

function add(){
	//parent.openModuleStoreWin('');	
	parent.openSimpleExtWindow('新建模型分类', 500, 270, basePath + "repository/application/getCreatePage?applicationId=&typeName=模型分类");
	if (parent.activeDialog) {
		parent.activeDialog.purgeListeners();
		parent.activeDialog.addListener('show', parent.handleShow);
		parent.activeDialog.addListener('beforehide', parent.handleHide);
		parent.activeDialog.show();
	}
}
function remove(){
	var selectedKeys = grid.selModel.selections.keys;
	if(selectedKeys.length>0){
					 Ext.MessageBox.confirm('提示', '是否要删除模型库?', function(btn){
					 		if(btn=='yes'){
					 			
					 			Ext.Ajax.request({
											url : basePath+'repository/application/remove',
											method : 'POST',
											params : {
												ids : selectedKeys
											},// end params
											failure : function(response, options) {
												Ext.MessageBox.alert('操作失败', '操作失败！可能是网络超时！');
												return false;
											},// end failure block
											success : function(response, options) {
												if(response.responseText=='ok'){
													Ext.MessageBox.alert('操作提示', "删除成功!");
													store.reload();
													parent.refreshCurrNode();
												}else if(response.responseText=='workList'){
													Ext.MessageBox.alert('操作提示', "数据列表不为空,不允许删除!");
												}else if(response.responseText=='formList'){
													Ext.MessageBox.alert('操作提示', "表单列表不为空,不允许删除!");
												}
											}
										});
					 		}
					 		
					 	});
		}else{
			Ext.MessageBox.alert('提示', '请选择要删除的模型库');
		}
}