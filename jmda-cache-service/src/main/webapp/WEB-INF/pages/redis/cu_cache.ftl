<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>缓存表单</title>
    <!--公共资源配置开始-->
     <#include "../../comm/eformMeta.ftl" />
    <!--公共资源配置结束-->
    
    ${extMeta}
</head>
<body>
     <form id="data_form" method="post" action="" class="page-form">
      <div class="buts-row-top row ">
         	<div class="col-sm-12 text-align-left buts-col" >
         	 <button type="button" class="btn btn-success" onClick="saveData();"><i class="fa fa-check right"></i>保存</button>&nbsp;
        	</div>
        </div>
        <div class="but-row-top-shadow">
      </div>
        <div class="form-title">
            缓存表单
        </div>
        <div class="row">
            <div class="col-sm-10">
                <div class="form-group">
                	<label >缓存KEY</label>
                    <span class="input-icon icon-right">
                         ${cacheId}
                    </span>
                </div>
            </div>
            <div class="col-sm-10">
                <div class="form-group">
                	<label >名称</label>
                    <span class="input-icon icon-right">
                         ${name}
                    </span>
                </div>
            </div>
            <div class="col-sm-10">
                <div class="form-group">
                	<label >索引字段名称</label>
                    <span class="input-icon icon-right">
                         ${fieldColumn}
                    </span>
                </div>
            </div>
            <div class="col-sm-10">
                <div class="form-group">
                	<label >数据源</label>
                    <span class="input-icon icon-right">
                         ${dataSource}
                    </span>
                </div>
            </div>
            <div class="col-sm-10">
                <div class="form-group">
                	<label >查询SQL</label>
                    <span class="input-icon icon-right">
                         ${cacheSql}
                    </span>
                </div>
            </div>
            <div class="col-sm-10">
                <div class="form-group">
                	<label >缓存描述</label>
                    <span class="input-icon icon-right">
                         ${cacheDesc}
                    </span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
