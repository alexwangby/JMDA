/**
 * 保存按钮 保存当前表单数据
 *
 * @returns {Boolean}
 */
function saveData() {
    //  表单校验跟进JMDA配置的表单属性 为非空验证
    var isValid = $('#data_form').form('validate');
    if (isValid) {
    } else {
        jQuery.messager.alert('提示', '验证不通过');
        return false;
    }
    LoadModel.show();
    // 将表单数据提交到后台进行处理
    $("#data_form").form('submit', {
        url: basePath + "/redis/CuCache/save",
        dataType: 'text',
        success: function (data) {
            // 保存失败
            if (data == "no") {
                jQuery.messager.alert('提示', '保存失败!');
                LoadModel.hide();
            } else if (data == "ok") {
                // 保存成功
                jQuery.messager.alert('提示', '保存成功');
                // 刷新父窗体表格
                try {
                    parent.dataGridRefresh();
                } catch (e) {
                }
                LoadModel.hide();
                // 关闭当期表单窗口
                parent.DialogHelper.closeDialog();
            }

        }

    });

}

/**
 * 物料选择器 回调执行的函数
 *
 * @param datas
 *            选择器选择的数据集
 */
function example_selector(data) {
    // 遍历数据集将 数据集中的数据回写到当前表单中
    $("input[name='view_warehouseId']").val(data.NAME);
    $("input[name='warehouseId']").val(data.ID);
    // 关闭选择器
    SelectorHelper.closeSelector();
}
/**
 * 将子表中列显示为文本框输入
 * 注：需要将 table_id 替换为当前表格中的字段
 * @param datas
 *
 */
function example_imput_Formatter(value, row, index) {
    return jmdaInputFormat('table_id', value, row, index);
}
/**
 * 将子表中插入数据
 * 注：需要将row中的属性字段配置成表格中的字段
 * 注：需要就$('#dg') 中dg 配置成 表格ID
 * @param datas
 *
 */
function example_grid_add() {
    $('#dg').datagrid('insertRow', {
        index: 1,	// 索引从0开始
        row: {
            name: '新名称',
            age: 30,
            note: '新消息'
        }
    });
}