<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<#include "../../comm/worklistMeta.ftl" />
${extendJavascript}
    <style>
        .combo {
            white-space: nowrap;
            margin-top: -50px;
            margin-left: 90px;
        }
    </style>
    <script type="text/javascript">
        function clearDataGridSelected() {
            $('#data_table').datagrid('clearSelections');
        }
        function getDictionarySelections() {
            var row = $('#data_table').datagrid('getSelections');
            if (row == null || row == '' || row.length == 0) {
                jQuery.messager.alert('提示', '请选择数据！');
                return '';
            }
            if (row) {
                var rows = $('#data_table').datagrid('getSelections');

                return coverToJsonDatas(rows);
            }
        }
        function coverToJsonDatas(rows) {
            var json = [];
            for (var r = 0; r < rows.length; r++) {
                var row = rows[r];
                var rowIndex = $('#data_table').datagrid('getRowIndex', rows[r]);
                var param = '';
                var fields = $('#data_table').datagrid('getColumnFields');
                for (var i = 0; i < fields.length; i++) {
                    var filed_name = fields[i];
                    //判断是否存在 自定义输入事件
                    if ($('#hbdiy_grid_text_' + rowIndex + '_' + filed_name).length > 0) {
                        param += ',\"' + filed_name + '\":\"' + $('#hbdiy_grid_text_' + rowIndex + '_' + filed_name).val() + '\"';
                    } else {
                        var value = String(row[filed_name]).replace(new RegExp('"', 'g'), '”');
                        param += ',\"' + filed_name + '\":\"' + value + '\"';
                    }

                }
                if ($('#data_table').attr('singleSelect') == 'true') {
                    //console.log(eval('({' + param.substring(1, param.length) + '})'))
                    return eval('({' + param.substring(1, param.length) + '})');
                } else {
                    json.push(eval('({' + param.substring(1, param.length) + '})'));
                }
            }
            return json;
        }

        <#if fields?? >
            <#list fields as g_field>
                <#if g_field.formatter=='numberbox'>
                function hbdiy${g_field.column}Format(value, row, index) {
                    return '<input id="hbdiy_grid_text_' + index + '_${g_field.column}" type="text" class="easyui-numberbox"  style="ime-mode: disabled; width: 93%; height: 16px; line-height: 20px;" value="" hbdiyType="dictionary_input" hbdiyOption="{\'column\':\'${g_field.column}\',\'index\':' + index + '}\" />';
                }
                </#if>
            </#list>
        </#if>

    </script>
    <title>${title}</title>
</head>
<body class="easyui-layout" data-options="fit:true">

<#if tree!=''>
<div data-options="region:'west',split:true" style="height:100%;width: 20%">
    <ul id="tree"></ul>
</div>
<div data-options="region:'center',split:true" style="height:100%;width:80%">
<#else>
<div data-options="region:'center',split:true" style="height:100%;width:100%">
</#if>
    <!-- id="datagrid_toolbar" 必填  -->
    <div id="datagrid_toolbar">
        <form id="search_form" class="search-form" role="form">
            <div class="row">
            <#if conditions?exists>
                <#list conditions as condition>
                    <div class="hb-group col-sm-3 no-padding" id="div_${condition.column}" style="display:${condition.display}">
                        <label class="col-sm-4 hb-label">${condition.title}</label>
                        <#if condition.ui!=''> ${condition.ui}
                        <#else> <#if condition.defaultValue!=''>
                            <div class="col-sm-8 hb-control"><input type="text" id="search_${condition.column}" name="search_" value="${condition.defaultValue}" on disabled/></div>
                        <#else>
                            <div class="col-sm-8 hb-control"><input type="text" class="form-control" id="search_${condition.column}" name="search_" value=""/></div>
                        </#if> </#if>
                    </div>
                </#list>
                <#if tree?? >
                    <#if tree!=''>
                        <input type="hidden" class="form-control" id="search_nodeIds" name="search_nodeIds" value=""/>
                    </#if>
                </#if>
                <div class="col-sm-3 form-buttons float-right " style="margin-top: 6px;">
                     <a class="btn btn-sm  btn-default  float-right margin-left-10 " href="javascript:resetForm('search_form', 'data_table')">
                            <i class="btn-label fa  fa-reply "></i>重 置</a>
                    <a class="btn btn-sm btn-blue float-right " href="javascript:workListSearch('data_table', 'search_form');">
                        <i class="btn-label fa fa-search "></i>查 询
                    </a>
                    <div class="clearfix"></div>
                </div>
            </#if>
            </div>

            <input type="text" value="" style="display:none"/>
        </form>
    </div>

    <!-- id="data_table" 必填  -->
    <table id="data_table" class="easyui-datagrid" style="width: 100%"
           data-options="singleSelect:true,url:'./getDatas?id=${id}', toolbar:'#datagrid_toolbar'" pagination="true" rownumbers="true"
    <#if fitColumns=="false" > fitColumns="false" <#else> fitColumns="true" </#if>
           fit="true" <#if multiple=="false" > singleSelect="true" <#else> singleSelect="false" selectOnCheck="true"
           checkOnSelect="true" </#if>
    >
        <thead>
        <tr>
            <th data-options="field:'ck',checkbox:true">选择</th>
        <#if fields?? >
            <#list fields as g_field>
                <th data-options="field:'${g_field.column}',
							<#if g_field.sortable=='true'>
								sortable:true,
							</#if>
							<#if g_field.formatter?? >
								formatter:hbdiy${g_field.column}Format,
							</#if>
							<#if g_field.align?? >
								align:'${g_field.align}'
							</#if> "
                    width='${g_field.width}'
                    <#if g_field.hidden=='true'>
                    hidden='true'
                    </#if>

                >${g_field.title}</th>
            </#list>
        </#if>
        </tr>
        </thead>
    </table>
</div>
    <script>
        <#if tree?? >
            <#if tree!=''>
            var treedata =${treeData};
            $('#tree').tree({
                data: treedata
            });
            $('#tree').tree({
                onClick: function (node) {
                    children = $('#tree').tree('getChildren', node.target);
                    var nodeIds = new Array();
                    for (j = 0; j < children.length; j++) {
                        nodeIds.push(children[j].id);
                    }
                    nodeIds.push(node.id);
                    var nodeIdsStr = nodeIds.join(',');
                    console.log(nodeIdsStr);
                    if(nodeIdsStr=='14452B1EA1244EDD90DEFFF60E42ED27'){
                   		var d1 = document.getElementById("div_drawingName"); 
                   		if(d1==null){
                   		}else{
                    	d1.style.display="block";
                   		}
                    	var d2 = document.getElementById("div_sizeName"); 
                    	if(d2==null){
                   		}else{
                   		d2.style.display="block";
                   		}
                    	var d3 = document.getElementById("div_colourName"); 
                    	if(d3==null){
                   		}else{
                    	d3.style.display="block";
                   		}
                    }else{
                  		var d1 = document.getElementById("div_drawingName"); 
                   		if(d1==null){
                   		}else{
                    	d1.style.display="none";
                   		}
                    	var d2 = document.getElementById("div_sizeName"); 
                    	if(d2==null){
                   		}else{
                   		d2.style.display="none";
                   		}
                    	var d3 = document.getElementById("div_colourName"); 
                    	if(d3==null){
                   		}else{
                    	d3.style.display="none";
                   		}
                    }
                    $("#search_nodeIds").val(nodeIdsStr);
                    workListSearch('data_table', 'search_form');
                }
            });
            </#if>
        </#if>

    </script>
</body>
</html>