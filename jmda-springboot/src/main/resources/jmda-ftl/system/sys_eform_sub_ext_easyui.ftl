<div class="form-title">${g_title}
    <a class="icon-only float-right blue btn-min" href="javascript:void(0);" onclick="showHideFormPart(this)">
        <i class="fa fa-minus"></i>
    </a>
</div>
<div class="row">
    <div class="col-sm-12">
        <div id="${xieyin_code}_datagrid_toolbar">
            <div class="buttons-preview" style="padding:10px 10px 0px 10px">
            <#if buttons?exists>
                <#list buttons as button>
                    <#if button.customHtml?length lt 1>
                        <a class="${button.iconCls}" href="#" onClick="${button.handler};"><#if button.iconClsChild?exists> <i class="${button.iconClsChild}"></i><#else><i class="fa fa-plus"></i></#if>${button.title}</a>
                    <#else> ${button.customHtml}
                    </#if>
                </#list>
            </#if>
            </div>
        </div>
        <table id="${xieyin_code}_data_table" class="easyui-datagrid" border="0" rownumbers="true" fitColumns="true"
               data-options="singleSelect:false,
		            collapsible:true,url:'${datas_url}',
		            method:'get',
		            striped: true,
		            onLoadSuccess:function(data){
		                try{
		                  ${xieyin_code}_onLoadSuccess(data);
		                }catch(e){}
		             },
		             onClickCell:function (rowIndex, field, value){
		             	try{
		                  ${xieyin_code}_onClickCell(rowIndex, field, value);
		                }catch(e){}
		             },
		             onDblClickRow:function (rowIndex, rowData){
		             	try{
		                  ${xieyin_code}_onDblClickRow(rowIndex, rowData);
		                }catch(e){}
		             },
		             ${groupHtml}
		            toolbar:'#${xieyin_code}_datagrid_toolbar'">
            <thead>
            <tr>
                <th data-options="field:'check',checkbox:true">选择</th> <#if columns?exists > <#list columns as column>
                <th data-options="field:'${column.column_lable}<#if column.ref_type!="">_</#if>',width:width(0.1)<#if column.hidden==true>,hidden:true</#if><#if column.sortable==true>,sortable:true</#if><#if column.column_align?if_exists>,align:'${column.column_align}'</#if><#if column.formatter?if_exists>,formatter:${column.formatter}</#if>">${column.column_title}</th> </#list> </#if>
            </tr>
            </thead>
        </table>
    </div>
</div>
<input type="hidden" name="${xieyin_code}" id="${xieyin_code}" value=""/>
