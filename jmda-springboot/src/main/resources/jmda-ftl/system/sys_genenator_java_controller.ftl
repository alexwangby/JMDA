package ${project_path}.${model.applicationId}.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fms.common.model.po.${model.javaClass}PO;
import ${project_path}.${model.applicationId}.service.${model.javaClass}Service;

@Controller
@RequestMapping("/${request_mapper}/${model.javaClass}")
public class ${model.javaClass}Controller {

	@Autowired
	${model.javaClass}Service service;
	
	@RequestMapping(value = "/save", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String save(${model.javaClass}PO record,HttpServletRequest request) {
			//TODO 如果有子表数据时可通过如下例子进行json数据转换为 po 对象
			/**
			    ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			 try {
				List<WmStockInListPO> list  = mapper.readValue(jsondatas, new TypeReference<ArrayList<WmStockInListPO>>() {});
				System.out.println(list.size());
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 */
			 
		return "ok";
	}

	@RequestMapping(value = "/deleteDatas", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String deleteDatas(String ids) {
		service.deleteDatas(ids);
		return "ok";
	}
}
