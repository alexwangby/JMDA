package ${project_path}.${model.applicationId}.service.imp;

import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fms.common.model.po.${model.javaClass}PO;
import com.fms.common.dao.table.mapper.${model.javaClass}POMapper;
import ${project_path}.${model.applicationId}.service.${model.javaClass}Service;

@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
@Service
public class ${model.javaClass}ServiceImp extends SqlSessionDaoSupport implements ${model.javaClass}Service {
	// 由系统工具生成的数据库表的DAO对象文件 可参照metadata工程相关代码
	@Autowired
	${model.javaClass}POMapper mapper;

	// TODO 表中写入一条数据
	@Override
	public void insert(${model.javaClass}PO record) {
		mapper.insertSelective(record);
	}

	// TODO 通过主键ID批量删除mm_goods 表中数据
	@Override
	public void deleteDatas(String ids) {
		String[] idArray = ids.split(",");
		for (int i = 0; i < idArray.length; i++) {
			String id = idArray[i];
			mapper.deleteByPrimaryKey(id);
		}
	}

	// TODO 通过主键修改表中数据
	@Override
	public void update(${model.javaClass}PO record) {
		mapper.updateByPrimaryKeySelective(record);
	}

	// TODO 通过主键读取表中数据
	@Override
	public ${model.javaClass}PO get${model.javaClass}PO(String id) {
		return mapper.selectByPrimaryKey(id);
	}
}
