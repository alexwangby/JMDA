<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>${form_name}</title>
    <!--公共资源配置开始-->
     ${"<#include \"../../comm/eformMeta.ftl\" />"}
    <!--公共资源配置结束-->

    ${extMeta}
</head>
<body>
     <form id="data_form" method="post" action="" class="page-form">
     <#if buttons?exists>
      <div class="buts-row-top row ">
         	<div class="col-sm-12 text-align-left buts-col" >
         	 <#list buttons as button>
         	 <button type="button" class="${button.iconCls}" onClick="${button.handler}"><i class="${button.iconClsChild}"></i>${button.title}</button>&nbsp;
         	 </#list>
        	</div>
        </div>
        </#if>
        <div class="but-row-top-shadow">
      </div>
     <#if workflow?exists>
         <div class="form-title">
             流程
             <a class="icon-only float-right blue btn-min" href="javascript:void(0);" onclick="showHideFormPart(this)">
                 <i class="fa fa-minus"></i>
             </a>
         </div>
         <div class="row" style="padding-bottom: 20px;padding-top: 20px">
         ${workflow}
         </div>
     </#if>
        <div class="form-title">
            ${form_name}
            <a class="icon-only float-right blue btn-min" href="javascript:void(0);" onclick="showHideFormPart(this)">
            <i class="fa fa-minus"></i>
            </a>
        </div>
         <#assign count=1>
         <#if mainFormMetadataMapList?exists>
			<#list mainFormMetadataMapList as mainFormMetadataMap>
			<#if mainFormMetadataMap.ui_type=='隐藏域'>
			  ${"${"+mainFormMetadataMap.column_lable+"}"}
            <#else>
            <#if count==1>
			<div class="row">
			</#if>
            <div class="hb-group col-sm-3 no-padding">

                	<label class="col-sm-4 hb-label">${mainFormMetadataMap.column_title}</label>
                	<div class="col-sm-8 hb-control">
                	<#if mainFormMetadataMap.ui_type=='无'||mainFormMetadataMap.ui_type=='Label'||mainFormMetadataMap.ui_type=='REDIS关联显示'||mainFormMetadataMap.ui_type=='SQL关联显示'>
                	<label class="hb-label">
                         ${"${"+mainFormMetadataMap.column_lable+"}"}
                    </label>
                    <#else>
                      ${"${"+mainFormMetadataMap.column_lable+"}"}
                     </#if>
                    </div>

            </div>
            <#if count==4>
            </div>
            </#if>
            <#assign count=count+1 />
            <#if count==5>
            <#assign count=1 />
            </#if>
            </#if>
			</#list>
			<#if count!=1>
			   </div>
			</#if>
		</#if>
        <#if oneFormMetadataMapList?exists>
           <#list oneFormMetadataMapList as oneFormMetadataMap>
               <div class="row">
                   <div class="hb-group col-sm-10 no-padding">

                	<label class="col-sm-1 hb-label" style="margin-left: 10px">${oneFormMetadataMap.column_title}</label>
                	<div class="col-sm-8 hb-control" >
                         ${"${"+oneFormMetadataMap.column_lable+"}"}
                    </div>
                   </div>
               </div>
           </#list>
        </#if>
        <#if subFormMetadataList?exists>
			<#list subFormMetadataList as subFormMetadata>
			 ${"${"+subFormMetadata.xieyin_code+"_GRID}"}
			</#list>
		</#if>
    </form>
</body>
</html>
