	<!-- id="datagrid_toolbar" 必填  -->
	<div id="datagrid_toolbar">
	<form id="data_form_excel" name="data_form_excel" method="post">
	</form>
	<#if searchs?exists>
		<form id="search_form" class="search-form" role="form">
		<#assign count=1>
		<#list searchs as srarch>
		 <#if count==1>
		  <div class="row" style='margin-right: 1px;'>
		 </#if>

			<div class="hb-group col-sm-3 no-padding">
			 <#if srarch.customHtml?length lt 1>
			 <label class="col-sm-4 hb-label">${srarch.title}</label>
			 <div class="col-sm-8 hb-control">
			   ${srarch.html}
			 </div>
			 <#else> ${srarch.customHtml}
			 </#if>

			</div>
			<#if count==4>
            </div>
            </#if>
            <#assign count=count+1 />
            <#if count==5>
            <#assign count=1 />
            </#if>
			</#list>
		   <#if count==1>
		   <div class="row" style='margin-right: 1px;'>
		   </#if>
			<div class="col-sm-3 form-buttons float-right ">
                        <!-- 隐藏显示查询条件  -->
                        <a href="javascript:hidshowSearchForm();" class="btn btn-default btn-xs shiny icon-only  float-right blue btn-min"><i class="fa fa-minus"></i></a>

                        <a class="btn btn-sm  btn-default  float-right margin-left-10 " href="javascript:resetForm('search_form', 'data_table')">
                            <i class="btn-label fa  fa-reply "></i>重 置</a>

                        <a class="btn btn-sm  btn-blue float-right " href="javascript:workListSearch('data_table', 'search_form');">
                            <i class="btn-label fa  fa-search "></i>查 询</a>
                        <div class="clearfix"></div>
             </div>
			</div>

		<input type="text" value="" style="display:none"/>
		<input type="hidden" value="${is_execute_query}" id="search_is_execute_query" />
		</form>
		</#if>
		<#if buttons?exists>
		<div class="buttons-preview" style="padding: 10px 10px 0px 10px">

			 <#list buttons as button>
			 <#if button.customHtml?length lt 1>
			 <#if button.iconCls=='fa fa-plus'>
			 <a class="btn btn-default  btn-sm" href="#" onClick="${button.handler};"><#if button.iconCls?exists> <i class="${button.iconCls}"></i><#else><i class="fa fa-plus"></i></#if>${button.title}</a>
			 <#elseif button.iconCls=='fa fa-edit'>
			 <a class="btn btn-primary btn-sm" href="#" onClick="${button.handler};"><#if button.iconCls?exists> <i class="${button.iconCls}"></i><#else><i class="fa fa-plus"></i></#if>${button.title}</a>
			 <#elseif button.iconCls=='fa fa-times'>
			 <a class="btn btn-danger btn-sm" href="#" onClick="${button.handler};"><#if button.iconCls?exists> <i class="${button.iconCls}"></i><#else><i class="fa fa-plus"></i></#if>${button.title}</a>
			 <#elseif button.iconCls=='fa fa-check'>
			 <a class="btn btn-success btn-sm" href="#" onClick="${button.handler};"><#if button.iconCls?exists> <i class="${button.iconCls}"></i><#else><i class="fa fa-plus"></i></#if>${button.title}</a>
			 <#else>
			  <a class="btn btn-primary btn-sm" href="#" onClick="${button.handler};"><#if button.iconCls?exists> <i class="${button.iconCls}"></i><#else><i class="fa fa-plus"></i></#if>${button.title}</a>
			</#if>
			<#else> ${button.customHtml}
            </#if>
				<!--<a class="btn btn-primary btn-sm" href="#" onClick="${button.handler};"><#if button.iconCls?exists> <i class="${button.iconCls}"></i><#else><i class="fa fa-plus"></i></#if>${button.title}</a> -->
			  </#list>
			<!-- 设置数据表格  -->
			<a class="btn btn-default btn-xs shiny icon-only  float-right red " style="margin: 3px 0px 0px 0px;" href="javascript:jmdaSettingDatagrid()"> <i class="fa fa-cog"></i></a>
		</div>
            <script>
                    <#assign sum=0>
                var fieldNameMap = {
                    <#if columnList?exists  >
                        <#list columnList as column>
                            <#if column.hidden !=true>
                                <#if sum==0>
                                "${column.columnName}<#if column.ref_type?exists  >_Desc </#if> ": "${column.title}"
                                    <#assign sum=1>
                                <#else>
                                    ,"${column.columnName}<#if column.ref_type?exists  >_Desc</#if> ": "${column.title}"
                                </#if>
                            </#if>

                        </#list>
                    </#if>
                };
            </script>
		</#if>
	</div>