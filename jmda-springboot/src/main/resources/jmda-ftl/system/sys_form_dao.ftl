<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${id}">
<#if metadataList?exists> 
   <#list metadataList as metadata>
<select id="getList_${metadata.id}" parameterType="map" resultType="java.util.HashMap">
	${metadata.sql}
	
	<if  test="sort !=null">
	  ${orderBy}
	</if>
</select>
<select id="getCount_${metadata.id}" parameterType="map" resultType="java.lang.Integer">
  select count(*) from (
  ${metadata.sql}
   ) tmp_count_worklist_table

</select>
   </#list>
</#if>   

</mapper>