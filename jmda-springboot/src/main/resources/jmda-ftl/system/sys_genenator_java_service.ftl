package ${project_path}.${model.applicationId}.service;


import com.fms.common.model.po.${model.javaClass}PO;


public interface ${model.javaClass}Service {

	// TODO 写入一条数据
	public void insert(${model.javaClass}PO record);

	// TODO 通过主键ID批量删除表中数据
	public void deleteDatas(String ids) ;

	// TODO 通过主键修改 表中数据
	public void update(${model.javaClass}PO record) ;

	// TODO 通过主键读取表中数据
	public ${model.javaClass}PO get${model.javaClass}PO(String id);
	
}
