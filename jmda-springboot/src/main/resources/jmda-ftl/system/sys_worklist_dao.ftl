<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${id}">
  <select id="getList" parameterType="map" resultType="java.util.HashMap">
	${sql}
	
	<if  test="sort !=null">
	  ${orderBy}
	</if>
  </select>
  <select id="getCount" parameterType="map" resultType="java.lang.Integer">

  select count(*) from (
  ${sql}
   ) tmp_count_worklist_table

</select>
</mapper>