	<table id="data_table" class="easyui-datagrid" pagination="${is_page}" pageSize='20' border="0" rownumbers="true" fitColumns="true" fit="true"
		data-options="singleSelect:false,
            collapsible:true,url:'${basePath}/execute/worklist/ext/${urlMethod}?worklistId=${worklistId}${dataparams}',
            method:'get',
            striped: true,
            onDblClickRow:onDblClickRow,
            onClickRow:onClickRow,
            onLoadSuccess:function(data){ try{ grid_onLoadSuccess(data);}catch(e){}},
            remoteSort:true,
            ${groupHtml}
            toolbar:'#datagrid_toolbar'">
		<thead>
		<#if isChecked=="true">
			<tr>
				<th data-options="field:'check',checkbox:true">选择</th>
				<#if columnList?exists  >
				<#list columnList as column>
				<th data-options="field:'${column.columnName}<#if column.ref_type?exists  >_Desc</#if>',width:width(${column.width})<#if column.hidden==true>,hidden:true</#if><#if column.sortable==true>,sortable:true</#if><#if column.column_align?if_exists>,align:'${column.column_align}'</#if><#if column.formatter?if_exists>,formatter:${column.formatter}</#if>">${column.title}</th>
				</#list>
				</#if>

				<#if extColumnList?exists  >
				<#list extColumnList as column>
				<th data-options="field:'${column.columnName}<#if column.ref_type?exists  >_Desc</#if>',width:width(${column.width})<#if column.hidden==true>,hidden:true</#if><#if column.sortable==true>,sortable:true</#if><#if column.column_align?if_exists>,align:'${column.column_align}'</#if><#if column.formatter?if_exists>,formatter:${column.formatter}</#if>">${column.title}</th>
				</#list>
				</#if>

			</tr>
			<#else>
			   <#if columnList?exists  >
				   <#list columnList as column>
				     <#if column.row_symbol=="start">
				     <tr>
				     </#if>
				     <th data-options="field:'${column.columnName}<#if column.ref_type?exists  >_Desc</#if>',width:width(${column.width})<#if column.hidden==true>,hidden:true</#if><#if column.sortable==true>,sortable:true</#if><#if column.column_align?if_exists>,align:'${column.column_align}'</#if><#if column.formatter?if_exists>,formatter:${column.formatter}</#if>" <#if column.rowspan gt 0  >rowspan="${column.rowspan}"</#if> <#if column.colspan gt 0  >colspan="${column.colspan}"</#if>>
				     ${column.title}
				     </th>
				     <#if column.row_symbol=="end">
				     </tr>
				     </#if>
				   </#list>
			   </#if>
		</#if>
		</thead>
	</table>