﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>数据列表</title>
<#include "comm/worklistMeta.ftl" />
${extMeta}
<script type="text/javascript">
var leftUrl = "${leftUrl}";
var rightUrl = "${rightUrl}";
var worklistId="${worklistId}";
var dataparams = "${dataparams}";
</script>
</head>
<body  class="easyui-layout" fit=true >
<#include "system/sys_worklist_easyui_leftTree.ftl" />
<div data-options="region:'center'">

<#include "system/sys_worklist_easyui_searchs.ftl" />
	<!-- id="data_table" 必填  -->
<#include "system/sys_worklist_easyui_treegrid.ftl" />
	</div>
<#include "system/sys_worklist_easyui_rightTree.ftl" />
</body>
</html>
