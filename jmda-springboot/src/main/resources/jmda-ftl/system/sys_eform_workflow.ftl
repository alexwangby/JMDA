<input name="workflowId" type="hidden" value="${workflowId}"/>
<ol class="ui-step ui-step-blue ui-step-${bussinessWorkFlowBeanList?size}">
<#list bussinessWorkFlowBeanList as bussinessWorkFlowBean>
    <#if bussinessWorkFlowBean_index==0>
            <li class="step-start
    <#elseif bussinessWorkFlowBean_index==((bussinessWorkFlowBeanList?size)-1)>
        <li class="step-end
    <#else>
            <li class="
    </#if>
    <#if bussinessWorkFlowBean.wfTaskLogId?? >

        <#if bussinessWorkFlowBean.wfTaskLogId!=''>
            step-done
        </#if>
    </#if>
    ">
    <div class="ui-step-line"></div>
    <div class="ui-step-cont">
        <span class="ui-step-cont-number">${bussinessWorkFlowBean_index+1}</span>
        <span class="ui-step-cont-text">${bussinessWorkFlowBean.name}</span>
    </div>
</li>
</#list>
</ol>