	<table id="data_table" class="easyui-treegrid"  border="0" rownumbers="true" fitColumns="true" fit="true"
		data-options="
            url:'${basePath}/execute/worklist/ext/getAllDatas?worklistId=${worklistId}${dataparams}',
            method:'get',
            idField: 'id',
            treeField: 'text',
            lines: true,
	        singleSelect: true,
            ${groupHtml}
            toolbar:'#datagrid_toolbar'">
		<thead>
		<#if isChecked=="true">
			<tr>
				<#if columnList?exists  >
				<#list columnList as column>
				<th data-options="field:'${column.columnName}<#if column.ref_type?exists  >_Desc</#if>',width:width(${column.width})<#if column.hidden==true>,hidden:true</#if><#if column.sortable==true>,sortable:true</#if><#if column.column_align?if_exists>,align:'${column.column_align}'</#if><#if column.formatter?if_exists>,formatter:${column.formatter}</#if>">${column.title}</th>
				</#list>
				</#if>

				<#if extColumnList?exists  >
				<#list extColumnList as column>
				<th data-options="field:'${column.columnName}<#if column.ref_type?exists  >_Desc</#if>',width:width(${column.width})<#if column.hidden==true>,hidden:true</#if><#if column.sortable==true>,sortable:true</#if><#if column.column_align?if_exists>,align:'${column.column_align}'</#if><#if column.formatter?if_exists>,formatter:${column.formatter}</#if>">${column.title}</th>
				</#list>
				</#if>

			</tr>
			<#else>
			   <#if columnList?exists  >
				   <#list columnList as column>
				     <#if column.row_symbol=="start">
				     <tr>
				     </#if>
				     <th data-options="field:'${column.columnName}<#if column.ref_type?exists  >_Desc</#if>',width:width(${column.width})<#if column.hidden==true>,hidden:true</#if><#if column.sortable==true>,sortable:true</#if><#if column.column_align?if_exists>,align:'${column.column_align}'</#if><#if column.formatter?if_exists>,formatter:${column.formatter}</#if>" <#if column.rowspan gt 0  >rowspan="${column.rowspan}"</#if> <#if column.colspan gt 0  >colspan="${column.colspan}"</#if>>
				     ${column.title}
				     </th>
				     <#if column.row_symbol=="end">
				     </tr>
				     </#if>
				   </#list>
			   </#if>
		</#if>
		</thead>
	</table>