<html>
<head>
<title>业务建模</title>
<#include "../../comm/radMeta.ftl" />
<script type="text/javascript">
	if ((typeof Range !== "undefined") && !Range.prototype.createContextualFragment) {
		Range.prototype.createContextualFragment = function(html) {
			var frag = document.createDocumentFragment(), div = document.createElement("div");
			frag.appendChild(div);
			div.outerHTML = html;
			return frag;
		};
	}
</script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/repository/application/sys_repository_application.js"></script>

</head>
<body>
	<form name='mainForm' action=''>
		<div id="westPanel" style="height: 100%">
			<div id="treePanel" style="height: 100%; width: 100%"></div>
		</div>
		<div id="tabList">
			<div id="listPanel"></div>
		</div>
		<div id="BMNewWindow">
			<div id="BMNewTree"></div>
		</div>
	</form>
</body>
</html>