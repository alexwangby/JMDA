<html>
<head>
<title>新建模型</title>
<#include "../../comm/radMeta.ftl" />
<script type="text/javascript">
	var typeName = '${typeName}';
	var applicationId ='${applicationId}';
	function save() {
	    if(typeName=="模型分类"){ 
	    var form_name = document.getElementById('form_name').value;
		form_name = trim(form_name);
		var xieyin_code = document.getElementById('xieyin_code').value;
		xieyin_code = trim(xieyin_code);
		
		if (form_name == '') {
			Ext.MessageBox.alert('提示', '[${typeName}名称]不能为空!');
			return false;
		} else if (isIncSym2(form_name)) {
			Ext.MessageBox.alert('提示', '[${typeName}]不能有特殊符号!');
			return false;
		} else if (xieyin_code == '') {
			Ext.MessageBox.alert('提示', '[编码]不能为空!');
			return false;
		}else {
			Ext.getBody().mask("处理中...");
			Ext.Ajax.request({
				url : basePath + 'repository/application/createModel',
				method : 'POST',
				params : {
					name : form_name,
					typeName : typeName,
					xieyin_code : xieyin_code,
					applicationId : applicationId
				},// end params
				failure : function(response, options) {
					Ext.MessageBox.alert('操作失败', '操作失败,可能是网络超时！');
					return false;
				},// end failure block
				success : function(response, options) {
					Ext.getBody().unmask(true);
					if (response.responseText == 'fail') {
						Ext.MessageBox.alert('操作失败', "创建过程中出现错误,请重试");
						return false;
					} else {
	                       var applicationId = response.responseText;
					     	parent.regreshNodeById('root', "F_"+applicationId);
					     	parent.createViewTab('root', '应用模型库', '', basePath + 'repository/application/getApplicationGridPage', true);
						    parent.activeDialog.hide();
					}
				}
			});
		}
	    }else{
	    var form_name = document.getElementById('form_name').value;
		form_name = trim(form_name);
		var xieyin_code = document.getElementById('xieyin_code').value;
		xieyin_code = trim(xieyin_code);
		var form_group = document.getElementById('form_group').value;
		if (form_name == '') {
			Ext.MessageBox.alert('提示', '[${typeName}名称]不能为空!');
			return false;
		} else if (isIncSym2(form_name)) {
			Ext.MessageBox.alert('提示', '[${typeName}]不能有特殊符号!');
			return false;
		} else if (xieyin_code == '') {
			Ext.MessageBox.alert('提示', '[编码]不能为空!');
			return false;
		} else if (isIncSym2(xieyin_code)) {
			Ext.MessageBox.alert('提示', '[编码]不能有特殊符号!');
			return false;
		} else if (form_group == '') {
			Ext.MessageBox.alert('提示', '[模型分类]不能为空!');
			return false;
		} else {
			Ext.getBody().mask("处理中...");
			Ext.Ajax.request({
				url : basePath + 'repository/application/createModel',
				method : 'POST',
				params : {
					name : form_name,
					typeName : typeName,
					xieyin_code : xieyin_code,
					applicationId : form_group
				},// end params
				failure : function(response, options) {
					Ext.MessageBox.alert('操作失败', '操作失败,可能是网络超时！');
					return false;
				},// end failure block
				success : function(response, options) {
					Ext.getBody().unmask(true);
					if (response.responseText == 'fail') {
						Ext.MessageBox.alert('操作失败', "创建过程中出现错误,请重试");
						return false;
					} else {
						if (typeName == '表单') {
							var newNodeId = 'FORM_' + response.responseText;
							parent.regreshNodeById('F_' + document.getElementById('form_group').value, newNodeId);
							parent.createViewTab('second_floor_form', form_name, '', basePath + 'repository/form/openForm?formId=' + response.responseText, true);
						}else if (typeName == '数据列表') {
							var newNodeId = 'LISTDATA_' + response.responseText;
							parent.regreshNodeById('LIST_' + document.getElementById('form_group').value, newNodeId);
							parent.createViewTab('second_floor_worklist', form_name, '', basePath + 'repository/worklist/openWorkList?worklistid=' + response.responseText, true);
						}else if(typeName == '模型分类'){
						    var applicationId = response.responseText;
					     	parent.regreshNodeById('ROOT', "F_"+applicationId);
					     	parent.createViewTab('root', '应用模型库', '', basePath + 'repository/application/getApplicationGridPage', true);
						}
						parent.activeDialog.hide();
					}
				}
			});
		}
	    }


	}
</script>
</head>
<body>
	<form name='mainForm' action="" style="">
		<table class="pbs_table_second" border="0" align="center">
			<tr>
				<td class="pbs_td_text_right">${typeName}名称(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><input name="form_name" id='form_name' type="text" style="width: 200px" value="${name}"  /></td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">编码(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><input name="xieyin_code" id='xieyin_code' type="text" style="width: 200px" value="${xieyin_code}" /></td>
			</tr>
			<#if typeName!="模型分类">
			<tr>
				<td class="pbs_td_text_right">模型分类(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><select name="form_group" id='form_group' style="width: 210px" class="pbs_select_box">
						<option value='' <#if applicationId=="undefined" >selected</#if> >请选择</option>
						<#list applicationList as sysapplication>
						<option value='${sysapplication.id}' <#if applicationId==sysapplication.id >selected </#if> >${sysapplication.name}</option>
                    </#list>
				</select></td>
			</tr></#if>
		</table>
	</form>
</body>
</html>