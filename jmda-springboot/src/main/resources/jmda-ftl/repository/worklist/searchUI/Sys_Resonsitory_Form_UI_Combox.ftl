<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<#include "../../comm/radMeta.ftl" />
<style type="text/css">
.STYLE3 {
	font-size: 12px
}
</style>
<title>ui下拉列表</title>
</head>
<body>

	<table border="0" align="left" style="width: 90%; margin-left: 2%;" class="pbs_table_second">
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">扩展代码: CSS/javaScript</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css"><textarea cols="50" rows="4" name="extendedCode" id="extendedCode">${extendedCode}</textarea></td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">组件属性(<font color='red'>*</font>):
			</td>
			<td style="width: 70%" class="pbs_td_text_left" valign="top" id="selectId">
			取值字段：<input style="width: 50%; margin-bottom: 5px;" type="text" name="getFieldValue" id="getFieldValue" value="${getFieldValue }" /><br /> 
			显示字段：<input style="width: 50%; margin-bottom: 5px;" type="text" name="displayNameValue" id="displayNameValue" value="${displayNameValue}" /><br /> 
			sql&nbsp;语&nbsp;句：<input style="width: 50%;margin-bottom: 5px;" type="text" name="sql" id="sql" style="width: 350px;" value="${sql}" /><br />
			扩&nbsp;&nbsp;展&nbsp;类：<input style="width: 50%;margin-bottom: 5px;" type="text" name="extClass" id="extClass" style="width: 350px;" value="${extClass}" />
			<!-- &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="checkSQL();">测试</a> -->
			</td>
		</tr>
	</table>

	<script type="text/javascript">
		function getSettingParams() {
			return '{"extendedCode":"' + document.getElementById('extendedCode').value + '","getFieldValue":"' + document.getElementById('getFieldValue').value + '","displayNameValue":"' + document.getElementById('displayNameValue').value + '","sql":"' + document.getElementById('sql').value + '","extClass":"' + document.getElementById('extClass').value +'"}';
		}
	</script>
</body>
</html>