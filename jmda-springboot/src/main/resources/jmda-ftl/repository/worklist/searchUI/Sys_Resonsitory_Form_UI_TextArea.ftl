<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<#include "../../comm/radMeta.ftl" />
<style type="text/css">
.STYLE3 {
	font-size: 12px
}
</style>
<title>ui多行</title>
</head>
<body>
	<table border="0" align="left" style="width: 90%; margin-left: 2%" class="pbs_table_second">
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">录入宽度:</td>
			<td style="width: 70%" class="pbs_td_text_left"><input style="width: 50%; float: left;" name="entryWidth" type="text" id="entryWidth" size="10" value="${entryWidth}" /> <select id="lengthType" style="float: left; margin-left: 5px;" class="pbs_select_box">
					<option value="px" <#if lengthType == 'px'>
					   selected="selected"
					</#if> >像素</option>
					<option value="%" <#if lengthType == '%'>
					   selected="selected"
					</#if> >百分比</option>
			</select></td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">扩展代码: CSS/javaScript</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css"><textarea cols="50" rows="4" name="extendedCode" id="extendedCode">${extendedCode}</textarea></td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">行数：</td>
			<td style="width: 70%" class="pbs_td_text_left" valign="top" id="hId"><input style="width: 50%" type="text" name="lineSize" id="lineSize" value="${lineSize}" /></td>
		</tr>
	</table>

	<script type="text/javascript">
		function getSettingParams() {
			return '{"entryWidth":"' + document.getElementById('entryWidth').value + '","lengthType":"' + document.getElementById('lengthType').value + '","extendedCode":"' + document.getElementById('extendedCode').value + '","lineSize":' + document.getElementById('lineSize').value + '}';
			;
		}
	</script>
</body>
</html>