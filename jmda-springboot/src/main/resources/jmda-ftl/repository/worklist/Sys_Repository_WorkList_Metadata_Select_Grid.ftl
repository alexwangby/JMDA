<html>
<head>
<#include "../../comm/radMeta.ftl" />
<style>
.x-selectable, .x-selectable * { 
	user-select: text! important;
	-o-user-select: text! important;
	-moz-user-select: text! important;
	-khtml-user-select: text! important;
	-webkit-user-select: text! important;
}
</style>
<script type="text/javascript" src="${staticPath}/jmda-static/js/repository/worklist/sys_repository_worklist_metadata_select_grid.js"></script>
<style type="text/css">
A:LINK {
	color: #53709A;
	text-decoration: none;
}

A:VISITED {
	color: #53709A;
	TEXT-DECORATION: none;
}

A:HOVER {
	color: #53709A
}
</style>
<script type="text/javascript">
	var id = "${id}";
</script>
<title>FORM</title>
</head>
<body>
	<form name="formForm">
		<div id="grid"></div>
	</form>
</body>
</html>