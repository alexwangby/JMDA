<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>列表编辑</title>
<#include "../../comm/radMeta.ftl" />

<script type="text/javascript">
var pbsBasePath = "${basePath}/";
	Ext.onReady(function() {
        getConfiguration();
		Ext.QuickTips.init();
		var tb = new Ext.Toolbar();
		var btnAdd = new Ext.Action({
			text : '保存',
			handler : function() {
				save();
			},
			iconCls : 'iconSave'
		});
		tb.add(btnAdd);
		tb.render('toolbar');
	});
 function getConfiguration(){
    document.getElementById("treeFieldTr").style.display = "none";
    var templateFile_layout = document.getElementById('templateFile').value; 
	if(templateFile_layout=="list_rightTree"){
	   document.getElementById("leftConfiguration").style.display = "none";
	   document.getElementById("rightConfiguration").style.display = "";
	  // document.getElementById("treeFieldTr").style.display = "none";
	}else if(templateFile_layout=="leftTree_list"){
	   document.getElementById("leftConfiguration").style.display = "";
	   document.getElementById("rightConfiguration").style.display = "none";
	 //  document.getElementById("treeFieldTr").style.display = "none";
	}else if(templateFile_layout=="leftTree_list_rightTree"){
	   document.getElementById("leftConfiguration").style.display = "";
	   document.getElementById("rightConfiguration").style.display = "";
	 //  document.getElementById("treeFieldTr").style.display = "none";
	}else if(templateFile_layout=="treelist"){
	   document.getElementById("leftConfiguration").style.display = "none";
	   document.getElementById("rightConfiguration").style.display = "none";
	 //  document.getElementById("treeFieldTr").style.display = "";
	}else if(templateFile_layout=="treelist_rightTree"){
	   document.getElementById("leftConfiguration").style.display = "none";
	   document.getElementById("rightConfiguration").style.display = "";
	  // document.getElementById("treeFieldTr").style.display = "";
	}else if(templateFile_layout=="leftTree_treelist"){
	   document.getElementById("leftConfiguration").style.display = "";
	   document.getElementById("rightConfiguration").style.display = "none";
	  // document.getElementById("treeFieldTr").style.display = "";
	}else if(templateFile_layout=="leftTree_treelist_rightTree"){
	   document.getElementById("leftConfiguration").style.display = "";
	   document.getElementById("rightConfiguration").style.display = "";
	 //  document.getElementById("treeFieldTr").style.display = "";
	}else {
	   document.getElementById("leftConfiguration").style.display = "none";
	   document.getElementById("rightConfiguration").style.display = "none";
	 //  document.getElementById("treeFieldTr").style.display = "none";
	}
}
	function save() {
		var form_name = document.getElementById('form_name').value;
		form_name = trim(form_name);
		var form_group = document.getElementById('form_group').value;
		var id = document.getElementById('id').value;
		var sql = document.getElementById('sql').value;
		var extendJavascript = document.getElementById('extendJavascript').value;
		var templateFile = document.getElementById('templateFile').value;
		var leftTitle = document.getElementById('leftTitle').value;
		var leftUrl = document.getElementById('leftUrl').value;
		var rightTitle = document.getElementById('rightTitle').value;
		var rightUrl = document.getElementById('rightUrl').value;
		var xieyin_code = document.getElementById('xieyin_code').value;
		
		var templateName = document.getElementById('templateName').value;
		if (form_name == '') {
			Ext.MessageBox.alert('提示', '[列表名称]不能为空!');
			return false;
		} else if (isIncSym2(form_name)) {
			Ext.MessageBox.alert('提示', '[列表名称]不能有特殊符号!');
			return false;
		}else if (xieyin_code == '') {
			Ext.MessageBox.alert('提示', '[编码]不能为空!');
			return false;
		}  else if (form_group == '') {
			Ext.MessageBox.alert('提示', '[模型分类]不能为空!');
			return false;
		} else if (sql == '') {
			Ext.MessageBox.alert('提示', '[查询SQL]不能为空!');
			return false;
		}else if (templateFile == '') {
			Ext.MessageBox.alert('提示', '[模板文件]不能为空!');
			return false;
		}else if(templateFile == 'leftTree_list'&&(leftTitle==''||leftUrl=='')){
			Ext.MessageBox.alert('提示', '[左侧树标题与URL]不能为空!');
			return false;
		}else if(templateFile == 'list_rightTree'&&(rightTitle==''||rightUrl=='')){
			Ext.MessageBox.alert('提示', '[右侧树标题与URL]不能为空!');
			return false;
		}  else {
			Ext.getBody().mask("处理中...");
			Ext.Ajax.request({
				url : basePath + 'repository/worklist/saveWorkListBase',
				method : 'POST',
				params : {
					id : id,
					name : form_name,
					sql : sql,
					extendJavascript : extendJavascript,
					form_group : form_group,
					templateFile:templateFile,
					leftTitle:leftTitle,
					leftUrl:leftUrl,
					rightTitle:rightTitle,
					xieyin_code:xieyin_code,
					formateClass:document.getElementById('formateClass').value,
					rightUrl:rightUrl,
					is_detail:document.getElementById('is_detail').value,
					is_page:document.getElementById('is_page').value,
					is_execute_query:document.getElementById('is_execute_query').value,
					treeField:document.getElementById('treeField').value,
					templateName:templateName
					
				},// end params
				failure : function(response, options) {
					Ext.MessageBox.alert('操作失败', '操作失败,可能是网络超时！');
					return false;
				},// end failure block
				success : function(response, options) {
					Ext.getBody().unmask(true);
					if (response.responseText == 'ok') {
						parent.parent.regreshNodeById('LIST_' + form_group);
						Ext.MessageBox.alert('提示', ' 数据列表保存 成 功! ');
						return false;
					} else {
						Ext.MessageBox.alert('操作失败', "数据列表保存失败!</br>"+response.responseText);
						return false;
					}
				}
			});
		}

	}
</script>
</head>
<body>
	<form name='mainForm' action="">
		<div id="toolbar"></div>
		<table width="80%" border="0" align="center" class="pbs_table_second">
			<tr>
				<td class="pbs_td_text_right">唯一标识
				</td>
				<td class="pbs_td_text_left">${id}</td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">列表名称(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><input name="form_name" id='form_name' type="text" style="width: 200px" value="${name}" /></td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">编码(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><input name="xieyin_code" id='xieyin_code' type="text" style="width: 200px" value="${xieyin_code}" /></td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">模型分类(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><select name="form_group" id='form_group' style="width: 200px" class="pbs_select_box">
						<option value='' <#if groupId=="" >selected</#if> >请选择</option>
						<#list applicationList as sysapplication>
						<option value='${sysapplication.id}' <#if groupId==sysapplication.id >selected </#if> >${sysapplication.name}</option>
                    </#list>
				</select></td>
			</tr>
			
			<tr>
				<td class="pbs_td_text_right">查询SQL(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><textarea style="width: 80%" rows="7" name="sql" id="sql">${sql}</textarea></td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">模板(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left">
			    <select name="templateName" id='templateName' style="width: 200px" class="pbs_select_box" >
					<option value='/system/sys_worklist_easyui' <#if "/system/sys_worklist_easyui"==templateName >selected </#if> >默认</option>
					<option value='/system/sys_worklist_tree_easyui' <#if "/system/sys_worklist_tree_easyui"==templateName >selected </#if> >树结构列表</option>
					<option value='/system/sys_worklist_custom_1_easyui' <#if "/system/sys_worklist_custom_1_easyui"==templateName >selected </#if> >自定义1</option>
				</select>
				</td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">展示方式(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left">
			<!--	<input name="templateFile" id='templateFile' type="text" style="width: 300px" value="${templateFile}" />-->
			    <select name="templateFile" id='templateFile' style="width: 200px" class="pbs_select_box" onchange="getConfiguration();">
					<option value='list' <#if "list"==templateFile >selected </#if> >列表</option>
					<option value='list_rightTree' <#if "list_rightTree"==templateFile >selected </#if> >左列表右树</option>
					<option value='leftTree_list' <#if "leftTree_list"==templateFile >selected </#if> >左树右列表</option>
					<option value='leftTree_list_rightTree' <#if "leftTree_list_rightTree"==templateFile >selected </#if> >左树中列表右树</option>
					
					<option value='treelist' <#if "treelist"==templateFile >selected </#if> >树形列表</option>
					<option value='treelist_rightTree' <#if "treelist_rightTree"==templateFile >selected </#if> >左树形列表右树</option>
					<option value='leftTree_treelist' <#if "leftTree_treelist"==templateFile >selected </#if> >左树右树形列表</option>
					<option value='leftTree_treelist_rightTree' <#if "leftTree_treelist_rightTree"==templateFile >selected </#if> >左树中树形列表右树</option>
				</select>
				</td>
			</tr>
			<tr id="treeFieldTr">
				<td class="pbs_td_text_right">树形列表父字段(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><input name="treeField" id='treeField' type="text" style="width: 150px" value="${treeField}" />
				</td>
			</tr>
			<tr id="leftConfiguration">
				<td class="pbs_td_text_right">左侧树标题与URL(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><input name="leftTitle" id='leftTitle' type="text" style="width: 150px" value="${leftTitle}" />/
				<input name="leftUrl" id='leftUrl' type="text" style="width: 250px" value="${leftUrl}" />
				</td>
			</tr>
			<tr id="rightConfiguration">
				<td class="pbs_td_text_right">右侧树标题与URL(<font color='red'>*</font>)
				</td>
				<td class="pbs_td_text_left"><input name="rightTitle" id='rightTitle' type="text" style="width: 150px" value="${rightTitle}" />/
				<input name="rightUrl" id='rightUrl' type="text" style="width: 250px" value="${rightUrl}" />
				</td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">扩展JavaScript/CSS</td>
				<td class="pbs_td_text_left"><textarea style="width: 80%" rows="3" name="extendJavascript" id="extendJavascript">${extendJavascript}</textarea></td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">扩展JAVA格式化事件</td>
				<td class="pbs_td_text_left"><input name="formateClass" id='formateClass' type="text" style="width: 80%;" value="${formateClass}" /></td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">列表是否扩展详情
				</td>
				<td class="pbs_td_text_left">
				 <select name="templateFile" id='is_detail' style="width: 200px" class="pbs_select_box" >
					<option value='false' <#if false==is_detail >selected </#if> >否</option>
					<option value='true' <#if true==is_detail >selected </#if> >是</option>
				</select>
				</td>
			</tr>
			<tr>
			<td class="pbs_td_text_right">是否分页
				</td>
				<td class="pbs_td_text_left">
				 <select name="is_page" id='is_page' style="width: 200px" class="pbs_select_box" >
					<option value='false' <#if false==is_page >selected </#if> >是</option>
					<option value='true' <#if true==is_page >selected </#if> >否</option>
				</select>
				</td>
			</tr>
			<tr>
			<td class="pbs_td_text_right">是否默认执行查询
				</td>
				<td class="pbs_td_text_left">
				 <select name="is_execute_query" id='is_execute_query' style="width: 200px" class="pbs_select_box" >
					<option value='false' <#if false==is_execute_query >selected </#if> >是</option>
					<option value='true' <#if true==is_execute_query >selected </#if> >否</option>
				</select>
				</td>
			</tr>
		</table>
		<input type="hidden" id="id" name="id" value="${id}" />
	</form>
</body>
</html>