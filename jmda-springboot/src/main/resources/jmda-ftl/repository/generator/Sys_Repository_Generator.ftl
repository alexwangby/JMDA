<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>代码生成</title>
<#include "../../comm/radMeta.ftl" />
<script type="text/javascript">

	function save() {
		Ext.getBody().mask("处理中...");
			Ext.Ajax.request({
				url : basePath + 'repository/generator/execute',
				method : 'POST',
				params : {
					javaClass : javaClass,
					applicationId : document.getElementById("applicationId").value,
					javaController : document.getElementById("javaController").value,
					javaService:document.getElementById('javaService').value,
					formJs : document.getElementById("formJs").value,
					worklistJs : document.getElementById("worklistJs").value,
					worklistId : worklistId,
					formId : formId
				},// end params
				failure : function(response, options) {
					Ext.MessageBox.alert('操作失败', '操作失败,可能是网络超时！');
					return false;
				},// end failure block
				success : function(response, options) {
					Ext.getBody().unmask(true);
					if (response.responseText == 'ok') {
						Ext.MessageBox.alert('提示', '代码生成成功,请刷新工程代码');
						return false;
					} else {
						Ext.MessageBox.alert('代码生成错误提示', response.responseText);
						return false;
					}
				}
			});
	}
	var formId='';
	var worklistId='';
	var javaBasePackage='/static/js';
	var javaClass='';
	function tableChange(className){
		if(className!=''){
			 javaClass=className;
			 var javaBasePackage=document.getElementById("javaBasePackage").value;
			 var applicationId=document.getElementById("applicationId").value;
			 document.getElementById("javaController").value=javaBasePackage+'.'+applicationId+'.controller.'+className+'Controller';
			 document.getElementById("javaService").value=javaBasePackage+'.'+applicationId+'.service.'+className+'Service';
		 }else{
		 	 javaClass='';
		 	 document.getElementById("javaController").value='';
			 document.getElementById("javaService").value='';
		 }
	}
	
	function formChange(formCode,id){
		 if(formCode!=''){
		 	 formId=id;
			 var applicationId=document.getElementById("applicationId").value;
			 var appPath=applicationId.replace(new RegExp("\\.","gm"),"/")  
			 document.getElementById("formJs").value=javaBasePackage+'/'+appPath+'/'+formCode+'_form.js';
		 }else{
		 	formId='';
		 	document.getElementById("formJs").value='';
		 }
		
	}
	
	function workListChange(workListCode,id){
		if(workListCode!=''){
		 	worklistId=id;
		 	var applicationId=document.getElementById("applicationId").value;
		 	 var appPath=applicationId.replace(new RegExp("\\.","gm"),"/")
		 	document.getElementById("worklistJs").value=javaBasePackage+'/'+appPath+'/'+workListCode+'_worklist.js';
		 }else{
		 	worklistId='';
		 	document.getElementById("worklistJs").value='';
		 }
		 
	}
</script>
</head>
<body>
	<form name='mainForm' action="">
		<table width="98%" border="0" align="center" class="pbs_table_second">
			<tr>
				<td class="pbs_td_text_right" style="width:150px">WEB工程</td>
				<td class="pbs_td_text_left">${webBasePath}</td>
			</tr>
			<tr>
				<td class="pbs_td_text_right" style="width:150px">模块名称</td>
				<td class="pbs_td_text_left"><input name="applicationId" id='applicationId' type="text" style="width: 80%" value="${applicationId}" /></td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">JAVA代码
				</td>
				<td class="pbs_td_text_left">
					<select name="javaClass" id='javaClass' style="width: 80%" class="pbs_select_box" onChange="tableChange(this.options[this.options.selectedIndex].value)">
							<option value='' selected >请选择数据库表</option>
							<#list tableList as tableModel>
							<option value='${tableModel.className}'>${tableModel.tableName}</option>
                  		   </#list>
					</select>
				</td>
			</tr>
			
			<tr>
				<td class="pbs_td_text_right">&nbsp;</td>
				<td class="pbs_td_text_left"><input name="javaController" id='javaController' type="text" style="width: 80%" value="" /></td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">&nbsp;</td>
				<td class="pbs_td_text_left"><input name="javaService" id='javaService' type="text" style="width: 80%" value="" /></td>
			</tr>
			
			<tr>
				<td class="pbs_td_text_right">JAVASCRIPT代码
				</td>
				<td class="pbs_td_text_left">
				<select name="formCode" id='formCode' style="width: 80%" class="pbs_select_box" onChange="formChange(this.options[this.options.selectedIndex].value,this.options[this.options.selectedIndex].id)" >
						<option value='' >请选择表单模型</option>
						
                  		 <#list formList as formModel>
						<option value='${formModel.xieyin_code}' id='${formModel.id}'>${formModel.name}[表单模型]</option>
                 	</#list>
				</select>
				</td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">&nbsp;</td>
				<td class="pbs_td_text_left"><input name="formJs" id='formJs' type="text" style="width: 80%" value="" /></td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">&nbsp;</td>
				<td class="pbs_td_text_left">
				<select name="worklistCode" id='worklistCode' style="width: 80%" class="pbs_select_box" onChange="workListChange(this.options[this.options.selectedIndex].value,this.options[this.options.selectedIndex].id)" >
						<option value='' >请选择列表模型</option>
						<#list worklistList as worklist>
							<option value='${worklist.xieyin_code}' id='${worklist.id}'>${worklist.name}[列表模型]</option>
                  		</#list>
				</select>
				</td>
			</tr>
			<tr>
				<td class="pbs_td_text_right">&nbsp;</td>
				<td class="pbs_td_text_left"><input name="worklistJs" id='worklistJs' type="text" style="width: 80%" value="" /></td>
			</tr>
		</table>
		<input type="hidden" id="javaBasePackage" name="javaBasePackage" value="${javaBasePackage}" />
	</form>
</body>
</html>