<html>
<head>
<#include "../../comm/radMeta.ftl" />
<script type="text/javascript" src="${staticPath}/jmda-static/js/repository/form/sys_repository_form_metadata_map.js"></script>
<script type="text/javascript">
	var form_id = "${form_id}";
	var formMetadataId = "${formMetadataId}";
	var formMetadataName = "${formMetadataName}";
	var is_master_dataset=${is_master_dataset};
	var page_size = "<input type='text' name='page_size' id='page_size'    value='${page_size}' style='width:100px'/>";
	var grid_height = "<input type='text' name='grid_height' id='grid_height'    value='${grid_height}' style='width:100px'/>";
	var is_group = ${is_group};
	var is_group_html = "";
	var is_detail = ${is_detail};
	<!--
	var is_details_html = "";
	-->
	if(is_group){
	  is_group_html ="<input type='checkbox' name='is_group' id='is_group' checked='checked'/>"
	}else{
	 is_group_html ="<input type='checkbox' name='is_group' id='is_group'  />"
	}
	var group_field_html = "<input type='text' name='group_field' id='group_field'    value='${group_field}' style='width:100px'/>";
	
	
	if(is_detail){
	  is_detail_html ="<input type='checkbox' name='is_detail' id='is_detail' checked='checked'/>"
	}else{
	 is_detail_html ="<input type='checkbox' name='is_detail' id='is_detail'  />"
	}
	var ref = ${ref};
	<!--
	var details_field_html = "<input type='text' name='details_field' id='details_field'    value='${details_field}' style='width:100px'/>";
	-->
	
</script>
<title>FormUI</title>
</head>
<body>
	<form name="formUIForm">
		<div id="grid"></div>
	</form>
</body>
</html>