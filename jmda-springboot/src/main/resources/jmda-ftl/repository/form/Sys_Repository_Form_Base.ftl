<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>FORM编辑</title>
<#include "../../comm/radMeta.ftl" />
    <script type="text/javascript">
        var pbsBasePath = "${basePath}/";
        Ext.onReady(function () {
            Ext.QuickTips.init();
            var tb = new Ext.Toolbar();
            var btnAdd = new Ext.Action({
                text: '保存',
                handler: function () {
                    save();
                },
                iconCls: 'iconSave'
            });
            tb.add(btnAdd);
            tb.render('toolbar');
        });
        function save() {
            var form_name = document.getElementById('form_name').value;
            form_name = trim(form_name);
            var form_group = document.getElementById('form_group').value;
            var id = document.getElementById('id').value;
            var xieyin_code = document.getElementById('xieyin_code').value;
            var workflow = document.getElementById('workflow').value;
            if (form_name == '') {
                Ext.MessageBox.alert('提示', '[表单名称]不能为空!');
                return false;
            } else if (xieyin_code == '') {
                Ext.MessageBox.alert('提示', '[编码]不能为空!');
                return false;
            } else if (isIncSym2(form_name)) {
                Ext.MessageBox.alert('提示', '[表单名称]不能有特殊符号!');
                return false;
            } else if (form_group == '') {
                Ext.MessageBox.alert('提示', '[模型分类]不能为空!');
                return false;
            } else {
                Ext.getBody().mask("处理中...");
                Ext.Ajax.request({
                    url: basePath + 'repository/form/save',
                    method: 'POST',
                    params: {
                        id: id,
                        form_name: form_name,
                        xieyin_code: xieyin_code,
                        extendJavascript: document.getElementById('extendJavascript').value,
                        form_group: form_group,
                        workflow: workflow
                    },// end params
                    failure: function (response, options) {
                        Ext.MessageBox.alert('操作失败', '操作失败,可能是网络超时！');
                        return false;
                    },// end failure block
                    success: function (response, options) {
                        Ext.getBody().unmask(true);
                        if (response.responseText == 'ok') {
                            parent.refreshParent("F_" + form_group);
                            Ext.MessageBox.alert('提示', ' 保 存 表 单 成 功! ');
                            return false;
                        } else {
                            Ext.MessageBox.alert('操作失败', "保 存 表 单 失 败!");
                            return false;
                        }
                    }
                });
            }

        }
    </script>
</head>
<body>
<form name='mainForm' action="">
    <div id="toolbar"></div>
    <table width="600" border="0" align="center" class="pbs_table_second">
        <tr>
            <td class="pbs_td_text_right">唯一标识
            </td>
            <td class="pbs_td_text_left">${form_id}</td>
        </tr>
        <tr>
            <td class="pbs_td_text_right">表单名称(<font color='red'>*</font>)
            </td>
            <td class="pbs_td_text_left"><input name="form_name"
                                                id='form_name' type="text" style="width: 200px"
                                                value="${form_name}"/></td>
        </tr>
        <tr>
            <td class="pbs_td_text_right">表单编码(<font color='red'>*</font>)
            </td>
            <td class="pbs_td_text_left"><input name="xieyin_code"
                                                id='xieyin_code' type="text" style="width: 200px"
                                                value="${xieyin_code}"/></td>
        </tr>
        <tr>
            <td class="pbs_td_text_right">模型分类(<font color='red'>*</font>)
            </td>
            <td class="pbs_td_text_left"><select name="form_group"
                                                 id='form_group' style="width: 200px" class="pbs_select_box">
                <option value='' <#if groupId=="" >selected</#if>>请选择</option>
            <#list applicationList as sysapplication>
                <option value='${sysapplication.id}' <#if groupId==sysapplication.id >selected </#if>>${sysapplication.name}</option>
            </#list>
            </select></td>
        </tr>
        <tr>
            <td class="pbs_td_text_right">扩展JavaScript/CSS</td>
            <td class="pbs_td_text_left"><textarea style="width: 80%" rows="3" name="extendJavascript" id="extendJavascript">${extendJavascript}</textarea></td>
        </tr>
        <tr>
            <td class="pbs_td_text_right">流程
            </td>
            <td class="pbs_td_text_left"><select name="workflow"
                                                 id='workflow' style="width: 200px" class="pbs_select_box">
                <option value='' <#if workflowId=="" >selected</#if>>请选择</option>
            <#list workflowList as workflow>
                <option value='${workflow.workflowId}' <#if workflowId==workflow.workflowId >selected </#if>>${workflow.name}</option>
            </#list>
            </select></td>
        </tr>
    </table>
    <input type="hidden" id="id" name="id" value="${form_id}"/>
</form>
</body>
</html>