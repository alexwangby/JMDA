<html>
<head>
<#include "../../comm/radMeta.ftl" />
    <script type="text/javascript">
        var form_id = '${form_id}';
        var formMetadataId = '${formMetadataId}';
        function save() {
            var form_name = document.getElementById('form_name').value;
            form_name = trim(form_name);
            var xieyin_code = document.getElementById('xieyin_code').value;
            xieyin_code = trim(xieyin_code);
            var formGridDatasFormatEventFunctionName = document.getElementById('formGridDatasFormatEventFunctionName').value;
            formGridDatasFormatEventFunctionName = trim(formGridDatasFormatEventFunctionName);

            var sql = document.getElementById('sql').value;
            if (form_name == '') {
                Ext.MessageBox.alert('提示', '[数据集名称]不能为空!');
                return false;
            } else if (isIncSym2(form_name)) {
                Ext.MessageBox.alert('提示', '[数据集名称]不能有特殊符号!');
                return false;
            } else if (sql == '') {
                Ext.MessageBox.alert('提示', '[查询SQL]不能为空!');
                return false;
            } else if (xieyin_code == '') {
                Ext.MessageBox.alert('提示', '[谐音码]不能为空!');
                return false;
            } else if (isIncSym2(xieyin_code)) {
                Ext.MessageBox.alert('提示', '[谐音码]不能有特殊符号!');
                return false;
            } else {
                Ext.getBody().mask("处理中...");
                Ext.Ajax.request({
                    url: basePath + 'repository/form/metadata/saveFormMetadaDataSet',
                    method: 'POST',
                    params: {
                        sql: sql,
                        formName: form_name,
                        form_id: form_id,
                        xieyin_code: xieyin_code,
                        formMetadataId: formMetadataId,
                        formGridDatasFormatEventFunctionName: formGridDatasFormatEventFunctionName
                    },// end params
                    failure: function (response, options) {
                        Ext.MessageBox.alert('操作失败', '操作失败,可能是网络超时！');
                        return false;
                    },// end failure block
                    success: function (response, options) {
                        Ext.getBody().unmask(true);
                        if (response.responseText == 'fail') {
                            Ext.MessageBox.alert('操作失败', "创建过程中出现错误,请重试");
                            return false;
                        } else {
                            //TODO 刷新表格 窗口关闭
                            //parent.OperateWinObj.hide();
                            parent.refreshTree();
                        }
                    }
                });
            }

        }
    </script>
</head>
<body>
<form name='mainForm' action="" style="">
    <table class="pbs_table_second" border="0" align="center">
        <tr>
            <td class="pbs_td_text_right">数据集名称(<font color='red'>*</font>)
            </td>
            <td class="pbs_td_text_left"><input name="form_name" id='form_name' type="text" value="${form_name}" style="width: 500px"/></td>
        </tr>
        <tr>
            <td class="pbs_td_text_right">编码(<font color='red'>*</font>)
            </td>
            <td class="pbs_td_text_left"><input name="xieyin_code" id='xieyin_code' type="text" value="${xieyin_code}" style="width: 500px"/></td>
        </tr>
        <tr>
            <td class="pbs_td_text_right">数据格式化类
            </td>
            <td class="pbs_td_text_left">
                <textarea name="formGridDatasFormatEventFunctionName" id='formGridDatasFormatEventFunctionName' cols="75" rows="1">${formGridDatasFormatEventFunctionName}
                </textarea>
            </td>
        </tr>
        <tr>
            <td class="pbs_td_text_right">查询SQL(<font color='red'>*</font>)
            </td>
            <td class="pbs_td_text_left"><textarea cols="75" rows="7" name="sql" id="sql">${sql}</textarea></td>
        </tr>

    </table>
</form>
</body>
</html>