<html>
<head>
    <title>添加虚拟字段</title>
<#include "../../comm/radMeta.ftl" />
    <script type="text/javascript">
        var form_id = "${form_id}";
        var formMetadataId = "${formMetadataId}";
        function save() {
            var column_type = document.getElementById('column_type').value;
            var title = document.getElementById('title').value;
            var columnName = document.getElementById('columnName').value;

            var json = [];
            var subData = '{TABLE_COLUMN_TYPE: "' + column_type + '", COLUMN_TITLE: "' + title + '", COLUMN_NAME: "' + columnName + '"}';
            json.push(eval('(' + subData + ')'));
            var selectJson = Ext.util.JSON.encode(json);
            Ext.getBody().mask("处理中...");
            Ext.Ajax.request({
                url: basePath + 'repository/form/metadatamap/addFormMetadataMap',
                method: 'POST',
                params: {
                    selectJson: selectJson,
                    form_id: form_id,
                    formMetadataId: formMetadataId
                },
                failure: function (response, options) {
                    Ext.MessageBox.alert('操作失败', '操作失败,可能是网络超时！');
                    return false;
                },
                success: function (response, options) {
                    Ext.getBody().unmask(true);
                    if (response.responseText == 'fail') {
                        Ext.MessageBox.alert('操作失败', "创建过程中出现错误,请重试");
                        return false;
                    } else {
                        parent.activeDialog.hide();
                        parent.tableReload();
                    }
                }
            });
        }
    </script>
</head>
<body>
<form name='mainForm' action="" style="">
    <input name="column_type" id='column_type' type="hidden" style="width: 200px" value="virtual"/>
    <table class="pbs_table_second" border="0" align="center">
        <tr>
            <td class="pbs_td_text_right">显示名称(<font color='red'>*</font>)
            </td>
            <td class="pbs_td_text_left"><input name="title" id='title' type="text" style="width: 200px"/></td>
        </tr>
        <tr>
            <td class="pbs_td_text_right">表格字段(<font color='red'>*</font>)
            </td>
            <td class="pbs_td_text_left"><input name="columnName" id='columnName' type="text" style="width: 200px"/></td>
        </tr>
    </table>
</form>
</body>
</html>