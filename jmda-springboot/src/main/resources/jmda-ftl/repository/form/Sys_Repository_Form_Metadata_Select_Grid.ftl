<html>
<head>
<#include "../../comm/radMeta.ftl" />
<script type="text/javascript" src="${staticPath}/jmda-static/js/repository/form/sys_repository_form_metadata_select_grid.js"></script>
<style type="text/css">
A:LINK {
	color: #53709A;
	text-decoration: none;
}

A:VISITED {
	color: #53709A;
	TEXT-DECORATION: none;
}

A:HOVER {
	color: #53709A
}
</style>
<script type="text/javascript">
	var basePath = "${basePath}/";
	var form_id = "${form_id}";
	var formMetadataId = "${formMetadataId}";
</script>
<title>FORM</title>
</head>
<body>
	<form name="formForm">
		<div id="grid"></div>
	</form>
</body>
</html>