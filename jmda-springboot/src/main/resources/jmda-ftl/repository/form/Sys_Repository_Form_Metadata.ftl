<html>
<head>
<#include "../../comm/radMeta.ftl" />
<script type="text/javascript">
	var form_id = "${formId}";
	var form_title = "${form_name}";
</script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/repository/form/sys_repository_form_metadata.js"></script>
<title>表单</title>
</head>
<body>
	<form action="" id="formForm" name="formForm">
		<div id="formList_Tree" style="height: 100%"></div>
		<input type="hidden" name="formId" id="formId" value="${formId}" />
		<input type="hidden" name="uiType" id="uiType" value="" />
		<input type="hidden" name="masterDataset" id="masterDataset" value="" />
		<input type="hidden" name="uiParams" id="uiParams" value="" />
	</form>
</body>
</html>