<html>
<head>
<#include "../../comm/radMeta.ftl" />
<script type="text/javascript" src="${staticPath}/jmda-static/js/repository/form/sys_repository_form_metadata_buttons.js"></script>
<title></title>
</head>
<body>
	<script type="text/javascript">
		var form_id = "${form_id}";
		var formMetadataId = "${formMetadataId}";
		var formMetadataName = "${formMetadataName}";
		var is_master_dataset = ${is_master_dataset};
	</script>
	<form name="formPage">
		<div id="grid"></div>
	</form>
</body>
</html>