<!DOCTYPE html>
<html>
<head>
<#include "../../comm/radMeta.ftl" />
<script type="text/javascript" src="${staticPath}/jmda-static/plugs/ckeditor/ckeditor.js"></script>
<style type="text/css">
.footer_nav {
	bottom: 0;
	position: fixed;
	_position: absolute;
	z-index: 9999;
	bottom: 0px;
	_top: expression(eval(document.compatMode &&   
        document.compatMode == 'CSS1Compat')?   
        documentElement.scrollTop+(documentElement.clientHeight-this.clientHeight)-
		 0: document.body.scrollTop+(document.body.clientHeight-this.clientHeight)-
		 0);
	width: 100%;
	z-index: 1;
}
</style>
<title>表单设计</title>
<script type="text/javascript">
	var formId = "${formId}";
	var templateName = "${templateName}";
	var templatePage = "${templatePage}";
	var tdJson = "${tdJson}";
</script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/repository/form/sys_repository_form_templete.js"></script>
</head>
<body style="height: 100%">
	<form name="formForm" id="formForm" method="post">
		<div id="toolbar"></div>
		<textarea id="editor_id" name="editor_id">${ftlHtml}</textarea>
		<script type="text/javascript">
			var editor = CKEDITOR.replace('editor_id');
		</script>
	</form>
	<div id="bottomDiv" class="footer_nav"></div>
</body>
</html>