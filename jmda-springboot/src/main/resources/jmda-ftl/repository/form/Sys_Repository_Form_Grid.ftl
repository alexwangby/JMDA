<html>
<head>
<#include "../../comm/radMeta.ftl" />
<script type="text/javascript" src="${staticPath}/jmda-static/js/repository/form/sys_repository_form_grid.js"></script>
<style type="text/css">
A:LINK {
	color: #53709A;
	text-decoration: none;
}

A:VISITED {
	color: #53709A;
	TEXT-DECORATION: none;
}

A:HOVER {
	color: #53709A
}
</style>
<script type="text/javascript">
	var applicationId = "${applicationId}";
</script>
<title>FORM</title>
</head>
<body>
	<form name="formForm">
		<div id="grid"></div>
		<input type="hidden" name="groupId" id="groupId" value="${model_lib_id}" />
	</form>
</body>
</html>