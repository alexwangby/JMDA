<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<#include "../../comm/radMeta.ftl" />
<style type="text/css">
.STYLE3 {
	font-size: 12px
}
</style>
<title>ui下拉列表</title>
</head>
<script type="text/javascript">
  var type = "${type}";
  window.onload = function() {
    if(type!='sql'){
       document.getElementById("radionormallID").checked = "checked";
       document.getElementById("sqlTr").style.display = "none";
	   document.getElementById("clTr").style.display = "";
    }else{
       document.getElementById("radiosqlID").checked = "checked";
       document.getElementById("clTr").style.display = "none";
	   document.getElementById("sqlTr").style.display = "";
    }
  }
</script>
<body>

	<table border="0" align="left" style="width: 90%; margin-left: 2%;" class="pbs_table_second">

		<tr>
			<td style="width: 25%" class="pbs_td_text_right">扩展代码: CSS/javaScript</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css"><textarea cols="50" rows="4" name="extendedCode" id="extendedCode">${extendedCode}</textarea></td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">组件类别</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css"><input type="radio" name="radio" id="radionormallID" onclick="chang1()" checked="checked"/>
	常量&nbsp;&nbsp;<input type="radio" id="radiosqlID" name="radio" onclick="chang2()"/>&nbsp;sql</td>
		</tr>
		<tr id="clTr">
			<td style="width: 25%" class="pbs_td_text_right">组件属性(<font color='red'>*</font>):
			</td>
			<td style="width: 70%" class="pbs_td_text_left" valign="top" id="selectId">
			<input style="width: 70%" type="text" name="normalValue" id="normalValue" value="${normalValue }"/>
			</td>
		</tr>
		<tr id="sqlTr">
			<td style="width: 25%" class="pbs_td_text_right">组件属性(<font color='red'>*</font>):
			</td>
			<td style="width: 70%" class="pbs_td_text_left" valign="top" id="selectId">
			取值字段：<input style="width: 50%; margin-bottom: 5px;" type="text" name="getFieldValue" id="getFieldValue" value="${getFieldValue }" /><br /> 
			显示字段：<input style="width: 50%; margin-bottom: 5px;" type="text" name="displayNameValue" id="displayNameValue" value="${displayNameValue}" /><br /> 
			sql&nbsp;语&nbsp;句：<input style="width: 50%" type="text" name="sql" id="sql" style="width: 350px;" value="${sql}" /><br/>
			扩&nbsp;&nbsp;展&nbsp;类：<input style="width: 70%;margin-bottom: 5px;" type="text" name="extClass" id="extClass"  value="${extClass}" />
			<!-- &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="checkSQL();">测试</a> -->
			</td>
		</tr>
	</table>

	<script type="text/javascript">
		function getSettingParams() {
		    var type="normal";
		    if(document.getElementById("radiosqlID").checked){
		      type = "sql";
		      
		    }
			return '{"extendedCode":"' + document.getElementById('extendedCode').value + '","getFieldValue":"' + document.getElementById('getFieldValue').value + '","displayNameValue":"' + document.getElementById('displayNameValue').value + '","sql":"' + document.getElementById('sql').value + '","type":"' + type + '","normalValue":"' + document.getElementById('normalValue').value + '","extClass":"' + document.getElementById('extClass').value +'"}';
		}
		function chang1() {
	      document.getElementById("getFieldValue").value = "";
	      document.getElementById("displayNameValue").value = "";
	      document.getElementById("sql").value = "";
	      document.getElementById("sqlTr").style.display = "none";
	      document.getElementById("clTr").style.display = "";
         }
        function chang2() {
	      document.getElementById("normalValue").value = "";
          document.getElementById("clTr").style.display = "none";
	      document.getElementById("sqlTr").style.display = "";
         }
	</script>
</body>
</html>