<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<#include "../../comm/radMeta.ftl" />
<style type="text/css">
.STYLE3 {
	font-size: 12px
}
</style>
<title>ui单行文本</title>
</head>
<body>
	<table border="0" align="left" style="width: 90%; margin-left: 2%" class="pbs_table_second">
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">选择器XML文件</td>
			<td style="width: 70%" class="pbs_td_text_left">
				<input style="width: 50%; float: left;" name="selectorId" type="text" id="selectorId" size="10" value="${selectorId}" />
			</td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">显示标题</td>
			<td style="width: 70%" class="pbs_td_text_left">
				<input style="width: 50%; float: left;" name="ui_title" type="text" id="ui_title" size="10" value="${title}" />
			</td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">数据映射</td>
			<td style="width: 70%" class="pbs_td_text_left">
				<input style="width: 50%; float: left;" name="ui_title" type="text" id="ref_params" size="10" value="${ref_params}" />
			</td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">宽度</td>
			<td style="width: 70%" class="pbs_td_text_left">
				<input style="width: 50%; float: left;" name="ui_width" type="text" id="ui_width" size="10" value="${width}" />
			</td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">高度</td>
			<td style="width: 70%" class="pbs_td_text_left">
				<input style="width: 50%; float: left;" name="ui_height" type="text" id="ui_height" size="10" value="${height}" />
			</td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">选择JS函数</td>
			<td style="width: 70%" class="pbs_td_text_left">
				<input style="width: 50%; float: left;" name="callBackFunction" type="text" id="callBackFunction" size="10" value="${callBackFunction}" />
			</td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">URL参数</td>
			<td style="width: 70%" class="pbs_td_text_left">
				<input style="width: 50%; float: left;" name="urlParams" type="text" id="urlParams" size="10" value="${urlParams}" />
			</td>
		</tr>
		<tr style="display:none;">
			<td style="width: 25%" class="pbs_td_text_right">扩展代码: CSS/javaScript</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css"><textarea cols="50" rows="4" name="extendedCode" id="extendedCode">${extendedCode}</textarea></td>
		</tr>
	</table>

	<script type="text/javascript">
		function getSettingParams() {
			return '{"selectorId":"' + document.getElementById('selectorId').value + 
			'","title":"' + document.getElementById('ui_title').value + 
			'","width":"' + document.getElementById('ui_width').value + 
			'","height":"' + document.getElementById('ui_height').value + 
			'","callBackFunction":"' + document.getElementById('callBackFunction').value + 
			'","urlParams":"' + document.getElementById('urlParams').value + 
			'","ref_params":"' + document.getElementById('ref_params').value + 
			'","extendedCode":"' + document.getElementById('extendedCode').value + '"}';
		}
	</script>
</body>
</html>