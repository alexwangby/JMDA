<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<#include "../../comm/radMeta.ftl" />
<style type="text/css">
.STYLE3 {
	font-size: 12px
}
</style>
<title>ui单行文本</title>
</head>
<body>
<table border="0" align="left" style="width: 90%; margin-left: 2%" class="pbs_table_second">

		<tr>
			<td style="width: 25%" class="pbs_td_text_right">最小位数</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css">
			<input style="width: 50%; float: left;" name="minLength" type="text" id="minLength" size="10" value="${minLength}" />
			</td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">最大位数</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css">
			<input style="width: 50%; float: left;" name="maxLength" type="text" id="maxLength" size="10" value="${maxLength}" />
			</td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">小数点位数</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css">
			<input style="width: 50%; float: left;" name="decimal_point" type="text" id="decimal_point" size="10" value="${decimal_point}" />
			</td>
		</tr>
	<!--	<tr>
			<td style="width: 25%" class="pbs_td_text_right">千分位符号</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css">
			<input style="width: 50%; float: left;" name="thousandth" type="text" id="thousandth" size="10" value="${thousandth}" />
			</td>
		</tr>
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">整数与小数分隔符</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css">
			<input style="width: 50%; float: left;" name="delimiter" type="text" id="delimiter" size="10" value="${delimiter}" />
			</td>
		</tr>-->
		<tr>
			<td style="width: 25%" class="pbs_td_text_right">扩展代码: CSS/javaScript</td>
			<td style="width: 70%" class="pbs_td_text_left pbs_textarea_css"><textarea cols="50" rows="3" name="extendedCode" id="extendedCode">${extendedCode}</textarea></td>
		</tr>
	</table>

<script type="text/javascript">
	function getSettingParams() {
		return '{"minLength":"' + document.getElementById('minLength').value + '","maxLength":"' + document.getElementById('maxLength').value + '","extendedCode":"' + document.getElementById('extendedCode').value + '","decimal_point":"' + document.getElementById('decimal_point').value + '"}';
	}
</script>
</body>
</html>