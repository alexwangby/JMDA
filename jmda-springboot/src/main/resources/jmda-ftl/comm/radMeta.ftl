<#if !basePath??><#assign basePath=request.contextPath ></#if> 
<#if !Application.STATIC_SERVER??><#assign staticPath=request.contextPath ></#if>
<meta name="description" content="blank page" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />  
<meta http-equiv="Content-Type" content="textml; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="${staticPath}/jmda-static/css/public.css">
<link rel="stylesheet" type="text/css" href="${staticPath}/jmda-static/css/pbulic_table_base.css">
<script type="text/javascript" src="${staticPath}/jmda-static/js/validate.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/pinyin.js"></script>
<link rel="stylesheet" type="text/css" href="${staticPath}/jmda-static/js/extjs3/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="${staticPath}/jmda-static/css/bizmodel.css" />
<script type="text/javascript">
var basePath = "${basePath}/";
var STATIC_SERVER = '${staticPath}/';
var application_STATIC_SERVER = '${staticPath}/';
</script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/extjs3/ext-base.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/extjs3/ext-all.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/extjs3/ext-lang-zh_CN.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/extjs3/ExtWindow.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/extjs3/SimpleExtWindow.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/extjs3/ux/CheckColumn.js"></script>