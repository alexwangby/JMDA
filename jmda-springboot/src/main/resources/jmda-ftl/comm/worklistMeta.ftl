<#if !basePath??><#assign basePath=request.contextPath ></#if>
<#if !Application.STATIC_SERVER??><#assign staticPath=request.contextPath ></#if>
<meta name="description" content="blank page" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
    var BASE_PATH = '${basePath}';
    var basePath = '${basePath}';
    var STATIC_SERVER = '${staticPath}/';
</script>
<link rel="shortcut icon" href="${staticPath}/jmda-static/assets/img/favicon.png" type="image/x-icon">
<link href="${staticPath}/jmda-static/assets/css/custom-theme/jquery-ui-1.10.0.custom.css" rel="stylesheet" type="text/css" />
<!--local Styles-->
<link href="${staticPath}/jmda-static/assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="${staticPath}/jmda-static/assets/css/weather-icons.min.css" rel="stylesheet" />
<link id="beyond-link" href="${staticPath}/jmda-static/assets/css/beyond.min.css" rel="stylesheet" />
<link href="${staticPath}/jmda-static/assets/css/demo.min.css" rel="stylesheet" />
<link href="${staticPath}/jmda-static/assets/css/typicons.min.css" rel="stylesheet" />
<link href="${staticPath}/jmda-static/assets/css/animate.min.css" rel="stylesheet" />
<link id="skin-link" href="${staticPath}/jmda-static/assets/css/skins/azure.min.css" rel="stylesheet" type="text/css" />
<link href="${staticPath}/jmda-static/common/plugs/load.css" rel="stylesheet" />
<link rel="stylesheet" href="${staticPath}/jmda-static/assets/css/chosen/chosen.min.css"/>
<!--local Styles-->
<link href="${basePath}/assets/css/font-awesome.min.css" rel="stylesheet" />

<script src="${staticPath}/jmda-static/assets/js/jquery.min.js"></script>
<script src="${staticPath}/jmda-static/assets/js/bootstrap.min.js"></script>
<script src="${staticPath}/jmda-static/assets/js/slimscroll/jquery.slimscroll.min.js"></script>
<script src="${staticPath}/jmda-static/assets/js/jquery-ui-1.10.0.custom.min.js"></script>	
<!--footer-->
<script src="${staticPath}/jmda-static/assets/js/beyond.js"></script>
<script src="${staticPath}/jmda-static/assets/js/toastr/toastr.js"></script>
<script src="${staticPath}/jmda-static/assets/js/bootbox/bootbox.js"></script>

<!--easyui -->
<link rel="stylesheet" type="text/css" href="${staticPath}/jmda-static/assets/js/jquery-easyui-1.4.3/themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="${staticPath}/jmda-static/assets/js/jquery-easyui-1.4.3/themes/icon.css">
<script type="text/javascript" src="${staticPath}/jmda-static/assets/js/jquery-easyui-1.4.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/assets/js/jquery-easyui-1.4.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/common/plugs/jquery.load.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/common/plugs/datagrid-detailview.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/assets/js/chosen/chosen.jquery.min.js"></script>


<!--init-->
<script src="${staticPath}/jmda-static/assets/js/init.js"></script>

<!--diy-->
<script type="text/javascript" src="${staticPath}/jmda-static/js/selector-helper.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/engine/worklist/sys_engine_worklist_main.js"></script>

<#--map引入-->
<script type="text/javascript" src="${staticPath}/jmda-static/js/WeJson.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/validate.js"></script>
