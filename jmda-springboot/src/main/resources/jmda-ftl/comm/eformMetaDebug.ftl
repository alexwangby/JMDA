<#if !basePath??><#assign basePath=request.contextPath ></#if>
<meta name="description" content="blank page"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script type="text/javascript">
    var BASE_PATH = '${basePath}';
    var basePath = '${basePath}';
    var STATIC_SERVER = 'http://192.168.10.20/hbdiy-static/';
    var UPLOAD_SERVER = '${Application.UPLOAD_SERVER}';
    var FILES_SERVER = '${Application.FILES_SERVER}';
</script>
<link rel="shortcut icon" href="http://192.168.10.20/hbdiy-static//assets/img/favicon.png" type="image/x-icon">
<link href="http://192.168.10.20/hbdiy-static//assets/css/custom-theme/jquery-ui-1.10.0.custom.css" rel="stylesheet" type="text/css"/>
<!--local Styles-->
<link href="${basePath}/assets/css/bootstrap.min.css" rel="stylesheet"/>
<link href="http://192.168.10.20/hbdiy-static//assets/css/weather-icons.min.css" rel="stylesheet"/>
<link id="beyond-link" href="http://192.168.10.20/hbdiy-static//assets/css/beyond.min.css" rel="stylesheet"/>
<link href="http://192.168.10.20/hbdiy-static//assets/css/demo.min.css" rel="stylesheet"/>
<link href="http://192.168.10.20/hbdiy-static//assets/css/typicons.min.css" rel="stylesheet"/>
<link href="http://192.168.10.20/hbdiy-static//assets/css/animate.min.css" rel="stylesheet"/>
<link id="skin-link" href="http://192.168.10.20/hbdiy-static//assets/css/skins/azure.min.css" rel="stylesheet" type="text/css"/>
<!--local Styles-->
<link href="${basePath}/assets/css/font-awesome.min.css" rel="stylesheet"/>

<script src="http://192.168.10.20/hbdiy-static//assets/js/jquery.min.js"></script>
<script src="http://192.168.10.20/hbdiy-static//assets/js/bootstrap.min.js"></script>
<script src="http://192.168.10.20/hbdiy-static//assets/js/slimscroll/jquery.slimscroll.min.js"></script>
<script src="http://192.168.10.20/hbdiy-static//assets/js/jquery-ui-1.10.0.custom.min.js"></script>
<!--footer-->
<script src="http://192.168.10.20/hbdiy-static//assets/js/beyond.js"></script>
<script src="http://192.168.10.20/hbdiy-static//assets/js/toastr/toastr.js"></script>
<script src="http://192.168.10.20/hbdiy-static//assets/js/bootbox/bootbox.js"></script>

<!--easyui -->
<link rel="stylesheet" type="text/css" href="http://192.168.10.20/hbdiy-static//assets/js/jquery-easyui-1.4.3/themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="http://192.168.10.20/hbdiy-static//assets/js/jquery-easyui-1.4.3/themes/icon.css">
<script type="text/javascript" src="http://192.168.10.20/hbdiy-static//assets/js/jquery-easyui-1.4.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="http://192.168.10.20/hbdiy-static//assets/js/jquery-easyui-1.4.3/locale/easyui-lang-zh_CN.js"></script>
<!--
<script type="text/javascript" src="${basePath}/assets/js/jquery-validation/dist/jquery.validate.js"></script>
<script type="text/javascript" src="${basePath}/assets/js/jquery-validation/dist/localization/messages_zh.js"></script>
-->

<!--init-->
<script src="http://192.168.10.20/hbdiy-static//assets/js/init.js"></script>
<#--打印控件引入-->
<script type="text/javascript" src="${staticPath}/jmda-static/js/LodopFuncs.js"></script>
<#--map引入-->
<script type="text/javascript" src="${staticPath}/jmda-static/js/WeJson.js"></script>
<script type="text/javascript" src="${staticPath}/jmda-static/js/validate.js"></script>
<link href="${staticPath}/jmda-static/css/iF.step.css" rel="stylesheet" />