// grid和布局的全局变量
var sm;
var cm;
var store;
var grid;
var viewport;
/*if  (!Ext.grid.GridView.prototype.templates) {
    Ext.grid.GridView.prototype.templates = {};    
}   
Ext.grid.GridView.prototype.templates.cell =  new  Ext.Template(    
     '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}" tabIndex="0" {cellAttr}>' ,    
     '<div class="x-grid3-cell-inner x-grid3-col-{id}" {attr}>{value}</div>' ,    
     '</td>'    
); */
Ext.onReady(function() {
	Ext.QuickTips.init();

	/* 表格 */
	var sm = new Ext.grid.CheckboxSelectionModel();

	cm = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(), sm, {
		header : '字段名称',
		dataIndex : 'COLUMN_LABEL',
		width : 100
	}, {
		header : '表名称',
		dataIndex : 'OPERATOR',
		align : 'left',
		dataIndex : 'TABLE_NAME',
		width : 100
	}, {
		header : '显示标题',
		dataIndex : 'COLUMN_TITLE',
		align : 'left',
		width : 100
	}, {
		header : '字段类型',
		dataIndex : 'COLUMN_TYPE',
		align : 'center',
		width : 100
	}, {
		header : 'COLUMN_NAME',
		dataIndex : 'COLUMN_NAME',
		align : 'center',
		hidden:true,
		width : 100
	} , {
		header : '字段长度',
		dataIndex : 'COLUMN_LENGTH',
		align : 'center',
		width : 100
	}]);
	var Plant = Ext.data.Record.create([ {
		name : 'TABLE_NAME',
		type : 'string'
	}, {
		name : 'COLUMN_NAME',
		type : 'string'
	}, {
		name : 'COLUMN_TITLE',
		type : 'string'
	}, {
		name : 'COLUMN_TYPE',
		type : 'string'
	}, {
		name : 'COLUMN_LENGTH',
		type : 'string'
	}, {
		name : 'COLUMN_LABEL',
		type : 'string'
	} ]);
	
	store = new Ext.data.Store({
		baseParams : {
			id : id
		},
		proxy : new Ext.data.HttpProxy({
			url : basePath + 'repository/worklist/columns/getWorListMetadataMapSelectJsonData'
		}),
		reader : new Ext.data.JsonReader({
			totalProperty : "total",
			root : "root",
			id : "COLUMN_LABEL"
		}, [ 'COLUMN_NAME','COLUMN_LABEL', 'TABLE_NAME', 'COLUMN_TITLE', 'COLUMN_TYPE', 'COLUMN_LENGTH' ], Plant)
	});
	grid = new Ext.grid.GridPanel({
		region : 'grid',
		title : '请选中需要添加的字段',
		store : store,
		region : 'center',
		cm : cm,
		sm : sm,
		trackMouseOver : false,
		loadMask : {
			msg : '正在加载数据，请稍候...'
		},
		border : false,
		viewConfig : {
			forceFit : true ,
		    templates: {        
		        cell: new Ext.Template(      
		          '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id}   x-selectable {css}" style="{style}"   tabIndex="0" {cellAttr}>',     
		          '<div class="x-grid3-cell-inner x-grid3-col-{id}"  {attr}>{value}</div>',    
		          '</td>'    
		           )    
		      }  
		}
	});

	viewport = new Ext.Viewport({
		layout : 'border',
		items : [ grid ]
	});
	store.load();
});

function getSelectVavlue() {
	var json = [];
	var arr = grid.getSelectionModel().getSelections();
	for (var i = 0; i < arr.length; i++) {
		var subData = '{';
		for (var colIndex = 0; colIndex < cm.getColumnCount(); colIndex++) {
			var fieldName = cm.getDataIndex(colIndex);
			if (fieldName != '') {
				var cellValue = arr[i].get(fieldName);
				subData = subData + fieldName + ":" + "\"" + cellValue + "\",";
			}
		}
		if (subData.length > 1) {
			subData = subData.substr(0, subData.length - 1);
		}

		subData = subData + '}';
		json.push(eval('(' + subData + ')'));
	}
	return  Ext.util.JSON.encode(json);
}
