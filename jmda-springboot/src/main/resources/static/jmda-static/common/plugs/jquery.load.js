
var LoadModel = {
		show:function(text){
			$("#load_model").remove();
			if (text == null || text == '' || (typeof text) == 'undefined') {
				text = "<div class='loader-inner ball-clip-rotate'><div></div></div>";
			}
		    var css_type=text;
		    var modeldiv=$("<div id='load_model' style='display: block;  position: fixed;  top: 0%;  left: 0%;  width: 100%;  height: 100%;  background-color: black;  z-index:1001;  -moz-opacity: 0.7;  opacity:.70;  filter: alpha(opacity=70);'><div class='loader'  style='left: 50%;right: 50%;position:fixed;top: 50%;bottom:50%;'>"+css_type+"</div></div>");
		    $("body").append(modeldiv);
		},
		hide:function(){
		    $("#load_model").remove();
		}
}