package com.jmda.platform.repository.worklist.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.commom.SpringContextUtil;
import com.jmda.platform.engine.worklist.component.WorkListSearchUIComponentAbst;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class SearchModel {
	@XStreamAlias("id")
	@JsonProperty
	private String id;
	@XStreamAlias("columnName")
	@JsonProperty
	private String columnName;
	@XStreamAlias("title")
	@JsonProperty
	private String title;
	@XStreamAlias("where")
	@JsonProperty
	private String where;
	@XStreamAlias("uiType")
	@JsonProperty
	private String uiType;
	@XStreamAlias("uiParam")
	@JsonProperty
	private String uiParam;
	
	@JsonProperty
	private String html;
	
	@JsonProperty
	private String showSearchNum;
	
	@XStreamAlias("orderIndex")
	@JsonProperty
	private int orderIndex;
	@XStreamAlias("customHtml")
	@JsonProperty
	private String customHtml;
	
	public String getShowSearchNum() {
		return showSearchNum;
	}

	public void setShowSearchNum(String showSearchNum) {
		this.showSearchNum = showSearchNum;
	}

	public String getCustomHtml() {
		return customHtml;
	}

	public void setCustomHtml(String customHtml) {
		this.customHtml = customHtml;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWhere() {
		return where;
	}

	public void setWhere(String where) {
		this.where = where;
	}

	public String getUiType() {
		return uiType;
	}

	public void setUiType(String uiType) {
		this.uiType = uiType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUiParam() {
		return uiParam;
	}

	public void setUiParam(String uiParam) {
		this.uiParam = uiParam;
	}

	public int getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}

	public String getHtml() {
		WorkListSearchUIComponentAbst abs = SpringContextUtil.getBean("WORKLIST_UI_SEARCH_" + uiType);
		if (abs != null) {
			Map<String, Object> params = LocalCacheManager.getContextParams();
			abs.setSearchModel(this);
			abs.setMapParams(params);
			String htmlDefine=abs.getHtmlDefine(uiParam);
			if (htmlDefine != null) {
				return htmlDefine;
			}
		}
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}
	

}
