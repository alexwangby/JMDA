package com.jmda.platform.repository.worklist.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.math.BigDecimal;

public class ExtColumnModel {
    @XStreamAlias("columnName")
    private String columnName;
    @XStreamAlias("title")
    private String title;
    @XStreamAlias("width")
    private BigDecimal width;
    @XStreamAlias("hidden")
    private boolean hidden = false;
    @XStreamAlias("sortable")
    private boolean sortable = false;
    @XStreamAlias("formatter")
    private String formatter;
    @XStreamAlias("column_align")
    private String column_align;
    @XStreamAlias("ref_type")
    String ref_type;
    @XStreamAlias("ref_params")
    String ref_params;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }

    public String getFormatter() {
        return formatter;
    }

    public void setFormatter(String formatter) {
        this.formatter = formatter;
    }

    public String getColumn_align() {
        return column_align;
    }

    public void setColumn_align(String column_align) {
        this.column_align = column_align;
    }

    public String getRef_type() {
        return ref_type;
    }

    public void setRef_type(String ref_type) {
        this.ref_type = ref_type;
    }

    public String getRef_params() {
        return ref_params;
    }

    public void setRef_params(String ref_params) {
        this.ref_params = ref_params;
    }

}
