package com.jmda.platform.repository.application.cache;

import com.jmda.platform.commom.BaseCache;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.commom.ResourcePathConfig;
import com.jmda.platform.commom.util.BlobUtil;
import com.jmda.platform.commom.util.FileUtil;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.repository.RespositoryConstant;
import com.jmda.platform.repository.application.model.ApplicationModel;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import net.sf.ehcache.search.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ApplicationCache implements BaseCache<ApplicationModel> {
	@Resource
	ResourcePathConfig resourceLoaderPath;
	@Resource
	DataBaseService db;
	@Override
	public void put(ApplicationModel model) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		cache.remove(model.getId());
		Element element = new Element(model.getId(), model);
		cache.put(element);
	}

	@Override
	public List<ApplicationModel> getAll() {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		List<ApplicationModel> list = new LinkedList<ApplicationModel>();
		Query query = cache.createQuery().includeValues();
		Attribute<Integer> order = cache.getSearchAttribute("index");
		query.addOrderBy(order, Direction.ASCENDING);
		Results rs = query.execute();
		List<Result> all = rs.all();
		for (Result r : all) {
			ApplicationModel model = (ApplicationModel) r.getValue();
			list.add(model);
		}
		query.end();
		return list;
	}

	@Override
	public ApplicationModel get(String id) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		Element element = cache.get(id);
		if (element != null) {
			return (ApplicationModel) element.getObjectValue();
		}
		return null;
	}

	@Override
	public void remove(String id) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		cache.remove(id);
	}



	@Override
	public List<ApplicationModel> queryList(ApplicationModel param) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		List<ApplicationModel> list = new LinkedList<ApplicationModel>();
		Query query = cache.createQuery().includeValues();
		LocalCacheManager.addSearchAttribute(cache, query, param);
		Results rs = query.execute();
		List<Result> all = rs.all();
		for (Result r : all) {
			ApplicationModel model = (ApplicationModel) r.getValue();
			list.add(model);
		}
		query.end();
		return list;
	}

	@Override
	public String getFilePath() throws IOException {
		// TODO Auto-generated method stub
		return resourceLoaderPath.getApplicationPath();
	}
	@PostConstruct
	public void initialize() throws IOException {
		// TODO Auto-generated method stub
		String[] searchs = { "id", "name","index","xieyin_code" };
		Cache cache = LocalCacheManager.registCache(this.getClass(), searchs);
		if(RespositoryConstant.MDA_RELY){
			String filePath = getFilePath();
			List<String> ls = FileUtil.getXmlFileList(filePath);
			for (String fileName : ls) {
				ApplicationModel model = FileUtil.coverXmlToBean(ApplicationModel.class, filePath + fileName);
				if (model != null) {
					Element element = new Element(model.getId(), model);
					cache.put(element);
				}
			}
		}else{
		}
	}
}
