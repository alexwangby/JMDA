package com.jmda.platform.repository.worklist.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.commom.SequenceService;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.worklist.model.ButtonModel;
import com.jmda.platform.repository.worklist.model.WorkListModel;
import com.jmda.platform.repository.worklist.service.WorkListService;
import com.jmda.platform.repository.worklist.util.WorkListButtonsCommonMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/repository/worklist/buttons")
public class WorkListButtonsController {
	@Resource
	WorkListService workListService;
	@Resource
	SequenceService seq;
	@Resource
	private WorkListButtonsCommonMethod workListButtonsCommonMethod = null;
	@RequestMapping(value = "/getWorkListButtonsPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getWorkListButtonsPage(String id, ModelMap map) throws ResositoryException {
		WorkListModel model = workListService.get(id);
		map.put("id", id);
		map.put("name", model.getName());
		return "repository/worklist/Sys_Repository_WorkList_Buttons";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getWorkListButtonsJson", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getWorkListButtonsJson(String id) throws JsonProcessingException, ResositoryException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> json = new HashMap<String, Object>();
		WorkListModel model = workListService.get(id);
		List<ButtonModel> list = model.getButton();
		list = list == null ? new ArrayList<ButtonModel>() : list;
		if(list!=null){
		Collections.sort(list, new Comparator(){   
			@Override
			public int compare(Object o1, Object o2) {
			       if(((ButtonModel)o1).getOrderIndex() > ((ButtonModel)o2).getOrderIndex()){   

	                    return 1;   
	                }   
	                if(((ButtonModel)o1).getOrderIndex() == ((ButtonModel)o2).getOrderIndex()){   
	                    return 0;   
	               }   
	               return -1;   
			}
        }); 
		}
		json.put("root", list);
		json.put("total", list == null ? 0 : list.size());
		return mapper.writeValueAsString(json);
	}

	@RequestMapping(value = "/saveWorkListButton", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String saveWorkListButton(String id, String datas) throws ResositoryException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		List<ButtonModel> newButtonList = mapper.readValue(datas, new TypeReference<ArrayList<ButtonModel>>() {
		});

		if (newButtonList != null && newButtonList.size() > 0) {
			WorkListModel workListModel = workListService.get(id);
			List<ButtonModel> workListButtonModellist = workListModel.getButton();
			workListButtonModellist = workListButtonModellist == null ? new ArrayList<ButtonModel>() : workListButtonModellist;
			//新建按钮
			for (ButtonModel newButtonModel : newButtonList) {
				if (newButtonModel.getId() == null || newButtonModel.getId().trim().length() == 0) {
					newButtonModel.setId(seq.getUUID());
					newButtonModel.setOrderIndex(workListService.getWorkListButtonNextIndex(workListModel));
					workListButtonModellist.add(newButtonModel);
				}
			}
			//修改的按钮
			for (ButtonModel button : workListButtonModellist) {
				for (ButtonModel newButtonModel : newButtonList) {
					if (button.getId().equals(newButtonModel.getId())) {
						button.setHandler(newButtonModel.getHandler());
						button.setIconCls(newButtonModel.getIconCls());
						button.setTitle(newButtonModel.getTitle());
						button.setTooltip(newButtonModel.getTooltip());
						button.setCustomHtml(newButtonModel.getCustomHtml());
					}
				}
			}
			workListModel.setButton(workListButtonModellist);
			workListService.store(workListModel);
		}

		return "ok";
	}

	@RequestMapping(value = "/removeWorkListButton", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String removeWorkListButton(String id, String ids) throws ResositoryException, IOException {
		String[] idArray = ids.split(",");
		if (idArray != null && idArray.length > 0) {
			WorkListModel workListModel = workListService.get(id);
			List<ButtonModel> workListButtonModellist = workListModel.getButton();
			List<ButtonModel> newworkListButtonModellist = new ArrayList<ButtonModel>();
			for (ButtonModel buttonModel : workListButtonModellist) {
				boolean isOk = true;
				for (int i = 0; i < idArray.length; i++) {
					if (buttonModel.getId().equals(idArray[i])) {
						isOk = false;
						break;
					}
				}
				if (isOk) {
					newworkListButtonModellist.add(buttonModel);
				}
			}
			workListModel.setButton(newworkListButtonModellist);
			workListService.store(workListModel);
		}
		return "ok";
	}
	@RequestMapping(value = "/sortGird", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String sortGirdMap( String sourceId,  String targetId,String id) {
		return workListButtonsCommonMethod.workListButtonOrder(id, sourceId, targetId);
	}

}
