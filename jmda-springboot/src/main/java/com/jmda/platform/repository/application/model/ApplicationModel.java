package com.jmda.platform.repository.application.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;

@XStreamAlias("application")
public class ApplicationModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5976339217971159366L;
	@XStreamAlias("name")
	private String name;
	@XStreamAlias("index")
	private int index;
	@XStreamAlias("id")
	private String id;
	@XStreamAlias("xieyin_code")
	@JsonProperty
	private String xieyin_code;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getXieyin_code() {
		return xieyin_code;
	}

	public void setXieyin_code(String xieyin_code) {
		this.xieyin_code = xieyin_code;
	}

}
