package com.jmda.platform.repository.worklist.action;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.engine.mybatis.MybatisEngine;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.worklist.model.MaterialInfoTreeModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/repository/worklist/tree")
public class WorkListTreeController {
    @Resource
    MybatisEngine mybatisEngine;
    @RequestMapping( method = {RequestMethod.POST, RequestMethod.GET}) 
    public String getMaterialInfoTreePage(String materialId,ModelMap map) throws ResositoryException {
    	map.put("materialId", materialId);
    	return "system/sys_material_easyui";
    }
    //获取物料的信息树
    @RequestMapping(value = "/getMaterialInfoTree", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String getMaterialInfoTree(String materialId) throws ResositoryException {
        ObjectMapper objectMapper = new ObjectMapper();
        //根节点集合
        List<MaterialInfoTreeModel> roots = new ArrayList();
        if (materialId != null && !"".equalsIgnoreCase(materialId.trim())) {
            //基本信息
            MaterialInfoTreeModel baseInfoNode = new MaterialInfoTreeModel();
            baseInfoNode.setText("基本信息");
            baseInfoNode.setState("open");
            Map baseInfoParams = new HashMap<>();
            baseInfoParams.put("materialId", materialId);
            Map baseInfo = mybatisEngine.selectOne("com.hbdiy.erp.wm.md.dao.mm_material_selector_DAO", "getMaterialBaseInfo", baseInfoParams);
            if (baseInfo != null && baseInfo.size() != 0) {
                List<MaterialInfoTreeModel> baseInfoNodeChildren = new ArrayList();
                MaterialInfoTreeModel materialNameNode = new MaterialInfoTreeModel();
                materialNameNode.setText("物料编码：" + baseInfo.get("MATERIAL_ID"));
                materialNameNode.setState("open");
                MaterialInfoTreeModel materialIdNode = new MaterialInfoTreeModel();
                materialIdNode.setText("物料名称：" + baseInfo.get("NAME"));
                materialIdNode.setState("open");
                baseInfoNodeChildren.add(materialNameNode);
                baseInfoNodeChildren.add(materialIdNode);
                baseInfoNode.setChildren(baseInfoNodeChildren);
            }

            //库存信息
            MaterialInfoTreeModel stockInfoNode = new MaterialInfoTreeModel();
            stockInfoNode.setText("库存信息");
            stockInfoNode.setState("open");
            roots.add(baseInfoNode);
            roots.add(stockInfoNode);
        }
        try {
            return objectMapper.writeValueAsString(roots);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
