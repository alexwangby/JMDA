package com.jmda.platform.repository.application.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.RespositoryConstant;
import com.jmda.platform.repository.application.model.ApplicationModel;
import com.jmda.platform.repository.application.service.ApplicationSevice;
import com.jmda.platform.repository.form.model.FormModel;
import com.jmda.platform.repository.form.service.FormService;
import com.jmda.platform.repository.worklist.model.WorkListModel;
import com.jmda.platform.repository.worklist.service.WorkListService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/repository/application")
public class ApplicationController {
	@RequestMapping(method = { RequestMethod.POST, RequestMethod.GET })
	public String getDesignerPortal() {
		return "repository/application/Sys_Repository_Application";
	}

	@Resource
	ApplicationSevice applicationSevice;
	@Resource
	FormService formService;
	@Resource
	WorkListService workListService;
	@RequestMapping(value = "/getCreatePage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getCreatePage(String applicationId, String typeName, ModelMap map) throws ResositoryException {
		List<ApplicationModel> list = applicationSevice.getAll();
		map.put("applicationList", list);
		map.put("typeName", typeName);
		map.put("applicationId", applicationId == null ? "" : applicationId);
		if(typeName!=null&&typeName.equals("模型分类")){
			ApplicationModel applicationModel = applicationSevice.get(applicationId);
			if(applicationModel!=null){
				map.put("name", applicationModel.getName());
				map.put("xieyin_code", applicationModel.getXieyin_code());
			}
		}
		return "repository/application/Sys_Repository_Application_Create";
	}

	@RequestMapping(value = "/createModel", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String createModel(String name, String typeName, String applicationId, String xieyin_code) throws ResositoryException {
		if (typeName.equals(RespositoryConstant.SYS_MODEL_TYPE_FORM_NAME)) {
			return formService.create(name, applicationId, xieyin_code);
		}else if (typeName.equals(RespositoryConstant.SYS_MODEL_TYPE_WORKLIST_NAME)) {
			return workListService.create(name, applicationId, xieyin_code);
		}else if(typeName.equals(RespositoryConstant.SYS_MODEL)){
			return storeApplication( name,  applicationId, xieyin_code);
		}
		return "ok";
	}

	@RequestMapping(value = "/getTreeJson", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getTreeJson(@RequestParam("node") String node) throws ResositoryException, JsonProcessingException {
		List<Map<String, Object>> json = new ArrayList<Map<String, Object>>();
		if (node.equals("root")) {
			List<ApplicationModel> modelLibList = applicationSevice.getAll();
			for (ApplicationModel model : modelLibList) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", "APP_" + model.getId());
				map.put("text", model.getName());
				map.put("leaf", false);
				map.put("cls", "treeBMCategory");
				map.put("draggable", false);
				json.add(map);
			}
		} else if (node.startsWith("APP_")) {
			String[] array = node.split("_");
			Map<String, Object> listmap = new HashMap<String, Object>();
			listmap.put("id", "LIST_" + array[1]);
			listmap.put("leaf", false);
			listmap.put("text", "列表模型");
			listmap.put("cls", "worklistview");
			listmap.put("draggable", false);
			json.add(listmap);

			Map<String, Object> formmap = new HashMap<String, Object>();
			formmap.put("id", "F_" + array[1]);
			formmap.put("leaf", false);
			formmap.put("text", "表单模型");
			formmap.put("cls", "treeBMFormCategory");
			formmap.put("draggable", false);
			json.add(formmap);

//			Map<String, Object> dataDicmap = new HashMap<String, Object>();
//			dataDicmap.put("id", "DD_" + array[1]);
//			dataDicmap.put("leaf", false);
//			dataDicmap.put("text", "数据字典");
//			dataDicmap.put("cls", "dictionary");
//			dataDicmap.put("draggable", false);
//			json.add(dataDicmap);

		} else if (node.startsWith("F_")) {
			String[] array = node.split("_");
			FormModel param = new FormModel();
			param.setApplication_id(array[1]);
			List<FormModel> formList = formService.queryList(param);
			if (formList != null && formList.size() > 0) {
				for (FormModel formModel : formList) {
					Map<String, Object> formmap = new HashMap<String, Object>();
					formmap.put("id", "FORM_" + formModel.getId());
					formmap.put("leaf", true);
					formmap.put("text", formModel.getName());
					formmap.put("cls", "treeBMForm");
					formmap.put("draggable", false);
					json.add(formmap);
				}
			}
		}else if(node.startsWith("LIST_")){
			String[] array = node.split("_");
			WorkListModel param=new WorkListModel();
			param.setApplicationId(array[1]);
			List<WorkListModel>  workListList=workListService.queryList(param);
			if (workListList != null && workListList.size() > 0) {
				for (WorkListModel workListViewModel : workListList) {
					Map<String, Object> workListMap = new HashMap<String, Object>();
					workListMap.put("id", "LISTDATA_" + workListViewModel.getId());
					workListMap.put("leaf", true);
					workListMap.put("text", workListViewModel.getName());
					workListMap.put("cls", "worklist");
					workListMap.put("draggable", false);
					json.add(workListMap);
				}
			}
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(json);
	}

	@RequestMapping("/storeApplication")
	@ResponseBody
	public String storeApplication(String applicatioName, String applicationId,String xieyin_code) {
		ApplicationModel model = new ApplicationModel();
		model.setName(applicatioName);
		model.setId(applicationId);
		model.setXieyin_code(xieyin_code);
		try {
			return applicationSevice.store(model);
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "no";
	}

	@RequestMapping("/getApplicationGridPage")
	public String getApplicationGridPage() {
		return "repository/application/Sys_Repository_Application_Grid";
	}

	@RequestMapping("/getApplicationGridJson")
	@ResponseBody
	public String getApplicationGridJson(HttpServletRequest request) throws ResositoryException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> json = new HashMap<String, Object>();
		List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
		List<ApplicationModel> modelLibList = applicationSevice.getAll();
		for (ApplicationModel model : modelLibList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("ID", model.getId());
			map.put("NAME", model.getName());
			map.put("OPERATOR", "<a href='javascript:void(0);' onClick=\"eidt('" + model.getId() + "');\" >编辑</a>&nbsp;&nbsp;&nbsp;<a href='javascript:void(0);' onClick=\"generatorCode('" + model.getId() + "');\" >代码生成器</a>");
			map.put("ORDER_INDEX", model.getIndex());
			//map.put("xieyin_code", model.getXieyin_code());
			records.add(map);
		}
		json.put("root", records);
		json.put("total", modelLibList.size());
		return mapper.writeValueAsString(json);
	}
	@RequestMapping(value = "/remove", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String remove(String ids) throws ResositoryException {
		String[] idArray = ids.split(",");
		if (idArray != null && idArray.length > 0) {
			for (int i = 0; i < idArray.length; i++) {
				WorkListModel workListModel = new WorkListModel();
				workListModel.setApplicationId(idArray[i]);
				List<WorkListModel> workList = workListService.queryList(workListModel);
				if(workList!=null&&workList.size()>0){
					return "workList";
				}
				FormModel param = new FormModel();
				param.setApplication_id(idArray[i]);
				List<FormModel> formList = formService.queryList(param);
				if(formList!=null&&formList.size()>0){
					return "formList";
				}
				
			}
			for (int i = 0; i < idArray.length; i++) {
				applicationSevice.remove(idArray[i]);
			}
		}
		return "ok";
	}
}
