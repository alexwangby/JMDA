package com.jmda.platform.repository.worklist.model;

import java.io.Serializable;
import java.sql.Blob;


public class WorkListDBModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String WORKLIST_ID;
	private String NAME;
	private Blob XML_CONTENT;
	public String getWORKLIST_ID() {
		return WORKLIST_ID;
	}
	public void setWORKLIST_ID(String wORKLIST_ID) {
		WORKLIST_ID = wORKLIST_ID;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public Blob getXML_CONTENT() {
		return XML_CONTENT;
	}
	public void setXML_CONTENT(Blob xML_CONTENT) {
		XML_CONTENT = xML_CONTENT;
	}

}
