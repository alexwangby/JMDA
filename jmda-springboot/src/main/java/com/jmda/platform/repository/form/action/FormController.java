package com.jmda.platform.repository.form.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.plugs.workflow.WorkFlowService;
import com.jmda.platform.plugs.workflow.model.WorkflowBean;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.application.model.ApplicationModel;
import com.jmda.platform.repository.application.service.ApplicationSevice;
import com.jmda.platform.repository.form.model.FormModel;
import com.jmda.platform.repository.form.service.FormService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("/repository/form")
public class FormController {
    @Resource
    FormService formServer;
    @Resource
    ApplicationSevice applicationSevice;
    public static ServiceLoader<WorkFlowService> serviceLoader = ServiceLoader.load(WorkFlowService.class);

    @RequestMapping("/getFormGridPage")
    public String getApplicationGridPage(String applicationId, ModelMap map) {
        map.put("applicationId", applicationId);
        return "repository/form/Sys_Repository_Form_Grid";
    }

    @RequestMapping(value = "/getFormGridJson", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String getFormGridJson(String applicationId, HttpServletRequest request) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> json = new HashMap<String, Object>();
        List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
        FormModel param = new FormModel();
        param.setApplication_id(applicationId);
        List<FormModel> list = formServer.queryList(param);
        if (list != null && list.size() > 0) {
            for (FormModel formModel : list) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("ID", formModel.getId());
                map.put("NAME", formModel.getName());
                map.put("TEMPLETE_NAME", formModel.getTemplete_name());
                map.put("OPERATOR", "<a href='javascript:void(0);' onclick=\"openForm('" + formModel.getName() + "','" + formModel.getId() + "');return false;\" >编辑</a>");
                records.add(map);
            }
        }
        json.put("root", records);
        json.put("total", records == null ? 0 : records.size());
        return mapper.writeValueAsString(json);
    }

    @RequestMapping(value = "/openForm", method = {RequestMethod.POST, RequestMethod.GET})
    public String openForm(String formId, ModelMap map) throws ResositoryException {
        map.put("formId", formId);
        FormModel formModel = formServer.get(formId);
        if (formModel != null) {
            map.put("formName", formModel.getName());
            String applicationId = formModel.getApplication_id();
            ApplicationModel applicationModel = applicationSevice.get(applicationId);
            if (applicationModel != null) {
                map.put("applicationName", applicationModel.getName());
            }
        }
        return "repository/form/Sys_Repository_Form";
    }

    @RequestMapping(value = "/getFormBasePage", method = {RequestMethod.POST, RequestMethod.GET})
    public String getFormBasePage(String form_id, ModelMap map) throws ResositoryException {
        List<ApplicationModel> list = applicationSevice.getAll();
        FormModel model = formServer.get(form_id);
        List<WorkflowBean> workflowList = new ArrayList<>();
        for (WorkFlowService service : serviceLoader) {
            workflowList = service.getAll();
        }
        if (model != null) {
            map.put("form_name", model.getName());
            map.put("groupId", model.getApplication_id());
            map.put("workflowId", model.getWorkflowId());
            map.put("xieyin_code", model.getXieyin_code());
            map.put("extendJavascript", model.getExtendJavascript());
        } else {
            map.put("form_name", "");
            map.put("groupId", "");
            map.put("workflowId", "");
            map.put("xieyin_code", "");
            map.put("extendJavascript", "");
        }
        map.put("applicationList", list);
        map.put("workflowList", workflowList);
        map.put("form_id", form_id);

        return "repository/form/Sys_Repository_Form_Base";
    }

    @RequestMapping(value = "/removeForm", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String removeForm(String formMetadataId, String ids) throws ResositoryException {
        String[] idArray = ids.split(",");
        if (idArray != null && idArray.length > 0) {
            for (int i = 0; i < idArray.length; i++) {
                formServer.remove(idArray[i]);
            }
        }
        return "ok";
    }

    @RequestMapping(value = "/save", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String save(String id, String form_name, String form_group, String workflow, String extendJavascript, String xieyin_code, ModelMap map) throws ResositoryException {
        FormModel formModel = formServer.get(id);
        formModel.setName(form_name);
        formModel.setApplication_id(form_group);
        formModel.setXieyin_code(xieyin_code);
        formModel.setWorkflowId(workflow);
        formModel.setExtendJavascript(extendJavascript);
        formServer.store(formModel);
        return "ok";
    }
}
