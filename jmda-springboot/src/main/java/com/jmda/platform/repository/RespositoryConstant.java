package com.jmda.platform.repository;

public class RespositoryConstant {
	public final static String SYS_MODEL_TYPE_FORM_NAME = "表单";
	public final static String SYS_MODEL_TYPE_WORKLIST_NAME = "数据列表";
	public final static String SYS_MODEL = "模型分类";
	public final static String SYS_MODEL_TYPE_DICTIONARY_NAME = "数据字典";
	public final static int SYS_FORM_METADATA_TYPE_MASTER = 1;
	public final static int SYS_FORM_METADATA_TYPE_SUB = 0;
	public final static String SYS_FORM_METADATA_MAP_DISPLAY_TYPE_OK = "显示";
	public final static String SYS_FORM_METADATA_MAP_DISPLAY_TYPE_NO = "不显示";
	public final static String SYS_FORM_METADATA_MAP_DISPLAY_TYPE_HIDDEN = "隐藏";
	public final static String WORKLIST_RUNTIME_QUERY_LIST_ID = "getList";
	public final static String WORKLIST_RUNTIME_QUERY_COUNT_ID = "getCount";
	public final static String WORKLIST_MYBATIS_RESULT_TYPE_INT = "java.lang.Integer";
	public final static String WORKLIST_MYBATIS_RESULT_TYPE_STRING = "java.lang.String";
	public final static String WORKLIST_MYBATIS_RESULT_TYPE_MAP = "java.util.HashMap";
	public final static String WORKLIST_MYBATIS_PARAM_TYPE_INT = "int";
	public final static String WORKLIST_MYBATIS_PARAM_TYPE_STRING = "string";
	public final static String WORKLIST_MYBATIS_PARAM_TYPE_MAP = "map";
	public final static Boolean MDA_RELY = true;
}
