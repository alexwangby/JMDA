package com.jmda.platform.repository.worklist.util;

import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.worklist.model.ColumnModel;
import com.jmda.platform.repository.worklist.model.WorkListModel;
import com.jmda.platform.repository.worklist.service.WorkListService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("WorkListColumnCommonMethod")
public class WorkListColumnCommonMethod {
	@Resource
	WorkListService workListService;
	public String workListColumnOrder(String id,String sourceId,String targetId) {
		if (!sourceId.equals("") && !targetId.equals("")) {
			ColumnModel columnModel1 = getColumnById(sourceId,id);
		    ColumnModel columnModel2 = getColumnById(targetId,id);
		    if(columnModel1!=null&&columnModel2!=null){
		    	int order1=columnModel1.getOrderIndex();
		    	int order2=columnModel2.getOrderIndex();
		    	int num=order1-order2;
		    	if(num>1){
		    		  int r=gridOrderColumn(order1, order2,id);
		    		  if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderColumn(columnModel1,id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else if(num<-1){
		    		 int r=gridOrderColumnDown(order1, order2,id);
		    		 if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderColumn(columnModel1,id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else{
			    	int isCommit1=orderColumn(columnModel1,id,order2);
					int isCommit2=orderColumn(columnModel2,id,order1);
					if(isCommit1==0||isCommit2==0){
						return "-1";
					}
		    	}
		    }else{
		    	return "-1";
		    }
			
	}
	return "1";
	}
	private ColumnModel getColumnById(String coulmnId,String id){
		ColumnModel columnModel=new ColumnModel();
		try {
			WorkListModel model = workListService.get(id);
			List<ColumnModel> listc=null;
			if(model!=null&&model.getColumn()!=null){
					listc=model.getColumn();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 ColumnModel column=listc.get(i);
				 String  cid=column.getId();
				 if(column!=null&&cid!=null){
				   if(cid.equals(coulmnId)){
					  columnModel=listc.get(i);
					  break;
				   }
				  }
			 }
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return columnModel;
	}
	private int gridOrderColumn(int order1,int order2,String id){
		try {
			WorkListModel model = workListService.get(id);
			List<ColumnModel> listc=null;
			if(model!=null&&model.getColumn()!=null){
				listc=model.getColumn();
			}
			if(listc!=null){
			for (int i = 0; i <listc.size(); i++) {
				ColumnModel column=listc.get(i);
				if(column.getOrderIndex()<order1&&column.getOrderIndex()>=order2){
					column.setOrderIndex(column.getOrderIndex()+1);
				}
			}
			}
			if (model != null && listc != null) {
				model.setColumn(listc);
				String reid = workListService.create(model);
				if (reid != null) {
					return 1;
				}else{
					return 0;
				}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}

		return 0;
	}
	private int orderColumn(ColumnModel column,String id,int order){
		String columnId=column.getId();
		try {
			WorkListModel model = workListService.get(id);
			List<ColumnModel> listc=null;
			if(model!=null&&model.getColumn()!=null){
				listc=model.getColumn();
			}
			if(listc!=null){
			for (int i = 0; i <listc.size(); i++) {
				ColumnModel columnModel=listc.get(i);
				if(columnModel.getId().equals(columnId)){
					columnModel.setOrderIndex(order);
				}
			}
			}
			if (model != null && listc != null) {
				model.setColumn(listc);
				String reid = workListService.create(model);
				if (reid != null) {
					return 1;
				}else{
					return 0;
				}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	private int gridOrderColumnDown(int order1,int order2,String id){
		try {
			WorkListModel model = workListService.get(id);
			List<ColumnModel> listc=null;
			if(model!=null&&model.getColumn()!=null){
				listc=model.getColumn();
			}
			if(listc!=null){
			for (int i = 0; i <listc.size(); i++) {
				ColumnModel column=listc.get(i);
				if(column.getOrderIndex()>order1&&column.getOrderIndex()<=order2){
					column.setOrderIndex(column.getOrderIndex()-1);
				}
			}
			}
			if (model != null && listc != null) {
				model.setColumn(listc);
				String reid = workListService.create(model);
				if (reid != null) {
					return 1;
				}else{
					return 0;
				}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
