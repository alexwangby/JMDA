package com.jmda.platform.repository.generator.model;

public class GeneratorModel {
	private String javaClass;
	private String applicationId;
	private String javaController;
	private String javaService;
	private String formJs;
	private String worklistJs;
	private String formId;
	private String worklistId;

	public String getJavaClass() {
		return javaClass;
	}

	public void setJavaClass(String javaClass) {
		this.javaClass = javaClass;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getJavaController() {
		return javaController;
	}

	public void setJavaController(String javaController) {
		this.javaController = javaController;
	}

	public String getJavaService() {
		return javaService;
	}

	public void setJavaService(String javaService) {
		this.javaService = javaService;
	}

	public String getFormJs() {
		return formJs;
	}

	public void setFormJs(String formJs) {
		this.formJs = formJs;
	}

	public String getWorklistJs() {
		return worklistJs;
	}

	public void setWorklistJs(String worklistJs) {
		this.worklistJs = worklistJs;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getWorklistId() {
		return worklistId;
	}

	public void setWorklistId(String worklistId) {
		this.worklistId = worklistId;
	}

}
