package com.jmda.platform.repository.generator.model;

public class GeneratorTablePo {
	private String tableName;
	private String className;
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	
}
