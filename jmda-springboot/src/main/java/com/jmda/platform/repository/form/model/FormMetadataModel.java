package com.jmda.platform.repository.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FormMetadataModel implements Serializable {
	/**
	 * 表单模型数据源
	 */
	private static final long serialVersionUID = -6014130472742347946L;
	@XStreamAlias("id")
	@JsonProperty
	private String id;
	@XStreamAlias("sql")
	@JsonProperty
	private String sql;
	@XStreamAlias("type")
	@JsonProperty
	private int type;
	@XStreamAlias("page_size")
	@JsonProperty
	private int page_size;
	@XStreamAlias("grid_height")
	@JsonProperty
	private int grid_height;

	@XStreamAlias("name")
	@JsonProperty
	private String name;
	@XStreamAlias("xieyin_code")
	@JsonProperty
	private String xieyin_code;

	@JsonProperty
	@XStreamImplicit(itemFieldName = "formMetadataMap")
	private List<FormMetadataMapModel> formMetadataMapList;
	
	@JsonProperty
	@XStreamImplicit(itemFieldName = "formMetadataButton")
	private List<FormMetadataButtonModel> formMetadataButtonList;
	@XStreamAlias("form_id")
	@JsonProperty
	private String form_id;
	//是否分组
	@XStreamAlias("is_group")
	@JsonProperty
	private Boolean is_group;
	//分组字段
	@XStreamAlias("group_field")
	@JsonProperty
	private String group_field;
	//是否详细
	@XStreamAlias("is_detail")
	@JsonProperty
	private Boolean is_detail;

    @XStreamAlias("formGridDatasFormatEventFunctionName")
    @JsonProperty
    private String formGridDatasFormatEventFunctionName;

    public String getFormGridDatasFormatEventFunctionName() {
        return formGridDatasFormatEventFunctionName;
    }

    public void setFormGridDatasFormatEventFunctionName(String formGridDatasFormatEventFunctionName) {
        this.formGridDatasFormatEventFunctionName = formGridDatasFormatEventFunctionName;
    }

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getPage_size() {
		return page_size;
	}

	public void setPage_size(int page_size) {
		this.page_size = page_size;
	}

	public int getGrid_height() {
		return grid_height;
	}

	public void setGrid_height(int grid_height) {
		this.grid_height = grid_height;
	}
	public List<FormMetadataMapModel> getFormMetadataMapList() {
		//排序
        if (formMetadataMapList != null) {
            Collections.sort(formMetadataMapList, new Comparator<Object>() {
                @Override
                public int compare(Object o1, Object o2) {
                    if (((FormMetadataMapModel) o1).getOrder_index() > ((FormMetadataMapModel) o2).getOrder_index()) {

                        return 1;
                    }
                    if (((FormMetadataMapModel) o1).getOrder_index() == ((FormMetadataMapModel) o2).getOrder_index()) {
                        return 0;
                    }
                    return -1;
                }
            });
        }
		return formMetadataMapList;
	}
	public List<FormMetadataMapModel> getNormalFormMetadataMapList() {
		return formMetadataMapList;
	}
	public void setFormMetadataMapList(List<FormMetadataMapModel> formMetadataMapList) {
		this.formMetadataMapList = formMetadataMapList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getXieyin_code() {
		return xieyin_code;
	}

	public void setXieyin_code(String xieyin_code) {
		this.xieyin_code = xieyin_code;
	}

	public List<FormMetadataButtonModel> getFormMetadataButtonList() {
		return formMetadataButtonList;
	}

	public void setFormMetadataButtonList(List<FormMetadataButtonModel> formMetadataButtonList) {
		this.formMetadataButtonList = formMetadataButtonList;
	}

	public String getForm_id() {
		return form_id;
	}

	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}

	public Boolean getIs_group() {
		return is_group;
	}

	public void setIs_group(Boolean is_group) {
		this.is_group = is_group;
	}

	public String getGroup_field() {
		return group_field;
	}

	public void setGroup_field(String group_field) {
		this.group_field = group_field;
	}

	public Boolean getIs_detail() {
		return is_detail;
	}

	public void setIs_detail(Boolean is_detail) {
		this.is_detail = is_detail;
	}

}
