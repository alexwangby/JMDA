package com.jmda.platform.repository.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;

public class FormMetadataMapModel implements Serializable {
    /**
     * 表单字段模型
     */
    private static final long serialVersionUID = -5087610091546932816L;
    @XStreamAlias("id")
    @JsonProperty
    private String id;
    @XStreamAlias("column_name")
    @JsonProperty
    private String column_name;
    @XStreamAlias("column_lable")
    @JsonProperty
    private String column_lable;
    @XStreamAlias("db_column_lable")
    @JsonProperty
    private String db_column_lable;
    @XStreamAlias("table_name")
    @JsonProperty
    private String table_name;
    @XStreamAlias("column_title")
    @JsonProperty
    private String column_title;
    @XStreamAlias("column_type")
    @JsonProperty
    private String column_type;
    @XStreamAlias("default_value")
    @JsonProperty
    private String default_value;
    @XStreamAlias("is_null")
    @JsonProperty
    private boolean is_null;
    @XStreamAlias("ui_type")
    @JsonProperty
    private String ui_type;
    @XStreamAlias("ui_length")
    @JsonProperty
    private String ui_length;
    @XStreamAlias("ui_param")
    @JsonProperty
    private String ui_param;
    @XStreamAlias("index")
    @JsonProperty
    private int index;
    @XStreamAlias("hidden")
    @JsonProperty
    private boolean hidden;
    @XStreamAlias("order_index")
    @JsonProperty
    private int order_index;
    @XStreamAlias("sortable")
    @JsonProperty
    private boolean sortable = false;
    
//    @XStreamAlias("showdetails")
//    @JsonProperty
//    private boolean showdetails = false;
    
    @XStreamAlias("column_align")
    @JsonProperty
    private String column_align;
    @XStreamAlias("formatter")
    @JsonProperty
    private String formatter;
    @XStreamAlias("ref_type")
    @JsonProperty
    String ref_type;
    @XStreamAlias("ref_params")
    @JsonProperty
    String ref_params;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    public String getColumn_lable() {
        return column_lable;
    }

    public void setColumn_lable(String column_lable) {
        this.column_lable = column_lable;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getColumn_title() {
        return column_title;
    }

    public void setColumn_title(String column_title) {
        this.column_title = column_title;
    }

    public String getDefault_value() {
        return default_value;
    }

    public void setDefault_value(String default_value) {
        this.default_value = default_value;
    }

    public boolean isIs_null() {
        return is_null;
    }

    public void setIs_null(boolean is_null) {
        this.is_null = is_null;
    }

    public String getUi_type() {
        return ui_type;
    }

    public void setUi_type(String ui_type) {
        this.ui_type = ui_type;
    }

    public String getUi_length() {
        return ui_length;
    }

    public void setUi_length(String ui_length) {
        this.ui_length = ui_length;
    }

    public String getUi_param() {
        return ui_param;
    }

    public void setUi_param(String ui_param) {
        this.ui_param = ui_param;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public int getOrder_index() {
        return order_index;
    }

    public void setOrder_index(int order_index) {
        this.order_index = order_index;
    }

//    public boolean isShowdetails() {
//        return showdetails;
//    }
    public boolean isSortable() {
        return sortable;
    }
//    public void setShowdetails(boolean showdetails) {
//        this.showdetails = showdetails;
//    }
    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }


    public String getColumn_align() {
        return column_align;
    }

    public void setColumn_align(String column_align) {
        this.column_align = column_align;
    }

    public String getFormatter() {
        return formatter;
    }

    public void setFormatter(String formatter) {
        this.formatter = formatter;
    }

    public String getDb_column_lable() {
        return db_column_lable;
    }

    public void setDb_column_lable(String db_column_lable) {
        this.db_column_lable = db_column_lable;
    }

    public String getColumn_type() {
        return column_type;
    }

    public void setColumn_type(String column_type) {
        this.column_type = column_type;
    }

    public String getRef_type() {
        return ref_type;
    }

    public void setRef_type(String ref_type) {
        this.ref_type = ref_type;
    }

    public String getRef_params() {
        return ref_params;
    }

    public void setRef_params(String ref_params) {
        this.ref_params = ref_params;
    }
}
