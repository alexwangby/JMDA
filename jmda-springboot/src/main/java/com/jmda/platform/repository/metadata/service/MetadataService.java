package com.jmda.platform.repository.metadata.service;

import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.metadata.cache.MetadataCache;
import com.jmda.platform.repository.metadata.model.MetadataMapModel;
import com.jmda.platform.repository.metadata.model.MetadataModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
public class MetadataService {
	@Resource
	MetadataCache cache;
	@Resource
	DataBaseService db;

	public List<MetadataModel> getAll() throws ResositoryException {
		// TODO Auto-generated method stub
		return cache.getAll();
	}

	public MetadataModel get(String id) throws ResositoryException {
		// TODO Auto-generated method stub
		return cache.get(id);
	}

	public List<MetadataModel> queryList(MetadataModel param) {
		// TODO Auto-generated method stub
		return cache.queryList(param);
	}

	public MetadataModel queryOne(MetadataModel param) {
		// TODO Auto-generated method stub
		List<MetadataModel> list = cache.queryList(param);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	public MetadataMapModel queryMetadataMapModel(String tableName, String columnName) {
		// TODO Auto-generated method stub
		MetadataModel param = new MetadataModel();
		param.setTABLE_NAME(tableName);
		MetadataModel meatadataModel = queryOne(param);
		if (meatadataModel != null) {
			List<MetadataMapModel> list = meatadataModel.getMetadataMaplist();
			if (list != null) {
				for (MetadataMapModel mapModel : list) {
					if (mapModel != null && mapModel.getCOLUMN_NAME().equals(columnName)) {
						return mapModel;
					}
				}
			}
		}
		return null;

	}

	public MetadataModel queryMetadataModel(String tableName) {
		// TODO Auto-generated method stub
		MetadataModel param = new MetadataModel();
		param.setTABLE_NAME(tableName);
		return queryOne(param);
	}

/*	public List<MetadataMapModel> getMetadataMapListBySql(String sql) {
		List<MetadataMapModel> list = new ArrayList<MetadataMapModel>();
		if (sql != null && sql.trim().length() > 0) {
			Connection conn = null;
			PreparedStatement stmt = null;
			try {
				conn = db.getConnection();
				stmt = conn.prepareStatement(sql);
				// 获取结果集元数据
				ResultSetMetaData rsmd = stmt.getMetaData();
				// 总列数
				int size = rsmd.getColumnCount();
				for (int i = 1; i <= size; i++) {
					MetadataMapModel model = new MetadataMapModel();
					model.setTABLE_NAME(rsmd.getTableName(i));
					model.setCOLUMN_LABEL(rsmd.getColumnLabel(i));
					model.setCOLUMN_NAME(rsmd.getColumnName(i));
					MetadataMapModel purModel = queryMetadataMapModel(model.getTABLE_NAME(), model.getCOLUMN_NAME());
					model.setCOLUMN_TITLE(purModel == null || purModel.getCOLUMN_TITLE() == null
							|| purModel.getCOLUMN_TITLE().trim().length() == 0 ? model.getCOLUMN_LABEL()
									: purModel.getCOLUMN_TITLE());
					model.setCOLUMN_TYPE(rsmd.getColumnTypeName(i));
					String length = rsmd.getPrecision(i) + "," + rsmd.getScale(i);
					model.setCOLUMN_LENGTH(length);
					list.add(model);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					stmt.close();
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return list;
	}*/
	public List<MetadataMapModel> getMetadataMapListBySql(String sql) {
		List<MetadataMapModel> list = new ArrayList<MetadataMapModel>();
		if (sql != null && sql.trim().length() > 0) {
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			try {
				conn = db.getConnection();
//				stmt = conn.prepareStatement(sql);
				sql = sql.replace("?", "''");
				stmt = conn.createStatement();
				rs= stmt.executeQuery(sql);
		        
				// 获取结果集元数据
				//ResultSetMetaData rsmd = stmt.getMetaData();
				ResultSetMetaData rsmd = rs.getMetaData();
				// 总列数
				int size = rsmd.getColumnCount();
				for (int i = 1; i <= size; i++) {
					MetadataMapModel model = new MetadataMapModel();
					model.setTABLE_NAME(rsmd.getTableName(i));
					model.setCOLUMN_LABEL(rsmd.getColumnLabel(i));
					model.setCOLUMN_NAME(rsmd.getColumnName(i));
					MetadataMapModel purModel = queryMetadataMapModel(model.getTABLE_NAME(), model.getCOLUMN_NAME());
					model.setCOLUMN_TITLE(purModel == null || purModel.getCOLUMN_TITLE() == null
							|| purModel.getCOLUMN_TITLE().trim().length() == 0 ? model.getCOLUMN_LABEL()
									: purModel.getCOLUMN_TITLE());
					model.setCOLUMN_TYPE(rsmd.getColumnTypeName(i));
					String length = rsmd.getPrecision(i) + "," + rsmd.getScale(i);
					model.setCOLUMN_LENGTH(length);
					list.add(model);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					if(rs!=null){
						rs.close();
					}
					if(stmt!=null){
						stmt.close();
					}
					if(conn!=null){
						conn.close();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return list;
	}
}
