package com.jmda.platform.repository.worklist.util;

import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.worklist.model.SearchModel;
import com.jmda.platform.repository.worklist.model.WorkListModel;
import com.jmda.platform.repository.worklist.service.WorkListService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("WorkListSearchsCommonMethod")
public class WorkListSearchsCommonMethod {
	@Resource
	WorkListService workListService;
	public String workListSearchOrder(String id,String sourceId,String targetId) {
		if (!sourceId.equals("") && !targetId.equals("")) {
			SearchModel searchModel1 = getSearchWhereById(sourceId,id);
		    SearchModel searchModel2 = getSearchWhereById(targetId,id);
		    if(searchModel1!=null&&searchModel2!=null){
		    	int order1=searchModel1.getOrderIndex();
		    	int order2=searchModel2.getOrderIndex();
		    	int num=order1-order2;
		    	if(num>1){
		    		  int r=gridOrderSearch(order1, order2,id);
		    		  if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderSearch(searchModel1,id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else if(num<-1){
		    		 int r=gridOrderSearchDown(order1, order2,id);
		    		 if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderSearch(searchModel1,id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else{
			    	int isCommit1=orderSearch(searchModel1,id,order2);
					int isCommit2=orderSearch(searchModel2,id,order1);
					if(isCommit1==0||isCommit2==0){
						return "-1";
					}
		    	}
		    }else{
		    	return "-1";
		    }
			
	}
	return "1";
	}
	private SearchModel getSearchWhereById(String coulmnId,String id){
		SearchModel searchModel=new SearchModel();
		try {
			WorkListModel model = workListService.get(id);
			List<SearchModel> listc=null;
			if(model!=null&&model.getSearchWhere()!=null){
					listc=model.getSearchWhere();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 SearchModel search=listc.get(i);
				 String  cid=search.getId();
				 if(search!=null&&cid!=null){
				   if(cid.equals(coulmnId)){
					  searchModel=listc.get(i);
					  break;
				   }
				  }
			 }
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return searchModel;
	}
	private int gridOrderSearch(int order1,int order2,String id){
		try {
			WorkListModel model = workListService.get(id);
			List<SearchModel> listc=null;
			if(model!=null&&model.getSearchWhere()!=null){
				listc=model.getSearchWhere();
			}
			if(listc!=null){
			for (int i = 0; i <listc.size(); i++) {
				SearchModel search=listc.get(i);
				if(search.getOrderIndex()<order1&&search.getOrderIndex()>=order2){
					search.setOrderIndex(search.getOrderIndex()+1);
				}
			}
			}
			if (model != null && listc != null) {
				model.setSearchWhere(listc);
				String reid = workListService.create(model);
				if (reid != null) {
					return 1;
				}else{
					return 0;
				}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}

		return 0;
	}
	private int orderSearch(SearchModel search,String id,int order){
		String searchId=search.getId();
		try {
			WorkListModel model = workListService.get(id);
			List<SearchModel> listc=null;
			if(model!=null&&model.getSearchWhere()!=null){
				listc=model.getSearchWhere();
			}
			if(listc!=null){
			for (int i = 0; i <listc.size(); i++) {
				SearchModel searchModel=listc.get(i);
				if(searchModel.getId().equals(searchId)){
					searchModel.setOrderIndex(order);
				}
			}
			}
			if (model != null && listc != null) {
				model.setSearchWhere(listc);
				String reid = workListService.create(model);
				if (reid != null) {
					return 1;
				}else{
					return 0;
				}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	private int gridOrderSearchDown(int order1,int order2,String id){
		try {
			WorkListModel model = workListService.get(id);
			List<SearchModel> listc=null; 
			if(model!=null&&model.getSearchWhere()!=null){
				listc=model.getSearchWhere();
			}
			if(listc!=null){
			for (int i = 0; i <listc.size(); i++) {
				SearchModel search=listc.get(i);
				if(search.getOrderIndex()>order1&&search.getOrderIndex()<=order2){
					search.setOrderIndex(search.getOrderIndex()-1);
				}
			}
			}
			if (model != null && listc != null) {
				model.setSearchWhere(listc);
				String reid = workListService.create(model);
				if (reid != null) {
					return 1;
				}else{
					return 0;
				}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
