package com.jmda.platform.repository.worklist.cache;

import com.jmda.platform.commom.BaseCache;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.commom.ResourcePathConfig;
import com.jmda.platform.commom.util.BlobUtil;
import com.jmda.platform.commom.util.FileUtil;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.repository.RespositoryConstant;
import com.jmda.platform.repository.worklist.model.WorkListModel;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import net.sf.ehcache.search.Query;
import net.sf.ehcache.search.Result;
import net.sf.ehcache.search.Results;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class WorkListCache implements BaseCache<WorkListModel> {

	@Resource
	ResourcePathConfig resourceLoaderPath;
	@Resource
	DataBaseService db;
	@Override
	public void put(WorkListModel model) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		cache.remove(model.getId());
		Element element = new Element(model.getId(), model);
		cache.put(element);
	}

	@Override
	public List<WorkListModel> getAll() {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		List<WorkListModel> list = new LinkedList<WorkListModel>();
		Query query = cache.createQuery().includeValues();
		Results rs = query.execute();
		List<Result> all = rs.all();
		for (Result r : all) {
			WorkListModel model = (WorkListModel) r.getValue();
			list.add(model);
		}
		query.end();
		return list;
	}

	@Override
	public WorkListModel get(String id) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		Element element = cache.get(id);
		if (element != null) {
			return (WorkListModel) element.getObjectValue();
		}
		return null;
	}

	@Override
	public void remove(String id) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		cache.remove(id);
	}

	@Override
	public List<WorkListModel> queryList(WorkListModel param) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		List<WorkListModel> list = new LinkedList<WorkListModel>();
		Query query = cache.createQuery().includeValues();
		LocalCacheManager.addSearchAttribute(cache, query, param);
		Results rs = query.execute();
		List<Result> all = rs.all();
		for (Result r : all) {
			WorkListModel model = (WorkListModel) r.getValue();
			list.add(model);
		}
		query.end();
		return list;
	}

	@Override
	public String getFilePath() throws IOException {
		// TODO Auto-generated method stub
		return resourceLoaderPath.getWorklistPath();
	}

	@PostConstruct
	public void initialize() throws IOException {
		// TODO Auto-generated method stub
		String[] searchs = { "id", "name", "applicationId", "page_size", "xieyin_code" };
		Cache cache = LocalCacheManager.registCache(this.getClass(), searchs);
		if(RespositoryConstant.MDA_RELY){
			String filePath = getFilePath();
			List<String> ls = FileUtil.getXmlFileList(filePath);
			for (String fileName : ls) {
				if (fileName.indexOf("_dao.xml") < 0) {
					WorkListModel model = FileUtil.coverXmlToBean(WorkListModel.class, filePath + fileName);
					if (model != null) {
						Element element = new Element(model.getId(), model);
						cache.put(element);
					}
				}
			}
		}else{
		}
	}

}
