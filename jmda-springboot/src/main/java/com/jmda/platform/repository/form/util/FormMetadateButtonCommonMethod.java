package com.jmda.platform.repository.form.util;

import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.form.model.FormMetadataButtonModel;
import com.jmda.platform.repository.form.model.FormMetadataModel;
import com.jmda.platform.repository.form.model.FormModel;
import com.jmda.platform.repository.form.service.FormMetadataService;
import com.jmda.platform.repository.form.service.FormService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("FormMetadateButtonCommonMethod")
public class FormMetadateButtonCommonMethod {
	@Resource
	FormMetadataService formMetadataService;
	@Resource
	FormService formService;
	public String formMetadataButtonModelOrder(String form_id,String form_er_id,String sourceId,String targetId) {
		if (!sourceId.equals("") && !targetId.equals("")) {
			FormMetadataButtonModel formMetadataButtonModel1 = getMetadataButtonById(sourceId,form_id,form_er_id);
			FormMetadataButtonModel formMetadataButtonModel2 = getMetadataButtonById(targetId,form_id,form_er_id);
		    if(formMetadataButtonModel1!=null&&formMetadataButtonModel2!=null){
		    	int order1=formMetadataButtonModel1.getOrderIndex();
		    	int order2=formMetadataButtonModel2.getOrderIndex();
		    	int num=order1-order2;
		    	if(num>1){
		    		  int r=gridOrderMetadataButton(order1, order2,form_id,form_er_id);
		    		  if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderFormMetadataButton(formMetadataButtonModel1,form_id,form_er_id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else if(num<-1){
		    		 int r=gridOrderMetadataButtonDown(order1, order2,form_id,form_er_id);
		    		 if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderFormMetadataButton(formMetadataButtonModel1,form_id,form_er_id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else{
			    	int isCommit1=orderFormMetadataButton(formMetadataButtonModel1,form_id,form_er_id,order2);
					int isCommit2=orderFormMetadataButton(formMetadataButtonModel2,form_id,form_er_id,order1);
					if(isCommit1==0||isCommit2==0){
						return "-1";
					}
		    	}
		    }else{
		    	return "-1";
		    }
			
	}
	return "1";
	}
	private FormMetadataButtonModel getMetadataButtonById(String uiId,String form_id,String form_er_id){
		FormMetadataButtonModel metadataButtonModel=new FormMetadataButtonModel();
		FormMetadataModel metadataModel=new FormMetadataModel();
		try {
			FormModel model = formService.get(form_id);
			List<FormMetadataModel> listc=null;
			if(model!=null&&model.getFormMetadataList()!=null){
					listc=model.getFormMetadataList();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 FormMetadataModel metadata=listc.get(i);
				 String  metadataId=metadata.getId();
				 if(metadata!=null&&metadataId!=null){
				   if(metadataId.equals(form_er_id)){
					   metadataModel=listc.get(i);
					  break;
				   }
				  }
			 }
			 List<FormMetadataButtonModel> listd = metadataModel.getFormMetadataButtonList();
			 if(listd!=null){
				 for (int i = 0; i < listd.size(); i++) {
					 FormMetadataButtonModel metadataButton = listd.get(i);
					 if(metadataButton!=null){
						 String metadataMapId = metadataButton.getId();
						 if(metadataMapId!=null){
							   if(metadataMapId.equals(uiId)){
								  metadataButtonModel=listd.get(i);
								  break;
							   }
						 }
					 }
				 }
			 }

			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return metadataButtonModel;
	}
	private int gridOrderMetadataButton(int order1,int order2,String form_id,String form_er_id){
		//FormMetadataMapModel metadataMapModel=new FormMetadataMapModel();
		FormMetadataModel metadataModel=new FormMetadataModel();
		try {
			FormModel model = formService.get(form_id);
			List<FormMetadataModel> listc=null;
			if(model!=null&&model.getFormMetadataList()!=null){
					listc=model.getFormMetadataList();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 FormMetadataModel metadata=listc.get(i);
				 String  metadataId=metadata.getId();
				 if(metadata!=null&&metadataId!=null){
				   if(metadataId.equals(form_er_id)){
					   metadataModel=listc.get(i);
					  break;
				   }
				  }
			 }
			 List<FormMetadataButtonModel> listd = metadataModel.getFormMetadataButtonList();
			 if(listd!=null){
				 for (int i = 0; i < listd.size(); i++) {
					 FormMetadataButtonModel metadataButton = listd.get(i);
					 if(metadataButton.getOrderIndex()<order1&&metadataButton.getOrderIndex()>=order2){
						 metadataButton.setOrderIndex(metadataButton.getOrderIndex()+1);
					 }
				 }
			 }
			if (model != null && listc != null) {
					String reid = formService.create(model);
					if (reid != null) {
						return 1;
					}else{
						return 0;
					}
			}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	private int orderFormMetadataButton(FormMetadataButtonModel metadataButton,String form_id,String form_er_id,int order){
		//FormMetadataMapModel metadataMapModel=new FormMetadataMapModel();
		FormMetadataModel metadataModel=new FormMetadataModel();
		String metadataButtonId=metadataButton.getId();
		try {
			FormModel model = formService.get(form_id);
			List<FormMetadataModel> listc=null;
			if(model!=null&&model.getFormMetadataList()!=null){
					listc=model.getFormMetadataList();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 FormMetadataModel metadata=listc.get(i);
				 String  metadataId=metadata.getId();
				 if(metadata!=null&&metadataId!=null){
				   if(metadataId.equals(form_er_id)){
					   metadataModel=listc.get(i);
					  break;
				   }
				  }
			 }
			 List<FormMetadataButtonModel> listd = metadataModel.getFormMetadataButtonList();
			 if(listd!=null){
				 for (int i = 0; i < listd.size(); i++) {
					 FormMetadataButtonModel metadataButtonModel = listd.get(i);
					 if(metadataButtonModel!=null){
						 String metadataButton_id = metadataButtonModel.getId();
						 if(metadataButton_id!=null){
							   if(metadataButton_id.equals(metadataButtonId)){
								   metadataButtonModel.setOrderIndex(order);
							   }
						 }
					 }
				 }
			 }
			if (model != null && listc != null) {
				if (model != null && listc != null) {
					String reid = formService.create(model);
					if (reid != null) {
						return 1;
					}else{
						return 0;
					}
			  }
			}
		} 
		}catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	private int gridOrderMetadataButtonDown(int order1,int order2,String form_id,String form_er_id){
		FormMetadataModel metadataModel=new FormMetadataModel();
		try {
			FormModel model = formService.get(form_id);
			List<FormMetadataModel> listc=null;
			if(model!=null&&model.getFormMetadataList()!=null){
					listc=model.getFormMetadataList();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 FormMetadataModel metadata=listc.get(i);
				 String  metadataId=metadata.getId();
				 if(metadata!=null&&metadataId!=null){
				   if(metadataId.equals(form_er_id)){
					   metadataModel=listc.get(i);
					  break;
				   }
				  }
			 }
			 List<FormMetadataButtonModel> listd = metadataModel.getFormMetadataButtonList();
			 if(listd!=null){
				 for (int i = 0; i < listd.size(); i++) {
					 FormMetadataButtonModel metadataButtonModel = listd.get(i);
					 if(metadataButtonModel!=null){
						if(metadataButtonModel.getOrderIndex()>order1&&metadataButtonModel.getOrderIndex()<=order2){
							metadataButtonModel.setOrderIndex(metadataButtonModel.getOrderIndex()-1);
						}
					 }
				 }
			 }
			if (model != null && listc != null) {
				if (model != null && listc != null) {
					String reid = formService.create(model);
					if (reid != null) {
						return 1;
					}else{
						return 0;
					}
			  }
			}
		} 
		}catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
