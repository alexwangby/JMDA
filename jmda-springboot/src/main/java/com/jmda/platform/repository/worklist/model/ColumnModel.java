package com.jmda.platform.repository.worklist.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.math.BigDecimal;

public class ColumnModel {
    @XStreamAlias("id")
    private String id;
    @XStreamAlias("talbelName")
    private String talbelName;
    @XStreamAlias("columnName")
    private String columnName;
    @XStreamAlias("dbColumnName")
    private String dbColumnName;
    @XStreamAlias("title")
    private String title;
    @XStreamAlias("width")
    private BigDecimal width;
    @XStreamAlias("supportFuzzySearch")
    private boolean supportFuzzySearch;
    @XStreamAlias("orderIndex")
    private int orderIndex;
    @XStreamAlias("hotlinkName")
    private String hotlinkName;
    @XStreamAlias("hotlinkParam")
    private String hotlinkParam;
    @XStreamAlias("hidden")
    private boolean hidden = false;
    @XStreamAlias("pk")
    private boolean pk = false;
    @XStreamAlias("sortable")
    private boolean sortable = false;
    @XStreamAlias("formatter")
    private String formatter;
    @XStreamAlias("column_align")
    private String column_align;
    @XStreamAlias("column_type")
    private String column_type;
    @XStreamAlias("order_index")
    private String order_index;
    //数据映射类型
    @XStreamAlias("ref_type")
    String ref_type;
    //数据映射
    @XStreamAlias("ref_params")
    String ref_params;
    //合并行
    @XStreamAlias("rowspan")
    private int rowspan;
    //合并列
    @XStreamAlias("colspan")
    private int colspan;
    //行符号
    @XStreamAlias("row_symbol")
    private String row_symbol;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSupportFuzzySearch() {
        return supportFuzzySearch;
    }

    public void setSupportFuzzySearch(boolean supportFuzzySearch) {
        this.supportFuzzySearch = supportFuzzySearch;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    public BigDecimal getWidth() {
    	if(width==null||width.compareTo(new BigDecimal(1))>=0){
    		width = new BigDecimal(0.1);
    	}
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    public String getTalbelName() {
        return talbelName;
    }

    public void setTalbelName(String talbelName) {
        this.talbelName = talbelName;
    }

    public String getHotlinkName() {
        return hotlinkName == null || hotlinkName.trim().length() == 0 ? "无" : hotlinkName;
    }

    public void setHotlinkName(String hotlinkName) {
        this.hotlinkName = hotlinkName;
    }

    public String getHotlinkParam() {
        return hotlinkParam;
    }

    public void setHotlinkParam(String hotlinkParam) {
        this.hotlinkParam = hotlinkParam;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isPk() {
        return pk;
    }

    public void setPk(boolean pk) {
        this.pk = pk;
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }

    public String getFormatter() {
        return formatter;
    }

    public void setFormatter(String formatter) {
        this.formatter = formatter;
    }

    public String getColumn_align() {
        return column_align;
    }

    public void setColumn_align(String column_align) {
        this.column_align = column_align;
    }

    public String getOrder_index() {
        return order_index;
    }

    public void setOrder_index(String order_index) {
        this.order_index = order_index;
    }

    public String getDbColumnName() {
        return dbColumnName;
    }

    public void setDbColumnName(String dbColumnName) {
        this.dbColumnName = dbColumnName;
    }

    public String getRef_type() {
        return ref_type;
    }

    public void setRef_type(String ref_type) {
        this.ref_type = ref_type;
    }

    public String getRef_params() {
        return ref_params;
    }

    public void setRef_params(String ref_params) {
        this.ref_params = ref_params;
    }

    public String getColumn_type() {
        return column_type;
    }

    public void setColumn_type(String column_type) {
        this.column_type = column_type;
    }

	public int getRowspan() {
		return rowspan;
	}

	public void setRowspan(int rowspan) {
		this.rowspan = rowspan;
	}

	public int getColspan() {
		return colspan;
	}

	public void setColspan(int colspan) {
		this.colspan = colspan;
	}

	public String getRow_symbol() {
		return row_symbol;
	}

	public void setRow_symbol(String row_symbol) {
		this.row_symbol = row_symbol;
	}
	
}
