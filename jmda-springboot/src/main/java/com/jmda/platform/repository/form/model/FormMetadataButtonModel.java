package com.jmda.platform.repository.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class FormMetadataButtonModel {
	@JsonProperty
	@XStreamAlias("id")
	private String id;
	@JsonProperty
	@XStreamAlias("title")
	private String title;
	@JsonProperty
	@XStreamAlias("handler")
	private String handler;
	@JsonProperty
	@XStreamAlias("iconCls")
	private String iconCls;
	@JsonProperty
	@XStreamAlias("tooltip")
	private String tooltip;
	@JsonProperty
	@XStreamAlias("orderIndex")
	private int orderIndex;
	@JsonProperty
	@XStreamAlias("iconClsChild")
	private String iconClsChild;
	//自定义html
	@JsonProperty
	@XStreamAlias("customHtml")
	private String customHtml;
	public String getIconClsChild() {
		return iconClsChild;
	}

	public void setIconClsChild(String iconClsChild) {
		this.iconClsChild = iconClsChild;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHandler() {
		return handler;
	}

	public void setHandler(String handler) {
		this.handler = handler;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public int getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}

	public String getCustomHtml() {
		return customHtml;
	}

	public void setCustomHtml(String customHtml) {
		this.customHtml = customHtml;
	}

	public String getMasterButtonHtml() {
		String onClick=handler==null||handler.trim().length()==0?"":"onClick=\""+handler+"\"";
		return "<div class=\"pbs_tab_preserve\"><input type=\"button\" id=\""+id+"\"  value=\""+title+"\" "+onClick+" class=\"button\">";
	}
}
