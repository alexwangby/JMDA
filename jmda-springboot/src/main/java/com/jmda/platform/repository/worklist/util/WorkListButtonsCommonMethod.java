package com.jmda.platform.repository.worklist.util;

import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.worklist.model.ButtonModel;
import com.jmda.platform.repository.worklist.model.WorkListModel;
import com.jmda.platform.repository.worklist.service.WorkListService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("WorkListButtonsCommonMethod")
public class WorkListButtonsCommonMethod {
	@Resource
	WorkListService workListService;
	public String workListButtonOrder(String id,String sourceId,String targetId) {
		if (!sourceId.equals("") && !targetId.equals("")) {
			ButtonModel buttonModel1 = getButtonById(sourceId,id);
		    ButtonModel buttonModel2 = getButtonById(targetId,id);
		    if(buttonModel1!=null&&buttonModel2!=null){
		    	int order1=buttonModel1.getOrderIndex();
		    	int order2=buttonModel2.getOrderIndex();
		    	int num=order1-order2;
		    	if(num>1){
		    		  int r=gridOrderButton(order1, order2,id);
		    		  if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderButton(buttonModel1,id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else if(num<-1){
		    		 int r=gridOrderButtonDown(order1, order2,id);
		    		 if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderButton(buttonModel1,id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else{
			    	int isCommit1=orderButton(buttonModel1,id,order2);
					int isCommit2=orderButton(buttonModel2,id,order1);
					if(isCommit1==0||isCommit2==0){
						return "-1";
					}
		    	}
		    }else{
		    	return "-1";
		    }
			
	}
	return "1";
	}
	private ButtonModel getButtonById(String coulmnId,String id){
		ButtonModel buttonModel=new ButtonModel();
		try {
			WorkListModel model = workListService.get(id);
			List<ButtonModel> listc=null;
			if(model!=null&&model.getButton()!=null){
					listc=model.getButton();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 ButtonModel button=listc.get(i);
				 String  cid=button.getId();
				 if(button!=null&&cid!=null){
				   if(cid.equals(coulmnId)){
					  buttonModel=listc.get(i);
					  break;
				   }
				  }
			 }
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return buttonModel;
	}
	private int gridOrderButton(int order1,int order2,String id){
		try {
			WorkListModel model = workListService.get(id);
			List<ButtonModel> listc=null;
			if(model!=null&&model.getButton()!=null){
				listc=model.getButton();
			}
			if(listc!=null){
			for (int i = 0; i <listc.size(); i++) {
				ButtonModel button=listc.get(i);
				if(button.getOrderIndex()<order1&&button.getOrderIndex()>=order2){
					button.setOrderIndex(button.getOrderIndex()+1);
				}
			}
			}
			if (model != null && listc != null) {
				model.setButton(listc);
				String reid = workListService.create(model);
				if (reid != null) {
					return 1;
				}else{
					return 0;
				}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}

		return 0;
	}
	private int orderButton(ButtonModel button,String id,int order){
		String buttonId=button.getId();
		try {
			WorkListModel model = workListService.get(id);
			List<ButtonModel> listc=null;
			if(model!=null&&model.getButton()!=null){
				listc=model.getButton();
			}
			if(listc!=null){
			for (int i = 0; i <listc.size(); i++) {
				ButtonModel buttonModel=listc.get(i);
				if(buttonModel.getId().equals(buttonId)){
					buttonModel.setOrderIndex(order);
				}
			}
			}
			if (model != null && listc != null) {
				model.setButton(listc);
				String reid = workListService.create(model);
				if (reid != null) {
					return 1;
				}else{
					return 0;
				}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	private int gridOrderButtonDown(int order1,int order2,String id){
		try {
			WorkListModel model = workListService.get(id);
			List<ButtonModel> listc=null;
			if(model!=null&&model.getButton()!=null){
				listc=model.getButton();
			}
			if(listc!=null){
			for (int i = 0; i <listc.size(); i++) {
				ButtonModel button=listc.get(i);
				if(button.getOrderIndex()>order1&&button.getOrderIndex()<=order2){
					button.setOrderIndex(button.getOrderIndex()-1);
				}
			}
			}
			if (model != null && listc != null) {
				model.setButton(listc);
				String reid = workListService.create(model);
				if (reid != null) {
					return 1;
				}else{
					return 0;
				}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
