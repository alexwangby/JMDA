package com.jmda.platform.repository.form.action;

import com.jmda.platform.commom.SpringContextUtil;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/repository/form/metadatamap/ui")
public class FormMetadataMapUIController {
	@RequestMapping(value = "/getSettingPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getSettingPage(String uiType, String uiParams, boolean masterDataset, ModelMap map, HttpServletRequest request) {
		if (masterDataset) {
			FormUIComponentAbst abs = SpringContextUtil.getBean("UI_FORM_" + uiType);
			if (abs != null) {
				return abs.getParamSetingWeb(uiParams, request, map);
			}
		} else {
			/*GridUIComponentAbst abs = SpringContextUtil.getBean("UI_GRID_" + uiType);
			if (abs != null) {
				return abs.getParamSetingWeb(uiParams, request, map);
			}*/
		}
		return "";
	}
}
