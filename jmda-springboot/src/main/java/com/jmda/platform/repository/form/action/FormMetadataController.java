package com.jmda.platform.repository.form.action;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.commom.SequenceService;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.RespositoryConstant;
import com.jmda.platform.repository.form.model.FormMetadataModel;
import com.jmda.platform.repository.form.model.FormModel;
import com.jmda.platform.repository.form.service.FormMetadataService;
import com.jmda.platform.repository.form.service.FormService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/repository/form/metadata")
public class FormMetadataController {
    @Resource
    FormMetadataService formMetadataService;
    @Resource
    FormService formService;
    @Resource
    SequenceService seq;

    @RequestMapping(value = "/getDataSourceForm", method = {RequestMethod.POST, RequestMethod.GET})
    public String getDataSourceForm(String formId, ModelMap map) throws ResositoryException {
        FormMetadataModel model = formMetadataService.getMasterFormMetadataModel(formId);
        if (model != null) {
            map.put("form_name", model.getName());
        }
        map.put("formId", formId);
        map.put("mainTable", true);
        return "repository/form/Sys_Repository_Form_Metadata";
    }

    @RequestMapping(value = "/getTreeJson", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String getTreeJson(String requestType, String formId, String param1) throws JsonGenerationException, JsonMappingException, IOException, ResositoryException {
        List<Map<String, Object>> json = new ArrayList<Map<String, Object>>();
        if (requestType.equals("root")) {
            FormMetadataModel model = formMetadataService.getMasterFormMetadataModel(formId);
            if (model != null) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("id", "F_" + model.getId());
                map.put("text", model.getName());
                map.put("leaf", false);
                map.put("cls", "treeBMBOMap");
                map.put("draggable", false);
                map.put("type", "father");
                map.put("expanded", true);
                json.add(map);
                
            }
        } else if (requestType.equals("father")) {
            List<FormMetadataModel> formSubList = formMetadataService.getSubFormMetadataList(formId);
            for (FormMetadataModel model : formSubList) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("id", "C_" + model.getId());
                map.put("text", model.getName());
                map.put("leaf", true);
                map.put("cls", "treeBMBO");
                map.put("draggable", false);
                map.put("type", "child");
                map.put("expanded", true);
                json.add(map);
            }
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(json);
    }

    @RequestMapping(value = "/openFormMetadaDataSetPage", method = {RequestMethod.POST, RequestMethod.GET})
    public String openFormMetadaDataSetPage(String form_id, String formMetadataId, ModelMap map) throws ResositoryException {
        map.put("form_id", form_id);
        map.put("formMetadataId", formMetadataId);
        FormMetadataModel model = formMetadataService.getFormMetadataModel(form_id, formMetadataId);
        map.put("form_name", model == null ? "" : model.getName());
        map.put("sql", model == null ? "" : model.getSql());
        map.put("xieyin_code", model == null ? "" : model.getXieyin_code());
        map.put("formGridDatasFormatEventFunctionName", model == null ? "" : model.getFormGridDatasFormatEventFunctionName());
        return "repository/form/Sys_Repository_Form_Metadata_Dataset";
    }

    @RequestMapping(value = "/saveFormMetadaDataSet", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String saveFormMetadaDataSet(String form_id, String formGridDatasFormatEventFunctionName, String formMetadataId, String formName, String xieyin_code, String sql) throws ResositoryException, IOException {
        FormMetadataModel formMetadata = formMetadataService.getFormMetadataModel(form_id, formMetadataId);
        FormModel formModel = formService.get(form_id);
        if (formMetadata == null) {
            List<FormMetadataModel> list = formModel.getFormMetadataList();
            list = list == null ? new ArrayList<FormMetadataModel>() : list;
            formMetadata = new FormMetadataModel();
            formMetadata.setId(seq.getUUID());
            formMetadata.setName(formName);
            formMetadata.setFormGridDatasFormatEventFunctionName(formGridDatasFormatEventFunctionName);
            formMetadata.setXieyin_code(xieyin_code);
            formMetadata.setType(RespositoryConstant.SYS_FORM_METADATA_TYPE_SUB);
            formMetadata.setSql(sql);
            formMetadata.setPage_size(50);
            formMetadata.setGrid_height(300);
            formMetadata.setForm_id(form_id);
            list.add(formMetadata);
            formModel.setFormMetadataList(list);
            formService.saveMybaits(formMetadata);
            formService.store(formModel);
        } else {
            List<FormMetadataModel> list = formModel.getFormMetadataList();
            formMetadata.setForm_id(form_id);
            formMetadata.setName(formName);
            formMetadata.setSql(sql);
            formMetadata.setFormGridDatasFormatEventFunctionName(formGridDatasFormatEventFunctionName);
            formMetadata.setXieyin_code(xieyin_code);
            formModel.setFormMetadataList(list);
            formService.saveMybaits(formMetadata);
            formService.store(formModel);
        }
        return "ok";
    }

    @RequestMapping(value = "/removeFormMetadaDataSet", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String removeFormMetadaDataSet(String form_id, String formMetadataId) throws ResositoryException, IOException {
        FormModel formModel = formService.get(form_id);
        List<FormMetadataModel> formMetadataModellist = formModel.getFormMetadataList();
        for (FormMetadataModel formMetadataModel : formMetadataModellist) {
            // 找到对应的数据集
            if (formMetadataModel.getId().equals(formMetadataId)) {
                formService.remveMybaits(formMetadataModel);
                formMetadataModellist.remove(formMetadataModel);
                formService.store(formModel);
                break;
            }
        }
        return "ok";
    }
}
