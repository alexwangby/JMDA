package com.jmda.platform.repository.form.util;

import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.form.model.FormMetadataMapModel;
import com.jmda.platform.repository.form.model.FormMetadataModel;
import com.jmda.platform.repository.form.model.FormModel;
import com.jmda.platform.repository.form.service.FormMetadataMapService;
import com.jmda.platform.repository.form.service.FormMetadataService;
import com.jmda.platform.repository.form.service.FormService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("FormMetadateMapCommonMethod")
public class FormMetadateMapCommonMethod {
	@Resource
	FormMetadataService formMetadataService;
	@Resource
	FormService formService;
	@Resource
	FormMetadataMapService formMetadataMapService;
	public String formMetadataMapOrder(String form_id,String form_er_id,String sourceId,String targetId) {
		if (!sourceId.equals("") && !targetId.equals("")) {
			FormMetadataMapModel metadataMapModel1 = getMetadataMapById(sourceId,form_id,form_er_id);
			FormMetadataMapModel metadataMapModel2 = getMetadataMapById(targetId,form_id,form_er_id);
		    if(metadataMapModel1!=null&&metadataMapModel2!=null){
		    	int order1=metadataMapModel1.getOrder_index();
		    	int order2=metadataMapModel2.getOrder_index();
		    	int num=order1-order2;
		    	if(num>1){
		    		  int r=gridOrderMetadataMap(order1, order2,form_id,form_er_id);
		    		  if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderFormMetadataMap(metadataMapModel1,form_id,form_er_id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else if(num<-1){
		    		 int r=gridOrderMetadataMapDown(order1, order2,form_id,form_er_id);
		    		 if(r==0){
		    			  return "-1";
		    		  }else{
		    			  int isCommit =orderFormMetadataMap(metadataMapModel1,form_id,form_er_id,order2);
		    			  if(isCommit==0){
		    				  return "-1";
		    			  }
		    		  }
		    	}else{
			    	int isCommit1=orderFormMetadataMap(metadataMapModel1,form_id,form_er_id,order2);
					int isCommit2=orderFormMetadataMap(metadataMapModel2,form_id,form_er_id,order1);
					if(isCommit1==0||isCommit2==0){
						return "-1";
					}
		    	}
		    }else{
		    	return "-1";
		    }
			
	}
	return "1";
	}
	private FormMetadataMapModel getMetadataMapById(String uiId,String form_id,String form_er_id){
		FormMetadataMapModel metadataMapModel=new FormMetadataMapModel();
		FormMetadataModel metadataModel=new FormMetadataModel();
		try {
			FormModel model = formService.get(form_id);
			List<FormMetadataModel> listc=null;
			if(model!=null&&model.getFormMetadataList()!=null){
					listc=model.getFormMetadataList();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 FormMetadataModel metadata=listc.get(i);
				 String  metadataId=metadata.getId();
				 if(metadata!=null&&metadataId!=null){
				   if(metadataId.equals(form_er_id)){
					   metadataModel=listc.get(i);
					  break;
				   }
				  }
			 }
			 List<FormMetadataMapModel> listd = metadataModel.getFormMetadataMapList();
			 if(listd!=null){
				 for (int i = 0; i < listd.size(); i++) {
					 FormMetadataMapModel metadataMap = listd.get(i);
					 if(metadataMap!=null){
						 String metadataMapId = metadataMap.getId();
						 if(metadataMapId!=null){
							   if(metadataMapId.equals(uiId)){
								   metadataMapModel=listd.get(i);
								  break;
							   }
						 }
					 }
				 }
			 }

			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return metadataMapModel;
	}
	private int gridOrderMetadataMap(int order1,int order2,String form_id,String form_er_id){
		//FormMetadataMapModel metadataMapModel=new FormMetadataMapModel();
		FormMetadataModel metadataModel=new FormMetadataModel();
		try {
			FormModel model = formService.get(form_id);
			List<FormMetadataModel> listc=null;
			if(model!=null&&model.getFormMetadataList()!=null){
					listc=model.getFormMetadataList();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 FormMetadataModel metadata=listc.get(i);
				 String  metadataId=metadata.getId();
				 if(metadata!=null&&metadataId!=null){
				   if(metadataId.equals(form_er_id)){
					   metadataModel=listc.get(i);
					  break;
				   }
				  }
			 }
			 List<FormMetadataMapModel> listd = metadataModel.getFormMetadataMapList();
			 if(listd!=null){
				 for (int i = 0; i < listd.size(); i++) {
					 FormMetadataMapModel metadataMap = listd.get(i);
					 if(metadataMap.getOrder_index()<order1&&metadataMap.getOrder_index()>=order2){
						 metadataMap.setOrder_index(metadataMap.getOrder_index()+1);
					 }
				 }
			 }
			if (model != null && listc != null) {
					String reid = formService.create(model);
					if (reid != null) {
						return 1;
					}else{
						return 0;
					}
			}
			}
		} catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	private int orderFormMetadataMap(FormMetadataMapModel metadataMap,String form_id,String form_er_id,int order){
		//FormMetadataMapModel metadataMapModel=new FormMetadataMapModel();
		FormMetadataModel metadataModel=new FormMetadataModel();
		String metadataMapId=metadataMap.getId();
		try {
			FormModel model = formService.get(form_id);
			List<FormMetadataModel> listc=null;
			if(model!=null&&model.getFormMetadataList()!=null){
					listc=model.getFormMetadataList();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 FormMetadataModel metadata=listc.get(i);
				 String  metadataId=metadata.getId();
				 if(metadata!=null&&metadataId!=null){
				   if(metadataId.equals(form_er_id)){
					   metadataModel=listc.get(i);
					  break;
				   }
				  }
			 }
			 List<FormMetadataMapModel> listd = metadataModel.getFormMetadataMapList();
			 if(listd!=null){
				 for (int i = 0; i < listd.size(); i++) {
					 FormMetadataMapModel metadataMapModel = listd.get(i);
					 if(metadataMapModel!=null){
						 String metadataMap_id = metadataMapModel.getId();
						 if(metadataMapId!=null){
							   if(metadataMap_id.equals(metadataMapId)){
								   metadataMapModel.setOrder_index(order);
							   }
						 }
					 }
				 }
			 }
			if (model != null && listc != null) {
				if (model != null && listc != null) {
					String reid = formService.create(model);
					if (reid != null) {
						return 1;
					}else{
						return 0;
					}
			  }
			}
		} 
		}catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	private int gridOrderMetadataMapDown(int order1,int order2,String form_id,String form_er_id){
		FormMetadataModel metadataModel=new FormMetadataModel();
		try {
			FormModel model = formService.get(form_id);
			List<FormMetadataModel> listc=null;
			if(model!=null&&model.getFormMetadataList()!=null){
					listc=model.getFormMetadataList();
			}
			if(listc!=null){
			 for (int i = 0; i < listc.size(); i++) {
				 FormMetadataModel metadata=listc.get(i);
				 String  metadataId=metadata.getId();
				 if(metadata!=null&&metadataId!=null){
				   if(metadataId.equals(form_er_id)){
					   metadataModel=listc.get(i);
					  break;
				   }
				  }
			 }
			 List<FormMetadataMapModel> listd = metadataModel.getFormMetadataMapList();
			 if(listd!=null){
				 for (int i = 0; i < listd.size(); i++) {
					 FormMetadataMapModel metadataMapModel = listd.get(i);
					 if(metadataMapModel!=null){
						if(metadataMapModel.getOrder_index()>order1&&metadataMapModel.getOrder_index()<=order2){
								metadataMapModel.setOrder_index(metadataMapModel.getOrder_index()-1);
						}
					 }
				 }
			 }
			if (model != null && listc != null) {
				if (model != null && listc != null) {
					String reid = formService.create(model);
					if (reid != null) {
						return 1;
					}else{
						return 0;
					}
			  }
			}
		} 
		}catch (ResositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
