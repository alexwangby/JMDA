package com.jmda.platform.repository.worklist.model;

import java.util.List;

/**
 * Created by john on 2015/12/15.
 */
public class MaterialInfoTreeModel {
    private String id = "";
    private String text = "";
    private String state = "";
    private String iconCls = "";
    private Boolean checked = false;
    private List<MaterialInfoTreeModel> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIconCls() {
        return iconCls;
    }

    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public List<MaterialInfoTreeModel> getChildren() {
        return children;
    }

    public void setChildren(List<MaterialInfoTreeModel> children) {
        this.children = children;
    }
}
