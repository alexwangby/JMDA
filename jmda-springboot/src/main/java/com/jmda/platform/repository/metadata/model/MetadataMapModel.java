package com.jmda.platform.repository.metadata.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;

public class MetadataMapModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6902974685286771622L;
	@JsonProperty
	@XStreamAlias("TABLE_NAME")
	private String TABLE_NAME;
	@XStreamAlias("COLUMN_NAME")
	@JsonProperty
	private String COLUMN_NAME;
	@XStreamAlias("COLUMN_TITLE")
	@JsonProperty
	private String COLUMN_TITLE;
	@XStreamAlias("COLUMN_TYPE")
	@JsonProperty
	private String COLUMN_TYPE;
	@XStreamAlias("COLUMN_LENGTH")
	@JsonProperty
	private String COLUMN_LENGTH;
	@XStreamAlias("COLUMN_LABEL")
	@JsonProperty
	private String COLUMN_LABEL;
	@XStreamAlias("TABLE_COLUMN_TYPE")
	@JsonProperty
	private String TABLE_COLUMN_TYPE;
	public String getCOLUMN_NAME() {
		return COLUMN_NAME;
	}

	public void setCOLUMN_NAME(String cOLUMN_NAME) {
		COLUMN_NAME = cOLUMN_NAME;
	}

	public String getCOLUMN_TITLE() {
		return COLUMN_TITLE;
	}

	public void setCOLUMN_TITLE(String cOLUMN_TITLE) {
		COLUMN_TITLE = cOLUMN_TITLE;
	}

	public String getCOLUMN_TYPE() {
		return COLUMN_TYPE;
	}

	public void setCOLUMN_TYPE(String cOLUMN_TYPE) {
		COLUMN_TYPE = cOLUMN_TYPE;
	}

	public String getCOLUMN_LENGTH() {
		return COLUMN_LENGTH;
	}

	public void setCOLUMN_LENGTH(String cOLUMN_LENGTH) {
		COLUMN_LENGTH = cOLUMN_LENGTH;
	}

	public String getTABLE_NAME() {
		return TABLE_NAME;
	}

	public void setTABLE_NAME(String tABLE_NAME) {
		TABLE_NAME = tABLE_NAME;
	}

	public String getCOLUMN_LABEL() {
		return COLUMN_LABEL;
	}

	public void setCOLUMN_LABEL(String cOLUMN_LABEL) {
		COLUMN_LABEL = cOLUMN_LABEL;
	}

	public String getTABLE_COLUMN_TYPE() {
		return TABLE_COLUMN_TYPE;
	}

	public void setTABLE_COLUMN_TYPE(String TABLE_COLUMN_TYPE) {
		this.TABLE_COLUMN_TYPE = TABLE_COLUMN_TYPE;
	}
}