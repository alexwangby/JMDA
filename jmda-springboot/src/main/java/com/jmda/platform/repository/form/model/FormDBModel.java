package com.jmda.platform.repository.form.model;

import java.io.Serializable;
import java.sql.Blob;

public class FormDBModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String FORM_ID;
	private String NAME;
	private Blob XML_CONTENT;
	public String getFORM_ID() {
		return FORM_ID;
	}
	public void setFORM_ID(String fORM_ID) {
		FORM_ID = fORM_ID;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public Blob getXML_CONTENT() {
		return XML_CONTENT;
	}
	public void setXML_CONTENT(Blob xML_CONTENT) {
		XML_CONTENT = xML_CONTENT;
	}

}
