package com.jmda.platform.repository.application.model;

import java.io.Serializable;
import java.sql.Blob;


public class ApplicationDBModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String APPLICATION_ID;
	private String NAME;
	private Blob XML_CONTENT;
	public String getAPPLICATION_ID() {
		return APPLICATION_ID;
	}
	public void setAPPLICATION_ID(String aPPLICATION_ID) {
		APPLICATION_ID = aPPLICATION_ID;
	}
	public String getNAME() {
		return NAME;
	}
	public void setSYSTM_NAME(String nAME) {
		NAME = nAME;
	}
	public Blob getXML_CONTENT() {
		return XML_CONTENT;
	}
	public void setXML_CONTENT(Blob xML_CONTENT) {
		XML_CONTENT = xML_CONTENT;
	}
}
