package com.jmda.platform.repository.worklist.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
public class ButtonModel {
	@XStreamAlias("id")
	private String id;
	@XStreamAlias("title")
	private String title;
	@XStreamAlias("orderIndex")
	private int orderIndex;
	@XStreamAlias("tooltip")
	private String tooltip;
	@XStreamAlias("iconCls")
	private String iconCls;
	@XStreamAlias("handler")
	private String handler;
	@XStreamAlias("customHtml")
	private String customHtml;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public String getHandler() {
		return handler;
	}

	public void setHandler(String handler) {
		this.handler = handler;
	}

	public String getCustomHtml() {
		return customHtml;
	}

	public void setCustomHtml(String customHtml) {
		this.customHtml = customHtml;
	}

}
