package com.jmda.platform.repository.application.service;

import com.jmda.platform.commom.BaseService;
import com.jmda.platform.commom.SequenceService;
import com.jmda.platform.commom.util.FileUtil;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.application.cache.ApplicationCache;
import com.jmda.platform.repository.application.model.ApplicationModel;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@Service
public class ApplicationSevice implements BaseService<ApplicationModel> {
	@Resource
	ApplicationCache cache;
	@Resource
	SequenceService seq;
	@Resource
	DataBaseService db;
	@Override
	public List<ApplicationModel> getAll() throws ResositoryException {
		// TODO Auto-generated method stub
		return cache.getAll();
	}

	@Override
	public ApplicationModel get(String id) throws ResositoryException {
		// TODO Auto-generated method stub
		return cache.get(id);
	}

	@Override
	public void remove(String id) throws ResositoryException {
		// TODO Auto-generated method stub
		try {
			FileUtil.removeFile(cache.getFilePath() + id + ".xml");
			cache.remove(id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new ResositoryException(e);
		}

	}

	@Override
	public String store(ApplicationModel model) throws ResositoryException {
		// TODO Auto-generated method stub
		if (model.getId() == null || model.getId().trim().length() == 0||model.getId().equals("undefined")) {
			model.setId(seq.getUUID());
		}
		// 指定编码解析器
		XStream xstream = new XStream(new DomDriver("utf-8"));
		// 如果没有这句，xml中的根元素会是<包.类名>；或者说：注解根本就没生效，所以的元素名就是类的属性
		xstream.processAnnotations(ApplicationModel.class);
		xstream.aliasSystemAttribute(null, "class");
		String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + xstream.toXML(model);
		try {
			FileUtil.write(cache.getFilePath() + model.getId() + ".xml", xmlString);
			cache.put(model);
		} catch (IOException e) {
			throw new ResositoryException(e);
		}
		/*try {
			Object[] params = {model.getId()};
			List<ApplicationDBModel> list = db.queryList("select APPLICATION_ID from mda_application where APPLICATION_ID = ?", ApplicationDBModel.class, params);
			if(list!=null&&list.size()>0){ 
				Blob blob = new SerialBlob(xmlString.getBytes());//String 转 blo //Hibernate.createBlob(content.getBytes());  
			    Object[] upParams = {blob,model.getId()};
			    db.update("update mda_application set XML_CONTENT = ? where APPLICATION_ID = ?", upParams);
			}else{
				Blob blob = new SerialBlob(xmlString.getBytes());
				Object[] upParams = {model.getId(),model.getName(),blob};
				db.update("insert into mda_application(APPLICATION_ID,NAME,XML_CONTENT) values(?,?,?)", upParams);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		return model.getId();

	}

	@Override
	public List<ApplicationModel> queryList(ApplicationModel param) {
		// TODO Auto-generated method stub
		return cache.queryList(param);
	}

	@Override
	public ApplicationModel queryOne(ApplicationModel param) {
		// TODO Auto-generated method stub
		List<ApplicationModel> list = cache.queryList(param);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

}
