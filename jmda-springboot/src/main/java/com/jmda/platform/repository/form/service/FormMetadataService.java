package com.jmda.platform.repository.form.service;

import com.jmda.platform.commom.BaseService;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.RespositoryConstant;
import com.jmda.platform.repository.form.model.FormMetadataModel;
import com.jmda.platform.repository.form.model.FormModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class FormMetadataService implements BaseService<FormMetadataModel> {

	@Resource
	FormService formService;

	@Override
	public List<FormMetadataModel> getAll() throws ResositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	public List<FormMetadataModel> getSubFormMetadataList(String formId) throws ResositoryException {
		// TODO Auto-generated method stub
		List<FormMetadataModel> list = new ArrayList<FormMetadataModel>();
		FormModel formModel = formService.get(formId);
		if (formModel != null) {
			List<FormMetadataModel> metadataList = formModel.getFormMetadataList();
			if (metadataList != null && metadataList.size() > 0) {
				for (FormMetadataModel model : metadataList) {
					if (model.getType() == RespositoryConstant.SYS_FORM_METADATA_TYPE_SUB) {
						list.add(model);
					}
				}
			}
		}
		return list;
	}

	public FormMetadataModel getMasterFormMetadataModel(String formId) throws ResositoryException {
		FormModel formModel = formService.get(formId);
		if (formModel != null) {
			List<FormMetadataModel> list = formModel.getFormMetadataList();
			if (list != null && list.size() > 0) {
				for (FormMetadataModel model : list) {
					if (model.getType() == RespositoryConstant.SYS_FORM_METADATA_TYPE_MASTER) {
						return model;
					}
				}
			}
		}
		return null;
	}

	public FormMetadataModel getFormMetadataModel(String formId, String formMetadataId) throws ResositoryException {
		FormModel formModel = formService.get(formId);
		if (formModel != null) {
			List<FormMetadataModel> list = formModel.getFormMetadataList();
			if (list != null && list.size() > 0) {
				for (FormMetadataModel model : list) {
					if (model.getId().equals(formMetadataId)) {
						return model;
					}
				}
			}
		}
		return null;
	}

	@Override
	public FormMetadataModel get(String id) throws ResositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(String id) throws ResositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public String store(FormMetadataModel model) throws ResositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FormMetadataModel> queryList(FormMetadataModel param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FormMetadataModel queryOne(FormMetadataModel param) {
		// TODO Auto-generated method stub
		return null;
	}


}
