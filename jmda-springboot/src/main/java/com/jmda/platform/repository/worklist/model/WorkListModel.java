package com.jmda.platform.repository.worklist.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@XStreamAlias("worklist")
public class WorkListModel implements Serializable {
	private static final long serialVersionUID = -6430802484074170575L;
	@XStreamAlias("id")
	@JsonProperty
	private String id;
	@XStreamAlias("applicationId")
	@JsonProperty
	private String applicationId;
	@XStreamAlias("name")
	@JsonProperty
	private String name;
	@XStreamAlias("xieyin_code")
	@JsonProperty
	private String xieyin_code;
	@XStreamAlias("page_size")
	private int page_size;
	@XStreamAlias("extendJavascript")
	@JsonProperty
	private String extendJavascript;
	@XStreamAlias("sql")
	@JsonProperty
	private String sql;
	@XStreamImplicit(itemFieldName = "column")
	List<ColumnModel> column;
	@XStreamImplicit(itemFieldName = "execelColumn")
	List<ExecelColumnModel> execelColumn;
	
	@XStreamImplicit(itemFieldName = "searchWhere")
	List<SearchModel> searchWhere;

	@XStreamImplicit(itemFieldName = "button")
	List<ButtonModel> button;
	@XStreamAlias("templateFile")
	@JsonProperty
	private String templateFile;
	@XStreamAlias("layout")
	@JsonProperty
	private String layout;
	@XStreamAlias("leftUrl")
	@JsonProperty
	private String leftUrl;
	@XStreamAlias("rightUrl")
	@JsonProperty
	private String rightUrl;
	@XStreamAlias("leftTitle")
	@JsonProperty
	private String leftTitle;
	@XStreamAlias("rightTitle")
	@JsonProperty
	private String rightTitle;
	@XStreamAlias("formateClass")
	@JsonProperty
	private String formateClass;
	//是否详细
	@XStreamAlias("is_detail")
	@JsonProperty
	private Boolean is_detail;
	//是否分页
	@XStreamAlias("is_page")
	@JsonProperty
	private Boolean is_page;
	//是否首次打开页面执行查询
	@XStreamAlias("is_execute_query")
	@JsonProperty
	private Boolean is_execute_query;
	
	@XStreamAlias("db")
	@JsonProperty
	private String db;
	
	@XStreamAlias("treeField")
	@JsonProperty
	private String treeField;
	
	
	@XStreamAlias("templateName")
	@JsonProperty
	private String templateName;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getXieyin_code() {
		return xieyin_code;
	}

	public void setXieyin_code(String xieyin_code) {
		this.xieyin_code = xieyin_code;
	}

	public int getPage_size() {
		return page_size;
	}

	public void setPage_size(int page_size) {
		this.page_size = page_size;
	}

	public String getExtendJavascript() {
		return extendJavascript;
	}

	public void setExtendJavascript(String extendJavascript) {
		this.extendJavascript = extendJavascript;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
	public List<ColumnModel> getColumn() {
        if (column != null) {
            Collections.sort(column, new Comparator<Object>() {
                @Override
                public int compare(Object o1, Object o2) {
                    if (((ColumnModel) o1).getOrderIndex() > ((ColumnModel) o2).getOrderIndex()) {

                        return 1;
                    }
                    if (((ColumnModel) o1).getOrderIndex() == ((ColumnModel) o2).getOrderIndex()) {
                        return 0;
                    }
                    return -1;
                }
            });
        }
		return column;
	}

	public void setExecelColumn(List<ExecelColumnModel> column) {
		this.execelColumn = column;
	}
	
	public List<ExecelColumnModel> getExecelColumn() {
        if (execelColumn != null) {
            Collections.sort(execelColumn, new Comparator<Object>() {
                @Override
                public int compare(Object o1, Object o2) {
                    if (((ExecelColumnModel) o1).getOrderIndex() > ((ExecelColumnModel) o2).getOrderIndex()) {

                        return 1;
                    }
                    if (((ExecelColumnModel) o1).getOrderIndex() == ((ExecelColumnModel) o2).getOrderIndex()) {
                        return 0;
                    }
                    return -1;
                }
            });
        }
		return execelColumn;
	}

	public void setColumn(List<ColumnModel> column) {
		this.column = column;
	}

	public List<ButtonModel> getButton() {
		if(button!=null){
		Collections.sort(button, new Comparator<Object>(){   
			@Override
			public int compare(Object o1, Object o2) {
			       if(((ButtonModel)o1).getOrderIndex() > ((ButtonModel)o2).getOrderIndex()){   

	                    return 1;   
	                }   
	                if(((ButtonModel)o1).getOrderIndex() == ((ButtonModel)o2).getOrderIndex()){   
	                    return 0;   
	               }   
	               return -1;   
			}
        }); 
		}
		return button;
	}

	public void setButton(List<ButtonModel> button) {
		this.button = button;
	}

	public List<SearchModel> getSearchWhere() {
		return searchWhere;
	}

	public void setSearchWhere(List<SearchModel> searchWhere) {
		this.searchWhere = searchWhere;
	}

	public String getTemplateFile() {
		return templateFile;
	}

	public void setTemplateFile(String templateFile) {
		this.templateFile = templateFile;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getLeftUrl() {
		return leftUrl;
	}

	public void setLeftUrl(String leftUrl) {
		this.leftUrl = leftUrl;
	}

	public String getRightUrl() {
		return rightUrl;
	}

	public void setRightUrl(String rightUrl) {
		this.rightUrl = rightUrl;
	}

	public String getLeftTitle() {
		return leftTitle;
	}

	public void setLeftTitle(String leftTitle) {
		this.leftTitle = leftTitle;
	}

	public String getRightTitle() {
		return rightTitle;
	}

	public void setRightTitle(String rightTitle) {
		this.rightTitle = rightTitle;
	}

	public String getFormateClass() {
		return formateClass;
	}

	public void setFormateClass(String formateClass) {
		this.formateClass = formateClass;
	}

	public Boolean getIs_detail() {
		return is_detail;
	}

	public void setIs_detail(Boolean is_detail) {
		this.is_detail = is_detail;
	}

	public Boolean getIs_page() {
		return is_page;
	}

	public void setIs_page(Boolean is_page) {
		this.is_page = is_page;
	}

	public Boolean getIs_execute_query() {
		return is_execute_query;
	}

	public void setIs_execute_query(Boolean is_execute_query) {
		this.is_execute_query = is_execute_query;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public String getTreeField() {
		return treeField;
	}

	public void setTreeField(String treeField) {
		this.treeField = treeField;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	
	
}
