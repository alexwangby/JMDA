package com.jmda.platform.repository.form.service;

import com.jmda.platform.commom.BaseService;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.form.model.FormMetadataButtonModel;
import com.jmda.platform.repository.form.model.FormMetadataMapModel;
import com.jmda.platform.repository.form.model.FormMetadataModel;
import com.jmda.platform.repository.form.model.FormModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class FormMetadataMapService implements BaseService<FormMetadataMapModel> {
	@Resource
	FormService formService;
	@Resource
	FormMetadataService formMetadataService;

	@Override
	public List<FormMetadataMapModel> getAll() throws ResositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FormMetadataMapModel get(String id) throws ResositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(String id) throws ResositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public String store(FormMetadataMapModel model) throws ResositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FormMetadataMapModel> queryList(FormMetadataMapModel param) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<FormMetadataMapModel> getFormMetadataMapList(String formId, String formMdatadataId) throws ResositoryException {
		// TODO Auto-generated method stub
		List<FormMetadataMapModel> list = new ArrayList<FormMetadataMapModel>();
		FormModel formModel = formService.get(formId);
		List<FormMetadataModel> formMetadataList = formModel.getFormMetadataList();
		if (formMetadataList != null && formMetadataList.size() > 0) {
			for (FormMetadataModel formMetadataModel : formMetadataList) {
				if (formMetadataModel.getId().equals(formMdatadataId)) {
					List<FormMetadataMapModel> formMetadataMapList = formMetadataModel.getFormMetadataMapList();
					if (formMetadataMapList != null) {
						list = formMetadataMapList;
					}
					break;
				}
			}
		}
		return list;
	}
	
	public List<FormMetadataButtonModel> getFormMetadataButtonList(String formId, String formMdatadataId) throws ResositoryException {
		// TODO Auto-generated method stub
		List<FormMetadataButtonModel> list = new ArrayList<FormMetadataButtonModel>();
		FormModel formModel = formService.get(formId);
		List<FormMetadataModel> formMetadataList = formModel.getFormMetadataList();
		if (formMetadataList != null && formMetadataList.size() > 0) {
			for (FormMetadataModel formMetadataModel : formMetadataList) {
				if (formMetadataModel.getId().equals(formMdatadataId)) {
					List<FormMetadataButtonModel> formMetadataButtonList = formMetadataModel.getFormMetadataButtonList();
					if (formMetadataButtonList != null) {
						list = formMetadataButtonList;
					}
					break;
				}
			}
		}
		return list;
	}


	@Override
	public FormMetadataMapModel queryOne(FormMetadataMapModel param) {
		// TODO Auto-generated method stub
		return null;
	}
}
