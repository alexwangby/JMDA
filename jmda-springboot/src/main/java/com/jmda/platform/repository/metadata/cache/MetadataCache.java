package com.jmda.platform.repository.metadata.cache;

import com.jmda.platform.commom.BaseCache;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.repository.metadata.model.MetadataMapModel;
import com.jmda.platform.repository.metadata.model.MetadataModel;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import net.sf.ehcache.search.Query;
import net.sf.ehcache.search.Result;
import net.sf.ehcache.search.Results;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class MetadataCache implements BaseCache<MetadataModel> {
	@Resource
	DataBaseService db;
	public static final String SUPPLY_SQLSERVER = "sqlserver";

	public static final String SUPPLY_DB2 = "db2";

	public static final String SUPPLY_MYSQL = "mysql";

	public static final String SUPPLY_ORACLE = "oracle";
	@Override
	public void put(MetadataModel model) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		cache.remove(model.getTABLE_NAME());
		Element element = new Element(model.getTABLE_NAME(), model);
		cache.put(element);
	}

	@Override
	public List<MetadataModel> getAll() {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		List<MetadataModel> list = new LinkedList<MetadataModel>();
		Query query = cache.createQuery().includeValues();
		Results rs = query.execute();
		List<Result> all = rs.all();
		for (Result r : all) {
			MetadataModel model = (MetadataModel) r.getValue();
			list.add(model);
		}
		query.end();
		return list;
	}

	@Override
	public MetadataModel get(String id) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		Element element = cache.get(id);
		if (element != null) {
			return (MetadataModel) element.getObjectValue();
		}
		return null;
	}

	@Override
	public void remove(String id) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		cache.remove(id);
	}

	@Override
	public List<MetadataModel> queryList(MetadataModel param) {
		// TODO Auto-generated method stub
		Cache cache = LocalCacheManager.getCache(this.getClass());
		List<MetadataModel> list = new LinkedList<MetadataModel>();
		Query query = cache.createQuery().includeValues();
		LocalCacheManager.addSearchAttribute(cache, query, param);
		Results rs = query.execute();
		List<Result> all = rs.all();
		for (Result r : all) {
			MetadataModel model = (MetadataModel) r.getValue();
			list.add(model);
		}
		query.end();
		return list;
	}

	@PostConstruct
	public void initialize() throws IOException {
		// TODO Auto-generated method stub
		String[] searchs = { "TABLE_NAME", "TABLE_TITLE" };
		Cache cache = LocalCacheManager.registCache(this.getClass(), searchs);
		Connection conn = null;
		ResultSet tableSet = null;
		Statement st = null;
		try {
			String dbType = db.getSupply();
			if(dbType.equals(SUPPLY_ORACLE)){
				conn = db.getConnection();
				st = conn.createStatement();//创建sql执行对象
				tableSet = st.executeQuery("select * from  user_tables");//执行sql语句并返回结果集
				// 获取结果集元数据
				while(tableSet.next()) {
					MetadataModel model = new MetadataModel();
					String tableName = tableSet.getString("TABLE_NAME");
					model.setTABLE_NAME(tableName);
					List<MetadataMapModel> metadataMaplist = getOracleTableColumns(conn, model.getTABLE_NAME());
					model.setMetadataMaplist(metadataMaplist);
					Element element = new Element(model.getTABLE_NAME(), model);
					cache.put(element);
				}
			}else{
				conn = db.getConnection();
				DatabaseMetaData databaseMetaData = conn.getMetaData();
				tableSet = databaseMetaData.getTables(null, "HR", "%", new String[] { "TABLE" });
				while (tableSet.next()) {
					if (!tableSet.getString("TABLE_NAME").toLowerCase().startsWith("jmda_")&&!tableSet.getString("TABLE_NAME").toLowerCase().equals("sd_product")) {
						MetadataModel model = new MetadataModel();
						model.setTABLE_NAME(tableSet.getString("TABLE_NAME"));
						model.setTABLE_TITLE(tableSet.getString("REMARKS"));
						List<MetadataMapModel> metadataMaplist = getTableColumns(databaseMetaData, null, model.getTABLE_NAME());
						model.setMetadataMaplist(metadataMaplist);
						Element element = new Element(model.getTABLE_NAME(), model);
						cache.put(element);
					}
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(st!=null){
					st.close();
				}
				tableSet.close();
				db.close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public List<MetadataMapModel> getTableColumns(DatabaseMetaData databaseMetaData, String schemaName, String tableName) throws SQLException {
		ResultSet rs = null;
		List<MetadataMapModel> metadataMaplist = new ArrayList<MetadataMapModel>();
		try {
			rs = databaseMetaData.getColumns(null, schemaName, tableName, "%");
			while (rs.next()) {

				String columnName = rs.getString("COLUMN_NAME");// 列名
				String remarks = rs.getString("REMARKS");// 列描述
				String dataTypeName = rs.getString("TYPE_NAME");// java.sql.Types类型
				// TODO 暂时没用到
				// String tableName_ = rs.getString("TABLE_NAME");// 表名
				// int dataType = rs.getInt("DATA_TYPE"); // 对应的java.sql.Types类型
				// int columnSize = rs.getInt("COLUMN_SIZE");// 列大小
				// int decimalDigits = rs.getInt("DECIMAL_DIGITS");// 小数位数
				// boolean nullAble = rs.getBoolean("NULLABLE");// 是否允许为null
				// int charOctetLength = rs.getInt("CHAR_OCTET_LENGTH"); //
				// char类型的列中的最大字节数
				MetadataMapModel model = new MetadataMapModel();
				model.setCOLUMN_NAME(columnName);
				model.setCOLUMN_TITLE(remarks);
				model.setCOLUMN_TYPE(dataTypeName);
				metadataMaplist.add(model);
			}
		} catch (SQLException e) {
			System.out.println(tableName);
			e.printStackTrace();
		} finally {
			rs.close();
		}
		return metadataMaplist;
	}
	public List<MetadataMapModel> getOracleTableColumns(Connection conn, String tableName) {
		Statement st = null;
		ResultSet rs = null;
		List<MetadataMapModel> metadataMaplist = new ArrayList<MetadataMapModel>();
		try {
			st = conn.createStatement();
			rs = st.executeQuery("select * from  user_tab_columns where Table_Name='"+tableName+"'");//执行sql语句并返回结果集
			// 获取结果集元数据
			while(rs.next()) {
				String columnName = rs.getString("COLUMN_NAME");// 列名
				String dataTypeName = rs.getString("DATA_TYPE");// java.sql.Types类型
				MetadataMapModel model = new MetadataMapModel();
				model.setCOLUMN_NAME(columnName);
				model.setCOLUMN_TYPE(dataTypeName);
				metadataMaplist.add(model);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(st!=null){
				try {
					st.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}//创建sql执行对象
		
		return metadataMaplist;
	}
	@Override
	public String getFilePath() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
