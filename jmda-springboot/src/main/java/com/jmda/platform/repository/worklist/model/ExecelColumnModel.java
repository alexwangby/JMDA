package com.jmda.platform.repository.worklist.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ExecelColumnModel {

    @XStreamAlias("id")
    private String id;
    @XStreamAlias("talbelName")
    private String talbelName;
    @XStreamAlias("columnName")
    private String columnName;
    @XStreamAlias("title")
    private String title;
    @XStreamAlias("supportFuzzySearch")
    private boolean supportFuzzySearch;
    @XStreamAlias("orderIndex")
    private int orderIndex;
    @XStreamAlias("hotlinkName")
    private String hotlinkName;
    @XStreamAlias("hotlinkParam")
    private String hotlinkParam;
    //数据映射类型
    @XStreamAlias("ref_type")
    String ref_type;
    //数据映射
    @XStreamAlias("ref_params")
    String ref_params;
    

    public String getRef_type() {
		return ref_type;
	}

	public void setRef_type(String ref_type) {
		this.ref_type = ref_type;
	}

	public String getRef_params() {
		return ref_params;
	}

	public void setRef_params(String ref_params) {
		this.ref_params = ref_params;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSupportFuzzySearch() {
        return supportFuzzySearch;
    }

    public void setSupportFuzzySearch(boolean supportFuzzySearch) {
        this.supportFuzzySearch = supportFuzzySearch;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    public String getTalbelName() {
        return talbelName;
    }

    public void setTalbelName(String talbelName) {
        this.talbelName = talbelName;
    }

    public String getHotlinkName() {
        return hotlinkName == null || hotlinkName.trim().length() == 0 ? "无" : hotlinkName;
    }

    public void setHotlinkName(String hotlinkName) {
        this.hotlinkName = hotlinkName;
    }

    public String getHotlinkParam() {
        return hotlinkParam;
    }

    public void setHotlinkParam(String hotlinkParam) {
        this.hotlinkParam = hotlinkParam;
    }

}
