package com.jmda.platform.repository.login.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ConsoleLoginController {
	@GetMapping("/console")
	public String consoleIndex(){
		return "repository/application/Sys_Repository_Application";
	}
}
