package com.jmda.platform.repository.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.List;

@XStreamAlias("form")
public class FormModel implements Serializable {
    /**
     * 表单模型
     */
    private static final long serialVersionUID = 6581555117997471427L;
    @XStreamAlias("id")
    @JsonProperty
    private String id;
    @XStreamAlias("name")
    @JsonProperty
    private String name;
    @XStreamAlias("application_id")
    @JsonProperty
    private String application_id;
    @XStreamAlias("templete_name")
    @JsonProperty
    private String templete_name;
    @XStreamImplicit(itemFieldName = "formMetadata")
    @JsonProperty
    private List<FormMetadataModel> formMetadataList;

    @XStreamAlias("xieyin_code")
    @JsonProperty
    private String xieyin_code;

    @XStreamAlias("extendJavascript")
    @JsonProperty
    private String extendJavascript;

    @XStreamAlias("db")
    @JsonProperty

    private String db;
    @XStreamAlias("workflowId")
    @JsonProperty
    private String workflowId;

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

    public String getTemplete_name() {
        return templete_name;
    }

    public void setTemplete_name(String templete_name) {
        this.templete_name = templete_name;
    }

    public List<FormMetadataModel> getFormMetadataList() {
        return formMetadataList;
    }

    public void setFormMetadataList(List<FormMetadataModel> formMetadataList) {
        this.formMetadataList = formMetadataList;
    }

    public String getXieyin_code() {
        return xieyin_code;
    }

    public void setXieyin_code(String xieyin_code) {
        this.xieyin_code = xieyin_code;
    }

    public String getExtendJavascript() {
        return extendJavascript == null ? "" : extendJavascript;
    }

    public void setExtendJavascript(String extendJavascript) {
        this.extendJavascript = extendJavascript;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

}
