package com.jmda.platform.repository;

public class ResositoryException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1087336794068046400L;

	public ResositoryException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResositoryException(Throwable cause) {
		super(cause);
	}

	public ResositoryException(String message) {
		super(message);
	}
}
