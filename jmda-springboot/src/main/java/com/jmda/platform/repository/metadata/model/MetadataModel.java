package com.jmda.platform.repository.metadata.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@XStreamAlias("MetadataModel")
public class MetadataModel  implements Serializable{
	private static final long serialVersionUID = -4396822440547265807L;
	@XStreamAlias("TABLE_NAME")
	private String TABLE_NAME;
	@XStreamAlias("TABLE_TITLE")
	private String TABLE_TITLE;
	
	List<MetadataMapModel>  metadataMaplist=new ArrayList<MetadataMapModel>();
	public String getTABLE_NAME() {
		return TABLE_NAME;
	}

	public void setTABLE_NAME(String tABLE_NAME) {
		TABLE_NAME = tABLE_NAME;
	}

	public String getTABLE_TITLE() {
		return TABLE_TITLE;
	}

	public void setTABLE_TITLE(String tABLE_TITLE) {
		TABLE_TITLE = tABLE_TITLE;
	}

	public List<MetadataMapModel> getMetadataMaplist() {
		return metadataMaplist;
	}

	public void setMetadataMaplist(List<MetadataMapModel> metadataMaplist) {
		this.metadataMaplist = metadataMaplist;
	}
	
}
