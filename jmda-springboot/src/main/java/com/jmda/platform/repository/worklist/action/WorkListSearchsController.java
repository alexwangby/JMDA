package com.jmda.platform.repository.worklist.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.commom.SequenceService;
import com.jmda.platform.commom.SpringContextUtil;
import com.jmda.platform.engine.worklist.component.WorkListSearchUIComponentAbst;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.worklist.model.SearchModel;
import com.jmda.platform.repository.worklist.model.WorkListModel;
import com.jmda.platform.repository.worklist.service.WorkListService;
import com.jmda.platform.repository.worklist.util.WorkListSearchsCommonMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/repository/worklist/searchs")
public class WorkListSearchsController {
	@Resource
	WorkListService workListService;
	@Resource
	SequenceService seq;
	@Resource
	private WorkListSearchsCommonMethod workListSearchsCommonMethod = null;

	@RequestMapping(value = "/getWorkListSearchsPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getWorkListSearchsPage(String id, ModelMap map) throws ResositoryException {
		WorkListModel model = workListService.get(id);
		StringBuffer tdJson = new StringBuffer();
		tdJson.append("<select id='tdStyleSelect' name='tdStyleSelect'  style='width:300px;height:22px;'>");
		tdJson.append("<option value='col-sm-6'>二列</option>");
		tdJson.append("<option value='col-sm-4'>三列</option>");
		tdJson.append("<option value='col-sm-3' selected>四列</option>");
		tdJson.append("</select>");
		map.put("id", id);
		map.put("name", model.getName());
		map.put("tdJson", tdJson);
		return "repository/worklist/Sys_Repository_WorkList_Searchs";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getWorkListSearchsJson", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getWorkListSearchsJson(String id) throws JsonProcessingException, ResositoryException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> json = new HashMap<String, Object>();
		WorkListModel model = workListService.get(id);
		List<SearchModel> list = model.getSearchWhere();
		list = list == null ? new ArrayList<SearchModel>() : list;
		if (list != null) {
			Collections.sort(list, new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					if (((SearchModel) o1).getOrderIndex() > ((SearchModel) o2).getOrderIndex()) {

						return 1;
					}
					if (((SearchModel) o1).getOrderIndex() == ((SearchModel) o2).getOrderIndex()) {
						return 0;
					}
					return -1;
				}
			});
		}
		json.put("root", list);
		json.put("total", list == null ? 0 : list.size());
		return mapper.writeValueAsString(json);
	}

	@RequestMapping(value = "/saveWorkListSearch", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String saveWorkListSearch(String id, String datas,String tdStyleSelect) throws ResositoryException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		List<SearchModel> newSearchList = mapper.readValue(datas, new TypeReference<ArrayList<SearchModel>>() {
		});
		
//		if (newSearchList != null && newSearchList.size() > 0) {
			WorkListModel workListModel = workListService.get(id);
			List<SearchModel> workListSearchModellist = workListModel.getSearchWhere();
			workListSearchModellist = workListSearchModellist == null ? new ArrayList<SearchModel>()
					: workListSearchModellist;
			// 新建按钮
			for (SearchModel newSearchModel : newSearchList) {
				if (newSearchModel.getId() == null || newSearchModel.getId().trim().length() == 0) {
					newSearchModel.setShowSearchNum(tdStyleSelect);
					newSearchModel.setId("search_" + seq.getUUID());
					newSearchModel.setUiType("单行");
					newSearchModel.setOrderIndex(workListService.getWorkListSearchWhereNextIndex(workListModel));
					workListSearchModellist.add(newSearchModel);
				}
			}
			// 修改的按钮
			for (SearchModel searchModel : workListSearchModellist) {
				searchModel.setShowSearchNum(tdStyleSelect);
				for (SearchModel newSearchModel : newSearchList) {
					if (searchModel.getId().equals(newSearchModel.getId())) {
						searchModel.setColumnName(newSearchModel.getColumnName());
						searchModel.setTitle(newSearchModel.getTitle());
						searchModel.setWhere(newSearchModel.getWhere());
						searchModel.setUiType(newSearchModel.getUiType());
						searchModel.setUiParam(newSearchModel.getUiParam());
						searchModel.setCustomHtml(newSearchModel.getCustomHtml());
					}
				}
			}
			workListModel.setSearchWhere(workListSearchModellist);
			workListService.store(workListModel);
//		}
		return "ok";
	}

	@RequestMapping(value = "/removeWorkListSearch", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String removeWorkListSearch(String id, String ids) throws ResositoryException, IOException {
		String[] idArray = ids.split(",");
		if (idArray != null && idArray.length > 0) {
			WorkListModel workListModel = workListService.get(id);
			List<SearchModel> workListSearchModellist = workListModel.getSearchWhere();
			List<SearchModel> newworkListSearchModellist = new ArrayList<SearchModel>();
			for (SearchModel searchModel : workListSearchModellist) {
				boolean isOk = true;
				for (int i = 0; i < idArray.length; i++) {
					if (searchModel.getId().equals(idArray[i])) {
						isOk = false;
						break;
					}
				}
				if (isOk) {
					newworkListSearchModellist.add(searchModel);
				}
			}
			workListModel.setSearchWhere(newworkListSearchModellist);
			workListService.store(workListModel);
		}
		return "ok";
	}

	@RequestMapping(value = "/getSettingPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getSettingPage(String uiType, String uiParams, ModelMap map, HttpServletRequest request) {
		WorkListSearchUIComponentAbst abs = SpringContextUtil.getBean("WORKLIST_UI_SEARCH_" + uiType);
		if (abs != null) {
			return abs.getParamSetingWeb(uiParams, request, map);
		}
		return "";
	}

	@RequestMapping(value = "/sortGird", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String sortGirdMap(String sourceId, String targetId, String id) {
		return workListSearchsCommonMethod.workListSearchOrder(id, sourceId, targetId);
	}
}
