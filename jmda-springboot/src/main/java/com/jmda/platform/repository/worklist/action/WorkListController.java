package com.jmda.platform.repository.worklist.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.engine.worklist.event.WorkListFormatEvent;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.application.model.ApplicationModel;
import com.jmda.platform.repository.application.service.ApplicationSevice;
import com.jmda.platform.repository.worklist.model.WorkListModel;
import com.jmda.platform.repository.worklist.service.WorkListService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/repository/worklist")
public class WorkListController {
	@Resource
	WorkListService workListService;
	@Resource
	ApplicationSevice applicationSevice;

	@RequestMapping(value = "/openWorkList", method = { RequestMethod.POST, RequestMethod.GET })
	public String openWorkList(String worklistid, ModelMap map) throws ResositoryException {
		map.put("id", worklistid);
		WorkListModel workListModel = workListService.get(worklistid);
		if (workListModel != null) {
			map.put("name", workListModel.getName());
			String applicationId = workListModel.getApplicationId();
			ApplicationModel applicationModel = applicationSevice.get(applicationId);
			if (applicationModel != null) {
				map.put("applicationName", applicationModel.getName());
			}
		}
		return "repository/worklist/Sys_Repository_WorkList";
	}

	@RequestMapping(value = "/getWorkListBasePage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getWorkListBasePage(String worklistid, ModelMap map) throws ResositoryException {
		List<ApplicationModel> list = applicationSevice.getAll();
		WorkListModel workListModel = workListService.get(worklistid);
		if (workListModel != null) {
			map.put("name", workListModel.getName());
			map.put("sql", workListModel.getSql());
			map.put("groupId", workListModel.getApplicationId());
			map.put("extendJavascript", workListModel.getExtendJavascript());
			map.put("leftTitle", workListModel.getLeftTitle());
			map.put("leftUrl", workListModel.getLeftUrl());
			map.put("leftTitle", workListModel.getLeftTitle());
			map.put("rightTitle", workListModel.getRightTitle());
			map.put("rightUrl", workListModel.getRightUrl());
			map.put("xieyin_code", workListModel.getXieyin_code());
			map.put("formateClass", workListModel.getFormateClass());
			map.put("treeField", workListModel.getTreeField());
			map.put("is_detail", workListModel.getIs_detail());
			//是否分页
			map.put("is_page", workListModel.getIs_page());
			map.put("is_execute_query", workListModel.getIs_execute_query());
			if (workListModel.getTemplateName() == null || workListModel.getTemplateName().equals("")) {
				map.put("templateName", "/system/sys_worklist_easyui");
			} else {
				map.put("templateName", workListModel.getTemplateName());
			}
			if (workListModel.getTemplateFile() == null || workListModel.getTemplateFile().equals("")) {
				map.put("templateFile", "list");
			}else{
				map.put("templateFile", workListModel.getTemplateFile());
			}
		} else {
			map.put("name", "");
			map.put("groupId", "");
			map.put("sql", "");
			map.put("ExtendJavascript", "");
			map.put("formateClass", "");
			map.put("templateName", "/system/sys_worklist_easyui");
			map.put("templateFile", "list");
		}
		map.put("applicationList", list);
		map.put("id", worklistid);
		return "repository/worklist/Sys_Repository_WorkList_Base";
	}

	@RequestMapping(value = "/saveWorkListBase", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String saveWorkListBase(String id, String name, String sql, String extendJavascript, String form_group,
			String templateFile, String leftTitle, String leftUrl, String rightTitle, String rightUrl,
			String xieyin_code, String formateClass, Boolean is_detail,Boolean is_page,Boolean is_execute_query,String treeField,String templateName,ModelMap map) throws ResositoryException {
		WorkListModel workListModel = workListService.get(id);
		workListModel.setApplicationId(form_group);
		workListModel.setName(name);
		workListModel.setSql(sql);
		workListModel.setExtendJavascript(extendJavascript);
		workListModel.setTemplateFile(templateFile);
		workListModel.setLeftTitle(leftTitle);
		workListModel.setLeftUrl(leftUrl);
		workListModel.setRightTitle(rightTitle);
		workListModel.setRightUrl(rightUrl);
		workListModel.setXieyin_code(xieyin_code);
		workListModel.setIs_detail(is_detail);
		workListModel.setIs_page(is_page);
		workListModel.setIs_execute_query(is_execute_query);
		workListModel.setTreeField(treeField);
		workListModel.setTemplateName(templateName);
		if (formateClass != null && formateClass.trim().length() > 0) {
			try {
				Class<WorkListFormatEvent> eventClass = (Class<WorkListFormatEvent>) Class.forName(formateClass);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "不存在[" + formateClass
						+ "]java类，或是该类没有实现[om.jmda.platform.engine.worklist.event.WorkListFormatEvent]接口";
			}
		}
		workListModel.setFormateClass(formateClass);
		workListService.store(workListModel);

		return "ok";
	}

	@RequestMapping("/getGridPage")
	public String getGridPage(String applicationId, ModelMap map) {
		map.put("applicationId", applicationId);
		return "repository/worklist/Sys_Repository_WorkList_Grid";
	}

	@RequestMapping(value = "/getWorkListGridJson", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getWorkListGridJson(String applicationId, HttpServletRequest request) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> json = new HashMap<String, Object>();
		List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
		WorkListModel param = new WorkListModel();
		param.setApplicationId(applicationId);
		List<WorkListModel> list = workListService.queryList(param);
		if (list != null && list.size() > 0) {
			for (WorkListModel model : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("ID", model.getId());
				map.put("NAME", model.getName());
				map.put("OPERATOR", "<a href='javascript:void(0);' onclick=\"openWorklist('" + model.getName() + "','"
						+ model.getId() + "');return false;\" >编辑</a>");
				records.add(map);
			}
		}
		json.put("root", records);
		json.put("total", records == null ? 0 : records.size());
		return mapper.writeValueAsString(json);
	}

	@RequestMapping(value = "/remove", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String remove(String ids) throws ResositoryException {
		String[] idArray = ids.split(",");
		if (idArray != null && idArray.length > 0) {
			for (int i = 0; i < idArray.length; i++) {
				workListService.remove(idArray[i]);
			}
		}
		return "ok";
	}
}
