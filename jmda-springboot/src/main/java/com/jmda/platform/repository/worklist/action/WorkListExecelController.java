package com.jmda.platform.repository.worklist.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.commom.HttpRequestParamParser;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.commom.SequenceService;
import com.jmda.platform.commom.SpringContextUtil;
import com.jmda.platform.engine.mybatis.DataPager;
import com.jmda.platform.engine.mybatis.MybatisEngine;
import com.jmda.platform.engine.worklist.event.WorkListFormatEvent;
import com.jmda.platform.repository.ResositoryException;
import com.jmda.platform.repository.RespositoryConstant;
import com.jmda.platform.repository.metadata.model.MetadataMapModel;
import com.jmda.platform.repository.metadata.service.MetadataService;
import com.jmda.platform.repository.worklist.excel.ExportToExcelForMap;
import com.jmda.platform.repository.worklist.model.ExecelColumnModel;
import com.jmda.platform.repository.worklist.model.ExtColumnModel;
import com.jmda.platform.repository.worklist.model.WorkListModel;
import com.jmda.platform.repository.worklist.service.WorkListService;
import com.jmda.platform.repository.worklist.util.WorkListExecelCommonMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.*;

/**
 * execel导出
 * 
 * @author Dohia
 *
 */
@Controller
@RequestMapping("/repository/worklist/execel")
public class WorkListExecelController {

	@Resource
	MetadataService metadataService;
	@Resource
	WorkListService workListService;
	@Resource
	SequenceService seq;
	@Resource
	private WorkListExecelCommonMethod workListExecelOrder = null;

	/**
	 * execle导出
	 * 
	 * @param id
	 * @param map
	 * @return
	 * @throws ResositoryException
	 */
	@RequestMapping(value = "/getWorkListExecelPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getWorkListExecelPage(String id, ModelMap map) throws ResositoryException {
		WorkListModel model = workListService.get(id);
		map.put("id", id);
		map.put("name", model.getName());
		return "repository/worklist/Sys_Repository_WorkList_Execel";
	}

	@RequestMapping(value = "/getPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String getPage(String worklistId, HttpServletRequest request, ModelMap map)
			throws ResositoryException, JsonProcessingException {
		Map<String, Object> paramsMap = HttpRequestParamParser.formatParam(request);
		WorkListModel worklistModel = workListService.get(worklistId);
		List<ExecelColumnModel> columnList = worklistModel.getExecelColumn();
		StringBuffer datasparams = new StringBuffer();
		for (Map.Entry<String, Object> entry : paramsMap.entrySet()) {
			String key = entry.getKey().toString();
			String value = entry.getValue().toString();
			if (!key.equals("worklistId")) {
				datasparams.append("&").append(key).append("=").append(value);
			}
		}
		map.put("columnList", columnList);

		map.put("searchs", worklistModel.getSearchWhere());
		String basePath = request.getContextPath();
		String extMeata = worklistModel.getExtendJavascript() == null ? ""
				: worklistModel.getExtendJavascript().replaceAll("\\$\\{basePath\\}", basePath);
		String personContextStr = LocalCacheManager.getContextJsParams();
		map.put("extMeta", personContextStr + extMeata);
		map.put("worklistId", worklistId);
		map.put("dataparams", datasparams);
		map.put("basePath", basePath);
		String templateFile = worklistModel.getTemplateFile();
		map.put("templateFile", templateFile);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(map);
	}
    @RequestMapping(value = "/getAllDatas", method = {RequestMethod.POST, RequestMethod.GET})
    public void getAllDatas(String worklistId,  String searchConditions, String fuzzyCondition, HttpServletRequest request,HttpServletResponse response) throws ResositoryException, JsonProcessingException, SQLException {
    	List<Map<String, Object>> dataPage = null;
    	String bool = request.getParameter("search_is_execute_query");
    	ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> requestParams = HttpRequestParamParser.formatParam(request);
        WorkListModel worklistModel = workListService.get(worklistId);
        List<ExecelColumnModel> list = worklistModel.getExecelColumn();
        //默认不执行查询
        boolean _bool = true;
        if(worklistModel!=null&&worklistModel.getIs_execute_query()!=null&&worklistModel.getIs_execute_query()==true){
        	_bool = false;
        }
        if(bool!=null){
        	_bool = true;
        }
        if(_bool){
            Map<String, Object> params = LocalCacheManager.getContextParams();
            params.putAll(requestParams);
            dataPage = mybatis.selectList(worklistModel.getId(), RespositoryConstant.WORKLIST_RUNTIME_QUERY_LIST_ID, params);
            List<ExecelColumnModel> columnList = worklistModel.getExecelColumn();
            if (columnList != null) {
                for (ExecelColumnModel execelColumnModel : columnList) {
                    if (execelColumnModel.getRef_type() != null) {
                        for (Map<String, Object> record : dataPage) {
                            String value = String.valueOf(record.get(execelColumnModel.getColumnName()));
                            if (value != null && !value.equals("null") && value.trim().length() > 0) {
                                record.put(execelColumnModel.getColumnName() , LocalCacheManager.referenceData("redis", value, execelColumnModel.getRef_params()));
                            }
                        }

                    }
                }
            }
            // TODO 执行 表格格式化事件
            if (worklistModel.getFormateClass() != null && worklistModel.getFormateClass().trim().length() > 0) {
                try {
                    List<ExtColumnModel> extColumnList = new ArrayList<>();
                    Class<WorkListFormatEvent> eventClass = (Class<WorkListFormatEvent>) Class.forName(worklistModel.getFormateClass());
                    WorkListFormatEvent event = SpringContextUtil.getBean(eventClass);
                    if (event != null) {
                        event.formatColumnModel(extColumnList);
                        event.formatRecords(extColumnList, dataPage);
                    }
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    System.err.println("不存在[" + worklistModel.getFormateClass() + "]java类，或是该类没有实现[com.jmda.platform.engine.worklist.event.WorkListFormatEvent]接口");
                    e.printStackTrace();
                }
            }
        }
		ExportToExcelForMap excel = new ExportToExcelForMap();
		
		/*OutputStream out;
		try {
			File file = new File(path);
			ExportToExcelForMap.createFile(file);
			out = new FileOutputStream(path);
			excel.exportExcel(xieyinCode, headers, keys, resultList, out, "yyyy-MM-dd HH:mm:ss");
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		response.reset();
		String contentType = "application/octet-stream;charset=UTF-8";
		String agent = request.getHeader("USER-AGENT");
		response.setContentType(contentType);
		response.setCharacterEncoding("UTF-8");
		try {
			String xieyinCode = worklistModel.getXieyin_code();
			String[] headers = new String[list.size()];
			String[] keys = new String[list.size()];
			int i = 0;
			for (ExecelColumnModel execelColumnModel : list) {
				keys[i] = execelColumnModel.getColumnName();
				headers[i] = execelColumnModel.getTitle();
				i++;
			}
			response.setHeader("Content-disposition", "attachment;" + "filename=" + getFileName(System.currentTimeMillis() + ".xls", agent));
			OutputStream webOut = response.getOutputStream();
			excel.exportExcel(xieyinCode, headers, keys, dataPage, webOut, "yyyy-MM-dd HH:mm:ss");
			webOut.flush();
			webOut.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
	@RequestMapping(value = "/getDatas", method = { RequestMethod.POST, RequestMethod.GET })
	public void getDatas(String worklistId, String searchConditions, String fuzzyCondition,
			HttpServletRequest request,HttpServletResponse response) throws ResositoryException, JsonProcessingException, SQLException {
		Map<String, Object> requestParams = HttpRequestParamParser.formatParam(request);
		WorkListModel worklistModel = workListService.get(worklistId);
		String xieyinCode = worklistModel.getXieyin_code();
		Map<String, Object> params = LocalCacheManager.getContextParams();
		params.putAll(requestParams);
		DataPager<Map<String, Object>> dataPage = mybatis.selectAll(worklistModel.getId(),
				RespositoryConstant.WORKLIST_RUNTIME_QUERY_LIST_ID, params);
		List<ExecelColumnModel> list = worklistModel.getExecelColumn();
		 if (list != null) {
	            for (ExecelColumnModel columnModel : list) {
	                if (columnModel.getRef_type() != null) {
	                    for (Map<String, Object> record : dataPage.getRows()) {
	                        String value = String.valueOf(record.get(columnModel.getColumnName()));
	                        if (value != null && !value.equals("null") && value.trim().length() > 0) {
	                            record.put(columnModel.getColumnName(), LocalCacheManager.referenceData("redis", value, columnModel.getRef_params()));
	                        }
	                    }
	                }
	            }
	        }
		if (worklistModel.getFormateClass() != null && worklistModel.getFormateClass().trim().length() > 0) {
			try {
				List<ExtColumnModel> extColumnList = new ArrayList<>();
				Class<WorkListFormatEvent> eventClass = (Class<WorkListFormatEvent>) Class
						.forName(worklistModel.getFormateClass());
				WorkListFormatEvent event = SpringContextUtil.getBean(eventClass);
				if (event != null) {
					event.formatColumnModel(extColumnList);
					event.formatRecords(extColumnList, dataPage.getRows());
				}
			} catch (ClassNotFoundException e) {
				System.err.println("不存在[" + worklistModel.getFormateClass()
						+ "]java类，或是该类没有实现[com.jmda.platform.engine.worklist.event.WorkListFormatEvent]接口");
				e.printStackTrace();
			}
		}
		Map<String, Object> json = new HashMap<String, Object>();
		if (dataPage.getRows() == null) {
			json.put("rows", "[]");
			json.put("total", 0);
		} else {
			json.put("rows", dataPage.getRows());
			json.put("total", dataPage.getTotal());
		}
		
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		String[] headers = new String[list.size()];
		String[] keys = new String[list.size()];
		int i = 0;
		for (ExecelColumnModel execelColumnModel : list) {
			keys[i] = execelColumnModel.getColumnName();
			headers[i] = execelColumnModel.getTitle();
			i++;
		}
		for (Map<String, Object> t : dataPage.getRows()) {
			Map<String, Object> infoMap = new HashMap<String, Object>();
			for (int k = 0; k < keys.length; k++) {
				String key = keys[k];
				infoMap.put(key, t.get(key));
			}
			resultList.add(infoMap);
		}
		ExportToExcelForMap excel = new ExportToExcelForMap();
		
		/*OutputStream out;
		try {
			File file = new File(path);
			ExportToExcelForMap.createFile(file);
			out = new FileOutputStream(path);
			excel.exportExcel(xieyinCode, headers, keys, resultList, out, "yyyy-MM-dd HH:mm:ss");
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		response.reset();
		String contentType = "application/octet-stream;charset=UTF-8";
		String agent = request.getHeader("USER-AGENT");
		response.setContentType(contentType);
		response.setCharacterEncoding("UTF-8");
		try {
			response.setHeader("Content-disposition", "attachment;" + "filename=" + getFileName(System.currentTimeMillis() + ".xls", agent));
			OutputStream webOut = response.getOutputStream();
			excel.exportExcel(xieyinCode, headers, keys, resultList, webOut, "yyyy-MM-dd HH:mm:ss");
			webOut.flush();
			webOut.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*try {
			//清空一下response对象，防止出现缓存
			response.reset();
			//指明这是一个下载的respond
			response.setContentType("application/x-download");
			String filename=_org+"log.txt";
			//防止乱码
			filename=URLEncoder.encode(filename,"UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.addHeader("Content-Disposition","attachment;filename=" + filename);
			PrintWriter printWriter = new PrintWriter(response.getOutputStream());
	        printWriter.println(data);
	        printWriter.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getWorkListExecelJson", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getWorkListExecelJson(String id) throws JsonProcessingException, ResositoryException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> json = new HashMap<String, Object>();
		WorkListModel model = workListService.get(id);
		List<ExecelColumnModel> list = model.getExecelColumn();

		list = list == null ? new ArrayList<ExecelColumnModel>() : list;
		if (list != null) {
			Collections.sort(list, new Comparator() {
				@Override
				public int compare(Object o1, Object o2) {
					if (((ExecelColumnModel) o1).getOrderIndex() > ((ExecelColumnModel) o2).getOrderIndex()) {

						return 1;
					}
					if (((ExecelColumnModel) o1).getOrderIndex() == ((ExecelColumnModel) o2).getOrderIndex()) {
						return 0;
					}
					return -1;
				}
			});
		}

		json.put("root", list);
		json.put("total", list == null ? 0 : list.size());
		return mapper.writeValueAsString(json);
	}

	@RequestMapping(value = "/openWorkListMetadataMapSelectPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String openWorkListMetadataMapSelectPage(String id, ModelMap map) {
		map.put("id", id);
		return "repository/worklist/Sys_Repository_WorkList_Metadata_Select_Grid";
	}

	@Resource
	MybatisEngine mybatis;

	@RequestMapping(value = "/getWorListMetadataMapSelectJsonData", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getWorListMetadataMapSelectJsonData(String id) throws ResositoryException, JsonProcessingException {
		WorkListModel model = workListService.get(id);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> json = new HashMap<String, Object>();
		List<MetadataMapModel> list = metadataService.getMetadataMapListBySql(
				mybatis.getMapperSql(model.getId(), RespositoryConstant.WORKLIST_RUNTIME_QUERY_LIST_ID));
		json.put("root", list);
		json.put("total", list == null ? 0 : list.size());
		return mapper.writeValueAsString(json);
	}

	@RequestMapping(value = "/addWorkListMetadataMap", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String addWorkListMetadataMap(String id, String selectJson) throws ResositoryException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		List<MetadataMapModel> selectFormMetadataMapList = mapper.readValue(selectJson,
				new TypeReference<ArrayList<MetadataMapModel>>() {
				});
		if (selectFormMetadataMapList != null && selectFormMetadataMapList.size() > 0) {
			WorkListModel workListModel = workListService.get(id);
			List<ExecelColumnModel> workListColumnModellist = workListModel.getExecelColumn();
			workListColumnModellist = workListColumnModellist == null ? new ArrayList<ExecelColumnModel>()
					: workListColumnModellist;
			for (MetadataMapModel metadataMapModel : selectFormMetadataMapList) {
				boolean isOK = true;
				for (ExecelColumnModel execelColumnModel : workListColumnModellist) {
					if (metadataMapModel.getTABLE_COLUMN_TYPE() == null
							|| "".equalsIgnoreCase(metadataMapModel.getTABLE_COLUMN_TYPE().trim())
							|| "db".equalsIgnoreCase(metadataMapModel.getTABLE_COLUMN_TYPE())) {
						if (execelColumnModel.getColumnName().equals(metadataMapModel.getCOLUMN_LABEL())
								&& execelColumnModel.getTalbelName().equals(metadataMapModel.getTABLE_NAME())) {
							isOK = false;
							break;
						}
					} else {
						if (execelColumnModel.getColumnName().equals(metadataMapModel.getCOLUMN_NAME())) {
							isOK = false;
							break;
						}
					}

				}
				if (isOK) {
					ExecelColumnModel execelColumnModel = new ExecelColumnModel();
					execelColumnModel.setId(seq.getUUID());
					execelColumnModel.setOrderIndex(0);
					execelColumnModel.setSupportFuzzySearch(false);
					execelColumnModel.setTalbelName(metadataMapModel.getTABLE_NAME());
					execelColumnModel.setTitle(metadataMapModel.getCOLUMN_TITLE());
					execelColumnModel.setColumnName(metadataMapModel.getCOLUMN_LABEL());
					execelColumnModel.setOrderIndex(workListService.getWorkListColumnNextIndex(workListModel));
					workListColumnModellist.add(execelColumnModel);
				}
			}
			workListModel.setExecelColumn(workListColumnModellist);
			workListService.store(workListModel);
		}

		return "ok";
	}

	@RequestMapping(value = "/addWorkListMetadataVirtualMap", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String addWorkListMetadataVirtualMap(String id, String selectJson) throws ResositoryException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		List<MetadataMapModel> selectFormMetadataMapList = mapper.readValue(selectJson,
				new TypeReference<ArrayList<MetadataMapModel>>() {
				});
		int i = 0;
		if (selectFormMetadataMapList != null && selectFormMetadataMapList.size() > 0) {
			WorkListModel workListModel = workListService.get(id);
			List<ExecelColumnModel> workListColumnModellist = workListModel.getExecelColumn();
			workListColumnModellist = workListColumnModellist == null ? new ArrayList<ExecelColumnModel>()
					: workListColumnModellist;
			for (MetadataMapModel metadataMapModel : selectFormMetadataMapList) {
				boolean isOK = true;
				for (ExecelColumnModel execelColumnModel : workListColumnModellist) {
					if (execelColumnModel.getColumnName().equals(metadataMapModel.getCOLUMN_NAME())) {
						isOK = false;
						break;
					}
				}
				if (isOK) {
					ExecelColumnModel execelColumnModel = new ExecelColumnModel();
					execelColumnModel.setColumnName(metadataMapModel.getCOLUMN_LABEL());
					execelColumnModel.setId(seq.getUUID());
					execelColumnModel.setOrderIndex(0);
					execelColumnModel.setSupportFuzzySearch(false);
					execelColumnModel.setTalbelName(metadataMapModel.getTABLE_NAME());
					execelColumnModel.setTitle(metadataMapModel.getCOLUMN_TITLE());
					execelColumnModel.setOrderIndex(workListService.getWorkListColumnNextIndex(workListModel) + i);
					workListColumnModellist.add(execelColumnModel);
					i = i + 1;
				}
			}
			workListModel.setExecelColumn(workListColumnModellist);
			workListService.store(workListModel);
		}

		return "ok";
	}

	// @RequestMapping(value = "/saveWorkListColumn", method = {
	// RequestMethod.POST, RequestMethod.GET })
	// @ResponseBody
	// public String saveWorkListColumn(String id, String datas) throws
	// ResositoryException, IOException {
	// ObjectMapper mapper = new ObjectMapper();
	// List<ExecelColumnModel> newColumnList = mapper.readValue(datas,
	// new TypeReference<ArrayList<ExecelColumnModel>>() {
	// });
	// if (newColumnList != null && newColumnList.size() > 0) {
	// WorkListModel workListModel = workListService.get(id);
	// List<ExecelColumnModel> workListColumnModellist =
	// workListModel.getExecelColumnModel();
	// workListColumnModellist = workListColumnModellist == null ? new
	// ArrayList<ExecelColumnModel>()
	// : workListColumnModellist;
	// for (ExecelColumnModel column : workListColumnModellist) {
	// for (ExecelColumnModel newColumnModel : newColumnList) {
	// if (column.getId().equals(newColumnModel.getId())) {
	// column.setSupportFuzzySearch(newColumnModel.isSupportFuzzySearch());
	// column.setTitle(newColumnModel.getTitle());
	// column.setHotlinkName(newColumnModel.getHotlinkName());
	// column.setHotlinkParam(newColumnModel.getHotlinkParam());
	// }
	// }
	// }
	// workListService.store(workListModel);
	// }
	//
	// return "ok";
	// }

	@RequestMapping(value = "/saveWorkListColumnForExecel", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String saveWorkListColumnForExecel(String id, String datas) throws ResositoryException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		List<ExecelColumnModel> newColumnList = mapper.readValue(datas,
				new TypeReference<ArrayList<ExecelColumnModel>>() {
				});
		if (newColumnList != null && newColumnList.size() > 0) {
			WorkListModel workListModel = workListService.get(id);
			List<ExecelColumnModel> workListColumnModellist = workListModel.getExecelColumn();
			workListColumnModellist = workListColumnModellist == null ? new ArrayList<ExecelColumnModel>()
					: workListColumnModellist;
			for (ExecelColumnModel column : workListColumnModellist) {
				for (ExecelColumnModel newColumnModel : newColumnList) {
					if (column.getId().equals(newColumnModel.getId())) {
						column.setSupportFuzzySearch(newColumnModel.isSupportFuzzySearch());
						column.setTitle(newColumnModel.getTitle());
						column.setHotlinkName(newColumnModel.getHotlinkName());
						column.setHotlinkParam(newColumnModel.getHotlinkParam());
						if (newColumnModel.getRef_params() != null
								&& newColumnModel.getRef_params().trim().length() > 0) {
							column.setRef_type("redis");
							column.setRef_params(newColumnModel.getRef_params());
						} else {
							column.setRef_type(null);
							column.setRef_params(null);
						}
					}
				}
			}
			workListService.store(workListModel);
		}
		return "ok";
	}

	@RequestMapping(value = "/removeWorkListColumn", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String removeWorkListColumn(String id, String ids) throws ResositoryException, IOException {
		String[] idArray = ids.split(",");
		if (idArray != null && idArray.length > 0) {
			WorkListModel workListModel = workListService.get(id);
			List<ExecelColumnModel> workListColumnModellist = workListModel.getExecelColumn();
			List<ExecelColumnModel> newworkListColumnModellist = new ArrayList<ExecelColumnModel>();
			for (ExecelColumnModel column : workListColumnModellist) {
				boolean isOk = true;
				for (int i = 0; i < idArray.length; i++) {
					if (column.getId().equals(idArray[i])) {
						isOk = false;
						break;
					}
				}
				if (isOk) {
					newworkListColumnModellist.add(column);
				}
			}
			workListModel.setExecelColumn(newworkListColumnModellist);
			workListService.store(workListModel);
		}
		return "ok";
	}

	/**
	 * 拖动排序
	 *
	 * @param sourceId
	 * @param targetId
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/sortGird", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String sortGirdMap(String sourceId, String targetId, String id) {
		return workListExecelOrder.workListExecelOrder(id, sourceId, targetId);
	}

	/**
	 * 重新排序
	 * 
	 * @param id
	 * @return
	 * @throws ResositoryException
	 * @throws IOException
	 */
	@RequestMapping(value = "/afreshSortFormMetadataMap", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String afreshSortFormMetadataMap(String id) throws ResositoryException, IOException {
		WorkListModel workListModel = workListService.get(id);
		List<ExecelColumnModel> columnModelList = workListModel.getExecelColumn();
		int i = 1;
		for (ExecelColumnModel columnModel : columnModelList) {
			columnModel.setOrderIndex(i);
			i = i + 1;
		}
		return "ok";
	}
	/**
	 * 转码
	 * 
	 * @param fileName
	 * @param agent
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String getFileName(String fileName, String agent) throws UnsupportedEncodingException {
		String codedfilename = fileName;
		if (agent != null && agent.indexOf("Mozilla") != -1 && agent.indexOf("MSIE") == -1) {// ff,mozilla..
//			codedfilename = MimeUtility.encodeText(codedfilename, "GBK", "B");
		} else {
			codedfilename = new String(codedfilename.getBytes("GBK"), "ISO-8859-1");
		}
		return codedfilename;
	}
}
