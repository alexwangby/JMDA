package com.jmda.platform.engine.worklist.component.imp;


import com.jmda.platform.engine.worklist.component.WorkListSearchUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;
@Service("WORKLIST_UI_SEARCH_无")
public class WorkListSearchComponentNullImp extends WorkListSearchUIComponentAbst{

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		return "<input type=\"text\" value=\"\" id=\""+UUID.randomUUID()+"\"class=\"form-control\" style=\"display:none\"/>";
	}
	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		return "repository/worklist/searchUI/Sys_Resonsitory_Form_UI_Null";
	}
}
