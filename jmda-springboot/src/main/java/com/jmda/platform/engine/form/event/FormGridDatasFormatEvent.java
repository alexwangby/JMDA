package com.jmda.platform.engine.form.event;

import java.util.List;
import java.util.Map;

/**
 * Created by pierce-deng on 2015/6/25.
 */
public interface FormGridDatasFormatEvent {
    List<Map<String, Object>> format(String id, List<Map<String, Object>> datas);
}
