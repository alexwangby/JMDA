package com.jmda.platform.engine.worklist.component;

import java.util.Map;

import com.jmda.platform.repository.worklist.model.SearchModel;

public abstract class WorkListSearchUIComponentAbst implements WorkListSearchUIComponentInterface {
	SearchModel searchModel;
	private Map<String,Object> mapParams;
	public SearchModel getSearchModel() {
		return searchModel;
	}

	public void setSearchModel(SearchModel searchModel) {
		this.searchModel = searchModel;
	}

	public Map<String, Object> getMapParams() {
		return mapParams;
	}

	public void setMapParams(Map<String, Object> mapParams) {
		this.mapParams = mapParams;
	}

}
