package com.jmda.platform.engine.form.component.form;

import com.jmda.platform.repository.form.model.FormMetadataMapModel;

import java.util.Map;

import org.mybatis.spring.support.SqlSessionDaoSupport;

public abstract class FormUIComponentAbst implements FormUIComponentInterface{
	Object value;
	FormMetadataMapModel formMetadataMapModel;
	private String formId;
	private Map<String,Object> mapParams;
	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return value;
	}

	@Override
	public void setValue(Object value) {
		// TODO Auto-generated method stub
		this.value=value;
	}
	public FormMetadataMapModel getFormMetadataMapModel() {
		return formMetadataMapModel;
	}
	public void setFormMetadataMapModel(FormMetadataMapModel formMetadataMapModel) {
		this.formMetadataMapModel = formMetadataMapModel;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public Map<String, Object> getMapParams() {
		return mapParams;
	}

	public void setMapParams(Map<String, Object> mapParams) {
		this.mapParams = mapParams;
	}

}
