package com.jmda.platform.engine.worklist.event;

import com.jmda.platform.repository.worklist.model.ExtColumnModel;

import java.util.List;
import java.util.Map;

public interface WorkListFormatEvent {
	public void formatColumnModel(List<ExtColumnModel> list);

	public String formatRecords(List<ExtColumnModel> columnList,List<Map<String, Object>> records);

//	public String diyDataGrid();
}
