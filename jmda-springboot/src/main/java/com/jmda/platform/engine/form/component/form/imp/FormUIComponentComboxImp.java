package com.jmda.platform.engine.form.component.form.imp;

import com.alibaba.druid.pool.DruidDataSource;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.commom.SpringContextUtil;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import com.jmda.platform.engine.form.event.FormDatasComboxEvent;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Service("UI_FORM_列表")
public class FormUIComponentComboxImp extends FormUIComponentAbst {
	@Resource
	DataBaseService db;
   
    @Override
    public String getHtmlDefine(String params) {
        // TODO Auto-generated method stub
        StringBuffer html = new StringBuffer();
        String isNotNull = "";
        if (!super.getFormMetadataMapModel().isIs_null()) {
            isNotNull = "data-options=required:true";
        }
        FormUIComponentComboxParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
        if (paramModel.getType() != null && paramModel.getType().equals("sql")) {
        	
            html.append("<select " + (paramModel.getExtendedCode().trim().length() <= 0 ? "" : paramModel.getExtendedCode()) + " class='form-control  easyui-validatebox' " + isNotNull + " " + "  name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" >");
            html.append("<option value='' >请选择</option>");
            if (paramModel.getSql().trim().length() > 0 && paramModel.getDisplayNameValue().trim().length() > 0 && paramModel.getGetFieldValue().trim().length() > 0) {
                String sql = paramModel.getSql();
                try {
                    Map<String, Object> paramsMap = super.getMapParams();
                    if (paramsMap != null) {
                        for (Map.Entry<String, Object> entry : paramsMap.entrySet()) {
                            String key = "#{" + entry.getKey().toString() + "}";
                            String value=(entry.getValue() == null?"'" + "'":"'" + entry.getValue().toString() + "'");
                            if (sql.indexOf(key) > 0) {
                                sql = sql.replace(key, value);
                            }
                        }
                    }
                    List<Map<String, Object>> list = null;//db.queryList(sql, null);
                    
                    String extClass = paramModel.getExtClass();
                	if(extClass!=null&&!extClass.equals("")){
                        try {
                            Class<FormDatasComboxEvent> eventClass = (Class<FormDatasComboxEvent>) Class.forName(extClass);
                            FormDatasComboxEvent event = SpringContextUtil.getBean(eventClass);
                            if (event != null) {
                            	 String key = event.returnComboxDataSourceKey();
                            	 DruidDataSource comboPooledDataSource = SpringContextUtil.getBean(key, DruidDataSource.class);
                                 Connection conn = null;
                         		try {
                         			conn = comboPooledDataSource.getConnection();
                                	list = db.queryList(conn,sql, null);
                         		} catch (SQLException e) {
                         			// TODO Auto-generated catch block
                         			e.printStackTrace();
                         			return null;
                         		}
                            	
                            }
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                	}else{
                		list = db.queryList(sql, null);
                	}
                    if (list != null) {
                        for (Map<String, Object> map : list) {
                            String text = String.valueOf(map.get(paramModel.getDisplayNameValue()));
                            String value = String.valueOf(map.get(paramModel.getGetFieldValue()));
                            if (value != null && text != null) {
                                if (super.getValue() != null && super.getValue().toString().equals(value)) {
                                    html.append(" <option selected=\"selected\" value=\"" + value + "\">" + text + "</option>");
                                } else {
                                    html.append(" <option  value=\"" + value + "\">" + text + "</option>");
                                }
                            }
                        }
                    }
                } catch (SQLException e) {
                }
            }
            html.append("</select>");
        } else {
            html.append("<select " + (paramModel.getExtendedCode().trim().length() <= 0 ? "" : paramModel.getExtendedCode()) + " class='form-control  easyui-validatebox' " + isNotNull + " " + "  name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" >");
            html.append("<option value='' >请选择</option>");
            if (paramModel.getNormalValue().indexOf("|") != -1) {
                String selects[] = paramModel.getNormalValue().split("\\|");
                String selectsview[];
                for (int i = 0; i < selects.length; i++) {
                    if (selects[i].indexOf(":") != -1) {
                        selectsview = selects[i].split(":");
                        if (super.getValue() != null && super.getValue().toString().equals(selectsview[0])) {
                            html.append(" <option selected=\"selected\" value=\"" + selectsview[0] + "\">" + selectsview[1] + "</option>");
                        } else {
                            html.append(" <option value=\"" + selectsview[0] + "\">" + selectsview[1] + "</option>");
                        }
                    } else {
                        if (super.getValue() != null && super.getValue().toString().equals(selects[i])) {
                            html.append(" <option selected=\"selected\" value=\"" + selects[i] + "\">" + selects[i] + "</option>");
                        } else {
                            html.append(" <option value=\"" + selects[i] + "\">" + selects[i] + "</option>");
                        }
                    }
                }
            } else {
                if (super.getValue() != null && super.getValue().toString().equals(paramModel.getNormalValue())) {
                    html.append(" <option  selected=\"selected\" value=\"" + paramModel.getNormalValue() + "\">" + paramModel.getNormalValue() + "</option>");
                } else {
                    html.append(" <option value=\"" + paramModel.getNormalValue() + "\">" + paramModel.getNormalValue() + "</option>");
                }

            }
            html.append("</select>");
        }

        return html.toString();
    }

    @Override
    public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
        // TODO Auto-generated method stub
        FormUIComponentComboxParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
        map.put("type", paramModel.getType());
        map.put("normalValue", paramModel.getNormalValue());
        map.put("extendedCode", paramModel.getExtendedCode());
        map.put("sql", paramModel.getSql());
        map.put("getFieldValue", paramModel.getGetFieldValue());
        map.put("displayNameValue", paramModel.getDisplayNameValue());
        map.put("extClass", paramModel.getExtClass());
        return "repository/form/ui/Sys_Resonsitory_Form_UI_Combox";
    }

    private FormUIComponentComboxParamsModel buildFormUIComponentTextParamsModel(String params) {
        ObjectMapper mapper = new ObjectMapper();
        FormUIComponentComboxParamsModel paramModel = null;
        if (params != null && params.trim().length() > 0) {
            try {
                paramModel = mapper.readValue(params, FormUIComponentComboxParamsModel.class);
            } catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        paramModel = paramModel == null ? new FormUIComponentComboxParamsModel() : paramModel;
        return paramModel;
    }
}

class FormUIComponentComboxParamsModel {
    private String extendedCode = "";
    private String sql = "";
    private String getFieldValue = "";
    private String displayNameValue = "";
    private String type;
    private String normalValue;
    private String extClass;

    public String getExtendedCode() {
        return extendedCode;
    }

    public void setExtendedCode(String extendedCode) {
        this.extendedCode = extendedCode;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getGetFieldValue() {
        return getFieldValue;
    }

    public void setGetFieldValue(String getFieldValue) {
        this.getFieldValue = getFieldValue;
    }

    public String getDisplayNameValue() {
        return displayNameValue;
    }

    public String getExtClass() {
		return extClass;
	}

	public void setExtClass(String extClass) {
		this.extClass = extClass;
	}

	public void setDisplayNameValue(String displayNameValue) {
        this.displayNameValue = displayNameValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNormalValue() {
        return normalValue;
    }

    public void setNormalValue(String normalValue) {
        this.normalValue = normalValue;
    }

}
