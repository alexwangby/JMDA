package com.jmda.platform.engine.worklist.component;

import java.util.Map;

public interface JmdaComponentSelecterExtendInterface {
	Map<String,Object> getContextParams(String param);
}
