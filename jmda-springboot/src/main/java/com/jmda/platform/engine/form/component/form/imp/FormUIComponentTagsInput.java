package com.jmda.platform.engine.form.component.form.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service("UI_FORM_tagsInput")
public class FormUIComponentTagsInput extends FormUIComponentAbst {

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		FormUIComponentTagsInputParamsModel paramModel = buildFormUIComponentTagsInputParamsModel(params);
		StringBuffer verify_text = new StringBuffer();
		if (!super.getFormMetadataMapModel().isIs_null()) {
			verify_text.append("data-options=required:true");
			verify_text.append(",validType:[\"length[").append(paramModel.getMinLength()).append(",");
			verify_text.append(paramModel.getMaxLength()).append("]\"]");
			if(paramModel.getEntryWidth()==0){
				paramModel.setEntryWidth(100);
			}
			verify_text.append(" style='width:"+paramModel.getEntryWidth()+"%'");
		}else{
			verify_text.append("data-options=validType:[\"length[").append(paramModel.getMinLength()).append(",");
			verify_text.append(paramModel.getMaxLength()).append("]\"]");
			if(paramModel.getEntryWidth()==0){
				paramModel.setEntryWidth(100);
			}
			verify_text.append(" style='width:"+paramModel.getEntryWidth()+"%'");
		}
		Object value =  super.getValue();
		if(value ==null){
			value = "";
		}
		String html2="<input type='text' class='form-control easyui-validatebox' "+verify_text+" "+ (paramModel.getExtendedCode().trim().length() == 0 ? "" : paramModel.getExtendedCode()) + " name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value='" + value+ "'>";
		html2 = html2+"<\r>" +"<script>$('input[name=\""+super.getFormMetadataMapModel().getColumn_lable()+"\"]').tagsInput();</script>";
		return html2;
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentTagsInputParamsModel paramModel = buildFormUIComponentTagsInputParamsModel(params);
		map.put("minLength", paramModel.getMinLength());
		map.put("maxLength", paramModel.getMaxLength());
		map.put("extendedCode", paramModel.getExtendedCode());
		return "repository/form/ui/Sys_Resonsitory_Form_UI_TagsInput";
	}

	private FormUIComponentTagsInputParamsModel buildFormUIComponentTagsInputParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentTagsInputParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentTagsInputParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentTagsInputParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentTagsInputParamsModel {
	private int entryWidth;
	private String lengthType = "%";
	private String extendedCode = "";
    private int minLength = 0;
    private int maxLength =32;
    
    public String getLengthType() {
    	return lengthType;
    }
    
    public void setLengthType(String lengthType) {
    	this.lengthType = lengthType;
    }
	public int getMinLength() {
		return minLength;
	}

	public int getEntryWidth() {
		return entryWidth;
	}

	public void setEntryWidth(int entryWidth) {
		this.entryWidth = entryWidth;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}
	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

}
