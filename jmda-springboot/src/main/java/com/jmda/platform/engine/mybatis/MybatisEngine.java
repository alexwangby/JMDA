package com.jmda.platform.engine.mybatis;

import com.github.pagehelper.PageInfo;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.database.DataSource;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MybatisEngine extends CommonDao{
	public MybatisEngine(){
        
	}
	public <E> DataPager<E> selectList(String nameSpace, String id, Map<String, Object> params, int limit, int start) {
		DataSource dataSource = LocalCacheManager.setReadDataSource();
		List<E> results = super.getSqlSession().selectList(nameSpace + "." + id, params, new RowBounds(start, limit));
		PageInfo pageInfo = new PageInfo(results);
		DataPager<E> dataPage=new DataPager<E>();
		dataPage.setRows(results);
		dataPage.setTotal(pageInfo.getTotal());
		
        if (dataSource!=null) {
        	dataSource.setDefaultDB();
        }
       
        return dataPage;
	}
	public <E> DataPager<E> selectAll(String nameSpace, String id, Map<String, Object> params) {
		DataSource dataSource = LocalCacheManager.setReadDataSource();
		List<E> results = super.getSqlSession().selectList(nameSpace + "." + id, params);
		PageInfo pageInfo = new PageInfo(results);
		DataPager<E> dataPage=new DataPager<E>();
		dataPage.setRows(results);
		dataPage.setTotal(pageInfo.getTotal());
		
        if (dataSource!=null) {
        	dataSource.setDefaultDB();
        }
       
        return dataPage;
	}

	public int getListCount(String nameSpace, String id, Map<String, Object> params) {
		DataSource dataSource = LocalCacheManager.setReadDataSource();
		int count = super.getSqlSession().selectOne(nameSpace + "." + id, params);
        if (dataSource!=null) {
        	dataSource.setDefaultDB();
        }	
		return count;
	}

	public List<Map<String, Object>> selectList(String nameSpace, String id, Map<String, Object> params) {
		DataSource dataSource = LocalCacheManager.setReadDataSource();
		List<Map<String, Object>> list = super.getSqlSession().selectList(nameSpace + "." + id, params);
        if (dataSource!=null) {
        	dataSource.setDefaultDB();
        }	
		return list;
	}
	public List<Map<String, Object>> selectList(String nameSpace, Map<String, Object> params) {
		DataSource dataSource = LocalCacheManager.setReadDataSource();
		List<Map<String, Object>> list = super.getSqlSession().selectList(nameSpace, params);
        if (dataSource!=null) {
        	dataSource.setDefaultDB();
        }	
		return list;
	}

	public Map<String, Object> selectOne(String nameSpace, String id, Map<String, Object> params) {
		
		DataSource dataSource = LocalCacheManager.setReadDataSource();
		Map<String, Object> map = super.getSqlSession().selectOne(nameSpace + "." + id, params);
        if (dataSource!=null) {
        	dataSource.setDefaultDB();
        }	
		return map;
	}

	public String getMapperSql(String nameSpace, String id) {
		DataSource dataSource = LocalCacheManager.setReadDataSource();
		MappedStatement mapper = super.getSqlSession().getConfiguration().getMappedStatement(nameSpace + "." + id);
        if (dataSource!=null) {
        	dataSource.setDefaultDB();
        }	
		if (mapper != null) {
			BoundSql boundSql = mapper.getBoundSql(new HashMap<String, Object>());
			if (boundSql != null) {
				return boundSql.getSql();
			}

		}
		return "";
	}
	/*public <E> DataPager<E> selectList(String nameSpace, String id, Map<String, Object> params, int limit, int start) {
		DataSourceSupport dataSourceSupport = SpringContextUtil.getBean(DataSourceSupport.class);
		return dataSourceSupport.selectList(nameSpace, id, params, limit, start);
	}

	public int getListCount(String nameSpace, String id, Map<String, Object> params) {
		DataSourceSupport dataSourceSupport = SpringContextUtil.getBean(DataSourceSupport.class);
		return dataSourceSupport.getListCount(nameSpace,id,params);
	}

	public List<Map<String, Object>> selectList(String nameSpace, String id, Map<String, Object> params) {
		DataSourceSupport dataSourceSupport = SpringContextUtil.getBean(DataSourceSupport.class);
		return dataSourceSupport.selectList(nameSpace,id,params);
	}
	public List<Map<String, Object>> selectList(String nameSpace, Map<String, Object> params) {
		DataSourceSupport dataSourceSupport = SpringContextUtil.getBean(DataSourceSupport.class);
		return dataSourceSupport.selectList(nameSpace, params);
	}

	public Map<String, Object> selectOne(String nameSpace, String id, Map<String, Object> params) {
		DataSourceSupport dataSourceSupport = SpringContextUtil.getBean(DataSourceSupport.class);
		return dataSourceSupport.selectOne(nameSpace,id,params);
	}

	public String getMapperSql(String nameSpace, String id) {
		DataSourceSupport dataSourceSupport = SpringContextUtil.getBean(DataSourceSupport.class);
		return dataSourceSupport.getMapperSql(nameSpace,id);
	}*/

}
