package com.jmda.platform.engine.worklist.component.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.engine.worklist.component.WorkListSearchUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service("WORKLIST_UI_SEARCH_单行")
public class WorkListSearchUIComponentTextImp extends WorkListSearchUIComponentAbst {
	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		FormUIComponentTextParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		String html = "<input type=\"text\" class=\"form-control\" name=\""+super.getSearchModel().getId()+"\" id=\""+super.getSearchModel().getId()+"\" >";
		return html;
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentTextParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		map.put("extendedCode", paramModel.getExtendedCode());
		return "repository/worklist/searchUI/Sys_Resonsitory_Form_UI_Text";
	}

	private FormUIComponentTextParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentTextParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentTextParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentTextParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentTextParamsModel {
	//private int entryWidth = 160;
	//private String lengthType = "px";
	private String extendedCode = "";
	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

}
