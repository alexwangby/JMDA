package com.jmda.platform.engine.form.component.form.imp;


import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
@Service("UI_FORM_无")
public class FormUIComponentNullImp extends FormUIComponentAbst{

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		return super.getValue().toString();
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		return "repository/form/ui/Sys_Resonsitory_Form_UI_Null";
	}

}
