package com.jmda.platform.engine.worklist.component.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.commom.SpringContextUtil;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.engine.worklist.component.JmdaComponentSelecterExtendInterface;
import com.jmda.platform.engine.worklist.component.WorkListSearchUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Service("WORKLIST_UI_SEARCH_单选按钮")
public class WorkListSearchUIComponentRadioImp extends WorkListSearchUIComponentAbst {

	@Resource
	DataBaseService db;

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		StringBuffer html = new StringBuffer();
		FormUIComponentRadioParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		//html.append("<select " + (paramModel.getExtendedCode().trim().length() > 0 ? "" : paramModel.getExtendedCode()) + " class='form-control' name=\"" + super.getSearchModel().getId() + "\" >");
		//html.append("<option value='' selected=\"selected\">请选择</option>");
		if(paramModel.getExtClass()!=null){
			//下拉框的外部扩展类，实现JmdaComponentSelecterExtendInterface接口的外部类，可以把外部逻辑数据注入到jmda的下拉框组件中
			String className = paramModel.getExtClass().trim().contains("_")?paramModel.getExtClass().trim().split("_")[0]:paramModel.getExtClass().trim();
			String classParam = paramModel.getExtClass().trim().contains("_")?paramModel.getExtClass().trim().split("_")[1]:"";
			JmdaComponentSelecterExtendInterface event = SpringContextUtil.getBean(className);
			
			if (event != null) {
				Map<String, Object> extendParams = event.getContextParams(classParam);
				for (String key : extendParams.keySet()) {
					//html.append(" <option  value=\"" + key + "\">" + (String) extendParams.get(key) + "</option>");
					html.append("<input  name = \"" + super.getSearchModel().getId() + "\"   type=\"radio\" value=\"" + key + "\"/>" +"<label style=\"margin-top:-10px;\">"+ (String) extendParams.get(key) + "</label>&nbsp;&nbsp;");
				}
			}
			
		}else if (paramModel.getSql().trim().length() > 0 && paramModel.getDisplayNameValue().trim().length() > 0 && paramModel.getGetFieldValue().trim().length() > 0) {
			try {
				List<Map<String, Object>> list = db.queryList(paramModel.getSql(), null);
				
				if (list != null) {
					for (Map<String, Object> map : list) {
						String text = String.valueOf(map.get(paramModel.getDisplayNameValue()));
						String value = String.valueOf(map.get(paramModel.getGetFieldValue()));
						if (value != null && text != null) {
							html.append("<input  name = \"" + super.getSearchModel().getId() + "\"   type=\"radio\" value=\"" + value + "\"/>" +"<label style=\"margin-top:-10px;\">"+ text + "</label>&nbsp;&nbsp;");
						}
					}
				}
			} catch (SQLException e) {
			}
		}else if (paramModel.getSql()!=null&& paramModel.getDisplayNameValue().trim().length() > 0 && paramModel.getGetFieldValue().trim().length() > 0) {
			String[] displayNames = paramModel.getDisplayNameValue().trim().split(";");
			String[] fieldValues = paramModel.getGetFieldValue().trim().split(";");
			
			for (int i = 0; i < displayNames.length; i++) {
				String value = fieldValues.length - 1 >= i ? fieldValues[i]:"";
				html.append("<input  name = \"" + super.getSearchModel().getId() + "\"   type=\"radio\" value=\"" + value + "\"/>" +"<label style=\"margin-top:-10px;\">"+ displayNames[i] + "</label>&nbsp;&nbsp;");
			}
		}
		return html.toString();
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentRadioParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		//map.put("entryWidth", paramModel.getEntryWidth());
		//map.put("lengthType", paramModel.getLengthType());
		map.put("extendedCode", paramModel.getExtendedCode());
		map.put("sql", paramModel.getSql());
		map.put("getFieldValue", paramModel.getGetFieldValue());
		map.put("displayNameValue", paramModel.getDisplayNameValue());
		return "repository/worklist/searchUI/Sys_Resonsitory_Form_UI_Radio";
	}

	private FormUIComponentRadioParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentRadioParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentRadioParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentRadioParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentRadioParamsModel {
	//private int entryWidth = 160;
	//private String lengthType = "px";
	private String extendedCode = "";
	private String sql = "";
	private String getFieldValue = "";
	private String displayNameValue = "";
	private String extClass = "";

	public String getExtClass() {
		return extClass;
	}

	public void setExtClass(String extClass) {
		this.extClass = extClass;
	}

	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getGetFieldValue() {
		return getFieldValue;
	}

	public void setGetFieldValue(String getFieldValue) {
		this.getFieldValue = getFieldValue;
	}

	public String getDisplayNameValue() {
		return displayNameValue;
	}

	public void setDisplayNameValue(String displayNameValue) {
		this.displayNameValue = displayNameValue;
	}

}
