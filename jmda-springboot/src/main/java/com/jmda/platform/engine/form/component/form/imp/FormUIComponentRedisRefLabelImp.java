package com.jmda.platform.engine.form.component.form.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service("UI_FORM_REDIS关联显示")
public class FormUIComponentRedisRefLabelImp extends FormUIComponentAbst {

	@Resource
	DataBaseService db;

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		FormUIComponentRedisRefLabelParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		String text = "";
			if(getValue()!=null&&paramModel.getRef_params()!=null&&paramModel.getRef_params().trim().length()>0){
				text=LocalCacheManager.referenceData("redis",String.valueOf(getValue()), paramModel.getRef_params());
			}
		StringBuffer html = new StringBuffer();
		html.append("<span id='"+super.getFormMetadataMapModel().getColumn_lable()+"Span'>");
		html.append(text);
		html.append("</span>");
		Object value =  super.getValue();
		if(value ==null){
			value = "";
		}
		//html.append("<input type='text' disabled class='form-control' placeholder='Phone'  name=\"text_" + super.getFormMetadataMapModel().getColumn_lable() + "\" value='" + text + "'>");
		html.append("<input  type='hidden' id=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\"  value='" + value + "'>");
		return html.toString();
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentRedisRefLabelParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		map.put("ref_type", paramModel.getRef_type());
		map.put("ref_params", paramModel.getRef_params());
		return "repository/form/ui/Sys_Resonsitory_Form_UI_RefLabel_Redis";
	}

	private FormUIComponentRedisRefLabelParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentRedisRefLabelParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentRedisRefLabelParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentRedisRefLabelParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentRedisRefLabelParamsModel {
	private String ref_type;
	private String ref_params;

	public String getRef_type() {
		return ref_type;
	}

	public void setRef_type(String ref_type) {
		this.ref_type = ref_type;
	}

	public String getRef_params() {
		return ref_params;
	}

	public void setRef_params(String ref_params) {
		this.ref_params = ref_params;
	}

}
