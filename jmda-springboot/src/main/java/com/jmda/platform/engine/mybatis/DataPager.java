package com.jmda.platform.engine.mybatis;

import java.util.List;

public class DataPager<T> {
	private List<T> rows;
	private long total = 0;
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	
}
