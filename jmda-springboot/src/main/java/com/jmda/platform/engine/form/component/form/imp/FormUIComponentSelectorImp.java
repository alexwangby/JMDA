package com.jmda.platform.engine.form.component.form.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

@Service("UI_FORM_数据选择器")
public class FormUIComponentSelectorImp extends FormUIComponentAbst {
	@Resource
	DataBaseService db;
	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		FormUIComponentSelectorParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		String isNotNull = "";
		if (!super.getFormMetadataMapModel().isIs_null()) {
			isNotNull = "data-options=required:true";
		}
		Object value =  super.getValue();
		if(value ==null){
			value = "";
		}
		String viewName="";
		if(value!=null&&paramModel.getRef_params()!=null&&paramModel.getRef_params().trim().length()>0){
			if(paramModel.getRef_params().startsWith("sql:")){
				try {
					String[] sqlObj =paramModel.getRef_params().substring(4, paramModel.getRef_params().length()).split("\\|");
					String sql  = sqlObj[1].trim().replace("#{value}", "?");
					viewName = db.getValue(sql, sqlObj[0], new Object[] { super.getValue() }).toString();
				} catch (SQLException e) {
				}
			}else{
				viewName=LocalCacheManager.referenceData("redis",String.valueOf(value), paramModel.getRef_params());
			}
		}else{
			viewName=String.valueOf(value);
		}
		String html2 = "<input type='text'  class='form-control easyui-validatebox' " + isNotNull + " " + (paramModel.getExtendedCode().trim().length() == 0 ? "" : paramModel.getExtendedCode()) + " name=\"view_" + super.getFormMetadataMapModel().getColumn_lable() + "\" value='" + viewName + "' hbdiyType='selector' selectorName ='"+paramModel.getSelectorId()+"' backFunction='"+paramModel.getCallBackFunction()+"'>";
		html2 = html2 + "<i class=\"glyphicon glyphicon-search \" style=\"cursor: pointer;margin-left:-16px;\" onClick=\"openFormSelector('"+paramModel.getSelectorId()+"',"+paramModel.getCallBackFunction()+",'"+paramModel.getTitle()+"',"+paramModel.getWidth()+","+paramModel.getHeight()+",'"+paramModel.getUrlParams()+"')\"></i>";
		html2=html2+"<input type='hidden' name='"+super.getFormMetadataMapModel().getColumn_lable()+"' value='"+value+"'>";
		return html2;
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentSelectorParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		map.put("selectorId", paramModel.getSelectorId());
		map.put("callBackFunction", paramModel.getCallBackFunction());
		map.put("title", paramModel.getTitle());
		map.put("width", paramModel.getWidth());
		map.put("height", paramModel.getHeight());
		map.put("urlParams", paramModel.getUrlParams());
		map.put("ref_params", paramModel.getRef_params());
		map.put("extendedCode", paramModel.getExtendedCode());
		return "repository/form/ui/Sys_Resonsitory_Form_UI_Selector";
	}

	private FormUIComponentSelectorParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentSelectorParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentSelectorParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentSelectorParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentSelectorParamsModel {

	private String extendedCode = "";
	private String selectorId = "";
	private String callBackFunction = "";
	private String title = "";
	private int width = 700;
	private int height = 400;
	private String urlParams = "";
	private String ref_params;
	private String refType;
	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

	public String getSelectorId() {
		return selectorId;
	}

	public void setSelectorId(String selectorId) {
		this.selectorId = selectorId;
	}

	public String getCallBackFunction() {
		return callBackFunction;
	}

	public void setCallBackFunction(String callBackFunction) {
		this.callBackFunction = callBackFunction;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getUrlParams() {
		return urlParams;
	}

	public void setUrlParams(String urlParams) {
		this.urlParams = urlParams;
	}

	public String getRef_params() {
		return ref_params;
	}

	public void setRef_params(String ref_params) {
		this.ref_params = ref_params;
	}

	public String getRefType() {
		return refType;
	}

	public void setRefType(String refType) {
		this.refType = refType;
	}

}
