package com.jmda.platform.engine.form.component.form.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service("UI_FORM_多行")
public class FormUIComponentTextAreaImp extends FormUIComponentAbst {

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		FormUIComponentTextAreaParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		StringBuffer verify_text = new StringBuffer();
		if (!super.getFormMetadataMapModel().isIs_null()) {
			verify_text.append("data-options=required:true");
			verify_text.append(",validType:[\"length[").append(paramModel.getMinLength()).append(",");
			verify_text.append(paramModel.getMaxLength()).append("]\"]");
		}else{
			verify_text.append("data-options=validType:[\"length[").append(paramModel.getMinLength()).append(",");
			verify_text.append(paramModel.getMaxLength()).append("]\"]");
			
		}
		Object value =  super.getValue();
		if(value ==null){
			value = "";
		}
		String html = "<textarea  class='form-control easyui-validatebox' "+verify_text+" " + (paramModel.getExtendedCode().trim().length() == 0 ? "" : paramModel.getExtendedCode()) + " rows=\""+paramModel.getLineSize()+"\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" class='form-control' >" + value
				+ "</textarea>";
		return html;
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentTextAreaParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		map.put("minLength", paramModel.getMinLength());
		map.put("maxLength", paramModel.getMaxLength());
		map.put("extendedCode", paramModel.getExtendedCode());
		map.put("lineSize", paramModel.getLineSize());
		return "repository/form/ui/Sys_Resonsitory_Form_UI_TextArea";
	}

	private FormUIComponentTextAreaParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentTextAreaParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentTextAreaParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentTextAreaParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentTextAreaParamsModel {
	//private int entryWidth = 160;
	//private String lengthType = "px";
	private String extendedCode = "";
	private int lineSize = 3;
    private int minLength = 0;
    private int maxLength =12;

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

	public int getLineSize() {
		return lineSize;
	}

	public void setLineSize(int lineSize) {
		this.lineSize = lineSize;
	}

}
