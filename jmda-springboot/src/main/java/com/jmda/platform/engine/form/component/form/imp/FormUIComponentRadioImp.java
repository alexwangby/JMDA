package com.jmda.platform.engine.form.component.form.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Service("UI_FORM_单选按钮")
public class FormUIComponentRadioImp extends FormUIComponentAbst {

	@Resource
	DataBaseService db;


	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		StringBuffer html = new StringBuffer();
		FormUIComponentRadioParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		if(paramModel.getType()!=null&&paramModel.getType().equals("sql")){
			if (paramModel.getSql().trim().length() > 0 && paramModel.getDisplayNameValue().trim().length() > 0 && paramModel.getGetFieldValue().trim().length() > 0) {
				try {
					List<Map<String, Object>> list = db.queryList(paramModel.getSql(), null);
					if (list != null) {
						for (Map<String, Object> map : list) {
							String text = String.valueOf(map.get(paramModel.getDisplayNameValue()));
							String value = String.valueOf(map.get(paramModel.getGetFieldValue()));
							if (value != null && text != null) {
								if (super.getValue()!=null&&super.getValue().toString().equals(value)) {
									html.append("<label>");
									html.append(" <input checked=\"true\" type=\"radio\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value=\"" + value + "\"/> <label style=\"margin-top: -6px\">"+text+"</label>");
									html.append("</label>&nbsp;&nbsp;&nbsp;&nbsp;");
								} else {
									html.append("<label>");
									html.append(" <input type=\"radio\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value=\"" + value + "\"/>  <label style=\"margin-top: -6px\">"+text+"</label>");
									html.append("</label>&nbsp;&nbsp;&nbsp;&nbsp;");
								}
							}
						}
					}
				} catch (SQLException e) {
				}
			}
		}else{
			if (paramModel.getNormalValue().indexOf("|") != -1) {
				String selects[] = paramModel.getNormalValue().split("\\|");
				String selectsview[];
				for (int i = 0; i < selects.length; i++) {
					if (selects[i].indexOf(":") != -1) {
						selectsview = selects[i].split(":");
						if (super.getValue()!=null&&super.getValue().toString().equals(selectsview[0])) {
							html.append("<label>");
							html.append(" <input checked=\"true\" type=\"radio\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value=\"" + selectsview[0] + "\"/> <label style=\"margin-top: -6px\">" + selectsview[1] + "</label>");
							html.append("</label>&nbsp;&nbsp;&nbsp;&nbsp;");
						} else {
							html.append("<label>");
							html.append(" <input type=\"radio\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value=\"" + selectsview[0] + "\"/> <label style=\"margin-top: -6px\">" + selectsview[1] + "</label>");
							html.append("</label>&nbsp;&nbsp;&nbsp;&nbsp;");
						}
					} else {
						if (super.getValue()!=null&&super.getValue().equals(selects[i])) {
							html.append("<label>");
							html.append(" <input checked=\"true\" type=\"radio\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value=\"" + selects[i] + "\"/> <label style=\"margin-top: -6px\">" + selects[i] + "</label>");
							html.append("</label>&nbsp;&nbsp;&nbsp;&nbsp;");
						} else {
							html.append("<label>");
							html.append(" <input type=\"radio\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value=\"" + selects[i] + "\"/> <label style=\"margin-top: -6px\">" + selects[i] + "</label>");
							html.append("</label>&nbsp;&nbsp;&nbsp;&nbsp;");
						}
					}
				}
			} else {
				if (super.getValue()!=null&&super.getValue().equals(paramModel.getNormalValue())) {
					html.append("<label>");
					html.append(" <input checked=\"true\" type=\"radio\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value=\"" + paramModel.getNormalValue() + "\"/> <label style=\"margin-top: -6px\">" + paramModel.getNormalValue() + "</label>");
					html.append("</label>&nbsp;&nbsp;&nbsp;&nbsp;");
				} else {
					html.append("<label>");
					html.append(" <input type=\"radio\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value=\"" + paramModel.getNormalValue() + "\"/> <label style=\"margin-top: -6px\">" + paramModel.getNormalValue() + "</label>");
					html.append("</label>&nbsp;&nbsp;&nbsp;&nbsp;");
					
				}

			}
		}
		return html.toString();
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentRadioParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		map.put("type", paramModel.getType());
		map.put("normalValue", paramModel.getNormalValue());
		map.put("extendedCode", paramModel.getExtendedCode());
		map.put("sql", paramModel.getSql());
		map.put("getFieldValue", paramModel.getGetFieldValue());
		map.put("displayNameValue", paramModel.getDisplayNameValue());
		return "repository/form/ui/Sys_Resonsitory_Form_UI_Radio";
	}

	private FormUIComponentRadioParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentRadioParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentRadioParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentRadioParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentRadioParamsModel {
	private String extendedCode = "";
	private String sql = "";
	private String getFieldValue = "";
	private String displayNameValue = "";
	private String type ;
	private String normalValue;

	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getGetFieldValue() {
		return getFieldValue;
	}

	public void setGetFieldValue(String getFieldValue) {
		this.getFieldValue = getFieldValue;
	}

	public String getDisplayNameValue() {
		return displayNameValue;
	}

	public void setDisplayNameValue(String displayNameValue) {
		this.displayNameValue = displayNameValue;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNormalValue() {
		return normalValue;
	}

	public void setNormalValue(String normalValue) {
		this.normalValue = normalValue;
	}

}
