package com.jmda.platform.engine.form.component.form.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service("UI_FORM_数值")
public class FormUIComponentNumberImp extends FormUIComponentAbst {

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		FormUIComponentNumberParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		StringBuffer verify_text = new StringBuffer();
		if (!super.getFormMetadataMapModel().isIs_null()) {
			verify_text.append("data-options=\"required:true");
			if(paramModel.getMaxLength()!=0){
				verify_text.append(",min:").append(paramModel.getMinLength());
				verify_text.append(",max:");
				verify_text.append(paramModel.getMaxLength());
			}
            if(paramModel.getDecimal_point()!=0){
            	verify_text.append(",precision:").append(paramModel.getDecimal_point());
            }
			verify_text.append("\"");
		}else{
			verify_text.append("data-options=\"");
			if(paramModel.getMaxLength()!=0){
				verify_text.append("min:").append(paramModel.getMinLength());
				verify_text.append(",max:");
				verify_text.append(paramModel.getMaxLength());
				if(paramModel.getDecimal_point()!=0){
					verify_text.append(",precision:").append(paramModel.getDecimal_point());
				}
				verify_text.append("\"");
			}else{
				if(paramModel.getDecimal_point()!=0){
					verify_text.append("precision:").append(paramModel.getDecimal_point());
					verify_text.append("\"");
				}

			}
			
			
		}
		Object value =  super.getValue();
		if(value ==null){
			value = "";
		}
		String html2="<input type='text' class='form-control easyui-validatebox easyui-numberbox' "+verify_text+" "+ (paramModel.getExtendedCode().trim().length() == 0 ? "" : paramModel.getExtendedCode()) + " name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" value='" + value+ "'>";
		return html2;
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentNumberParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		map.put("minLength", paramModel.getMinLength());
		map.put("maxLength", paramModel.getMaxLength());
		map.put("decimal_point", paramModel.getDecimal_point());
		map.put("extendedCode", paramModel.getExtendedCode());
		return "repository/form/ui/Sys_Resonsitory_Grid_UI_Number";
	}

	private FormUIComponentNumberParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentNumberParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentNumberParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentNumberParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentNumberParamsModel {
	//private int entryWidth = 160;
	//private String lengthType = "px";
	private String extendedCode = "";
    private int minLength = 0;
    private int maxLength =99999999;
    private int decimal_point = 0;
    
   // private String thousandth ;
    
  //  private String delimiter="." ;
	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getDecimal_point() {
		return decimal_point;
	}

	public void setDecimal_point(int decimal_point) {
		this.decimal_point = decimal_point;
	}

}
