package com.jmda.platform.engine;

public class EngineException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1087336794068046400L;

	public EngineException(String message, Throwable cause) {
		super(message, cause);
	}

	public EngineException(Throwable cause) {
		super(cause);
	}

	public EngineException(String message) {
		super(message);
	}
}
