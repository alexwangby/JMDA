package com.jmda.platform.engine.form.component.form;

import com.jmda.platform.repository.form.model.FormMetadataMapModel;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;

public interface FormUIComponentInterface {
	/**
	 * 获取当前组件的值
	 * 
	 * @return .
	 */
	public abstract Object getValue();

	/**
	 * 设置当前组件的值
	 * 
	 * @param value
	 *            .
	 */
	public abstract void setValue(Object value);


	/**
	 * 获取当前组件值的客户端JavaScript代码
	 * 
	 * @param params
	 * @return .
	 */

	/**
	 * 获取编辑状态下的组件HTML代码,该方法应该被重载
	 * 
	 * @param params
	 *            额外的参数列表，可以为NULL
	 * @return
	 */
	public abstract String getHtmlDefine(String params);

	/**
	 * 获取当前字段模型的编辑界面
	 * 
	 * @return .
	 */
	public abstract String getParamSetingWeb(String params,HttpServletRequest request, ModelMap map);
	
	public FormMetadataMapModel getFormMetadataMapModel() ;
	public void setFormMetadataMapModel(FormMetadataMapModel formMetadataMapModel);
}
