package com.jmda.platform.engine.form.component.form.imp;


import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
@Service("UI_FORM_Label")
public class FormUIComponentLabelImp extends FormUIComponentAbst{

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		StringBuffer json = new StringBuffer();
		Object value =  super.getValue();
		if(value ==null){
			value = "";
		}
		json.append("<span id='"+super.getFormMetadataMapModel().getColumn_lable()+"_Span'>"+value+"</span>");
		json.append("<input type=\"hidden\" name=\"").append(super.getFormMetadataMapModel().getColumn_lable()).append("\" id=\"").append(super.getFormMetadataMapModel().getColumn_lable()).append("\" value='").append(value).append("' />");
		return json.toString();
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		return "repository/form/ui/Sys_Resonsitory_Form_UI_Null";
	}

}
