package com.jmda.platform.engine.form.component.form.imp;

import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;


@Service("UI_FORM_隐藏域")
public class FormUIComponentHiddenImp extends FormUIComponentAbst {

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		Object value =  super.getValue();
		if(value ==null){
			value = "";
		}
		String column = super.getFormMetadataMapModel().getColumn_lable();
		String html ="<input type=\"hidden\" name=\""+column+"\" id=\""+column+"\" value=\"" + value+ "\" />";
		return html;
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		return "repository/form/ui/Sys_Resonsitory_Form_UI_Null";
	}
}

