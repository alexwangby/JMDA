package com.jmda.platform.engine.form.component.form.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

@Service("UI_FORM_SQL关联显示")
public class FormUIComponentSQLRefLabelImp extends FormUIComponentAbst {

	@Resource
	DataBaseService db;

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		FormUIComponentRefLabelParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		String text = "";
		if (paramModel.getSql().trim().length() > 0 && paramModel.getDisplayNameValue().trim().length() > 0) {
			try {
				String sql = paramModel.getSql().trim().replace("#{value}", "?");
				text = db.getValue(sql, paramModel.getDisplayNameValue(), new Object[] { super.getValue() }).toString();
			} catch (SQLException e) {
			}
		}
		StringBuffer html = new StringBuffer();
		html.append("<span id='"+super.getFormMetadataMapModel().getColumn_lable()+"Span'>");
		html.append(text);
		html.append("</span>");
		Object value =  super.getValue();
		if(value ==null){
			value = "";
		}
		//html.append("<input type='text' disabled class='form-control' placeholder='Phone'  name=\"text_" + super.getFormMetadataMapModel().getColumn_lable() + "\" value='" + text + "'>");
		html.append("<input  type='hidden' id=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\"  value='" + value + "'>");
		return html.toString();
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentRefLabelParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		map.put("sql", paramModel.getSql());
		map.put("displayNameValue", paramModel.getDisplayNameValue());
		return "repository/form/ui/Sys_Resonsitory_Form_UI_RefLabel_SQL";
	}

	private FormUIComponentRefLabelParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentRefLabelParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentRefLabelParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentRefLabelParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentRefLabelParamsModel {
	private String sql = "";
	private String displayNameValue = "";

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getDisplayNameValue() {
		return displayNameValue;
	}

	public void setDisplayNameValue(String displayNameValue) {
		this.displayNameValue = displayNameValue;
	}

}
