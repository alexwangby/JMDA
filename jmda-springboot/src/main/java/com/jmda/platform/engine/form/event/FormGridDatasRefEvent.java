package com.jmda.platform.engine.form.event;


/**
 * Created by chens on 2016/6/23.
 */
public interface FormGridDatasRefEvent {
	/**
	 * 返回设置显示名字
	 * @return
	 */
    public String returnName(); 
    /**
     * 返回映射值
     * @param value
     * @param param
     * @return
     */
    public String returnValue(String value,String param); 
}
