package com.jmda.platform.engine.worklist.component.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.engine.worklist.component.WorkListSearchUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service("WORKLIST_UI_SEARCH_日期")
public class WorkListSearchComponentDateImp extends WorkListSearchUIComponentAbst {
	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		FormUIComponentDataParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		String html = "<input type=\"text\" class=\"form-control easyui-datebox\" name=\""+super.getSearchModel().getId()+"\" id=\""+super.getSearchModel().getId()+"\" >";
		return html;
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentDataParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		//map.put("entryWidth", paramModel.getEntryWidth());
		//map.put("lengthType", paramModel.getLengthType());
		map.put("extendedCode", paramModel.getExtendedCode());
		return "repository/worklist/searchUI/Sys_Resonsitory_Form_UI_Date";
	}

	private FormUIComponentDataParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentDataParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentDataParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentDataParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentDataParamsModel {
	//private int entryWidth = 160;
	//private String lengthType = "px";
	private String extendedCode = "";

	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

}
