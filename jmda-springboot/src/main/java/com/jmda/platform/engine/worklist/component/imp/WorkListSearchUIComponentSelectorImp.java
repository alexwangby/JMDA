package com.jmda.platform.engine.worklist.component.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.engine.worklist.component.WorkListSearchUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service("WORKLIST_UI_SEARCH_数据选择器")
public class WorkListSearchUIComponentSelectorImp extends WorkListSearchUIComponentAbst {

	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		FormUIComponentSelectorParamsModel paramModel = buildFormUIComponentTextParamsModel(params);

		String html2 = "<input type='text' disabled class='form-control'  " + (paramModel.getExtendedCode().trim().length() == 0 ? "" : paramModel.getExtendedCode()) + " name=\"view_" + super.getSearchModel().getId() + "\" id=\"view_"+super.getSearchModel().getId()+"\">";
		//
		html2 = html2 + "<i class=\"glyphicon glyphicon-search \" style=\"cursor: pointer;margin-left:-16px;\" onClick=\"openSearchSelector('"+paramModel.getSelectorId()+"',"+paramModel.getCallBackFunction()+",'"+paramModel.getTitle()+"',"+paramModel.getWidth()+","+paramModel.getHeight()+",'"+paramModel.getUrlParams()+"')\"></i>";
		html2 = html2+"<input type=\"hidden\" value=\"\" name=\""+super.getSearchModel().getId()+"\">";
		return html2;
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentSelectorParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		map.put("selectorId", paramModel.getSelectorId());
		map.put("callBackFunction", paramModel.getCallBackFunction());
		map.put("title", paramModel.getTitle());
		map.put("width", paramModel.getWidth());
		map.put("height", paramModel.getHeight());
		map.put("urlParams", paramModel.getUrlParams());
		map.put("extendedCode", paramModel.getExtendedCode());
		return "repository/worklist/searchUI/Sys_Resonsitory_Form_UI_Selector";
	}

	private FormUIComponentSelectorParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentSelectorParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentSelectorParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentSelectorParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentSelectorParamsModel {

	private String extendedCode = "";
	private String selectorId = "";
	private String callBackFunction = "";
	private String title = "";
	private int width = 700;
	private int height = 400;
	private String urlParams = "";

	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

	public String getSelectorId() {
		return selectorId;
	}

	public void setSelectorId(String selectorId) {
		this.selectorId = selectorId;
	}

	public String getCallBackFunction() {
		return callBackFunction;
	}

	public void setCallBackFunction(String callBackFunction) {
		this.callBackFunction = callBackFunction;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getUrlParams() {
		return urlParams;
	}

	public void setUrlParams(String urlParams) {
		this.urlParams = urlParams;
	}

}
