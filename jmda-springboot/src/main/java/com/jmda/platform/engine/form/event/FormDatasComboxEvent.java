package com.jmda.platform.engine.form.event;

import java.sql.Connection;

public interface FormDatasComboxEvent {
	/**
	 * 执行前方法
	 * @return
	 */
	 public String returnComboxDataSourceKey(); 
}
