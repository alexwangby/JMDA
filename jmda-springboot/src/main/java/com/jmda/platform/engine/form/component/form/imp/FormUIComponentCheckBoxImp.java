package com.jmda.platform.engine.form.component.form.imp;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.database.DataBaseService;
import com.jmda.platform.engine.form.component.form.FormUIComponentAbst;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Service("UI_FORM_复选框")
public class FormUIComponentCheckBoxImp extends FormUIComponentAbst {

	@Resource
	DataBaseService db;


	@Override
	public String getHtmlDefine(String params) {
		// TODO Auto-generated method stub
		StringBuffer html = new StringBuffer();
		FormUIComponentCheckBoxParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		String value_ = super.getValue()==null?"": super.getValue().toString();
		if(paramModel.getType()!=null&&paramModel.getType().equals("sql")){
			if (paramModel.getSql().trim().length() > 0 && paramModel.getDisplayNameValue().trim().length() > 0 && paramModel.getGetFieldValue().trim().length() > 0) {
				try {
					List<Map<String, Object>> list = db.queryList(paramModel.getSql(), null);
					if (list != null) {
						for (Map<String, Object> map : list) {
							String text = String.valueOf(map.get(paramModel.getDisplayNameValue()));
							String value = String.valueOf(map.get(paramModel.getGetFieldValue()));
							if (value != null && text != null) {
								if (super.getValue()!=null&&super.getValue().equals(value)) {
									html.append("<input "+paramModel.getExtendedCode()+" checked=\"checked\"  name = \"" + super.getFormMetadataMapModel().getColumn_lable() + "_\"   type=\"checkbox\" value=\"" + value + "\"/>" +"<label style=\"margin-top:-10px;\">"+ text + "</label>&nbsp;&nbsp;");
									
								} else {
									html.append(" <input "+paramModel.getExtendedCode()+" type=\"checkbox\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "_\" value=\"" + value + "\"/> <label style=\"margin-top:-10px;\">"+text+"</label>&nbsp;&nbsp;");
								}
							}
						}
					}
				} catch (SQLException e) {
				}
			}

			html.append("<input type=\"hidden\" value=\""+value_+"\" name=\""+super.getFormMetadataMapModel().getColumn_lable()+"\"/>");
		}else{
			if (paramModel.getNormalValue().indexOf("|") != -1) {
				String selects[] = paramModel.getNormalValue().split("\\|");
				String selectsview[];
				for (int i = 0; i < selects.length; i++) {
					if (selects[i].indexOf(":") != -1) {
						selectsview = selects[i].split(":");
						if (super.getValue()!=null&&super.getValue().equals(selectsview[0])) {
							html.append(" <input checked=\"checked\" type=\"checkbox\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "_\" value=\"" + selectsview[i] + "\"/> <label style=\"margin-top:-10px;\">" + selectsview[i] + "</label>");
							html.append("&nbsp;&nbsp;");
						} else {
							html.append(" <input type=\"checkbox\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "_\" value=\"" + selectsview[i] + "\"/> <label style=\"margin-top:-10px;\">" + selectsview[i] + "</label>");
							html.append("&nbsp;&nbsp;");
						}
					} else {
						if (super.getValue()!=null&&super.getValue().equals(selects[i])) {
							html.append(" <input checked=\"checked\" type=\"checkbox\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "_\" value=\"" + selects[i] + "\"/> <label style=\"margin-top:-10px;\">" + selects[i] + "</label>");
							html.append("&nbsp;&nbsp;");
						} else {
							html.append(" <input type=\"checkbox\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "_\" value=\"" + selects[i] + "\"/> <label style=\"margin-top:-10px;\">" + selects[i] + "</label>");
							html.append("&nbsp;&nbsp;");
						}
					}
				}
				html.append("<input type=\"hidden\" value=\""+value_+"\" name=\""+super.getFormMetadataMapModel().getColumn_lable()+"\"/>");
			} else {
				if(paramModel.getNormalValue()!=null&&paramModel.getNormalValue().indexOf(":")>0){
					if(paramModel.getNormalValue().split(":").length>1){
						if (super.getValue()!=null&&super.getValue().equals(paramModel.getNormalValue())) {
							html.append(" <input checked=\"checkbox\" type=\"checkBox\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "_\" value=\"" + super.getValue() + "\"/> <label style=\"margin-top:-10px;\">" + paramModel.getNormalValue() + "</label>");
							html.append("&nbsp;&nbsp;");
						} else {
							html.append(" <input type=\"checkbox\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "_\" value=\"" + paramModel.getNormalValue() + "\"/> <label style=\"margin-top:-10px;\">" + paramModel.getNormalValue() + "</label>");
							html.append("&nbsp;&nbsp;");
							
						}
						html.append("<input type=\"hidden\" value=\""+value_+"\" name=\""+super.getFormMetadataMapModel().getColumn_lable()+"\"/>");
					}else{
						if (super.getValue()!=null&&super.getValue().toString().equals(paramModel.getNormalValue().split(":")[0])) {
							html.append(" <input checked=\"checkbox\" type=\"checkbox\" value=\""+value_+"\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" /> ");
							
						} else {
							html.append(" <input type=\"checkbox\" value=\""+value_+"\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "\" />");
							
						}
					}

				}else{
					if (super.getValue()!=null&&super.getValue().equals(paramModel.getNormalValue())) {
						html.append(" <input checked=\"checkbox\" type=\"checkBox\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "_\" value=\"" + paramModel.getNormalValue() + "\"/> <label style=\"margin-top:-10px;\">" + paramModel.getNormalValue() + "</label>");
						html.append("&nbsp;&nbsp;");
					} else {
						html.append(" <input type=\"checkbox\" name=\"" + super.getFormMetadataMapModel().getColumn_lable() + "_\" value=\"" + paramModel.getNormalValue() + "\"/> <label style=\"margin-top:-10px;\">" + paramModel.getNormalValue() + "</label>");
						html.append("&nbsp;&nbsp;");
						
					}
					html.append("<input type=\"hidden\" value=\""+value_+"\" name=\""+super.getFormMetadataMapModel().getColumn_lable()+"\"/>");
				}


			}
		}
		return html.toString();
	}

	@Override
	public String getParamSetingWeb(String params, HttpServletRequest request, ModelMap map) {
		// TODO Auto-generated method stub
		FormUIComponentCheckBoxParamsModel paramModel = buildFormUIComponentTextParamsModel(params);
		map.put("type", paramModel.getType());
		map.put("normalValue", paramModel.getNormalValue());
		map.put("extendedCode", paramModel.getExtendedCode());
		map.put("sql", paramModel.getSql());
		map.put("getFieldValue", paramModel.getGetFieldValue());
		map.put("displayNameValue", paramModel.getDisplayNameValue());
		return "repository/form/ui/Sys_Resonsitory_Form_UI_CheckBox";
	}

	private FormUIComponentCheckBoxParamsModel buildFormUIComponentTextParamsModel(String params) {
		ObjectMapper mapper = new ObjectMapper();
		FormUIComponentCheckBoxParamsModel paramModel = null;
		if (params != null && params.trim().length() > 0) {
			try {
				paramModel = mapper.readValue(params, FormUIComponentCheckBoxParamsModel.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		paramModel = paramModel == null ? new FormUIComponentCheckBoxParamsModel() : paramModel;
		return paramModel;
	}
}

class FormUIComponentCheckBoxParamsModel {
	private String extendedCode = "";
	private String sql = "";
	private String getFieldValue = "";
	private String displayNameValue = "";
	private String type ;
	private String normalValue;

	public String getExtendedCode() {
		return extendedCode;
	}

	public void setExtendedCode(String extendedCode) {
		this.extendedCode = extendedCode;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getGetFieldValue() {
		return getFieldValue;
	}

	public void setGetFieldValue(String getFieldValue) {
		this.getFieldValue = getFieldValue;
	}

	public String getDisplayNameValue() {
		return displayNameValue;
	}

	public void setDisplayNameValue(String displayNameValue) {
		this.displayNameValue = displayNameValue;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNormalValue() {
		return normalValue;
	}

	public void setNormalValue(String normalValue) {
		this.normalValue = normalValue;
	}

}
