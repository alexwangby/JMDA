package com.jmda.platform.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface DataBaseService {
	public static final String SUPPLY_SQLSERVER = "sqlserver";

	public static final String SUPPLY_DB2 = "db2";

	public static final String SUPPLY_MYSQL = "mysql";

	public static final String SUPPLY_ORACLE = "oracle";

	public Connection getConnection() throws SQLException;

	public int update(Connection conn, String sql, Object params[]) throws SQLException;

	public int update(String sql, Object params[]) throws SQLException;

	public <T> T query(Connection conn, String sql, Class<T> type, Object params[]) throws SQLException;

	public <T> T query(String sql, Class<T> type, Object params[]) throws SQLException;

	public <T> List<T> queryList(Connection conn, String sql, Class<T> type, Object params[]) throws SQLException;

	public <T> List<T> queryList(String sql, Class<T> type, Object params[]) throws SQLException;

	public DBCell getValue(String sql, String columnName, Object[] params) throws SQLException;

	public DBCell getValue(Connection conn, String sql, String columnName, Object[] params) throws SQLException;

	public String getSupply() throws SQLException;

	public String convertLongDate(String date) throws SQLException;

	public String convertShortDate(String date) throws SQLException;

	public Map<String, Object> query(String sql, Object[] params) throws SQLException;

	public List<Map<String, Object>> queryList(String sql, Object[] params) throws SQLException;

	public Map<String, Object> query(Connection conn, String sql, Object[] params) throws SQLException;

	public List<Map<String, Object>> queryList(Connection conn, String sql, Object[] params) throws SQLException;

	public String convertDateField(String fieldName);

	/**
	 * 分页查询语句
	 * 
	 * @param sql
	 *            sql语句
	 * @param start
	 *            开始页
	 * @param lineNumber
	 *            分页数
	 * @return 分页的sql语句
	 * @throws SQLException
	 */
	public String getViewSQL(String sql, int start, int lineNumber) throws SQLException;

	public String getViewSQL(Connection conn, String dbSupply, String statment, int start, int lineNumber);

	public void close(Connection conn);
}
