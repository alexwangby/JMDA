package com.jmda.platform.database;

public interface DataSource {
    public void setReadDataSource();
    public void setDefaultDB();
}
