package com.jmda.platform.database;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class DBCell {
	private Object obj = null;

	public void setObj(Object obj) {
		this.obj = obj;
	}

	@Override
	public String toString() {
		if (obj == null) {
			return "";
		}
		return String.valueOf(obj);
	}

	public double toDouble() {
		if (obj == null) {
			return 0;
		}
		if (obj instanceof BigDecimal) {
			BigDecimal value = (BigDecimal) obj;
			return value.doubleValue();

		}
		return Double.valueOf(toString());
	}

	public int toInt() {
		if (obj == null) {
			return 0;
		}
		if (obj instanceof BigDecimal) {
			BigDecimal value = (BigDecimal) obj;
			return value.intValue();
		}
		return Integer.valueOf(toString());
	}

	public BigDecimal toBigDecimal() {
		if (obj instanceof BigDecimal) {
			return (BigDecimal) obj;
		}
		return null;
	}

	public Timestamp toTimestamp() {
		if (obj == null) {
			return null;
		}
		if (obj instanceof Timestamp) {
			return (Timestamp) obj;
		}
		return null;
	}
}
