package com.jmda.platform.plugs.context;

import java.util.Map;

public interface JmdaContextInterface {
	Map<String,Object> getContextParams();
}
