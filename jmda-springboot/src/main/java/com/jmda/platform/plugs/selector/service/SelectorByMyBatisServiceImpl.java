package com.jmda.platform.plugs.selector.service;

import com.github.pagehelper.PageInfo;
import com.jmda.platform.engine.mybatis.CommonDao;
import com.jmda.platform.engine.mybatis.DataPager;
import com.jmda.platform.plugs.selector.model.SelectorPo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("selectorByMyBatisService")
public class SelectorByMyBatisServiceImpl extends CommonDao {

    public <E> DataPager<E> queryDatas(SelectorPo cfg, int page, int pagesize, Map<String, Object> params) {
        String search_nodeIds = (String) params.get("search_nodeIds");
        if (search_nodeIds != null && !"".equals(search_nodeIds.trim())) {
            params.remove("search_nodeIds");
            String[] strs = search_nodeIds.split(",");
            params.put("search_nodeIds", strs);
        }

        List<E> results = super.getSqlSession().selectList(cfg.getMapperid(), params, new RowBounds(page, pagesize));
        PageInfo pageInfo = new PageInfo(results);
        DataPager<E> dataPage = new DataPager<E>();
        dataPage.setRows(results);
        dataPage.setTotal(pageInfo.getTotal());
        return dataPage;
    }

    public int queryDatasTotal(SelectorPo cfg, Map<String, Object> params) {
        return getSqlSession().selectOne(cfg.getTotalMapperid(), params);
    }
}
