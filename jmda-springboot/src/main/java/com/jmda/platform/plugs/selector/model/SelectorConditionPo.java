package com.jmda.platform.plugs.selector.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <p>
 * <label>库名称:</label> <input type="text" id="search_name" maxlength="64" />
 * </p>
 *
 * @author Administrator
 */
@XStreamAlias("condition")
public class SelectorConditionPo {
    @XStreamAlias("title")
    private String title;
    @XStreamAlias("column")
    private String column;
    @XStreamAlias("ui")
    private String ui;
    @XStreamAlias("hidden")
    private String hidden;
    @XStreamAlias("type")
    private String type;
    @XStreamAlias("isFuzzy")
    private boolean isFuzzy;
    @XStreamAlias("display")
    private String display;
    private String defaultValue;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getUi() {
        return ui;
    }

    public void setUi(String ui) {
        this.ui = ui;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isFuzzy() {
        return isFuzzy;
    }

    public void setFuzzy(boolean isFuzzy) {
        this.isFuzzy = isFuzzy;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

}
