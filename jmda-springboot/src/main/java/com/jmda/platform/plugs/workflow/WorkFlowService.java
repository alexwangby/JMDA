package com.jmda.platform.plugs.workflow;

import com.jmda.platform.plugs.workflow.model.BussinessWorkFlowBean;
import com.jmda.platform.plugs.workflow.model.WorkflowBean;

import java.util.List;

/**
 * Created by john on 2016/6/2.
 */
public interface WorkFlowService {
    List<WorkflowBean> getAll();

    List<BussinessWorkFlowBean> getBussinessWorkFlow(String workflowId, String bussinessId);
}
