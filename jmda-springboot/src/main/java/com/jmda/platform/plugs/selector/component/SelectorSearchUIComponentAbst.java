package com.jmda.platform.plugs.selector.component;

import com.jmda.platform.plugs.selector.model.SelectorConditionPo;

public abstract class SelectorSearchUIComponentAbst implements SelectorSearchUIComponentInterface {
	SelectorConditionPo selectorConditionPo;

	public SelectorConditionPo getSelectorConditionPo() {
		return selectorConditionPo;
	}

	public void setSelectorConditionPo(SelectorConditionPo selectorConditionPo) {
		this.selectorConditionPo = selectorConditionPo;
	}
	
}
