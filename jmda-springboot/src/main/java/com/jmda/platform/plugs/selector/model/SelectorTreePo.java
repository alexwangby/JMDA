package com.jmda.platform.plugs.selector.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by john on 2016/2/2.
 */
@XStreamAlias("tree")
public class SelectorTreePo {
    @XStreamAlias("className")
    private String className;
    @XStreamAlias("methodName")
    private String methodName;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
