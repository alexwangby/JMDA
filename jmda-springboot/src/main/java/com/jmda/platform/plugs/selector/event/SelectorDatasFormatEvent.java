package com.jmda.platform.plugs.selector.event;

import java.util.List;
import java.util.Map;

/**
 * Created by pierce-deng on 2015/6/25.
 */
public interface SelectorDatasFormatEvent {
    List<Map<String, Object>> format(String id, Map<String, Object> params, List<Map<String, Object>> datas);
}
