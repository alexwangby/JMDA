package com.jmda.platform.plugs.selector.event;

public interface SelectorDatasDataSourceEvent {
	public void beforeSetDataSource();
	public void afterSetDataSource();
}
