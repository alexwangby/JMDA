package com.jmda.platform.plugs.selector.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("field")
public class SelectorFieldPo {
    @XStreamAlias("title")
    private String title;
    @XStreamAlias("column")
    private String column;
    @XStreamAlias("hidden")
    private String hidden;
    @XStreamAlias("width")
    private int width;
    @XStreamAlias("formatter")
    private String formatter;
    @XStreamAlias("sortable")
    private String sortable;
    @XStreamAlias("align")
    private String align;

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }


    public String getFormatter() {
        return formatter;
    }

    public void setFormatter(String formatter) {
        this.formatter = formatter;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public String getSortable() {
        return sortable;
    }

    public void setSortable(String sortable) {
        this.sortable = sortable;
    }

}
