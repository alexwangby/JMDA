package com.jmda.platform.plugs.selector.component;


import com.jmda.platform.plugs.selector.model.SelectorConditionPo;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;

public interface SelectorSearchUIComponentInterface {

	/**
	 * 获取编辑状态下的组件HTML代码,该方法应该被重载
	 * 
	 * @param params
	 *            额外的参数列表，可以为NULL
	 * @return
	 */
	public abstract String getHtmlDefine(String params);

	/**
	 * 获取当前字段模型的编辑界面
	 * 
	 * @return .
	 */
	public abstract String getParamSetingWeb(String params,HttpServletRequest request, ModelMap map);
	public SelectorConditionPo getSelectorConditionPo() ;
	public void setSelectorConditionPo(SelectorConditionPo selectorConditionPo) ;
}
