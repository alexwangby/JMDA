package com.jmda.platform.plugs.selector.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmda.platform.commom.HttpRequestParamParser;
import com.jmda.platform.commom.LocalCacheManager;
import com.jmda.platform.commom.SpringContextUtil;
import com.jmda.platform.engine.form.event.FormDatasComboxEvent;
import com.jmda.platform.engine.mybatis.DataPager;
import com.jmda.platform.plugs.selector.event.SelectorDatasDataSourceEvent;
import com.jmda.platform.plugs.selector.event.SelectorDatasFormatEvent;
import com.jmda.platform.plugs.selector.model.SelectorConditionPo;
import com.jmda.platform.plugs.selector.model.SelectorPo;
import com.jmda.platform.plugs.selector.model.SelectorTreePo;
import com.jmda.platform.plugs.selector.service.SelectorByMyBatisServiceImpl;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.assertj.core.util.Lists;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/selectorController")
public class SelectorControlle {

    @Resource
    SelectorByMyBatisServiceImpl selectorByMyBatisService;

    @RequestMapping("/getPage")
    public String getPage(String id, HttpServletRequest request, ModelMap map) throws JsonParseException, JsonMappingException, IOException {
        Map<String, Object> paramsMap = HttpRequestParamParser.formatParam(request);
        XStream xstream = new XStream(new DomDriver("utf-8"));
        xstream.processAnnotations(SelectorPo.class);
        StringBuffer datasparams = new StringBuffer();
        String[] idArray = id.split("\\|");
        String realPath = getFilePath(idArray.length > 1 ? idArray[0] : id);
        if (realPath != null && realPath.trim().length() > 0) {
            InputStream input = this.getClass().getResourceAsStream(realPath);
            SelectorPo dictionaryModel = (SelectorPo) xstream.fromXML(input);
            List<SelectorConditionPo> conditons = dictionaryModel.getConditions();
            for (Map.Entry<String, Object> entry : paramsMap.entrySet()) {
                String key = entry.getKey().toString();
                String value = entry.getValue().toString();
                if (!key.equals("id")) {
                    datasparams.append("&").append(key).append("=").append(value);
                    paramsMap.put(key, value);
                }
            }
            SelectorTreePo selectorTreePo = dictionaryModel.getSelectorTreePo();
            String result = "";
            if (selectorTreePo != null) {
                try {
                    Class<?> classType = Class.forName(selectorTreePo.getClassName());
                    Object object = classType.newInstance();
                    Method method = classType.getMethod(selectorTreePo.getMethodName(), Map.class);
                    result = (String) method.invoke(object, paramsMap);
                    map.put("treeData", result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            map.put("tree", selectorTreePo);
            map.put("isOpenCondition", dictionaryModel.getIsOpenCondition());
            map.put("hasWestTable", dictionaryModel.getHasWestTable());
            map.put("actionValue", dictionaryModel.getActionValue());
            map.put("title", dictionaryModel.getTitle());
            map.put("id", idArray.length > 1 ? idArray[0] : id + datasparams.toString());
            // modify by wangwq 如果通过URL传递多选则执行多选
            map.put("multiple", idArray.length > 1 && idArray[1].equals("multiple") ? true : dictionaryModel.getMultiple());
            map.put("sql", dictionaryModel.getSql());
            map.put("fields", dictionaryModel.getFidlds());
            map.put("conditions", conditons);
            map.put("fitColumns", dictionaryModel.getFitColumns());
            map.put("paramsMap", paramsMap);
            String extendJavascript = dictionaryModel.getExtendJavascript();
            if (extendJavascript != null) {
                String basePath = request.getContextPath();
                String extMeata = extendJavascript == null ? "" : extendJavascript.replaceAll("\\$\\{basePath\\}", basePath);
                map.put("extendJavascript", extMeata);
            }
        }
        return "plugs/selector";
    }

    @RequestMapping("/queryOne")
    @ResponseBody
    public String queryOne(String id, HttpServletRequest request) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        XStream xstream = new XStream(new DomDriver("utf-8"));
        xstream.processAnnotations(SelectorPo.class);
        Map<String, Object> json = new HashMap<String, Object>();
        String[] idArray = id.split("\\|");
        String realPath = getFilePath(idArray.length > 1 ? idArray[0] : id);
        if (realPath != null && realPath.trim().length() > 0) {
            Map<String, Object> paramsMap = HttpRequestParamParser.formatParam(request);
            InputStream input = this.getClass().getResourceAsStream(realPath);
            SelectorPo dictionaryModel = (SelectorPo) xstream.fromXML(input);
            String multiple = dictionaryModel.getMultiple();
            List<Map<String, Object>> list = Lists.newArrayList();
            Map<String, Object> params = LocalCacheManager.getContextParams();
            params.putAll(paramsMap);
            String datasoureEventClass = dictionaryModel.getDataSourceEventClass();
            //标识是否启用数据源
            boolean b_datasource = false;
            SelectorDatasDataSourceEvent event = null;
            if (datasoureEventClass != null && !datasoureEventClass.equals("")) {
                try {
                    Class<SelectorDatasDataSourceEvent> eventClass = (Class<SelectorDatasDataSourceEvent>) Class.forName(datasoureEventClass);
                    event = SpringContextUtil.getBean(eventClass);
                    if (event != null) {
                        //设置所需数据源
                        event.beforeSetDataSource();
                        b_datasource = true;
                    }
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            DataPager<Map<String, Object>> dataPage = selectorByMyBatisService.queryDatas(dictionaryModel, 0, 20, params);
            if (b_datasource) {
                //重置数据源
                event.afterSetDataSource();
            }
            if (dataPage.getRows() != null && dataPage.getRows().size() == 1) {
                list = formatDatas(id, dictionaryModel, paramsMap, dataPage.getRows());
                json.put("rs", 1);
                if (multiple.equals("true")) {
                    json.put("datas", list);
                } else {
                    json.put("datas", list.get(0));
                }

            } else if (dataPage.getRows() != null && dataPage.getRows().size() > 1) {
                json.put("rs", 2);
            } else {
                json.put("rs", 0);
            }
        } else {
            json.put("rs", -1);
        }
        return mapper.writeValueAsString(json);
    }

    @RequestMapping("/getDatas")
    @ResponseBody
    public String getDictionaryDatas(String id, HttpServletRequest request) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> paramsMap = HttpRequestParamParser.formatParam(request);
        int page = Integer.parseInt((String) paramsMap.get("page"), 0);
//		if (page >= 1) {
//			page = page - 1;
//		}
        int pageSize =Integer.parseInt((String) paramsMap.get("rows"), 10);
        XStream xstream = new XStream(new DomDriver("utf-8"));
        xstream.processAnnotations(SelectorPo.class);
        Map<String, Object> json = new HashMap<String, Object>();
        String realPath = getFilePath(id);
        if (realPath != null && realPath.trim().length() > 0) {

            InputStream input = this.getClass().getResourceAsStream(realPath);
            SelectorPo dictionaryModel = (SelectorPo) xstream.fromXML(input);
            Map<String, Object> params = LocalCacheManager.getContextParams();
            params.putAll(paramsMap);

            String datasoureEventClass = dictionaryModel.getDataSourceEventClass();
            //标识是否启用数据源
            boolean b_datasource = false;
            SelectorDatasDataSourceEvent event = null;
            if (datasoureEventClass != null && !datasoureEventClass.equals("")) {
                try {
                    Class<SelectorDatasDataSourceEvent> eventClass = (Class<SelectorDatasDataSourceEvent>) Class.forName(datasoureEventClass);
                    event = SpringContextUtil.getBean(eventClass);
                    if (event != null) {
                        //设置所需数据源
                        event.beforeSetDataSource();
                        b_datasource = true;
                    }
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            String productAttrTypeIds = (String) paramsMap.get("productAttrTypeIds");
            String[] productAttrTypeIdArray;
            if (null != productAttrTypeIds && !"".equals(productAttrTypeIds.trim())) {
                productAttrTypeIdArray = productAttrTypeIds.split(",");
                params.put("productAttrTypeIdArray", productAttrTypeIdArray);
            }
            DataPager<Map<String, Object>> dataPage = selectorByMyBatisService.queryDatas(dictionaryModel, page, pageSize, params);
            if (b_datasource) {
                //重置数据源
                event.afterSetDataSource();
            }
            if (dataPage.getRows() != null && dataPage.getRows().size() > 0) {
                json.put("total", dataPage.getTotal());
                json.put("rows", formatDatas(id, dictionaryModel, paramsMap, dataPage.getRows()));
            } else {
                return "[]";
            }
        } else {
            return "[]";
        }

        return mapper.writeValueAsString(json);
    }

    public String getFilePath(String id) {
        return "/selector/" + id;
    }

    public List<Map<String, Object>> formatDatas(String id, SelectorPo dictionaryModel, Map<String, Object> params, List<Map<String, Object>> datas) {
        if (dictionaryModel.getEventClass() != null && dictionaryModel.getEventClass().trim().length() > 0) {
            try {
                Class<SelectorDatasFormatEvent> eventClass = (Class<SelectorDatasFormatEvent>) Class.forName(dictionaryModel.getEventClass());
                SelectorDatasFormatEvent event = SpringContextUtil.getBean(eventClass);
                if (event != null) {
                    return event.format(id, params, datas);
                }
            } catch (ClassNotFoundException e) {
                System.out.println("ClassNotFoundException");
            }
        }
        return datas;
    }
}
