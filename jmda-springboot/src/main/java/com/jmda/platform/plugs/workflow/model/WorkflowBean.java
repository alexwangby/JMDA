package com.jmda.platform.plugs.workflow.model;

/**
 * Created by john on 2016/6/2.
 */
public class WorkflowBean {
    private String workflowId;
    private String name;

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
