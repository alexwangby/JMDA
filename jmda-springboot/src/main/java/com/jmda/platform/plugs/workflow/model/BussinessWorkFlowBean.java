package com.jmda.platform.plugs.workflow.model;

/**
 * Created by john on 2016/6/3.
 */
public class BussinessWorkFlowBean {
    private String name;
    private Integer stepNo;
    private String wfTaskLogId;
    private String workflowId;
    private String bussinessId;
    private String stepId;
    private String auditMsg;
    private String taskLogMsg;
    private String taskUserId;
    private long startTime;
    private long endTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStepNo() {
        return stepNo;
    }

    public void setStepNo(Integer stepNo) {
        this.stepNo = stepNo;
    }

    public String getWfTaskLogId() {
        return wfTaskLogId;
    }

    public void setWfTaskLogId(String wfTaskLogId) {
        this.wfTaskLogId = wfTaskLogId;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getBussinessId() {
        return bussinessId;
    }

    public void setBussinessId(String bussinessId) {
        this.bussinessId = bussinessId;
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public String getAuditMsg() {
        return auditMsg;
    }

    public void setAuditMsg(String auditMsg) {
        this.auditMsg = auditMsg;
    }

    public String getTaskLogMsg() {
        return taskLogMsg;
    }

    public void setTaskLogMsg(String taskLogMsg) {
        this.taskLogMsg = taskLogMsg;
    }

    public String getTaskUserId() {
        return taskUserId;
    }

    public void setTaskUserId(String taskUserId) {
        this.taskUserId = taskUserId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}
