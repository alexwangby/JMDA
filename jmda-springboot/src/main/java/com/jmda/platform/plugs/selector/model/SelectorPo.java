package com.jmda.platform.plugs.selector.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("dictionary")
public class SelectorPo {
    @XStreamAlias("title")
    private String title;
    @XStreamAlias("isOpenCondition")
    private String isOpenCondition;
    @XStreamAlias("mapperid")
    private String mapperid;
    @XStreamAlias("totalMapperid")
    private String totalMapperid;
    @XStreamAlias("sql")
    private String sql;
    @XStreamAlias("beanName")
    private String beanName;
    @XStreamImplicit(itemFieldName = "field")
    List<SelectorFieldPo> fidlds;
    @XStreamImplicit(itemFieldName = "condition")
    List<SelectorConditionPo> conditions;
    @XStreamAlias("multiple")
    private String multiple;
    @XStreamAlias("fitColumns")
    private String fitColumns;
    @XStreamAlias("hasWestTable")
    private String hasWestTable;
    @XStreamAlias("actionValue")
    private String actionValue;
    @XStreamAlias("eventClass")
    private String eventClass;
    @XStreamAlias("extendJavascript")
    @JsonProperty
    private String extendJavascript;

    @XStreamAlias("tree")
    private SelectorTreePo selectorTreePo;
    @XStreamAlias("dataSourceEventClass")
    private String dataSourceEventClass;
    public SelectorTreePo getSelectorTreePo() {
        return selectorTreePo;
    }

    public void setSelectorTreePo(SelectorTreePo selectorTreePo) {
        this.selectorTreePo = selectorTreePo;
    }

    public String getActionValue() {
        return actionValue;
    }

    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }


    public String getHasWestTable() {
        return hasWestTable;
    }

    public void setHasWestTable(String hasWestTable) {
        this.hasWestTable = hasWestTable;
    }


    public String getEventClass() {
        return eventClass;
    }

    public void setEventClass(String eventClass) {
        this.eventClass = eventClass;
    }

    public String getIsOpenCondition() {
        return isOpenCondition;
    }

    public void setIsOpenCondition(String isOpenCondition) {
        this.isOpenCondition = isOpenCondition;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMapperid() {
        return mapperid;
    }

    public void setMapperid(String mapperid) {
        this.mapperid = mapperid;
    }

    public List<SelectorFieldPo> getFidlds() {
        return fidlds;
    }

    public void setFidlds(List<SelectorFieldPo> fidlds) {
        this.fidlds = fidlds;
    }

    public List<SelectorConditionPo> getConditions() {
        return conditions;
    }

    public void setConditions(List<SelectorConditionPo> conditions) {
        this.conditions = conditions;
    }

    public String getTotalMapperid() {
        return totalMapperid;
    }

    public void setTotalMapperid(String totalMapperid) {
        this.totalMapperid = totalMapperid;
    }

    public String getMultiple() {
        return multiple;
    }

    public void setMultiple(String multiple) {
        this.multiple = multiple;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getFitColumns() {
        return fitColumns;
    }

    public void setFitColumns(String fitColumns) {
        this.fitColumns = fitColumns;
    }

    public String getExtendJavascript() {
        return extendJavascript;
    }

    public void setExtendJavascript(String extendJavascript) {
        this.extendJavascript = extendJavascript;
    }

	public String getDataSourceEventClass() {
		return dataSourceEventClass;
	}

	public void setDataSourceEventClass(String dataSourceEventClass) {
		this.dataSourceEventClass = dataSourceEventClass;
	}


}
