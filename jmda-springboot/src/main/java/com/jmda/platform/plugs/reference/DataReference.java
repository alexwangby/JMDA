package com.jmda.platform.plugs.reference;

public interface DataReference {
    public String referenceData(String type, String value, String params);
}
