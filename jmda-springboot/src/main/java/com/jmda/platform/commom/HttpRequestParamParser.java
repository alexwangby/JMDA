package com.jmda.platform.commom;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class HttpRequestParamParser {
    //将前段传递的参数封装为一个map
    @SuppressWarnings("rawtypes")
    public static Map<String, Object> formatParam(HttpServletRequest request) {
        Map<String, Object> param = new HashMap<String, Object>();
        Enumeration e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String name = (String) e.nextElement();
            if (null != request.getParameter(name) && !"".equals(request.getParameter(name).trim())) {
                try {
                    param.put(name, java.net.URLDecoder.decode(request.getParameter(name).toString(), "UTF-8").trim());
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return param;
    }
}
