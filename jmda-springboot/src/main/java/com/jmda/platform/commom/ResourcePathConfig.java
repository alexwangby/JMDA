package com.jmda.platform.commom;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

@Service("ResourceLoaderPath")
public class ResourcePathConfig {
    @Resource
    ResourceLoader res;
    @Value("${jmda.runtime.path}")  
    private String jmdaPath;
    private String worklistPath = "/MDA/worklist";
    private String templateSystemPath = "/jmda-ftl/system/";
    private String templateEformPath = "/templates";
    private String applicationPath = "/MDA/application";
    private String formPath = "/MDA/form";

    //获取表单类xml文件的路径
    public String getFormPath() throws IOException {
        org.springframework.core.io.Resource path = res.getResource(jmdaPath+formPath);
        File file = path.getFile();
        String logoRealPathDir = file.getAbsolutePath();
        String realDir = logoRealPathDir + File.separator;
        realDir = realDir.replaceAll("\\\\", "/");
        return realDir;
    }

    //获取列表类xml文件的路径
    public String getWorklistPath() throws IOException {
        org.springframework.core.io.Resource path = res.getResource(jmdaPath+worklistPath);
        File file = path.getFile();
        String logoRealPathDir = file.getAbsolutePath();
        String realDir = logoRealPathDir + File.separator;
        realDir = realDir.replaceAll("\\\\", "/");
        return realDir;
    }

    public String getTemplateSystemPath() throws IOException {
        return templateSystemPath;
    }

    public String getTemplateEformPath() throws IOException {
        org.springframework.core.io.Resource path = res.getResource(jmdaPath+templateEformPath);
        File file = path.getFile();
        String logoRealPathDir = file.getAbsolutePath();
        String realDir = logoRealPathDir + File.separator;
        return realDir;
    }

    public String getRealPath(String classPath) throws IOException {
        org.springframework.core.io.Resource path = res.getResource(classPath);
        File file = path.getFile();
        String logoRealPathDir = file.getAbsolutePath();
        String realDir = logoRealPathDir + File.separator;
        return realDir;
    }

    public String getApplicationPath() throws IOException {
        org.springframework.core.io.Resource path = res.getResource(jmdaPath+applicationPath);
        File file = path.getFile();
        String logoRealPathDir = file.getAbsolutePath();
        String realDir = logoRealPathDir + File.separator;
        return realDir;
    }

}
