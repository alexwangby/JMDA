package com.jmda.platform.commom;

import com.jmda.platform.repository.ResositoryException;

import java.util.List;

public interface BaseService<T> {

	public List<T> getAll() throws ResositoryException;

	public T get(String id) throws ResositoryException;

	public void remove(String id) throws ResositoryException;

	public String store(T model) throws ResositoryException;

	public List<T> queryList(T param);

	public T queryOne(T param);
}
