package com.jmda.platform.commom;

import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;

@Service("ClassInfo")
public class ClassInfo {

	/**
	 * 获取当前线程的调用堆栈调试信息
	 * 
	 * @return
	 * @author jack
	 */
	public String getThreadStackTraces() {
		String value = "";
		Map activeThreads = Thread.getAllStackTraces();// 当前活动的线程
		StackTraceElement[] stacks = (StackTraceElement[]) activeThreads.get(Thread.currentThread());
		if (stacks != null) {
			for (int ii = 0; ii < stacks.length; ii++) {
				if (stacks[ii].getLineNumber() <= 0)
					value += stacks[ii].getClassName() + "." + stacks[ii].getMethodName() + "()\n";
				else
					value += stacks[ii].getClassName() + "." + stacks[ii].getMethodName() + "()第" + stacks[ii].getLineNumber() + "行\n";
			}
			value += "\n 记录时间：" +DateFormat.getInstance().format(new Date());
		}
		activeThreads = null;
		return value;
	}

	/**
	 * 
	 * @param obj
	 * 
	 */
	public void printClass(Object obj) {
		System.out.print("[调试 - 类信息]");
		if (obj == null) {
			System.out.println("类是空 null");
			return;
		}
		Class c = obj.getClass();
		Field fields[] = null;
		fields = c.getFields();
		System.out.print("类[- " + parseClassName(c.getName() + "@") + " -]");
		for (int i = 0; i < fields.length; i++) {
			Class classType = fields[i].getType();
			Object o = null;
			try {
				o = fields[i].get(obj);
			} catch (java.lang.IllegalAccessException e) {
				System.out.print("读取字段" + fields[i].getName() + "失败!" + e);
			}
			System.out.print("  字段" + i + " [" + fields[i].getName() + "] = " + o + " | ");
		}
		System.out.println();
	}

	private String parseClassName(String sName) {
		int iPointPos = sName.lastIndexOf(".");
		int iAtPos = sName.indexOf("@");
		if ((iPointPos == -1) || (iAtPos == -1)) {
			return sName;
		}
		sName = sName.substring(iPointPos + 1, iAtPos);
		return sName;
	}
}