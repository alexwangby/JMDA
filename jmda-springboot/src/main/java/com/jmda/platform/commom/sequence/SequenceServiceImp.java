package com.jmda.platform.commom.sequence;

import com.jmda.platform.commom.SequenceService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class SequenceServiceImp implements SequenceService {
    //获取一个uuid
    @Override
    public String getUUID() {
        // TODO Auto-generated method stub
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.replaceAll("-", "");
        return uuid;
    }

//	@Override
//	public int getDBsequenceNext(String sequenceName) {
//		// TODO Auto-generated method stub
//		return Integer.parseInt(KeyCodeFactory.nextCode(sequenceName));
//	}

}
