package com.jmda.platform.commom;

import java.io.IOException;
import java.util.List;

public interface BaseCache<T> {
	public void put(T model);

	public List<T> getAll();

	public T get(String id);

	public void remove(String id);

	public void initialize() throws IOException;

	public List<T> queryList(T param);
	
	public String getFilePath()throws IOException;
}
