package com.jmda.erp.common.dao.table.mapper;

import com.jmda.erp.common.model.po.CuGeneratorKeyCodePO;
import com.jmda.erp.common.model.po.CuGeneratorKeyCodePOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CuGeneratorKeyCodePOMapper {
    int countByExample(CuGeneratorKeyCodePOExample example);

    int deleteByExample(CuGeneratorKeyCodePOExample example);

    int deleteByPrimaryKey(String keyCodeId);

    int insert(CuGeneratorKeyCodePO record);

    int insertSelective(CuGeneratorKeyCodePO record);

    List<CuGeneratorKeyCodePO> selectByExample(CuGeneratorKeyCodePOExample example);

    CuGeneratorKeyCodePO selectByPrimaryKey(String keyCodeId);

    int updateByExampleSelective(@Param("record") CuGeneratorKeyCodePO record, @Param("example") CuGeneratorKeyCodePOExample example);

    int updateByExample(@Param("record") CuGeneratorKeyCodePO record, @Param("example") CuGeneratorKeyCodePOExample example);

    int updateByPrimaryKeySelective(CuGeneratorKeyCodePO record);

    int updateByPrimaryKey(CuGeneratorKeyCodePO record);
}