package com.jmda.erp.common.model.po;

public class CuJobLogsPO {
    private String jobLogsId;

    private String impClassPath;

    private Long startTime;

    private Long endTime;

    private String jobStatus;

    public String getJobLogsId() {
        return jobLogsId;
    }

    public void setJobLogsId(String jobLogsId) {
        this.jobLogsId = jobLogsId == null ? null : jobLogsId.trim();
    }

    public String getImpClassPath() {
        return impClassPath;
    }

    public void setImpClassPath(String impClassPath) {
        this.impClassPath = impClassPath == null ? null : impClassPath.trim();
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus == null ? null : jobStatus.trim();
    }
}