package com.jmda.erp.common.model.po;

public class WfTaskLogPO {
    private String wfTaskLogId;

    private String workflowId;

    private String bussinessId;

    private String stepId;

    private String auditMsg;

    private String taskLogMsg;

    private String taskUserId;

    private Long startTime;

    private Long endTime;

    public String getWfTaskLogId() {
        return wfTaskLogId;
    }

    public void setWfTaskLogId(String wfTaskLogId) {
        this.wfTaskLogId = wfTaskLogId == null ? null : wfTaskLogId.trim();
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId == null ? null : workflowId.trim();
    }

    public String getBussinessId() {
        return bussinessId;
    }

    public void setBussinessId(String bussinessId) {
        this.bussinessId = bussinessId == null ? null : bussinessId.trim();
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId == null ? null : stepId.trim();
    }

    public String getAuditMsg() {
        return auditMsg;
    }

    public void setAuditMsg(String auditMsg) {
        this.auditMsg = auditMsg == null ? null : auditMsg.trim();
    }

    public String getTaskLogMsg() {
        return taskLogMsg;
    }

    public void setTaskLogMsg(String taskLogMsg) {
        this.taskLogMsg = taskLogMsg == null ? null : taskLogMsg.trim();
    }

    public String getTaskUserId() {
        return taskUserId;
    }

    public void setTaskUserId(String taskUserId) {
        this.taskUserId = taskUserId == null ? null : taskUserId.trim();
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }
}