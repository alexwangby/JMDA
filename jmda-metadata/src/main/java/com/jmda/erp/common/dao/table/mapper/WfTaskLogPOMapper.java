package com.jmda.erp.common.dao.table.mapper;

import com.jmda.erp.common.model.po.WfTaskLogPO;
import com.jmda.erp.common.model.po.WfTaskLogPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WfTaskLogPOMapper {
    int countByExample(WfTaskLogPOExample example);

    int deleteByExample(WfTaskLogPOExample example);

    int deleteByPrimaryKey(String wfTaskLogId);

    int insert(WfTaskLogPO record);

    int insertSelective(WfTaskLogPO record);

    List<WfTaskLogPO> selectByExample(WfTaskLogPOExample example);

    WfTaskLogPO selectByPrimaryKey(String wfTaskLogId);

    int updateByExampleSelective(@Param("record") WfTaskLogPO record, @Param("example") WfTaskLogPOExample example);

    int updateByExample(@Param("record") WfTaskLogPO record, @Param("example") WfTaskLogPOExample example);

    int updateByPrimaryKeySelective(WfTaskLogPO record);

    int updateByPrimaryKey(WfTaskLogPO record);
}