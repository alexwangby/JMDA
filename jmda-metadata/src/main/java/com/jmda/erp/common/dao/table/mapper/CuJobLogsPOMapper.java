package com.jmda.erp.common.dao.table.mapper;

import com.jmda.erp.common.model.po.CuJobLogsPO;
import com.jmda.erp.common.model.po.CuJobLogsPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CuJobLogsPOMapper {
    int countByExample(CuJobLogsPOExample example);

    int deleteByExample(CuJobLogsPOExample example);

    int deleteByPrimaryKey(String jobLogsId);

    int insert(CuJobLogsPO record);

    int insertSelective(CuJobLogsPO record);

    List<CuJobLogsPO> selectByExample(CuJobLogsPOExample example);

    CuJobLogsPO selectByPrimaryKey(String jobLogsId);

    int updateByExampleSelective(@Param("record") CuJobLogsPO record, @Param("example") CuJobLogsPOExample example);

    int updateByExample(@Param("record") CuJobLogsPO record, @Param("example") CuJobLogsPOExample example);

    int updateByPrimaryKeySelective(CuJobLogsPO record);

    int updateByPrimaryKey(CuJobLogsPO record);
}