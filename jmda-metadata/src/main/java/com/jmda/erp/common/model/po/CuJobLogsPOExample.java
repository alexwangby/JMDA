package com.jmda.erp.common.model.po;

import java.util.ArrayList;
import java.util.List;

public class CuJobLogsPOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CuJobLogsPOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andJobLogsIdIsNull() {
            addCriterion("JOB_LOGS_ID is null");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdIsNotNull() {
            addCriterion("JOB_LOGS_ID is not null");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdEqualTo(String value) {
            addCriterion("JOB_LOGS_ID =", value, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdNotEqualTo(String value) {
            addCriterion("JOB_LOGS_ID <>", value, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdGreaterThan(String value) {
            addCriterion("JOB_LOGS_ID >", value, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_LOGS_ID >=", value, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdLessThan(String value) {
            addCriterion("JOB_LOGS_ID <", value, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdLessThanOrEqualTo(String value) {
            addCriterion("JOB_LOGS_ID <=", value, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdLike(String value) {
            addCriterion("JOB_LOGS_ID like", value, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdNotLike(String value) {
            addCriterion("JOB_LOGS_ID not like", value, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdIn(List<String> values) {
            addCriterion("JOB_LOGS_ID in", values, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdNotIn(List<String> values) {
            addCriterion("JOB_LOGS_ID not in", values, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdBetween(String value1, String value2) {
            addCriterion("JOB_LOGS_ID between", value1, value2, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andJobLogsIdNotBetween(String value1, String value2) {
            addCriterion("JOB_LOGS_ID not between", value1, value2, "jobLogsId");
            return (Criteria) this;
        }

        public Criteria andImpClassPathIsNull() {
            addCriterion("IMP_CLASS_PATH is null");
            return (Criteria) this;
        }

        public Criteria andImpClassPathIsNotNull() {
            addCriterion("IMP_CLASS_PATH is not null");
            return (Criteria) this;
        }

        public Criteria andImpClassPathEqualTo(String value) {
            addCriterion("IMP_CLASS_PATH =", value, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathNotEqualTo(String value) {
            addCriterion("IMP_CLASS_PATH <>", value, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathGreaterThan(String value) {
            addCriterion("IMP_CLASS_PATH >", value, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathGreaterThanOrEqualTo(String value) {
            addCriterion("IMP_CLASS_PATH >=", value, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathLessThan(String value) {
            addCriterion("IMP_CLASS_PATH <", value, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathLessThanOrEqualTo(String value) {
            addCriterion("IMP_CLASS_PATH <=", value, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathLike(String value) {
            addCriterion("IMP_CLASS_PATH like", value, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathNotLike(String value) {
            addCriterion("IMP_CLASS_PATH not like", value, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathIn(List<String> values) {
            addCriterion("IMP_CLASS_PATH in", values, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathNotIn(List<String> values) {
            addCriterion("IMP_CLASS_PATH not in", values, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathBetween(String value1, String value2) {
            addCriterion("IMP_CLASS_PATH between", value1, value2, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andImpClassPathNotBetween(String value1, String value2) {
            addCriterion("IMP_CLASS_PATH not between", value1, value2, "impClassPath");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("START_TIME is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("START_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Long value) {
            addCriterion("START_TIME =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Long value) {
            addCriterion("START_TIME <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Long value) {
            addCriterion("START_TIME >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("START_TIME >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Long value) {
            addCriterion("START_TIME <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Long value) {
            addCriterion("START_TIME <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Long> values) {
            addCriterion("START_TIME in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Long> values) {
            addCriterion("START_TIME not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Long value1, Long value2) {
            addCriterion("START_TIME between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Long value1, Long value2) {
            addCriterion("START_TIME not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("END_TIME is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("END_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Long value) {
            addCriterion("END_TIME =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Long value) {
            addCriterion("END_TIME <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Long value) {
            addCriterion("END_TIME >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("END_TIME >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Long value) {
            addCriterion("END_TIME <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Long value) {
            addCriterion("END_TIME <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Long> values) {
            addCriterion("END_TIME in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Long> values) {
            addCriterion("END_TIME not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Long value1, Long value2) {
            addCriterion("END_TIME between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Long value1, Long value2) {
            addCriterion("END_TIME not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andJobStatusIsNull() {
            addCriterion("JOB_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andJobStatusIsNotNull() {
            addCriterion("JOB_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andJobStatusEqualTo(String value) {
            addCriterion("JOB_STATUS =", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusNotEqualTo(String value) {
            addCriterion("JOB_STATUS <>", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusGreaterThan(String value) {
            addCriterion("JOB_STATUS >", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_STATUS >=", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusLessThan(String value) {
            addCriterion("JOB_STATUS <", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusLessThanOrEqualTo(String value) {
            addCriterion("JOB_STATUS <=", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusLike(String value) {
            addCriterion("JOB_STATUS like", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusNotLike(String value) {
            addCriterion("JOB_STATUS not like", value, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusIn(List<String> values) {
            addCriterion("JOB_STATUS in", values, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusNotIn(List<String> values) {
            addCriterion("JOB_STATUS not in", values, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusBetween(String value1, String value2) {
            addCriterion("JOB_STATUS between", value1, value2, "jobStatus");
            return (Criteria) this;
        }

        public Criteria andJobStatusNotBetween(String value1, String value2) {
            addCriterion("JOB_STATUS not between", value1, value2, "jobStatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}