package com.jmda.erp.common.dao.table.mapper;

import com.jmda.erp.common.model.po.CuCachePO;
import com.jmda.erp.common.model.po.CuCachePOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CuCachePOMapper {
    int countByExample(CuCachePOExample example);

    int deleteByExample(CuCachePOExample example);

    int deleteByPrimaryKey(String cacheId);

    int insert(CuCachePO record);

    int insertSelective(CuCachePO record);

    List<CuCachePO> selectByExample(CuCachePOExample example);

    CuCachePO selectByPrimaryKey(String cacheId);

    int updateByExampleSelective(@Param("record") CuCachePO record, @Param("example") CuCachePOExample example);

    int updateByExample(@Param("record") CuCachePO record, @Param("example") CuCachePOExample example);

    int updateByPrimaryKeySelective(CuCachePO record);

    int updateByPrimaryKey(CuCachePO record);
}