package com.jmda.erp.common.model.po;

import java.util.ArrayList;
import java.util.List;

public class CuCachePOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CuCachePOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCacheIdIsNull() {
            addCriterion("CACHE_ID is null");
            return (Criteria) this;
        }

        public Criteria andCacheIdIsNotNull() {
            addCriterion("CACHE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCacheIdEqualTo(String value) {
            addCriterion("CACHE_ID =", value, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdNotEqualTo(String value) {
            addCriterion("CACHE_ID <>", value, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdGreaterThan(String value) {
            addCriterion("CACHE_ID >", value, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdGreaterThanOrEqualTo(String value) {
            addCriterion("CACHE_ID >=", value, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdLessThan(String value) {
            addCriterion("CACHE_ID <", value, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdLessThanOrEqualTo(String value) {
            addCriterion("CACHE_ID <=", value, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdLike(String value) {
            addCriterion("CACHE_ID like", value, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdNotLike(String value) {
            addCriterion("CACHE_ID not like", value, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdIn(List<String> values) {
            addCriterion("CACHE_ID in", values, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdNotIn(List<String> values) {
            addCriterion("CACHE_ID not in", values, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdBetween(String value1, String value2) {
            addCriterion("CACHE_ID between", value1, value2, "cacheId");
            return (Criteria) this;
        }

        public Criteria andCacheIdNotBetween(String value1, String value2) {
            addCriterion("CACHE_ID not between", value1, value2, "cacheId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andFieldColumnIsNull() {
            addCriterion("FIELD_COLUMN is null");
            return (Criteria) this;
        }

        public Criteria andFieldColumnIsNotNull() {
            addCriterion("FIELD_COLUMN is not null");
            return (Criteria) this;
        }

        public Criteria andFieldColumnEqualTo(String value) {
            addCriterion("FIELD_COLUMN =", value, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnNotEqualTo(String value) {
            addCriterion("FIELD_COLUMN <>", value, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnGreaterThan(String value) {
            addCriterion("FIELD_COLUMN >", value, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnGreaterThanOrEqualTo(String value) {
            addCriterion("FIELD_COLUMN >=", value, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnLessThan(String value) {
            addCriterion("FIELD_COLUMN <", value, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnLessThanOrEqualTo(String value) {
            addCriterion("FIELD_COLUMN <=", value, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnLike(String value) {
            addCriterion("FIELD_COLUMN like", value, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnNotLike(String value) {
            addCriterion("FIELD_COLUMN not like", value, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnIn(List<String> values) {
            addCriterion("FIELD_COLUMN in", values, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnNotIn(List<String> values) {
            addCriterion("FIELD_COLUMN not in", values, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnBetween(String value1, String value2) {
            addCriterion("FIELD_COLUMN between", value1, value2, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andFieldColumnNotBetween(String value1, String value2) {
            addCriterion("FIELD_COLUMN not between", value1, value2, "fieldColumn");
            return (Criteria) this;
        }

        public Criteria andCacheSqlIsNull() {
            addCriterion("CACHE_SQL is null");
            return (Criteria) this;
        }

        public Criteria andCacheSqlIsNotNull() {
            addCriterion("CACHE_SQL is not null");
            return (Criteria) this;
        }

        public Criteria andCacheSqlEqualTo(String value) {
            addCriterion("CACHE_SQL =", value, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlNotEqualTo(String value) {
            addCriterion("CACHE_SQL <>", value, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlGreaterThan(String value) {
            addCriterion("CACHE_SQL >", value, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlGreaterThanOrEqualTo(String value) {
            addCriterion("CACHE_SQL >=", value, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlLessThan(String value) {
            addCriterion("CACHE_SQL <", value, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlLessThanOrEqualTo(String value) {
            addCriterion("CACHE_SQL <=", value, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlLike(String value) {
            addCriterion("CACHE_SQL like", value, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlNotLike(String value) {
            addCriterion("CACHE_SQL not like", value, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlIn(List<String> values) {
            addCriterion("CACHE_SQL in", values, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlNotIn(List<String> values) {
            addCriterion("CACHE_SQL not in", values, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlBetween(String value1, String value2) {
            addCriterion("CACHE_SQL between", value1, value2, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andCacheSqlNotBetween(String value1, String value2) {
            addCriterion("CACHE_SQL not between", value1, value2, "cacheSql");
            return (Criteria) this;
        }

        public Criteria andDataSourceIsNull() {
            addCriterion("DATA_SOURCE is null");
            return (Criteria) this;
        }

        public Criteria andDataSourceIsNotNull() {
            addCriterion("DATA_SOURCE is not null");
            return (Criteria) this;
        }

        public Criteria andDataSourceEqualTo(String value) {
            addCriterion("DATA_SOURCE =", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceNotEqualTo(String value) {
            addCriterion("DATA_SOURCE <>", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceGreaterThan(String value) {
            addCriterion("DATA_SOURCE >", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceGreaterThanOrEqualTo(String value) {
            addCriterion("DATA_SOURCE >=", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceLessThan(String value) {
            addCriterion("DATA_SOURCE <", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceLessThanOrEqualTo(String value) {
            addCriterion("DATA_SOURCE <=", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceLike(String value) {
            addCriterion("DATA_SOURCE like", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceNotLike(String value) {
            addCriterion("DATA_SOURCE not like", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceIn(List<String> values) {
            addCriterion("DATA_SOURCE in", values, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceNotIn(List<String> values) {
            addCriterion("DATA_SOURCE not in", values, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceBetween(String value1, String value2) {
            addCriterion("DATA_SOURCE between", value1, value2, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceNotBetween(String value1, String value2) {
            addCriterion("DATA_SOURCE not between", value1, value2, "dataSource");
            return (Criteria) this;
        }

        public Criteria andCacheDescIsNull() {
            addCriterion("CACHE_DESC is null");
            return (Criteria) this;
        }

        public Criteria andCacheDescIsNotNull() {
            addCriterion("CACHE_DESC is not null");
            return (Criteria) this;
        }

        public Criteria andCacheDescEqualTo(String value) {
            addCriterion("CACHE_DESC =", value, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescNotEqualTo(String value) {
            addCriterion("CACHE_DESC <>", value, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescGreaterThan(String value) {
            addCriterion("CACHE_DESC >", value, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescGreaterThanOrEqualTo(String value) {
            addCriterion("CACHE_DESC >=", value, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescLessThan(String value) {
            addCriterion("CACHE_DESC <", value, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescLessThanOrEqualTo(String value) {
            addCriterion("CACHE_DESC <=", value, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescLike(String value) {
            addCriterion("CACHE_DESC like", value, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescNotLike(String value) {
            addCriterion("CACHE_DESC not like", value, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescIn(List<String> values) {
            addCriterion("CACHE_DESC in", values, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescNotIn(List<String> values) {
            addCriterion("CACHE_DESC not in", values, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescBetween(String value1, String value2) {
            addCriterion("CACHE_DESC between", value1, value2, "cacheDesc");
            return (Criteria) this;
        }

        public Criteria andCacheDescNotBetween(String value1, String value2) {
            addCriterion("CACHE_DESC not between", value1, value2, "cacheDesc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}