package com.jmda.erp.common.dao.table.mapper;

import com.jmda.erp.common.model.po.CuJobPO;
import com.jmda.erp.common.model.po.CuJobPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CuJobPOMapper {
    int countByExample(CuJobPOExample example);

    int deleteByExample(CuJobPOExample example);

    int deleteByPrimaryKey(String jobId);

    int insert(CuJobPO record);

    int insertSelective(CuJobPO record);

    List<CuJobPO> selectByExample(CuJobPOExample example);

    CuJobPO selectByPrimaryKey(String jobId);

    int updateByExampleSelective(@Param("record") CuJobPO record, @Param("example") CuJobPOExample example);

    int updateByExample(@Param("record") CuJobPO record, @Param("example") CuJobPOExample example);

    int updateByPrimaryKeySelective(CuJobPO record);

    int updateByPrimaryKey(CuJobPO record);
}