package com.jmda.erp.common.model.po;

public class CuLookupTypePO {
    private String lookupType;

    private String typeName;

    public String getLookupType() {
        return lookupType;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType == null ? null : lookupType.trim();
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName == null ? null : typeName.trim();
    }
}