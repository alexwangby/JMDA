package com.jmda.erp.common.model.po;

public class CuCachePO {
    private String cacheId;

    private String name;

    private String fieldColumn;

    private String cacheSql;

    private String dataSource;

    private String cacheDesc;

    public String getCacheId() {
        return cacheId;
    }

    public void setCacheId(String cacheId) {
        this.cacheId = cacheId == null ? null : cacheId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getFieldColumn() {
        return fieldColumn;
    }

    public void setFieldColumn(String fieldColumn) {
        this.fieldColumn = fieldColumn == null ? null : fieldColumn.trim();
    }

    public String getCacheSql() {
        return cacheSql;
    }

    public void setCacheSql(String cacheSql) {
        this.cacheSql = cacheSql == null ? null : cacheSql.trim();
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource == null ? null : dataSource.trim();
    }

    public String getCacheDesc() {
        return cacheDesc;
    }

    public void setCacheDesc(String cacheDesc) {
        this.cacheDesc = cacheDesc == null ? null : cacheDesc.trim();
    }
}