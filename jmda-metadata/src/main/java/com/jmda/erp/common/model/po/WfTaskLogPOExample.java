package com.jmda.erp.common.model.po;

import java.util.ArrayList;
import java.util.List;

public class WfTaskLogPOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WfTaskLogPOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andWfTaskLogIdIsNull() {
            addCriterion("WF_TASK_LOG_ID is null");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdIsNotNull() {
            addCriterion("WF_TASK_LOG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdEqualTo(String value) {
            addCriterion("WF_TASK_LOG_ID =", value, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdNotEqualTo(String value) {
            addCriterion("WF_TASK_LOG_ID <>", value, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdGreaterThan(String value) {
            addCriterion("WF_TASK_LOG_ID >", value, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdGreaterThanOrEqualTo(String value) {
            addCriterion("WF_TASK_LOG_ID >=", value, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdLessThan(String value) {
            addCriterion("WF_TASK_LOG_ID <", value, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdLessThanOrEqualTo(String value) {
            addCriterion("WF_TASK_LOG_ID <=", value, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdLike(String value) {
            addCriterion("WF_TASK_LOG_ID like", value, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdNotLike(String value) {
            addCriterion("WF_TASK_LOG_ID not like", value, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdIn(List<String> values) {
            addCriterion("WF_TASK_LOG_ID in", values, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdNotIn(List<String> values) {
            addCriterion("WF_TASK_LOG_ID not in", values, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdBetween(String value1, String value2) {
            addCriterion("WF_TASK_LOG_ID between", value1, value2, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWfTaskLogIdNotBetween(String value1, String value2) {
            addCriterion("WF_TASK_LOG_ID not between", value1, value2, "wfTaskLogId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdIsNull() {
            addCriterion("WORKFLOW_ID is null");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdIsNotNull() {
            addCriterion("WORKFLOW_ID is not null");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdEqualTo(String value) {
            addCriterion("WORKFLOW_ID =", value, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdNotEqualTo(String value) {
            addCriterion("WORKFLOW_ID <>", value, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdGreaterThan(String value) {
            addCriterion("WORKFLOW_ID >", value, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdGreaterThanOrEqualTo(String value) {
            addCriterion("WORKFLOW_ID >=", value, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdLessThan(String value) {
            addCriterion("WORKFLOW_ID <", value, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdLessThanOrEqualTo(String value) {
            addCriterion("WORKFLOW_ID <=", value, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdLike(String value) {
            addCriterion("WORKFLOW_ID like", value, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdNotLike(String value) {
            addCriterion("WORKFLOW_ID not like", value, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdIn(List<String> values) {
            addCriterion("WORKFLOW_ID in", values, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdNotIn(List<String> values) {
            addCriterion("WORKFLOW_ID not in", values, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdBetween(String value1, String value2) {
            addCriterion("WORKFLOW_ID between", value1, value2, "workflowId");
            return (Criteria) this;
        }

        public Criteria andWorkflowIdNotBetween(String value1, String value2) {
            addCriterion("WORKFLOW_ID not between", value1, value2, "workflowId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdIsNull() {
            addCriterion("BUSSINESS_ID is null");
            return (Criteria) this;
        }

        public Criteria andBussinessIdIsNotNull() {
            addCriterion("BUSSINESS_ID is not null");
            return (Criteria) this;
        }

        public Criteria andBussinessIdEqualTo(String value) {
            addCriterion("BUSSINESS_ID =", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdNotEqualTo(String value) {
            addCriterion("BUSSINESS_ID <>", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdGreaterThan(String value) {
            addCriterion("BUSSINESS_ID >", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdGreaterThanOrEqualTo(String value) {
            addCriterion("BUSSINESS_ID >=", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdLessThan(String value) {
            addCriterion("BUSSINESS_ID <", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdLessThanOrEqualTo(String value) {
            addCriterion("BUSSINESS_ID <=", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdLike(String value) {
            addCriterion("BUSSINESS_ID like", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdNotLike(String value) {
            addCriterion("BUSSINESS_ID not like", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdIn(List<String> values) {
            addCriterion("BUSSINESS_ID in", values, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdNotIn(List<String> values) {
            addCriterion("BUSSINESS_ID not in", values, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdBetween(String value1, String value2) {
            addCriterion("BUSSINESS_ID between", value1, value2, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdNotBetween(String value1, String value2) {
            addCriterion("BUSSINESS_ID not between", value1, value2, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andStepIdIsNull() {
            addCriterion("STEP_ID is null");
            return (Criteria) this;
        }

        public Criteria andStepIdIsNotNull() {
            addCriterion("STEP_ID is not null");
            return (Criteria) this;
        }

        public Criteria andStepIdEqualTo(String value) {
            addCriterion("STEP_ID =", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotEqualTo(String value) {
            addCriterion("STEP_ID <>", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdGreaterThan(String value) {
            addCriterion("STEP_ID >", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdGreaterThanOrEqualTo(String value) {
            addCriterion("STEP_ID >=", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdLessThan(String value) {
            addCriterion("STEP_ID <", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdLessThanOrEqualTo(String value) {
            addCriterion("STEP_ID <=", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdLike(String value) {
            addCriterion("STEP_ID like", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotLike(String value) {
            addCriterion("STEP_ID not like", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdIn(List<String> values) {
            addCriterion("STEP_ID in", values, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotIn(List<String> values) {
            addCriterion("STEP_ID not in", values, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdBetween(String value1, String value2) {
            addCriterion("STEP_ID between", value1, value2, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotBetween(String value1, String value2) {
            addCriterion("STEP_ID not between", value1, value2, "stepId");
            return (Criteria) this;
        }

        public Criteria andAuditMsgIsNull() {
            addCriterion("AUDIT_MSG is null");
            return (Criteria) this;
        }

        public Criteria andAuditMsgIsNotNull() {
            addCriterion("AUDIT_MSG is not null");
            return (Criteria) this;
        }

        public Criteria andAuditMsgEqualTo(String value) {
            addCriterion("AUDIT_MSG =", value, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgNotEqualTo(String value) {
            addCriterion("AUDIT_MSG <>", value, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgGreaterThan(String value) {
            addCriterion("AUDIT_MSG >", value, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgGreaterThanOrEqualTo(String value) {
            addCriterion("AUDIT_MSG >=", value, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgLessThan(String value) {
            addCriterion("AUDIT_MSG <", value, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgLessThanOrEqualTo(String value) {
            addCriterion("AUDIT_MSG <=", value, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgLike(String value) {
            addCriterion("AUDIT_MSG like", value, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgNotLike(String value) {
            addCriterion("AUDIT_MSG not like", value, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgIn(List<String> values) {
            addCriterion("AUDIT_MSG in", values, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgNotIn(List<String> values) {
            addCriterion("AUDIT_MSG not in", values, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgBetween(String value1, String value2) {
            addCriterion("AUDIT_MSG between", value1, value2, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andAuditMsgNotBetween(String value1, String value2) {
            addCriterion("AUDIT_MSG not between", value1, value2, "auditMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgIsNull() {
            addCriterion("TASK_LOG_MSG is null");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgIsNotNull() {
            addCriterion("TASK_LOG_MSG is not null");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgEqualTo(String value) {
            addCriterion("TASK_LOG_MSG =", value, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgNotEqualTo(String value) {
            addCriterion("TASK_LOG_MSG <>", value, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgGreaterThan(String value) {
            addCriterion("TASK_LOG_MSG >", value, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgGreaterThanOrEqualTo(String value) {
            addCriterion("TASK_LOG_MSG >=", value, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgLessThan(String value) {
            addCriterion("TASK_LOG_MSG <", value, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgLessThanOrEqualTo(String value) {
            addCriterion("TASK_LOG_MSG <=", value, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgLike(String value) {
            addCriterion("TASK_LOG_MSG like", value, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgNotLike(String value) {
            addCriterion("TASK_LOG_MSG not like", value, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgIn(List<String> values) {
            addCriterion("TASK_LOG_MSG in", values, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgNotIn(List<String> values) {
            addCriterion("TASK_LOG_MSG not in", values, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgBetween(String value1, String value2) {
            addCriterion("TASK_LOG_MSG between", value1, value2, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskLogMsgNotBetween(String value1, String value2) {
            addCriterion("TASK_LOG_MSG not between", value1, value2, "taskLogMsg");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdIsNull() {
            addCriterion("TASK_USER_ID is null");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdIsNotNull() {
            addCriterion("TASK_USER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdEqualTo(String value) {
            addCriterion("TASK_USER_ID =", value, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdNotEqualTo(String value) {
            addCriterion("TASK_USER_ID <>", value, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdGreaterThan(String value) {
            addCriterion("TASK_USER_ID >", value, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("TASK_USER_ID >=", value, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdLessThan(String value) {
            addCriterion("TASK_USER_ID <", value, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdLessThanOrEqualTo(String value) {
            addCriterion("TASK_USER_ID <=", value, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdLike(String value) {
            addCriterion("TASK_USER_ID like", value, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdNotLike(String value) {
            addCriterion("TASK_USER_ID not like", value, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdIn(List<String> values) {
            addCriterion("TASK_USER_ID in", values, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdNotIn(List<String> values) {
            addCriterion("TASK_USER_ID not in", values, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdBetween(String value1, String value2) {
            addCriterion("TASK_USER_ID between", value1, value2, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andTaskUserIdNotBetween(String value1, String value2) {
            addCriterion("TASK_USER_ID not between", value1, value2, "taskUserId");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("START_TIME is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("START_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Long value) {
            addCriterion("START_TIME =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Long value) {
            addCriterion("START_TIME <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Long value) {
            addCriterion("START_TIME >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("START_TIME >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Long value) {
            addCriterion("START_TIME <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Long value) {
            addCriterion("START_TIME <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Long> values) {
            addCriterion("START_TIME in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Long> values) {
            addCriterion("START_TIME not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Long value1, Long value2) {
            addCriterion("START_TIME between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Long value1, Long value2) {
            addCriterion("START_TIME not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("END_TIME is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("END_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Long value) {
            addCriterion("END_TIME =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Long value) {
            addCriterion("END_TIME <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Long value) {
            addCriterion("END_TIME >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("END_TIME >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Long value) {
            addCriterion("END_TIME <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Long value) {
            addCriterion("END_TIME <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Long> values) {
            addCriterion("END_TIME in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Long> values) {
            addCriterion("END_TIME not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Long value1, Long value2) {
            addCriterion("END_TIME between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Long value1, Long value2) {
            addCriterion("END_TIME not between", value1, value2, "endTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}