package com.jmda.metadata.generator;

import com.jmda.metadata.generator.util.GeneratorMetadata;

/**
 * Created by john on 2016/4/8.
 */
public class Cleaner {
    public static void main(String[] args) {
        GeneratorMetadata.remove();
    }
}
