package com.jmda.metadata.generator;

import com.jmda.metadata.generator.util.GeneratorConfigXml;
import com.jmda.metadata.generator.util.GeneratorMetadata;

/**
 * Created by john on 2016/4/8.
 */
public class Generator {
    public static void main(String[] args) {
        String generatorConfigPath = GeneratorConfigXml.generate();
        GeneratorMetadata.remove();
        GeneratorMetadata.generate(generatorConfigPath);
    }
}
