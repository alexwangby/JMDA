package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.CuOperateLogPO;
import com.jmda.common.model.po.CuOperateLogPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CuOperateLogPOMapper {
    int countByExample(CuOperateLogPOExample example);

    int deleteByExample(CuOperateLogPOExample example);

    int deleteByPrimaryKey(String operateLogId);

    int insert(CuOperateLogPO record);

    int insertSelective(CuOperateLogPO record);

    List<CuOperateLogPO> selectByExample(CuOperateLogPOExample example);

    CuOperateLogPO selectByPrimaryKey(String operateLogId);

    int updateByExampleSelective(@Param("record") CuOperateLogPO record, @Param("example") CuOperateLogPOExample example);

    int updateByExample(@Param("record") CuOperateLogPO record, @Param("example") CuOperateLogPOExample example);

    int updateByPrimaryKeySelective(CuOperateLogPO record);

    int updateByPrimaryKey(CuOperateLogPO record);
}