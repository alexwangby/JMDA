package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.SysSessionLogPO;
import com.jmda.common.model.po.SysSessionLogPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysSessionLogPOMapper {
    int countByExample(SysSessionLogPOExample example);

    int deleteByExample(SysSessionLogPOExample example);

    int deleteByPrimaryKey(String sessionLogId);

    int insert(SysSessionLogPO record);

    int insertSelective(SysSessionLogPO record);

    List<SysSessionLogPO> selectByExample(SysSessionLogPOExample example);

    SysSessionLogPO selectByPrimaryKey(String sessionLogId);

    int updateByExampleSelective(@Param("record") SysSessionLogPO record, @Param("example") SysSessionLogPOExample example);

    int updateByExample(@Param("record") SysSessionLogPO record, @Param("example") SysSessionLogPOExample example);

    int updateByPrimaryKeySelective(SysSessionLogPO record);

    int updateByPrimaryKey(SysSessionLogPO record);
}