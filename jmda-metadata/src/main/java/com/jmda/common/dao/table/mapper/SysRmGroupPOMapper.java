package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.SysRmGroupPO;
import com.jmda.common.model.po.SysRmGroupPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysRmGroupPOMapper {
    int countByExample(SysRmGroupPOExample example);

    int deleteByExample(SysRmGroupPOExample example);

    int deleteByPrimaryKey(String rmGroupId);

    int insert(SysRmGroupPO record);

    int insertSelective(SysRmGroupPO record);

    List<SysRmGroupPO> selectByExample(SysRmGroupPOExample example);

    SysRmGroupPO selectByPrimaryKey(String rmGroupId);

    int updateByExampleSelective(@Param("record") SysRmGroupPO record, @Param("example") SysRmGroupPOExample example);

    int updateByExample(@Param("record") SysRmGroupPO record, @Param("example") SysRmGroupPOExample example);

    int updateByPrimaryKeySelective(SysRmGroupPO record);

    int updateByPrimaryKey(SysRmGroupPO record);
}