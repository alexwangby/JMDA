package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.SysRmGroupPersonPOExample;
import com.jmda.common.model.po.SysRmGroupPersonPOKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysRmGroupPersonPOMapper {
    int countByExample(SysRmGroupPersonPOExample example);

    int deleteByExample(SysRmGroupPersonPOExample example);

    int deleteByPrimaryKey(SysRmGroupPersonPOKey key);

    int insert(SysRmGroupPersonPOKey record);

    int insertSelective(SysRmGroupPersonPOKey record);

    List<SysRmGroupPersonPOKey> selectByExample(SysRmGroupPersonPOExample example);

    int updateByExampleSelective(@Param("record") SysRmGroupPersonPOKey record, @Param("example") SysRmGroupPersonPOExample example);

    int updateByExample(@Param("record") SysRmGroupPersonPOKey record, @Param("example") SysRmGroupPersonPOExample example);
}