package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.SysDepartmentPO;
import com.jmda.common.model.po.SysDepartmentPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysDepartmentPOMapper {
    int countByExample(SysDepartmentPOExample example);

    int deleteByExample(SysDepartmentPOExample example);

    int deleteByPrimaryKey(String departmentId);

    int insert(SysDepartmentPO record);

    int insertSelective(SysDepartmentPO record);

    List<SysDepartmentPO> selectByExample(SysDepartmentPOExample example);

    SysDepartmentPO selectByPrimaryKey(String departmentId);

    int updateByExampleSelective(@Param("record") SysDepartmentPO record, @Param("example") SysDepartmentPOExample example);

    int updateByExample(@Param("record") SysDepartmentPO record, @Param("example") SysDepartmentPOExample example);

    int updateByPrimaryKeySelective(SysDepartmentPO record);

    int updateByPrimaryKey(SysDepartmentPO record);
}