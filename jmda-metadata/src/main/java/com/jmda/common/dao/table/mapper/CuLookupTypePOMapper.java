package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.CuLookupTypePO;
import com.jmda.common.model.po.CuLookupTypePOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CuLookupTypePOMapper {
    int countByExample(CuLookupTypePOExample example);

    int deleteByExample(CuLookupTypePOExample example);

    int deleteByPrimaryKey(String lookupType);

    int insert(CuLookupTypePO record);

    int insertSelective(CuLookupTypePO record);

    List<CuLookupTypePO> selectByExample(CuLookupTypePOExample example);

    CuLookupTypePO selectByPrimaryKey(String lookupType);

    int updateByExampleSelective(@Param("record") CuLookupTypePO record, @Param("example") CuLookupTypePOExample example);

    int updateByExample(@Param("record") CuLookupTypePO record, @Param("example") CuLookupTypePOExample example);

    int updateByPrimaryKeySelective(CuLookupTypePO record);

    int updateByPrimaryKey(CuLookupTypePO record);
}