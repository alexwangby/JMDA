package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.SysCompanyPO;
import com.jmda.common.model.po.SysCompanyPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysCompanyPOMapper {
    int countByExample(SysCompanyPOExample example);

    int deleteByExample(SysCompanyPOExample example);

    int deleteByPrimaryKey(String companyId);

    int insert(SysCompanyPO record);

    int insertSelective(SysCompanyPO record);

    List<SysCompanyPO> selectByExample(SysCompanyPOExample example);

    SysCompanyPO selectByPrimaryKey(String companyId);

    int updateByExampleSelective(@Param("record") SysCompanyPO record, @Param("example") SysCompanyPOExample example);

    int updateByExample(@Param("record") SysCompanyPO record, @Param("example") SysCompanyPOExample example);

    int updateByPrimaryKeySelective(SysCompanyPO record);

    int updateByPrimaryKey(SysCompanyPO record);
}