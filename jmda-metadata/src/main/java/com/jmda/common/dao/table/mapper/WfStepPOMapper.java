package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.WfStepPO;
import com.jmda.common.model.po.WfStepPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WfStepPOMapper {
    int countByExample(WfStepPOExample example);

    int deleteByExample(WfStepPOExample example);

    int deleteByPrimaryKey(String stepId);

    int insert(WfStepPO record);

    int insertSelective(WfStepPO record);

    List<WfStepPO> selectByExample(WfStepPOExample example);

    WfStepPO selectByPrimaryKey(String stepId);

    int updateByExampleSelective(@Param("record") WfStepPO record, @Param("example") WfStepPOExample example);

    int updateByExample(@Param("record") WfStepPO record, @Param("example") WfStepPOExample example);

    int updateByPrimaryKeySelective(WfStepPO record);

    int updateByPrimaryKey(WfStepPO record);
}