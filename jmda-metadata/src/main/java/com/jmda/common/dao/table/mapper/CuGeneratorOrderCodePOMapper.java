package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.CuGeneratorOrderCodePO;
import com.jmda.common.model.po.CuGeneratorOrderCodePOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CuGeneratorOrderCodePOMapper {
    int countByExample(CuGeneratorOrderCodePOExample example);

    int deleteByExample(CuGeneratorOrderCodePOExample example);

    int deleteByPrimaryKey(String orderCodeId);

    int insert(CuGeneratorOrderCodePO record);

    int insertSelective(CuGeneratorOrderCodePO record);

    List<CuGeneratorOrderCodePO> selectByExample(CuGeneratorOrderCodePOExample example);

    CuGeneratorOrderCodePO selectByPrimaryKey(String orderCodeId);

    int updateByExampleSelective(@Param("record") CuGeneratorOrderCodePO record, @Param("example") CuGeneratorOrderCodePOExample example);

    int updateByExample(@Param("record") CuGeneratorOrderCodePO record, @Param("example") CuGeneratorOrderCodePOExample example);

    int updateByPrimaryKeySelective(CuGeneratorOrderCodePO record);

    int updateByPrimaryKey(CuGeneratorOrderCodePO record);
}