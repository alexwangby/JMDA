package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.SysRmPO;
import com.jmda.common.model.po.SysRmPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysRmPOMapper {
    int countByExample(SysRmPOExample example);

    int deleteByExample(SysRmPOExample example);

    int deleteByPrimaryKey(String rmId);

    int insert(SysRmPO record);

    int insertSelective(SysRmPO record);

    List<SysRmPO> selectByExample(SysRmPOExample example);

    SysRmPO selectByPrimaryKey(String rmId);

    int updateByExampleSelective(@Param("record") SysRmPO record, @Param("example") SysRmPOExample example);

    int updateByExample(@Param("record") SysRmPO record, @Param("example") SysRmPOExample example);

    int updateByPrimaryKeySelective(SysRmPO record);

    int updateByPrimaryKey(SysRmPO record);
}