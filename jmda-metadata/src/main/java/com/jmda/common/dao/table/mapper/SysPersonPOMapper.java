package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.SysPersonPO;
import com.jmda.common.model.po.SysPersonPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysPersonPOMapper {
    int countByExample(SysPersonPOExample example);

    int deleteByExample(SysPersonPOExample example);

    int deleteByPrimaryKey(String personId);

    int insert(SysPersonPO record);

    int insertSelective(SysPersonPO record);

    List<SysPersonPO> selectByExample(SysPersonPOExample example);

    SysPersonPO selectByPrimaryKey(String personId);

    int updateByExampleSelective(@Param("record") SysPersonPO record, @Param("example") SysPersonPOExample example);

    int updateByExample(@Param("record") SysPersonPO record, @Param("example") SysPersonPOExample example);

    int updateByPrimaryKeySelective(SysPersonPO record);

    int updateByPrimaryKey(SysPersonPO record);
}