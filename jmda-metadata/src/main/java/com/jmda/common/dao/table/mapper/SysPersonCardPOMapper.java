package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.SysPersonCardPO;
import com.jmda.common.model.po.SysPersonCardPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysPersonCardPOMapper {
    int countByExample(SysPersonCardPOExample example);

    int deleteByExample(SysPersonCardPOExample example);

    int insert(SysPersonCardPO record);

    int insertSelective(SysPersonCardPO record);

    List<SysPersonCardPO> selectByExample(SysPersonCardPOExample example);

    int updateByExampleSelective(@Param("record") SysPersonCardPO record, @Param("example") SysPersonCardPOExample example);

    int updateByExample(@Param("record") SysPersonCardPO record, @Param("example") SysPersonCardPOExample example);
}