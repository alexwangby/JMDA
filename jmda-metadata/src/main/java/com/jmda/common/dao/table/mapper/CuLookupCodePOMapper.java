package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.CuLookupCodePO;
import com.jmda.common.model.po.CuLookupCodePOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CuLookupCodePOMapper {
    int countByExample(CuLookupCodePOExample example);

    int deleteByExample(CuLookupCodePOExample example);

    int deleteByPrimaryKey(String lookupCode);

    int insert(CuLookupCodePO record);

    int insertSelective(CuLookupCodePO record);

    List<CuLookupCodePO> selectByExample(CuLookupCodePOExample example);

    CuLookupCodePO selectByPrimaryKey(String lookupCode);

    int updateByExampleSelective(@Param("record") CuLookupCodePO record, @Param("example") CuLookupCodePOExample example);

    int updateByExample(@Param("record") CuLookupCodePO record, @Param("example") CuLookupCodePOExample example);

    int updateByPrimaryKeySelective(CuLookupCodePO record);

    int updateByPrimaryKey(CuLookupCodePO record);
}