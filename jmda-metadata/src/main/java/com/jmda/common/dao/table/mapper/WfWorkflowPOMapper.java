package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.WfWorkflowPO;
import com.jmda.common.model.po.WfWorkflowPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WfWorkflowPOMapper {
    int countByExample(WfWorkflowPOExample example);

    int deleteByExample(WfWorkflowPOExample example);

    int deleteByPrimaryKey(String workflowId);

    int insert(WfWorkflowPO record);

    int insertSelective(WfWorkflowPO record);

    List<WfWorkflowPO> selectByExample(WfWorkflowPOExample example);

    WfWorkflowPO selectByPrimaryKey(String workflowId);

    int updateByExampleSelective(@Param("record") WfWorkflowPO record, @Param("example") WfWorkflowPOExample example);

    int updateByExample(@Param("record") WfWorkflowPO record, @Param("example") WfWorkflowPOExample example);

    int updateByPrimaryKeySelective(WfWorkflowPO record);

    int updateByPrimaryKey(WfWorkflowPO record);
}