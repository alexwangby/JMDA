package com.jmda.common.dao.table.mapper;

import com.jmda.common.model.po.SysRmGroupRmPOExample;
import com.jmda.common.model.po.SysRmGroupRmPOKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysRmGroupRmPOMapper {
    int countByExample(SysRmGroupRmPOExample example);

    int deleteByExample(SysRmGroupRmPOExample example);

    int deleteByPrimaryKey(SysRmGroupRmPOKey key);

    int insert(SysRmGroupRmPOKey record);

    int insertSelective(SysRmGroupRmPOKey record);

    List<SysRmGroupRmPOKey> selectByExample(SysRmGroupRmPOExample example);

    int updateByExampleSelective(@Param("record") SysRmGroupRmPOKey record, @Param("example") SysRmGroupRmPOExample example);

    int updateByExample(@Param("record") SysRmGroupRmPOKey record, @Param("example") SysRmGroupRmPOExample example);
}