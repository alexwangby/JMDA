package com.jmda.common.model.po;

public class SysRmGroupPersonPOKey {
    private String rmGroupId;

    private String personId;

    public String getRmGroupId() {
        return rmGroupId;
    }

    public void setRmGroupId(String rmGroupId) {
        this.rmGroupId = rmGroupId == null ? null : rmGroupId.trim();
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId == null ? null : personId.trim();
    }
}