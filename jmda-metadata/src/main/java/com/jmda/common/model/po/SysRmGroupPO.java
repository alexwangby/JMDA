package com.jmda.common.model.po;

public class SysRmGroupPO {
    private String rmGroupId;

    private String groupCode;

    private String name;

    private Boolean isDelete;

    private Long createTime;

    private Long updateTime;

    public String getRmGroupId() {
        return rmGroupId;
    }

    public void setRmGroupId(String rmGroupId) {
        this.rmGroupId = rmGroupId == null ? null : rmGroupId.trim();
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode == null ? null : groupCode.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
}