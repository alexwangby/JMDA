package com.jmda.common.model.po;

public class CuGeneratorOrderCodePO {
    private String orderCodeId;

    private String name;

    private String expression;

    private String currentCode;

    private Integer digits;

    private String digitsValue;

    private Long sequenceCurrent;

    public String getOrderCodeId() {
        return orderCodeId;
    }

    public void setOrderCodeId(String orderCodeId) {
        this.orderCodeId = orderCodeId == null ? null : orderCodeId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression == null ? null : expression.trim();
    }

    public String getCurrentCode() {
        return currentCode;
    }

    public void setCurrentCode(String currentCode) {
        this.currentCode = currentCode == null ? null : currentCode.trim();
    }

    public Integer getDigits() {
        return digits;
    }

    public void setDigits(Integer digits) {
        this.digits = digits;
    }

    public String getDigitsValue() {
        return digitsValue;
    }

    public void setDigitsValue(String digitsValue) {
        this.digitsValue = digitsValue == null ? null : digitsValue.trim();
    }

    public Long getSequenceCurrent() {
        return sequenceCurrent;
    }

    public void setSequenceCurrent(Long sequenceCurrent) {
        this.sequenceCurrent = sequenceCurrent;
    }
}