package com.jmda.common.model.po;

public class CuGeneratorKeyCodePO {
    private String keyCodeId;

    private String name;

    private String expression;

    private String currentKey;

    private String dateCode;

    private String type;

    private Integer digits;

    private String digitsValue;

    private Long sequenceCurrent;

    public String getKeyCodeId() {
        return keyCodeId;
    }

    public void setKeyCodeId(String keyCodeId) {
        this.keyCodeId = keyCodeId == null ? null : keyCodeId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression == null ? null : expression.trim();
    }

    public String getCurrentKey() {
        return currentKey;
    }

    public void setCurrentKey(String currentKey) {
        this.currentKey = currentKey == null ? null : currentKey.trim();
    }

    public String getDateCode() {
        return dateCode;
    }

    public void setDateCode(String dateCode) {
        this.dateCode = dateCode == null ? null : dateCode.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Integer getDigits() {
        return digits;
    }

    public void setDigits(Integer digits) {
        this.digits = digits;
    }

    public String getDigitsValue() {
        return digitsValue;
    }

    public void setDigitsValue(String digitsValue) {
        this.digitsValue = digitsValue == null ? null : digitsValue.trim();
    }

    public Long getSequenceCurrent() {
        return sequenceCurrent;
    }

    public void setSequenceCurrent(Long sequenceCurrent) {
        this.sequenceCurrent = sequenceCurrent;
    }
}