package com.jmda.common.model.po;

import java.util.ArrayList;
import java.util.List;

public class SysRmGroupRmPOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SysRmGroupRmPOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRmGroupIdIsNull() {
            addCriterion("RM_GROUP_ID is null");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdIsNotNull() {
            addCriterion("RM_GROUP_ID is not null");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdEqualTo(String value) {
            addCriterion("RM_GROUP_ID =", value, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdNotEqualTo(String value) {
            addCriterion("RM_GROUP_ID <>", value, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdGreaterThan(String value) {
            addCriterion("RM_GROUP_ID >", value, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdGreaterThanOrEqualTo(String value) {
            addCriterion("RM_GROUP_ID >=", value, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdLessThan(String value) {
            addCriterion("RM_GROUP_ID <", value, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdLessThanOrEqualTo(String value) {
            addCriterion("RM_GROUP_ID <=", value, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdLike(String value) {
            addCriterion("RM_GROUP_ID like", value, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdNotLike(String value) {
            addCriterion("RM_GROUP_ID not like", value, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdIn(List<String> values) {
            addCriterion("RM_GROUP_ID in", values, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdNotIn(List<String> values) {
            addCriterion("RM_GROUP_ID not in", values, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdBetween(String value1, String value2) {
            addCriterion("RM_GROUP_ID between", value1, value2, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmGroupIdNotBetween(String value1, String value2) {
            addCriterion("RM_GROUP_ID not between", value1, value2, "rmGroupId");
            return (Criteria) this;
        }

        public Criteria andRmIdIsNull() {
            addCriterion("RM_ID is null");
            return (Criteria) this;
        }

        public Criteria andRmIdIsNotNull() {
            addCriterion("RM_ID is not null");
            return (Criteria) this;
        }

        public Criteria andRmIdEqualTo(String value) {
            addCriterion("RM_ID =", value, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdNotEqualTo(String value) {
            addCriterion("RM_ID <>", value, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdGreaterThan(String value) {
            addCriterion("RM_ID >", value, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdGreaterThanOrEqualTo(String value) {
            addCriterion("RM_ID >=", value, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdLessThan(String value) {
            addCriterion("RM_ID <", value, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdLessThanOrEqualTo(String value) {
            addCriterion("RM_ID <=", value, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdLike(String value) {
            addCriterion("RM_ID like", value, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdNotLike(String value) {
            addCriterion("RM_ID not like", value, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdIn(List<String> values) {
            addCriterion("RM_ID in", values, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdNotIn(List<String> values) {
            addCriterion("RM_ID not in", values, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdBetween(String value1, String value2) {
            addCriterion("RM_ID between", value1, value2, "rmId");
            return (Criteria) this;
        }

        public Criteria andRmIdNotBetween(String value1, String value2) {
            addCriterion("RM_ID not between", value1, value2, "rmId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}