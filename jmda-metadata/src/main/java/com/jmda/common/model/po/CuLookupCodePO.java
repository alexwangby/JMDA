package com.jmda.common.model.po;

public class CuLookupCodePO {
    private String lookupCode;

    private String lookupType;

    private String codeName;

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode == null ? null : lookupCode.trim();
    }

    public String getLookupType() {
        return lookupType;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType == null ? null : lookupType.trim();
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName == null ? null : codeName.trim();
    }
}