package com.jmda.common.model.po;

import java.util.ArrayList;
import java.util.List;

public class CuGeneratorKeyCodePOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CuGeneratorKeyCodePOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andKeyCodeIdIsNull() {
            addCriterion("KEY_CODE_ID is null");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdIsNotNull() {
            addCriterion("KEY_CODE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdEqualTo(String value) {
            addCriterion("KEY_CODE_ID =", value, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdNotEqualTo(String value) {
            addCriterion("KEY_CODE_ID <>", value, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdGreaterThan(String value) {
            addCriterion("KEY_CODE_ID >", value, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdGreaterThanOrEqualTo(String value) {
            addCriterion("KEY_CODE_ID >=", value, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdLessThan(String value) {
            addCriterion("KEY_CODE_ID <", value, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdLessThanOrEqualTo(String value) {
            addCriterion("KEY_CODE_ID <=", value, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdLike(String value) {
            addCriterion("KEY_CODE_ID like", value, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdNotLike(String value) {
            addCriterion("KEY_CODE_ID not like", value, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdIn(List<String> values) {
            addCriterion("KEY_CODE_ID in", values, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdNotIn(List<String> values) {
            addCriterion("KEY_CODE_ID not in", values, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdBetween(String value1, String value2) {
            addCriterion("KEY_CODE_ID between", value1, value2, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andKeyCodeIdNotBetween(String value1, String value2) {
            addCriterion("KEY_CODE_ID not between", value1, value2, "keyCodeId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andExpressionIsNull() {
            addCriterion("EXPRESSION is null");
            return (Criteria) this;
        }

        public Criteria andExpressionIsNotNull() {
            addCriterion("EXPRESSION is not null");
            return (Criteria) this;
        }

        public Criteria andExpressionEqualTo(String value) {
            addCriterion("EXPRESSION =", value, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionNotEqualTo(String value) {
            addCriterion("EXPRESSION <>", value, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionGreaterThan(String value) {
            addCriterion("EXPRESSION >", value, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionGreaterThanOrEqualTo(String value) {
            addCriterion("EXPRESSION >=", value, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionLessThan(String value) {
            addCriterion("EXPRESSION <", value, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionLessThanOrEqualTo(String value) {
            addCriterion("EXPRESSION <=", value, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionLike(String value) {
            addCriterion("EXPRESSION like", value, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionNotLike(String value) {
            addCriterion("EXPRESSION not like", value, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionIn(List<String> values) {
            addCriterion("EXPRESSION in", values, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionNotIn(List<String> values) {
            addCriterion("EXPRESSION not in", values, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionBetween(String value1, String value2) {
            addCriterion("EXPRESSION between", value1, value2, "expression");
            return (Criteria) this;
        }

        public Criteria andExpressionNotBetween(String value1, String value2) {
            addCriterion("EXPRESSION not between", value1, value2, "expression");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyIsNull() {
            addCriterion("CURRENT_KEY is null");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyIsNotNull() {
            addCriterion("CURRENT_KEY is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyEqualTo(String value) {
            addCriterion("CURRENT_KEY =", value, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyNotEqualTo(String value) {
            addCriterion("CURRENT_KEY <>", value, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyGreaterThan(String value) {
            addCriterion("CURRENT_KEY >", value, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyGreaterThanOrEqualTo(String value) {
            addCriterion("CURRENT_KEY >=", value, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyLessThan(String value) {
            addCriterion("CURRENT_KEY <", value, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyLessThanOrEqualTo(String value) {
            addCriterion("CURRENT_KEY <=", value, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyLike(String value) {
            addCriterion("CURRENT_KEY like", value, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyNotLike(String value) {
            addCriterion("CURRENT_KEY not like", value, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyIn(List<String> values) {
            addCriterion("CURRENT_KEY in", values, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyNotIn(List<String> values) {
            addCriterion("CURRENT_KEY not in", values, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyBetween(String value1, String value2) {
            addCriterion("CURRENT_KEY between", value1, value2, "currentKey");
            return (Criteria) this;
        }

        public Criteria andCurrentKeyNotBetween(String value1, String value2) {
            addCriterion("CURRENT_KEY not between", value1, value2, "currentKey");
            return (Criteria) this;
        }

        public Criteria andDateCodeIsNull() {
            addCriterion("DATE_CODE is null");
            return (Criteria) this;
        }

        public Criteria andDateCodeIsNotNull() {
            addCriterion("DATE_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andDateCodeEqualTo(String value) {
            addCriterion("DATE_CODE =", value, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeNotEqualTo(String value) {
            addCriterion("DATE_CODE <>", value, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeGreaterThan(String value) {
            addCriterion("DATE_CODE >", value, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeGreaterThanOrEqualTo(String value) {
            addCriterion("DATE_CODE >=", value, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeLessThan(String value) {
            addCriterion("DATE_CODE <", value, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeLessThanOrEqualTo(String value) {
            addCriterion("DATE_CODE <=", value, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeLike(String value) {
            addCriterion("DATE_CODE like", value, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeNotLike(String value) {
            addCriterion("DATE_CODE not like", value, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeIn(List<String> values) {
            addCriterion("DATE_CODE in", values, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeNotIn(List<String> values) {
            addCriterion("DATE_CODE not in", values, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeBetween(String value1, String value2) {
            addCriterion("DATE_CODE between", value1, value2, "dateCode");
            return (Criteria) this;
        }

        public Criteria andDateCodeNotBetween(String value1, String value2) {
            addCriterion("DATE_CODE not between", value1, value2, "dateCode");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("TYPE is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("TYPE =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("TYPE <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("TYPE >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("TYPE >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("TYPE <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("TYPE <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("TYPE like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("TYPE not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("TYPE in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("TYPE not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("TYPE between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("TYPE not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andDigitsIsNull() {
            addCriterion("DIGITS is null");
            return (Criteria) this;
        }

        public Criteria andDigitsIsNotNull() {
            addCriterion("DIGITS is not null");
            return (Criteria) this;
        }

        public Criteria andDigitsEqualTo(Integer value) {
            addCriterion("DIGITS =", value, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsNotEqualTo(Integer value) {
            addCriterion("DIGITS <>", value, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsGreaterThan(Integer value) {
            addCriterion("DIGITS >", value, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsGreaterThanOrEqualTo(Integer value) {
            addCriterion("DIGITS >=", value, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsLessThan(Integer value) {
            addCriterion("DIGITS <", value, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsLessThanOrEqualTo(Integer value) {
            addCriterion("DIGITS <=", value, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsIn(List<Integer> values) {
            addCriterion("DIGITS in", values, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsNotIn(List<Integer> values) {
            addCriterion("DIGITS not in", values, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsBetween(Integer value1, Integer value2) {
            addCriterion("DIGITS between", value1, value2, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsNotBetween(Integer value1, Integer value2) {
            addCriterion("DIGITS not between", value1, value2, "digits");
            return (Criteria) this;
        }

        public Criteria andDigitsValueIsNull() {
            addCriterion("DIGITS_VALUE is null");
            return (Criteria) this;
        }

        public Criteria andDigitsValueIsNotNull() {
            addCriterion("DIGITS_VALUE is not null");
            return (Criteria) this;
        }

        public Criteria andDigitsValueEqualTo(String value) {
            addCriterion("DIGITS_VALUE =", value, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueNotEqualTo(String value) {
            addCriterion("DIGITS_VALUE <>", value, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueGreaterThan(String value) {
            addCriterion("DIGITS_VALUE >", value, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueGreaterThanOrEqualTo(String value) {
            addCriterion("DIGITS_VALUE >=", value, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueLessThan(String value) {
            addCriterion("DIGITS_VALUE <", value, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueLessThanOrEqualTo(String value) {
            addCriterion("DIGITS_VALUE <=", value, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueLike(String value) {
            addCriterion("DIGITS_VALUE like", value, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueNotLike(String value) {
            addCriterion("DIGITS_VALUE not like", value, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueIn(List<String> values) {
            addCriterion("DIGITS_VALUE in", values, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueNotIn(List<String> values) {
            addCriterion("DIGITS_VALUE not in", values, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueBetween(String value1, String value2) {
            addCriterion("DIGITS_VALUE between", value1, value2, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andDigitsValueNotBetween(String value1, String value2) {
            addCriterion("DIGITS_VALUE not between", value1, value2, "digitsValue");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentIsNull() {
            addCriterion("SEQUENCE_CURRENT is null");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentIsNotNull() {
            addCriterion("SEQUENCE_CURRENT is not null");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentEqualTo(Long value) {
            addCriterion("SEQUENCE_CURRENT =", value, "sequenceCurrent");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentNotEqualTo(Long value) {
            addCriterion("SEQUENCE_CURRENT <>", value, "sequenceCurrent");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentGreaterThan(Long value) {
            addCriterion("SEQUENCE_CURRENT >", value, "sequenceCurrent");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentGreaterThanOrEqualTo(Long value) {
            addCriterion("SEQUENCE_CURRENT >=", value, "sequenceCurrent");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentLessThan(Long value) {
            addCriterion("SEQUENCE_CURRENT <", value, "sequenceCurrent");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentLessThanOrEqualTo(Long value) {
            addCriterion("SEQUENCE_CURRENT <=", value, "sequenceCurrent");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentIn(List<Long> values) {
            addCriterion("SEQUENCE_CURRENT in", values, "sequenceCurrent");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentNotIn(List<Long> values) {
            addCriterion("SEQUENCE_CURRENT not in", values, "sequenceCurrent");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentBetween(Long value1, Long value2) {
            addCriterion("SEQUENCE_CURRENT between", value1, value2, "sequenceCurrent");
            return (Criteria) this;
        }

        public Criteria andSequenceCurrentNotBetween(Long value1, Long value2) {
            addCriterion("SEQUENCE_CURRENT not between", value1, value2, "sequenceCurrent");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}