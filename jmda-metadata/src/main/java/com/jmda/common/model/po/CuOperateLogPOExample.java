package com.jmda.common.model.po;

import java.util.ArrayList;
import java.util.List;

public class CuOperateLogPOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CuOperateLogPOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOperateLogIdIsNull() {
            addCriterion("OPERATE_LOG_ID is null");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdIsNotNull() {
            addCriterion("OPERATE_LOG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdEqualTo(String value) {
            addCriterion("OPERATE_LOG_ID =", value, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdNotEqualTo(String value) {
            addCriterion("OPERATE_LOG_ID <>", value, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdGreaterThan(String value) {
            addCriterion("OPERATE_LOG_ID >", value, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATE_LOG_ID >=", value, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdLessThan(String value) {
            addCriterion("OPERATE_LOG_ID <", value, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdLessThanOrEqualTo(String value) {
            addCriterion("OPERATE_LOG_ID <=", value, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdLike(String value) {
            addCriterion("OPERATE_LOG_ID like", value, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdNotLike(String value) {
            addCriterion("OPERATE_LOG_ID not like", value, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdIn(List<String> values) {
            addCriterion("OPERATE_LOG_ID in", values, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdNotIn(List<String> values) {
            addCriterion("OPERATE_LOG_ID not in", values, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdBetween(String value1, String value2) {
            addCriterion("OPERATE_LOG_ID between", value1, value2, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andOperateLogIdNotBetween(String value1, String value2) {
            addCriterion("OPERATE_LOG_ID not between", value1, value2, "operateLogId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("PERSON_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("PERSON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(String value) {
            addCriterion("PERSON_ID =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(String value) {
            addCriterion("PERSON_ID <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(String value) {
            addCriterion("PERSON_ID >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(String value) {
            addCriterion("PERSON_ID >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(String value) {
            addCriterion("PERSON_ID <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(String value) {
            addCriterion("PERSON_ID <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLike(String value) {
            addCriterion("PERSON_ID like", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotLike(String value) {
            addCriterion("PERSON_ID not like", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<String> values) {
            addCriterion("PERSON_ID in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<String> values) {
            addCriterion("PERSON_ID not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(String value1, String value2) {
            addCriterion("PERSON_ID between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(String value1, String value2) {
            addCriterion("PERSON_ID not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonNameIsNull() {
            addCriterion("PERSON_NAME is null");
            return (Criteria) this;
        }

        public Criteria andPersonNameIsNotNull() {
            addCriterion("PERSON_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andPersonNameEqualTo(String value) {
            addCriterion("PERSON_NAME =", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameNotEqualTo(String value) {
            addCriterion("PERSON_NAME <>", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameGreaterThan(String value) {
            addCriterion("PERSON_NAME >", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameGreaterThanOrEqualTo(String value) {
            addCriterion("PERSON_NAME >=", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameLessThan(String value) {
            addCriterion("PERSON_NAME <", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameLessThanOrEqualTo(String value) {
            addCriterion("PERSON_NAME <=", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameLike(String value) {
            addCriterion("PERSON_NAME like", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameNotLike(String value) {
            addCriterion("PERSON_NAME not like", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameIn(List<String> values) {
            addCriterion("PERSON_NAME in", values, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameNotIn(List<String> values) {
            addCriterion("PERSON_NAME not in", values, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameBetween(String value1, String value2) {
            addCriterion("PERSON_NAME between", value1, value2, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameNotBetween(String value1, String value2) {
            addCriterion("PERSON_NAME not between", value1, value2, "personName");
            return (Criteria) this;
        }

        public Criteria andOperateTableIsNull() {
            addCriterion("OPERATE_TABLE is null");
            return (Criteria) this;
        }

        public Criteria andOperateTableIsNotNull() {
            addCriterion("OPERATE_TABLE is not null");
            return (Criteria) this;
        }

        public Criteria andOperateTableEqualTo(String value) {
            addCriterion("OPERATE_TABLE =", value, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableNotEqualTo(String value) {
            addCriterion("OPERATE_TABLE <>", value, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableGreaterThan(String value) {
            addCriterion("OPERATE_TABLE >", value, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATE_TABLE >=", value, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableLessThan(String value) {
            addCriterion("OPERATE_TABLE <", value, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableLessThanOrEqualTo(String value) {
            addCriterion("OPERATE_TABLE <=", value, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableLike(String value) {
            addCriterion("OPERATE_TABLE like", value, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableNotLike(String value) {
            addCriterion("OPERATE_TABLE not like", value, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableIn(List<String> values) {
            addCriterion("OPERATE_TABLE in", values, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableNotIn(List<String> values) {
            addCriterion("OPERATE_TABLE not in", values, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableBetween(String value1, String value2) {
            addCriterion("OPERATE_TABLE between", value1, value2, "operateTable");
            return (Criteria) this;
        }

        public Criteria andOperateTableNotBetween(String value1, String value2) {
            addCriterion("OPERATE_TABLE not between", value1, value2, "operateTable");
            return (Criteria) this;
        }

        public Criteria andBussinessIdIsNull() {
            addCriterion("BUSSINESS_ID is null");
            return (Criteria) this;
        }

        public Criteria andBussinessIdIsNotNull() {
            addCriterion("BUSSINESS_ID is not null");
            return (Criteria) this;
        }

        public Criteria andBussinessIdEqualTo(String value) {
            addCriterion("BUSSINESS_ID =", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdNotEqualTo(String value) {
            addCriterion("BUSSINESS_ID <>", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdGreaterThan(String value) {
            addCriterion("BUSSINESS_ID >", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdGreaterThanOrEqualTo(String value) {
            addCriterion("BUSSINESS_ID >=", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdLessThan(String value) {
            addCriterion("BUSSINESS_ID <", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdLessThanOrEqualTo(String value) {
            addCriterion("BUSSINESS_ID <=", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdLike(String value) {
            addCriterion("BUSSINESS_ID like", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdNotLike(String value) {
            addCriterion("BUSSINESS_ID not like", value, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdIn(List<String> values) {
            addCriterion("BUSSINESS_ID in", values, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdNotIn(List<String> values) {
            addCriterion("BUSSINESS_ID not in", values, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdBetween(String value1, String value2) {
            addCriterion("BUSSINESS_ID between", value1, value2, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andBussinessIdNotBetween(String value1, String value2) {
            addCriterion("BUSSINESS_ID not between", value1, value2, "bussinessId");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("CREATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("CREATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Long value) {
            addCriterion("CREATE_TIME =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Long value) {
            addCriterion("CREATE_TIME <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Long value) {
            addCriterion("CREATE_TIME >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("CREATE_TIME >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Long value) {
            addCriterion("CREATE_TIME <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Long value) {
            addCriterion("CREATE_TIME <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Long> values) {
            addCriterion("CREATE_TIME in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Long> values) {
            addCriterion("CREATE_TIME not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Long value1, Long value2) {
            addCriterion("CREATE_TIME between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Long value1, Long value2) {
            addCriterion("CREATE_TIME not between", value1, value2, "createTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}