package com.jmda.common.model.po;

public class SysRmGroupRmPOKey {
    private String rmGroupId;

    private String rmId;

    public String getRmGroupId() {
        return rmGroupId;
    }

    public void setRmGroupId(String rmGroupId) {
        this.rmGroupId = rmGroupId == null ? null : rmGroupId.trim();
    }

    public String getRmId() {
        return rmId;
    }

    public void setRmId(String rmId) {
        this.rmId = rmId == null ? null : rmId.trim();
    }
}