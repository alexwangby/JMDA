drop table if exists CU_CACHE;

drop table if exists CU_GENERATOR_KEY_CODE;

drop table if exists CU_JOB;

drop table if exists CU_JOB_LOGS;

drop table if exists CU_LOOKUP_CODE;

drop table if exists CU_LOOKUP_TYPE;

drop table if exists WF_STEP;

drop table if exists WF_TASK_LOG;

drop table if exists WF_WORKFLOW;

/*==============================================================*/
/* Table: CU_CACHE                                              */
/*==============================================================*/
create table CU_CACHE
(
   CACHE_ID             varchar(32) not null comment '缓存标识',
   NAME                 varchar(128) comment '名称',
   FIELD_COLUMN         varchar(128) comment '标识字段名称',
   CACHE_SQL            varchar(255) comment '查询SQL',
   DATA_SOURCE          varchar(255) comment '数据源',
   CACHE_DESC           varchar(255) comment '查询描述',
   primary key (CACHE_ID)
);

alter table CU_CACHE comment 'CU-缓存服务 ';

/*==============================================================*/
/* Table: CU_GENERATOR_KEY_CODE                                 */
/*==============================================================*/
create table CU_GENERATOR_KEY_CODE
(
   KEY_CODE_ID          varchar(64) not null comment '主键生成器编码',
   NAME                 varchar(64) comment '生成器名称',
   EXPRESSION           varchar(255) comment '表达式',
   CURRENT_KEY          varchar(64) comment '当前主键值',
   DATE_CODE            varchar(64) comment '日期编码',
   TYPE                 varchar(64) comment '序列类型',
   DIGITS               int comment '进制',
   DIGITS_VALUE         varchar(64) comment '进制值',
   SEQUENCE_CURRENT     bigint comment '当前序列',
   primary key (KEY_CODE_ID)
);

alter table CU_GENERATOR_KEY_CODE comment 'CU-主键生成器表';

/*==============================================================*/
/* Table: CU_JOB                                                */
/*==============================================================*/
create table CU_JOB
(
   JOB_ID               varchar(32) not null comment '任务标识',
   NAME                 varchar(128) comment '任务名称',
   IMP_CLASS_PATH       varchar(255) comment '实现类路径',
   TIME_EXPRESSION      varchar(255) comment '时间表达式',
   DATA_SOURCE          varchar(255) comment '数据源',
   JOB_STATUS           char(3) comment '任务状态',
   PRIORITY             int comment '优先级',
   CREATOR_ID           varchar(32) comment '创建人标识',
   CREATE_TIME          bigint comment '创建时间',
   UPDATE_TIME          bigint comment '修改时间',
   RUN_STATUS           char(3) comment '任务状态',
   primary key (JOB_ID)
);

alter table CU_JOB comment 'CU-定时器任务表 ';

/*==============================================================*/
/* Table: CU_JOB_LOGS                                           */
/*==============================================================*/
create table CU_JOB_LOGS
(
   JOB_LOGS_ID          varchar(32) not null comment '日志标识',
   IMP_CLASS_PATH       varchar(255) comment '实现类路径',
   START_TIME           bigint comment '开始时间',
   END_TIME             bigint comment '结束时间',
   JOB_STATUS           char(3) comment '执行状态',
   primary key (JOB_LOGS_ID)
);

alter table CU_JOB_LOGS comment 'CU-定时器任务日志表 ';

/*==============================================================*/
/* Table: CU_LOOKUP_CODE                                        */
/*==============================================================*/
create table CU_LOOKUP_CODE
(
   LOOKUP_CODE          char(3) not null comment '二级编码',
   LOOKUP_TYPE          char(2) comment '一级编码',
   CODE_NAME            varchar(128) comment '二级编码名称',
   primary key (LOOKUP_CODE)
);

alter table CU_LOOKUP_CODE comment '二级码表';

/*==============================================================*/
/* Table: CU_LOOKUP_TYPE                                        */
/*==============================================================*/
create table CU_LOOKUP_TYPE
(
   LOOKUP_TYPE          char(2) not null comment '一级编码',
   TYPE_NAME            varchar(128) comment '一级编码名称',
   primary key (LOOKUP_TYPE)
);

alter table CU_LOOKUP_TYPE comment '一级码表 ';

/*==============================================================*/
/* Table: WF_STEP                                               */
/*==============================================================*/
create table WF_STEP
(
   STEP_ID              varchar(16) not null comment '步骤标识',
   WORKFLOW_ID          varchar(12) comment '流程标识',
   NAME                 varchar(128) comment '名称',
   STEP_NO              int comment '步骤序号',
   primary key (STEP_ID)
);

alter table WF_STEP comment 'WF-步骤定义表 ';

/*==============================================================*/
/* Table: WF_TASK_LOG                                           */
/*==============================================================*/
create table WF_TASK_LOG
(
   WF_TASK_LOG_ID       varchar(32) not null comment '流程日志标识',
   WORKFLOW_ID          varchar(12) comment '流程标识',
   BUSSINESS_ID         varchar(32) comment '业务标识',
   STEP_ID              varchar(16) comment '步骤标识',
   AUDIT_MSG            varchar(128) comment '审核类型',
   TASK_LOG_MSG         varchar(255) comment '流程日志信息',
   TASK_USER_ID         varchar(32) comment '办理人',
   START_TIME           bigint comment '开始时间',
   END_TIME             bigint comment '结束时间',
   primary key (WF_TASK_LOG_ID)
);

alter table WF_TASK_LOG comment 'WF-流程任务历史记录表 ';

/*==============================================================*/
/* Table: WF_WORKFLOW                                           */
/*==============================================================*/
create table WF_WORKFLOW
(
   WORKFLOW_ID          varchar(12) not null comment '流程标识',
   NAME                 varchar(128) comment '名称',
   primary key (WORKFLOW_ID)
);

alter table WF_WORKFLOW comment 'WF-流程定义表 ';

alter table CU_LOOKUP_CODE add constraint FK_SYS_LOOKUP_TYPE_1 foreign key (LOOKUP_TYPE)
      references CU_LOOKUP_TYPE (LOOKUP_TYPE) on delete restrict on update restrict;

alter table WF_STEP add constraint FK_Reference_194 foreign key (WORKFLOW_ID)
      references WF_WORKFLOW (WORKFLOW_ID) on delete restrict on update restrict;

alter table WF_TASK_LOG add constraint FK_Reference_213 foreign key (WORKFLOW_ID)
      references WF_WORKFLOW (WORKFLOW_ID) on delete restrict on update restrict;

alter table WF_TASK_LOG add constraint FK_Reference_214 foreign key (STEP_ID)
      references WF_STEP (STEP_ID) on delete restrict on update restrict;
