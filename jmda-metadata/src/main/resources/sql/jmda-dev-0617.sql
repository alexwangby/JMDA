/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : jmda-dev

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2016-06-17 14:49:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cu_lookup_code`
-- ----------------------------
DROP TABLE IF EXISTS `cu_lookup_code`;
CREATE TABLE `cu_lookup_code` (
  `LOOKUP_CODE` char(3) NOT NULL COMMENT '二级编码',
  `LOOKUP_TYPE` char(2) DEFAULT NULL COMMENT '一级编码',
  `CODE_NAME` varchar(128) DEFAULT NULL COMMENT '二级编码名称',
  PRIMARY KEY (`LOOKUP_CODE`),
  KEY `FK_SYS_LOOKUP_TYPE_1` (`LOOKUP_TYPE`),
  CONSTRAINT `FK_SYS_LOOKUP_TYPE_1` FOREIGN KEY (`LOOKUP_TYPE`) REFERENCES `cu_lookup_type` (`LOOKUP_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='二级码表';

-- ----------------------------
-- Records of cu_lookup_code
-- ----------------------------

-- ----------------------------
-- Table structure for `cu_lookup_type`
-- ----------------------------
DROP TABLE IF EXISTS `cu_lookup_type`;
CREATE TABLE `cu_lookup_type` (
  `LOOKUP_TYPE` char(2) NOT NULL COMMENT '一级编码',
  `TYPE_NAME` varchar(128) DEFAULT NULL COMMENT '一级编码名称',
  PRIMARY KEY (`LOOKUP_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='一级码表 ';

-- ----------------------------
-- Records of cu_lookup_type
-- ----------------------------
