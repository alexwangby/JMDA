package com.jmda.log.util;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 格式化输入工具类
 * @author lizhgb
 * @date  2015-10-14
 *
 */
public final class FormatJsonUtil {

    /**
     * 打印输入到控制台
     * @param jsonStr
     * @author   lizhgb
     * @Date   2015-10-14 下午1:17:22
     */
    public static void printJson(String jsonStr){
        System.out.println(formatJson(jsonStr));
    }



    public static String replaceEnter(String str)
    {
    	  String dest = "";
          if (str!=null) {
              Pattern p = Pattern.compile("\\s*|\\r|\\n|\\t");
              Matcher m = p.matcher(str);
              dest = m.replaceAll("");
          }
          return dest;

    }

    /**
     * 格式化
     * @param jsonStr
     * @return
     * @author   lizhgb
     * @Date   2015-10-14 下午1:17:35
     */
    public static String formatJson(String jsonStr) {
    	//System.out.println("输出前="+jsonStr);
     	jsonStr=jsonStr.replace("\r\n", "<br/>");
    	//System.out.println("输出后="+jsonStr);
        if (null == jsonStr || "".equals(jsonStr)) return "";
        StringBuilder sb = new StringBuilder();
        char last = '\0';
        char current = '\0';
        int indent = 0;
        for (int i = 0; i < jsonStr.length(); i++) {
            last = current;
            current = jsonStr.charAt(i);
            switch (current) {
                case '{':
                case '[':
                    sb.append(current);
                    sb.append('\n');
                    indent++;
                    addIndentBlank(sb, indent);
                    break;
                case '}':
                case ']':
                    sb.append('\n');
                    indent--;
                    addIndentBlank(sb, indent);
                    sb.append(current);
                    break;
                case ',':
                    sb.append(current);
                    if (last != '\\') {
                        sb.append('\n');
                        addIndentBlank(sb, indent);
                    }
                    break;
                default:
                    sb.append(current);
            }
        }

        return sb.toString();
    }
    /**
     * 格式化
     * @param jsonStr
     * @return
     * @author   lizhgb
     * @Date   2015-10-14 下午1:17:35
     */
    public static String formatMapTo(Map map,String format) {

    	StringBuffer jsonStr = new StringBuffer();
    	if(map!=null&&map.size()>0){
    		Iterator iterator = map.keySet().iterator();
    		while(iterator.hasNext()) {
    			String key = (String) iterator.next();
    			jsonStr.append("{");
    			jsonStr.append("\"").append(key).append("\":");
    			jsonStr.append("\"").append(map.get(key)).append("\"");
    			jsonStr.append("}");
    			jsonStr.append(format);
    		}
    	}

       /* if (null == jsonStr || "".equals(jsonStr)) return "";
        StringBuilder sb = new StringBuilder();
        char last = '\0';
        char current = '\0';
        int indent = 0;
        for (int i = 0; i < jsonStr.length(); i++) {
            last = current;
            current = jsonStr.charAt(i);
            switch (current) {
                case '{':
                case '[':
                    sb.append(current);
                    sb.append('\n');
                    indent++;
                    addIndentBlank(sb, indent);
                    break;
                case '}':
                case ']':
                    sb.append('\n');
                    indent--;
                    addIndentBlank(sb, indent);
                    sb.append(current);
                    break;
                case ',':
                    sb.append(current);
                    if (last != '\\') {
                        sb.append('\n');
                        addIndentBlank(sb, indent);
                    }
                    break;
                default:
                    sb.append(current);
            }
        }*/

        return jsonStr.toString();
    }

    /**
     * 添加space
     * @param sb
     * @param indent
     * @author   lizhgb
     * @Date   2015-10-14 上午10:38:04
     */
    private static void addIndentBlank(StringBuilder sb, int indent) {
        for (int i = 0; i < indent; i++) {
            sb.append('\t');
        }
    }
}
