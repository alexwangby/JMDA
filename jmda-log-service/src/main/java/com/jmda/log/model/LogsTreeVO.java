package com.jmda.log.model;

import java.util.List;
public class LogsTreeVO {
		//绑定节点的标识值，可以用于后续的查询，或者用于加载异步树
		private String id="";

		//节点用于显示的文本内容
		private String text="";

		//节点状态，'open' 或 'closed'，默认：'open'。如果为'closed'的时候，将不自动展开该节点。
		private String state="";

		//表示该节点是否被选中
		private Boolean checked=false;

		//子节点属性
		private List<LogsTreeVO> children;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public Boolean getChecked() {
			return checked;
		}

		public void setChecked(Boolean checked) {
			this.checked = checked;
		}
		public List<LogsTreeVO> getChildren() {
			return children;
		}

		public void setChildren(List<LogsTreeVO> children) {
			this.children = children;
		}

		public String toString(){
			if(null == children){
				return "{\"id\":\""+id+"\",\"text\":\""+text+"\",\"state\":\""+state+"\",\"checked\":"+checked+"}";
			}else{
				return "{\"id\":\""+id+"\",\"text\":\""+text+"\",\"state\":\""+state+"\",\"checked\":"+checked+",\"children\":"+children+"}";
			}
		}

}
