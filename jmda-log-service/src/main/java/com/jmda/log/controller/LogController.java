package com.jmda.log.controller;

import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jmda.log.model.LogContentVO;
import com.jmda.log.model.LogsTreeVO;
import com.jmda.log.service.LogService;
import com.mongodb.DB;
import com.mongodb.Mongo;

@Controller
@RequestMapping("/log")
public class LogController {
	@Autowired
	LogService logService;

	@RequestMapping(value = "/getPage", method = { RequestMethod.POST, RequestMethod.GET })
	public String list() {
		return "/log/cu_log";
	}

	/**
	 * 下载当前表中的所有日志文件
	 * @param id
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/getAllData", method = { RequestMethod.POST, RequestMethod.GET })
	public void getAllData(String id, Model model,HttpServletRequest request,HttpServletResponse response) {

		String[] strs = id.split(":");
		String _dbName = strs[0];
		String _org = strs[1];
		Mongo mongo = logService.getMongo();
		DB db = null;
		String dbName = "";
		String collectionName = "";
		List<String> dbNames = logService.getDatabaseNames(mongo);
		for (String str : dbNames) {
			if (str.equals(_dbName)) {
				Set<String> collectionNames = logService.getCollectionNames(mongo, str);
				for (String collection : collectionNames) {
					if (collection.equals(_org)) {
						dbName = str;
						db = logService.getDB(mongo, dbName);
						collectionName = collection;
						break;
					}
				}
			}
		}
		StringBuffer data=new StringBuffer();
		List<LogContentVO> record = logService.getLog(db, collectionName);
		for (LogContentVO logContentVO : record) {
			data.append(logContentVO.getContent());
		}

		try {
			//清空一下response对象，防止出现缓存
			response.reset();
			//指明这是一个下载的respond
			response.setContentType("application/x-download");
			String filename=_org+"log.txt";
			//防止乱码
			filename=URLEncoder.encode(filename,"UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.addHeader("Content-Disposition","attachment;filename=" + filename);
			PrintWriter printWriter = new PrintWriter(response.getOutputStream());
	        printWriter.println(data);
	        printWriter.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取前五十条数据到页面显示
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getData", method = { RequestMethod.POST, RequestMethod.GET })
	public String getData(String id, Model model) {
		model.addAttribute("id", id);
		String[] strs = id.split(":");
		String _dbName = strs[0];
		String _org = strs[1];
		Mongo mongo = logService.getMongo();
		DB db = null;
		String dbName = "";
		String collectionName = "";
		List<String> dbNames = logService.getDatabaseNames(mongo);
		for (String str : dbNames) {
			if (str.equals(_dbName)) {
				Set<String> collectionNames = logService.getCollectionNames(mongo, str);
				for (String collection : collectionNames) {
					if (collection.equals(_org)) {
						dbName = str;
						db = logService.getDB(mongo, dbName);
						collectionName = collection;
						break;
					}
				}
			}
		}
		List<LogContentVO> record = logService.getLogIndex(db, collectionName);
		model.addAttribute("records", record);
		return "/log/cu_log_form";
	}
	/**
	 * 查询符合条件的数据
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getMatchConditionData", method = { RequestMethod.POST, RequestMethod.GET })
	public String getMatchConditionData(String id,String contents, Model model) {
		model.addAttribute("id", id);
		model.addAttribute("contents", contents);
		String[] strs = id.split(":");
		String _dbName = strs[0];
		String _org = strs[1];
		Mongo mongo = logService.getMongo();
		DB db = null;
		String dbName = "";
		String collectionName = "";
		List<String> dbNames = logService.getDatabaseNames(mongo);
		for (String str : dbNames) {
			if (str.equals(_dbName)) {
				Set<String> collectionNames = logService.getCollectionNames(mongo, str);
				for (String collection : collectionNames) {
					if (collection.equals(_org)) {
						dbName = str;
						db = logService.getDB(mongo, dbName);
						collectionName = collection;
						break;
					}
				}
			}
		}
		List<LogContentVO> record = logService.getLogMatchCondition(db, collectionName,contents);
		model.addAttribute("records", record);
		return "/log/cu_log_form";
	}

	@RequestMapping(value = "/getTree", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getTree(LogsTreeVO Node) {
		List<LogsTreeVO> tree = logService.getTree(Node);
		return tree.toString();
	}
}
