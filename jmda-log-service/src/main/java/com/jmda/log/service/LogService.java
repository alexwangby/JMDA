package com.jmda.log.service;

import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.jmda.log.constants.SysConstants;
import com.jmda.log.model.LogContentVO;
import com.jmda.log.model.LogsTreeVO;
import com.jmda.log.util.FormatJsonUtil;
import com.jmda.platform.commom.LocalCacheManager;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

/**
 * mongo日志
 *
 * @author chens
 *
 */
@Service
public class LogService {
	private static SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * 获取Mongo对象
	 *
	 * @return
	 */
	public Mongo getMongo() {
		try {
			String host = LocalCacheManager.jmdaPropertiesMap.get("mongodb.ip");
			Mongo mongo = new Mongo(host, SysConstants.PORT);
			return mongo;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取DB
	 *
	 * @param mongo
	 * @param dbName
	 * @return
	 */
	public DB getDB(Mongo mongo, String dbName) {
		DB db = mongo.getDB(dbName);
		return db;
	}

	/**
	 * 获取数据库(一级)
	 *
	 * @param mongo
	 * @return
	 */
	public List<String> getDatabaseNames(Mongo mongo) {
		return mongo.getDatabaseNames();
	}

	/**
	 * 获取数据库集合(二级)
	 *
	 * @param mongo
	 * @param dbName
	 * @return
	 */
	public Set<String> getCollectionNames(Mongo mongo, String dbName) {
		DB db = mongo.getDB(dbName);
		Set<String> set = db.getCollectionNames();
		return set;
	}

	/**
	 * 日志输出
	 *
	 * @param db
	 * @param dbName
	 * @return
	 */
	public List<LogContentVO> getLog(DB db, String collectionName) {
		List<LogContentVO> list = new ArrayList<>();
		// StringBuffer json = new StringBuffer();
		DBCollection collection = db.getCollection(collectionName);
		DBCursor cursor = collection.find();
		while (cursor.hasNext()) {
			LogContentVO logsContentVO = new LogContentVO();
			DBObject dbObject = cursor.next();
			Map<String,Object> map = new HashMap<>();
			map.put("message", dbObject.get("message"));
			map.put("timestamp", datetimeFormat.format(dbObject.get("timestamp")));
			map.put("level", dbObject.get("level"));
			map.put("host", dbObject.get("host"));
			if(dbObject.get("throwables")!=null){
				map.put("throwables", dbObject.get("throwables"));
			}
			String content = FormatJsonUtil.formatMapTo(map,"\r\n");
			logsContentVO.setContent(content);
			list.add(logsContentVO);
		}
		return list;
	}

	/**
	 * 日志分页输出
	 *
	 * @param db
	 * @param dbName
	 * @return
	 */
	public List<LogContentVO> getLogIndex(DB db, String collectionName) {
		List<LogContentVO> list = new ArrayList<>();
		DBCollection collection = db.getCollection(collectionName);
		DBCursor cursor = collection.find().skip(0).limit(SysConstants.pageSize);//查询collection表中的前多少条数据
		while (cursor.hasNext()) {
			LogContentVO logsContentVO = new LogContentVO();
			DBObject dbObject = cursor.next();
			Map<String,Object> map = new HashMap<>();
			map.put("message", dbObject.get("message"));
			map.put("timestamp", datetimeFormat.format(dbObject.get("timestamp")));
			map.put("level", dbObject.get("level"));
			map.put("host", dbObject.get("host"));
			if(dbObject.get("throwables")!=null){
				map.put("throwables", dbObject.get("throwables"));
			}

			String content = FormatJsonUtil.formatMapTo(map,"<br/>");
			logsContentVO.setContent(content);
			list.add(logsContentVO);
		}
		return list;
	}

	/**
	 * 获取符合条件的日志
	 * @param db
	 * @param collectionName
	 * @param contents
	 * @return
	 */
	public List<LogContentVO> getLogMatchCondition(DB db, String collectionName,String contents) {
		List<LogContentVO> list = new ArrayList<>();
		if(contents.trim().length()>0){
			String[] strs=contents.split(":");

			String name=strs[0].trim();
			name=name.replaceAll("\"", "");
			String value=strs[1].trim();
			value=value.replaceAll("\"", "");
			DBCollection collection = db.getCollection(collectionName);
			BasicDBObject condition=new BasicDBObject();
			condition.put(name,value);
			DBCursor cursor = collection.find(condition);
			while (cursor.hasNext()) {
				LogContentVO logsContentVO = new LogContentVO();
				DBObject dbObject = cursor.next();
				Map<String,Object> map = new HashMap<>();
				map.put("message", dbObject.get("message"));
				map.put("timestamp", datetimeFormat.format(dbObject.get("timestamp")));
				map.put("level", dbObject.get("level"));
				map.put("host", dbObject.get("host"));
				if(dbObject.get("throwables")!=null){
					map.put("throwables", dbObject.get("throwables"));
				}
				String content = FormatJsonUtil.formatMapTo(map,"<br/>");
				logsContentVO.setContent(content);
				list.add(logsContentVO);
			}
		}else{
			list=getLog(db,collectionName);
		}
		return list;
	}
	/**
	 * 查询一共多少条数据
	 *
	 * @param collection
	 * @return
	 */
	public int count(DB db, String collectionName) {
		DBCollection collection = db.getCollection(collectionName);
		return collection.find().count();
	}


	/**
	 * 获取异步树
	 *
	 * @param orgs
	 * @return
	 */
	public List<LogsTreeVO> getTree(LogsTreeVO Node) {
		// 创建根节点集合
		List<LogsTreeVO> roots = new ArrayList<LogsTreeVO>();
		Mongo mongo = getMongo();
		List<String> orgs = getDatabaseNames(mongo);
		LogsTreeVO root = new LogsTreeVO();
		if (Node.getId() == null || Node.getId().equals("")) {
			// 创建一个根节点
			// 完善根节点属性
			root.setId("root@root");
			root.setText("日志系统");
			root.setState("closed");
			root.setChildren(null);
			roots.add(root);
		} else {
			String id = Node.getId();
			String[] strs = id.split("@");
			String noteId = strs[0];
			String noteEnd = strs[1];
			if (noteEnd.equals("root")) {
				for (String dbName : orgs) {
					if (dbName != null && !dbName.equals("")) {
						// 一级节点
						LogsTreeVO firstNode = new LogsTreeVO();
						firstNode.setId(dbName + "@dbName");
						firstNode.setText(dbName);
						firstNode.setState("closed");
						roots.add(firstNode);
					}
				}
			} else if (noteEnd.equals("dbName")) {
				Set<String> set = getCollectionNames(mongo, noteId);
				for (String org : set) {
					if (org != null && !org.equals("") && !org.equals("system.indexes")) {
						LogsTreeVO secondNode = new LogsTreeVO();
						secondNode.setId(noteId + ":" + org + "@org");
						secondNode.setText(org);
						secondNode.setState("open");
						roots.add(secondNode);
					}
				}
			}
		}
		// 遍历
		return roots;
	}

}
