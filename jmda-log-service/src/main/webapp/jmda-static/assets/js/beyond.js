/*Sets Themed Colors Based on Themes*/
if (typeof(STATIC_SERVER) == "undefined") {
    STATIC_SERVER = '';
} else {
    STATIC_SERVER = STATIC_SERVER + "/";
}


var themeprimary = getThemeColorFromCss('themeprimary');
var themesecondary = getThemeColorFromCss('themesecondary');
var themethirdcolor = getThemeColorFromCss('themethirdcolor');
var themefourthcolor = getThemeColorFromCss('themefourthcolor');
var themefifthcolor = getThemeColorFromCss('themefifthcolor');

//Gets Theme Colors From Selected Skin To Use For Drawing Charts
function getThemeColorFromCss(style) {
    var $span = $("<span></span>").hide().appendTo("body");
    $span.addClass(style);
    var color = $span.css("color");
    $span.remove();
    return color;
}

//Handle RTL SUpport for Changer CheckBox
$("#skin-changer li a").click(function () {
    //createCookie("current-skin", $(this).attr('rel'), 10);
    //window.location.reload();
});

//Checks Not to Do rtl-support for Arabic and Persian Demo Pages

var rtlchanger = document.getElementById('rtl-changer');
/**
 if (location.pathname != "/index-rtl-fa.html" && location.pathname != "index-rtl-ar.html") {
    if (readCookie("rtl-support")) {
        switchClasses("pull-right", "pull-left");
        switchClasses("databox-right", "databox-left");
        switchClasses("item-right", "item-left");
        $('.navbar-brand small img').attr('src', 'assets/img/logo-rtl.png');
        if (rtlchanger != null)
            document.getElementById('rtl-changer').checked = true;
    }
    else {
        if (rtlchanger != null)
            rtlchanger.checked = false;
    }

    if (rtlchanger != null) {
        rtlchanger.onchange = function () {
            if (this.checked) {
                createCookie("rtl-support", "true", 10);
            }
            else {
                eraseCookie("rtl-support");
            }
            setTimeout(function () {
                window.location.reload();
            }, 600);

        };
    }
}
 **/
/*Loading*/
//$(window).load(function () { setTimeout(function () { $('.loading-container').addClass('loading-inactive');}, 1000);});


/*Account Area --> Setting Button*/
$('#btn-setting')
    .on('click', function (e) {
        $('.navbar-account')
            .toggleClass('setting-open');
    });

/*Toggle FullScreen*/
$('#fullscreen-toggler')
    .on('click', function (e) {
        var element = document.documentElement;
        if (!$('body')
                .hasClass("full-screen")) {

            $('body')
                .addClass("full-screen");
            $('#fullscreen-toggler')
                .addClass("active");
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }


        } else {
            $('body')
                .removeClass("full-screen");
            $('#fullscreen-toggler')
                .removeClass("active");

            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }


        }
    });

/*Handles Popovers*/
var popovers = $('[data-toggle=popover]');
$.each(popovers, function () {
    $(this)
        .popover({
            html: true,
            template: '<div class="popover ' + $(this)
                .data("class") +
            '"><div class="arrow"></div><h3 class="popover-title ' +
            $(this)
                .data("titleclass") + '">Popover right</h3><div class="popover-content"></div></div>'
        });
});

var hoverpopovers = $('[data-toggle=popover-hover]');
$.each(hoverpopovers, function () {
    $(this)
        .popover({
            html: true,
            template: '<div class="popover ' + $(this)
                .data("class") +
            '"><div class="arrow"></div><h3 class="popover-title ' +
            $(this)
                .data("titleclass") + '">Popover right</h3><div class="popover-content"></div></div>',
            trigger: "hover"
        });
});


/*Handles ToolTips*/
$("[data-toggle=tooltip]")
    .tooltip({
        html: true
    });

InitiateSideMenu();
//InitiateSettings();
InitiateWidgets();

function InitiateSideMenu() {

    //Sidebar Toggler
    $(".sidebar-toggler").on('click', function () {
        $("#sidebar").toggleClass("hide");
        $(".sidebar-toggler").toggleClass("active");
        return false;
    });
    //End Sidebar Toggler

    //Sidebar Collapse

    var b = $("#sidebar").hasClass("menu-compact");
    $("#sidebar-collapse").on('click', function () {
        if (!$('#sidebar').is(':visible'))
            $("#sidebar").toggleClass("hide");
        $("#sidebar").toggleClass("menu-compact");
        $(".sidebar-collapse").toggleClass("active");
        b = $("#sidebar").hasClass("menu-compact");

        if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
            $(".sidebar-menu").slimScroll({destroy: true});
            $(".sidebar-menu").attr('style', '');
        }


        if (b) {
            $(".open > .submenu").removeClass("open");
        } else {
            if ($('.page-sidebar').hasClass('sidebar-fixed')) {
                var position = 'left'; // (readCookie("rtl-support") || location.pathname == "/index-rtl-fa.html" || location.pathname == "index-rtl-ar.html") ? 'right' : 'left';
                $('.sidebar-menu').slimscroll({
                    height: 'auto',
                    position: position,
                    size: '3px',
                    color: themeprimary
                });
            }
        }
        //Slim Scroll Handle


    });
    //End Sidebar Collapse


    //Sidebar Menu Handle

    $(".sidebar-menu").on('click', function (e) {
        var menuLink = $(e.target).closest("a");
        if (!menuLink || menuLink.length == 0)
            return;
        if (!menuLink.hasClass("menu-dropdown")) {
            if (b && menuLink.get(0).parentNode.parentNode == this) {
                var menuText = menuLink.find(".menu-text").get(0);
                if (e.target != menuText && !$.contains(menuText, e.target)) {
                    return false;
                }
            }
            return;
        }
        var submenu = menuLink.next().get(0);
        if (!$(submenu).is(":visible")) {
            var c = $(submenu.parentNode).closest("ul");
            if (b && c.hasClass("sidebar-menu"))
                return;
            c.find("> .open > .submenu").each(function () {
                <!--级联关闭-->
                if (this != submenu && !$(this.parentNode).hasClass("active"))
                    $(this).slideUp(200).parent().removeClass("open");

            });
        }
        if (b && $(submenu.parentNode.parentNode).hasClass("sidebar-menu"))
            return false;
        $(submenu).slideToggle(200).parent().toggleClass("open");
        return false;
    });

    //End Sidebar Menu Handle
}

function InitiateWidgets() {
    $('.widget-buttons *[data-toggle="maximize"]').on("click", function (event) {
        event.preventDefault();
        var widget = $(this).parents(".widget").eq(0);
        var button = $(this).find("i").eq(0);
        var compress = "fa-compress";
        var expand = "fa-expand";
        if (widget.hasClass("maximized")) {
            if (button) {
                button.addClass(expand).removeClass(compress);
            }
            widget.removeClass("maximized");
            widget.find(".widget-body").css("height", "auto");
        } else {
            if (button) {
                button.addClass(compress).removeClass(expand);
            }
            widget.addClass("maximized");
            maximize(widget);
        }
    });

    $('.widget-buttons *[data-toggle="collapse"]').on("click", function (event) {
        event.preventDefault();
        var widget = $(this).parents(".widget").eq(0);
        var body = widget.find(".widget-body");
        var button = $(this).find("i");
        var down = "fa-plus";
        var up = "fa-minus";
        var slidedowninterval = 300;
        var slideupinterval = 200;
        if (widget.hasClass("collapsed")) {
            if (button) {
                button.addClass(up).removeClass(down);
            }
            widget.removeClass("collapsed");
            body.slideUp(0, function () {
                body.slideDown(slidedowninterval);
            });
        } else {
            if (button) {
                button.addClass(down)
                    .removeClass(up);
            }
            body.slideUp(slideupinterval, function () {
                widget.addClass("collapsed");
            });
        }
    });

    $('.widget-buttons *[data-toggle="dispose"]').on("click", function (event) {
        event.preventDefault();
        var toolbarLink = $(this);
        var widget = toolbarLink.parents(".widget").eq(0);
        var disposeinterval = 300;
        widget.hide(disposeinterval, function () {
            widget.remove();
        });
    });
}

// Fullscreen Widget
function maximize(widgetbox) {
    if (widgetbox) {
        var windowHeight = $(window).height();
        var headerHeight = widgetbox.find(".widget-header").height();
        widgetbox.find(".widget-body").height(windowHeight - headerHeight);
    }
}

/* Scroll To */
function scrollTo(el, offeset) {
    var pos = (el && el.size() > 0) ? el.offset().top : 0;
    jQuery('html,body').animate({scrollTop: pos + (offeset ? offeset : 0)}, 'slow');
}

/*Show Notification*/
function Notify(message, position, timeout, theme, icon, closable) {
    toastr.options.positionClass = 'toast-' + position;
    toastr.options.extendedTimeOut = 0; //1000;
    toastr.options.timeOut = timeout;
    toastr.options.closeButton = closable;
    toastr.options.iconClass = icon + ' toast-' + theme;
    toastr['custom'](message);
}

/*#region handle Settings*/
function InitiateSettings() {
    /**
     if (readCookie("navbar-fixed-top") != null) {
        if (readCookie("navbar-fixed-top") == "true") {
            $('#checkbox_fixednavbar').prop('checked', true);
            $('.navbar').addClass('navbar-fixed-top');
        }
    }


     if (readCookie("sidebar-fixed") != null) {
        if (readCookie("sidebar-fixed") == "true") {
            $('#checkbox_fixedsidebar').prop('checked', true);
            $('.page-sidebar').addClass('sidebar-fixed');

            //Slim Scrolling for Sidebar Menu in fix state
            if (!$(".page-sidebar").hasClass("menu-compact")) {
                var position = (readCookie("rtl-support") || location.pathname == "/index-rtl-fa.html" || location.pathname == "index-rtl-ar.html") ? 'right' : 'left';
                $('.sidebar-menu').slimscroll({
                    height: 'auto',
                    position: position,
                    size: '3px',
                    color: themeprimary
                });
            }
        }

    }
     if (readCookie("breadcrumbs-fixed") != null) {
        if (readCookie("breadcrumbs-fixed") == "true") {
            $('#checkbox_fixedbreadcrumbs').prop('checked', true);
            $('.page-breadcrumbs').addClass('breadcrumbs-fixed');
        }
    }
     if (readCookie("page-header-fixed") != null) {
        if (readCookie("page-header-fixed") == "true") {
            $('#checkbox_fixedheader').prop('checked', true);
            $('.page-header').addClass('page-header-fixed');
        }
    }
     **/
    $('#checkbox_fixednavbar').prop('checked', true);
    $('.navbar').addClass('navbar-fixed-top');
    $('#checkbox_fixedsidebar').prop('checked', true);
    $('.page-sidebar').addClass('sidebar-fixed');

    //Slim Scrolling for Sidebar Menu in fix state
    if (!$(".page-sidebar").hasClass("menu-compact")) {
        var position = (readCookie("rtl-support") || location.pathname == "/index-rtl-fa.html" || location.pathname == "index-rtl-ar.html") ? 'right' : 'left';
        $('.sidebar-menu').slimscroll({
            height: 'auto',
            position: position,
            size: '3px',
            color: themeprimary
        });
    }

    $('#checkbox_fixedbreadcrumbs').prop('checked', true);
    $('.page-breadcrumbs').addClass('breadcrumbs-fixed');
    $('#checkbox_fixedheader').prop('checked', true);
    $('.page-header').addClass('page-header-fixed');


    $('#checkbox_fixednavbar')
        .change(function () {
            $('.navbar')
                .toggleClass('navbar-fixed-top');

            if (($('#checkbox_fixedsidebar')
                    .is(":checked"))) {
                $('#checkbox_fixedsidebar')
                    .prop('checked', false);
                $('.page-sidebar')
                    .toggleClass('sidebar-fixed');
            }

            if (($('#checkbox_fixedbreadcrumbs')
                    .is(":checked")) && !($(this)
                    .is(":checked"))) {
                $('#checkbox_fixedbreadcrumbs')
                    .prop('checked', false);
                $('.page-breadcrumbs')
                    .toggleClass('breadcrumbs-fixed');
            }

            if (($('#checkbox_fixedheader')
                    .is(":checked")) && !($(this)
                    .is(":checked"))) {
                $('#checkbox_fixedheader')
                    .prop('checked', false);
                $('.page-header')
                    .toggleClass('page-header-fixed');
            }
            setCookiesForFixedSettings();
        });

    $('#checkbox_fixedsidebar')
        .change(function () {

            $('.page-sidebar')
                .toggleClass('sidebar-fixed');

            if (!($('#checkbox_fixednavbar')
                    .is(":checked"))) {
                $('#checkbox_fixednavbar')
                    .prop('checked', true);
                $('.navbar')
                    .toggleClass('navbar-fixed-top');
            }
            if (($('#checkbox_fixedbreadcrumbs')
                    .is(":checked")) && !($(this)
                    .is(":checked"))) {
                $('#checkbox_fixedbreadcrumbs')
                    .prop('checked', false);
                $('.page-breadcrumbs')
                    .toggleClass('breadcrumbs-fixed');
            }

            if (($('#checkbox_fixedheader')
                    .is(":checked")) && !($(this)
                    .is(":checked"))) {
                $('#checkbox_fixedheader')
                    .prop('checked', false);
                $('.page-header')
                    .toggleClass('page-header-fixed');
            }
            setCookiesForFixedSettings();

        });
    $('#checkbox_fixedbreadcrumbs')
        .change(function () {

            $('.page-breadcrumbs')
                .toggleClass('breadcrumbs-fixed');


            if (!($('#checkbox_fixedsidebar')
                    .is(":checked"))) {
                $('#checkbox_fixedsidebar')
                    .prop('checked', true);
                $('.page-sidebar')
                    .toggleClass('sidebar-fixed');
            }
            if (!($('#checkbox_fixednavbar')
                    .is(":checked"))) {
                $('#checkbox_fixednavbar')
                    .prop('checked', true);
                $('.navbar')
                    .toggleClass('navbar-fixed-top');
            }
            if (($('#checkbox_fixedheader')
                    .is(":checked")) && !($(this)
                    .is(":checked"))) {
                $('#checkbox_fixedheader')
                    .prop('checked', false);
                $('.page-header')
                    .toggleClass('page-header-fixed');
            }
            setCookiesForFixedSettings();

        });

    $('#checkbox_fixedheader')
        .change(function () {

            $('.page-header')
                .toggleClass('page-header-fixed');


            if (!($('#checkbox_fixedbreadcrumbs')
                    .is(":checked"))) {
                $('#checkbox_fixedbreadcrumbs')
                    .prop('checked', true);
                $('.page-breadcrumbs')
                    .toggleClass('breadcrumbs-fixed');
            }

            if (!($('#checkbox_fixedsidebar')
                    .is(":checked"))) {
                $('#checkbox_fixedsidebar')
                    .prop('checked', true);
                $('.page-sidebar')
                    .toggleClass('sidebar-fixed');
            }
            if (!($('#checkbox_fixednavbar')
                    .is(":checked"))) {
                $('#checkbox_fixednavbar')
                    .prop('checked', true);
                $('.navbar')
                    .toggleClass('navbar-fixed-top');
            }

            setCookiesForFixedSettings();
        });
}

function setCookiesForFixedSettings() {
    //createCookie("navbar-fixed-top", $('#checkbox_fixednavbar').is(':checked'), 100);
    //createCookie("sidebar-fixed", $('#checkbox_fixedsidebar').is(':checked'), 100);
    //createCookie("breadcrumbs-fixed", $('#checkbox_fixedbreadcrumbs').is(':checked'), 100);
    //createCookie("page-header-fixed", $('#checkbox_fixedheader').is(':checked'), 100);

    var position = 'left'; // (readCookie("rtl-support") || location.pathname == "/index-rtl-fa.html" || location.pathname == "index-rtl-ar.html") ? 'right' : 'left';
    if ($('#checkbox_fixedsidebar').is(':checked')) {
        if (!$('.page-sidebar').hasClass('menu-compact')) {
            //Slim Scrolling for Sidebar Menu in fix state
            $('.sidebar-menu').slimscroll({
                position: position,
                size: '3px',
                color: themeprimary,
                height: 'auto',
            });
        }
    } else {
        if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
            $(".sidebar-menu").slimScroll({destroy: true});
            $(".sidebar-menu").attr('style', '');
        }
    }
}
/*#endregion handle Settings*/

//Chat
$("#chat-link").click(function () {
    $('.page-chatbar').toggleClass('open');
    $("#chat-link").toggleClass('open');
});
$('.page-chatbar .chatbar-contacts .contact').on('click', function (e) {
    $('.page-chatbar .chatbar-contacts').hide();
    $('.page-chatbar .chatbar-messages').show();
});

$('.page-chatbar .chatbar-messages .back').on('click', function (e) {
    $('.page-chatbar .chatbar-contacts').show();
    $('.page-chatbar .chatbar-messages').hide();
});
var position = 'left';// (readCookie("rtl-support") || location.pathname == "/index-rtl-fa.html" || location.pathname == "index-rtl-ar.html") ? 'right' : 'left';
$('.chatbar-messages .messages-list').slimscroll({
    position: position,
    size: '4px',
    color: themeprimary,
    height: $(window).height() - 250,
});
$('.chatbar-contacts .contacts-list').slimscroll({
    position: position,
    size: '4px',
    color: themeprimary,
    height: $(window).height() - 86,
});
//End Chat

/*#region Get Colors*/
//Get colors from a string base on theme colors
function getcolor(colorString) {
    switch (colorString) {
        case ("themeprimary"):
            return themeprimary;
        case ("themesecondary"):
            return themesecondary;
        case ("themethirdcolor"):
            return themethirdcolor;
        case ("themefourthcolor"):
            return themefourthcolor;
        case ("themefifthcolor"):
            return themefifthcolor;
        default:
            return colorString;
    }
}
/*#endregion Get Colors*/


//Switch Classes Function
function switchClasses(firstClass, secondClass) {

    var firstclasses = document.getElementsByClassName(firstClass);

    for (i = firstclasses.length - 1; i >= 0; i--) {
        if (!hasClass(firstclasses[i], 'dropdown-menu')) {
            addClass(firstclasses[i], firstClass + '-temp');
            removeClass(firstclasses[i], firstClass);
        }
    }

    var secondclasses = document.getElementsByClassName(secondClass);

    for (i = secondclasses.length - 1; i >= 0; i--) {
        if (!hasClass(secondclasses[i], 'dropdown-menu')) {
            addClass(secondclasses[i], firstClass);
            removeClass(secondclasses[i], secondClass);
        }
    }

    tempClasses = document.getElementsByClassName(firstClass + '-temp');

    for (i = tempClasses.length - 1; i >= 0; i--) {
        if (!hasClass(tempClasses[i], 'dropdown-menu')) {
            addClass(tempClasses[i], secondClass);
            removeClass(tempClasses[i], firstClass + '-temp');
        }
    }
}


//Add Classes Function
function addClass(elem, cls) {
    var oldCls = elem.className;
    if (oldCls) {
        oldCls += " ";
    }
    elem.className = oldCls + cls;
}

//Remove Classes Function
function removeClass(elem, cls) {
    var str = " " + elem.className + " ";
    elem.className = str.replace(" " + cls, "").replace(/^\s+/g, "").replace(/\s+$/g, "");
}

//Has Classes Function
function hasClass(elem, cls) {
    var str = " " + elem.className + " ";
    var testCls = " " + cls + " ";
    return (str.indexOf(testCls) != -1);
}

/**
 *#####################################################
 * readinglife js function
 * by zhanghui
 * 15-7-24
 *#####################################################
 **/
//TAB的操作类
var MainTab = {
    //创建TAB
    createTab: function (id, label, url) {
        if ($("#framework_body").length <= 0) {
            window.parent.MainTab.createTab(id, label, url);
        } else {
            var $el_lis = $("#main-tabs ul li");
            var TABNUM = 10;
            if ($el_lis.length < TABNUM) {
                var li_id = "li_" + id;
                var ifm_id = "ifm_" + id;

                if ($("#" + li_id).length <= 0) {
                    var tabTemplate = "<li id=\"" + li_id + "\"><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close float-left'></span></li>";
                    var li = $(tabTemplate.replace(/#\{href\}/g, "#" + ifm_id).replace(/#\{label\}/g, label));
                    $('#main-tabs').find(".ui-tabs-nav").append(li);

                    var ifm_html = "<div class=\"tab-iframe-div\" id=\"" + ifm_id + "\"  >" +
                        "<iframe name=\"" + ifm_id + "\" class=\"tab-iframe\" src=\"" + url + "\" width=\"100%\"  frameborder=\"no\" border=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"yes\" allowtransparency=\"yes\" ></iframe>" +
                        "</div>";

                    //"<div id='"+id+"'><p>"+tabContentHtml+"</p></div>";
                    $('#main-tabs').append(ifm_html);
                    $('#main-tabs').tabs("refresh");
                    refreshMainIframe();
                }
                var $lis = $("#main-tabs ul li");
                var index = 0;
                for (var i = 0; i < $lis.length; i++) {
                    var $li = $($lis[i]);
                    if ($li.attr("id") == li_id) {
                        index = i;
                        break;
                    }
                }
                if (id == 'home') {
                    $("#" + li_id).find(".ui-tabs-anchor").text(label);
                    $("#" + ifm_id).attr("scr", url);
                }
                $('#main-tabs').tabs("option", "active", index);

            } else {
                Notify('最多打开' + TABNUM + '个标签页!', 'top-right', '5000', 'warning', 'fa-warning', true);
            }
        }
        bindTabContextmenu();
    }
    ,
    //关闭当前TAB
    closeTab: function () {
        if ($("#framework_body").length <= 0) {
            window.parent.MainTab.closeTab();
        } else {
            var $lis = $("#main-tabs li");
            var $thisli;
            var index = 0;
            var length = $lis.length;
            for (var i = 0; i < $lis.length; i++) {
                var $li = $($lis[i]);
                if ($li.attr("aria-selected") == "true") {
                    $thisli = $li;
                    index = i;
                    break;
                }
            }
            var panelId = $thisli.remove().attr("aria-controls");
            $("#" + panelId).remove();
            $('#main-tabs').tabs("refresh");

        }
    }
    ,
    //关闭TAB并跳转到TAB
    closeTabAndGoto: function (gotoLabel) {

        if ($("#framework_body").length <= 0) {
            window.parent.MainTab.closeTabAndGoto(gotoLabel);
        } else {
            var $lis = $("#main-tabs li");
            var $thisli;
            var index = 0;
            var length = $lis.length;
            for (var i = 0; i < $lis.length; i++) {
                var $li = $($lis[i]);
                if ($li.attr("aria-selected") == "true") {
                    $thisli = $li;
                    index = i;
                    break;
                }
            }
            var panelId = $thisli.remove().attr("aria-controls");
            $("#" + panelId).remove();
            $('#main-tabs').tabs("refresh");


            //重新选中
            var $lis = $("#main-tabs li");
            var $thisli;
            var index = 0;
            var length = $lis.length;
            for (var i = 0; i < $lis.length; i++) {
                var $li = $($lis[i]);
                if ($li.find("a").text() == gotoLabel) {
                    $thisli = $li;
                    index = i;
                    break;
                }
            }
            $('#main-tabs').tabs("option", "active", index);
        }
    }

    ,
    //关闭TAB并刷新TAB
    closeTabAndRefresh: function (refreshLabel) {
        if ($("#framework_body").length <= 0) {
            window.parent.MainTab.closeTabAndRefresh(refreshLabel);
        } else {
            var $lis = $("#main-tabs li");
            var $thisli;
            var index = 0;
            var length = $lis.length;
            for (var i = 0; i < $lis.length; i++) {
                var $li = $($lis[i]);
                if ($li.attr("aria-selected") == "true") {
                    $thisli = $li;
                    index = i;
                    break;
                }
            }
            var panelId = $thisli.remove().attr("aria-controls");
            $("#" + panelId).remove();
            $('#main-tabs').tabs("refresh");


            //重新选中
            var $lis = $("#main-tabs li");
            var $thisli;
            var index = 0;
            var length = $lis.length;
            for (var i = 0; i < $lis.length; i++) {
                var $li = $($lis[i]);
                if ($li.find("a").text() == refreshLabel) {
                    $thisli = $li;
                    index = i;
                    break;
                }
            }
            $('#main-tabs').tabs("option", "active", index);

            var id = $thisli.attr("id").split("_")[1];
            var ifm_id = "ifm_" + id;
            var $iframe = $("#" + ifm_id).find("iframe");
            var src = $iframe.attr("src");
            $iframe.prop("src", src);
            $iframe.reload();

        }
    }

    ,
    //刷新指定TAB
    refreshTab: function (refreshLabel) {
        if ($("#framework_body").length <= 0) {
            window.parent.MainTab.refreshTab(refreshLabel);
        } else {
            var $lis = $("#main-tabs li");
            var $thisli;
            var index = 0;
            var length = $lis.length;
            for (var i = 0; i < $lis.length; i++) {
                var $li = $($lis[i]);
                if ($li.find("a").text() == refreshLabel) {
                    $thisli = $li;
                    index = i;
                    break;
                }
            }
            var id = $thisli.attr("id").split("_")[1];
            var ifm_id = "ifm_" + id;
            var $iframe = $("#" + ifm_id).find("iframe");

            try {
                eval($iframe.attr("name") + '.dataGridRefresh()');
            } catch (e) {
                var src = $iframe.attr("src");
                $iframe.attr("src", "");
                $iframe.attr("src", src);
            }

        }
    }


}


function maxminTab() {

    if ($("#framework_body").length <= 0) {
        window.parent.maxminTab();
    } else {
        var $li = $("#framework_body").find("#main-tabs .ui-tabs-active");
        var id = $li.attr("id").split("_")[1];
        var ifm_id = "ifm_" + id;
        if ($("#" + ifm_id).hasClass("widget")) {
            $("#" + ifm_id).removeClass("widget");
            $("#" + ifm_id).removeClass("maximized");
            $("#" + ifm_id).find(".widget-header").remove();

            var height = $(window).height() - 90;
            $("#" + ifm_id).css("height", height);
            $("#" + ifm_id).find("iframe").css("height", height);

        } else {
            $("#" + ifm_id).addClass("widget");
            $("#" + ifm_id).addClass("maximized");
            var label = $li.find("a.ui-tabs-anchor").text();

            var html = "<div class=\"widget-header bg-blue \">" +
                "<i class=\"widget-icon fa  fa-windows\"></i>" +
                "<span class=\"widget-caption\">" + label + "</span>" +
                "<div class=\"widget-buttons\">" +
                "<a href=\"javascript:maxminTab()\" ><i class=\"fa fa-minus\"></i></a>" +
                "</div>" +
                "</div>";
            $("#" + ifm_id).prepend(html);

            var height = $(window).height();
            $("#" + ifm_id).css({"height": height, "overflow": "no"});
            $("#" + ifm_id).find("iframe").css("height", height - 41);
        }
    }
}

function refreshTabPage() {
    if ($("#framework_body").length <= 0) {
        window.parent.refreshTabPage();
    } else {
        var $li = $("#framework_body").find("#main-tabs .ui-tabs-active");
        var id = $li.attr("id").split("_")[1];
        var ifm_id = "ifm_" + id;
        var src = $("#" + ifm_id).find("iframe").attr("src");
        $("#" + ifm_id).find("iframe").attr("scr", src);
    }

}


//刷新TAB
function refreshMainIframe() {
    var height = $(window).height() - 90;
    $(".tab-iframe-div").css({"height": height, "padding": "0px", "margin": "0px"});
    $(".tab-iframe").css({"height": height});
    $("#main-tabs").on("click", 'span.ui-icon-close', function () {
        var panelId = $(this).closest("li").remove().attr("aria-controls");
        $("#" + panelId).remove();
        $('#main-tabs').tabs("refresh");
    });
}

//绑定TAB右键菜单事件
function bindTabContextmenu() {
    $("#main-tabs li").bind('contextmenu', function (e) {
        /* 选中当前触发事件的选项卡 */
        var id = $(this).attr("id");
        var $lis = $("#main-tabs ul li");
        var index = 0;
        for (var i = 0; i < $lis.length; i++) {
            var $li = $($lis[i]);
            if ($li.attr("id") == id) {
                index = i;
                break;
            }
        }
        $('#main-tabs').tabs("option", "active", index);

        $("#tab_rightmenu").css({
            left: e.pageX,
            top: e.pageY
        });
        $("#tab_rightmenu").removeClass("hidden");
        return false;
    });
}


function openWindow(title, url, width, height, modal) {
    var $win = $("#" + title);
    if ($win.length == 0) {

    } else {
        $win.remove();
    }
    var ifm_html = "<iframe id=\"ifm_" + title + "\" class=\"tab-iframe\" src=\"" + url + "\"  width=\"100%\" height=\"" + (height - 40) + "\" frameborder=\"no\" border=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"auto\" allowtransparency=\"yes\"></iframe>";
    $("body").append("<div id=" + title + " class='easyui-window'>" + ifm_html + "</div>");
    $("#" + title).window({
        modal: modal,
        closed: true,
        maximizable: false,
        width: width,
        height: height,
        title: title,
        resizable: false,
        iconCls: 'fa fa-th-large padding-top-5'
    });

    $("#" + title).window("open");
}


function closeWindow(title) {
    var $win = $("#" + title);
    if ($win.length < 1) {
        window.parent.closeWindow(title);
    } else {
        //
        $(".easyui-datagrid").each(function (index) {
            var $this = $(this);
            $this.datagrid('reload');
        });
        $("#" + title).window("close");
    }
}

function closeWindowRefrresh(title) {
    var $win = $("#" + title);
    if ($win.length < 1) {
        window.parent.closeWindowRefrresh(title);
    } else {
        location.reload();
        $("#" + title).window("close");
    }
}


function width(num) {
    return (($(window).width()) * num);
}


//设置查询表单
function settingSearchForm() {
    var $groups = $("#search_form").find(".form-group");
    for (var i = 0; i < $groups.length; i++) {
        var $group = $($groups[i]);
        if ($group.hasClass("hidden")) {
            $group.removeClass("hidden");
            $group.prepend("<input class='settting-checkbox' type='checkbox'  ></input>");
        } else {
            $group.prepend("<input class='settting-checkbox' type='checkbox' checked='checked' ></input>");
        }
    }
    $("#search_form").find(".form-buttons").css("display", "none");

    var html = "<div class=\"setting-buttons text-align-center margin-bottom-5 \">" +
        "<a href=\"javascript:saveSearchSetting();\" class=\"btn btn-success\">保存配置</a>" +
        "<a href=\"javascript:cancelSearchSetting();\" class=\"btn btn-danger\">取消配置</a>" +
        "</div>" +
        "<div class=\"clearfix\" ></div>";
    $("#search_form").append(html);

}
//保存查询表单配置
function saveSearchSetting() {
    var $groups = $("#search_form").find(".form-group");
    for (var i = 0; i < $groups.length; i++) {
        var $group = $($groups[i]);
        if ($group.find(".settting-checkbox").prop("checked")) {//checked

        } else {
            $group.addClass("hidden");
        }
        $group.find(".settting-checkbox").remove();
    }
    $("#search_form").find(".form-buttons").css("display", "");
    $("#search_form").find(".setting-buttons").remove();
    $("#data_table").datagrid("reload");
}
//取消查询表单配置
function cancelSearchSetting() {
    saveSearchSetting();
}

//显示或隐藏查询表单
function hidshowSearchForm() {
    if ($("#search_form").hasClass("hidden")) {
        $("#search_form").removeClass("hidden");
        $("#datagrid_toolbar .buttons-preview").removeClass("hidden");
        $("#datagrid_toolbar").find(".search-header").remove();

    } else {
        $("#search_form").addClass("hidden");
        $("#datagrid_toolbar .buttons-preview").addClass("hidden");
        var label = "查询条件";
        var html = "<div class=\"search-header widget-header  \">" +
            "<i class=\"widget-icon fa   fa-search-minus \"></i>" +
            "<span class=\"widget-caption\">" + label + "</span>" +
            "<div class=\"widget-buttons\">" +
            "<a href=\"javascript:hidshowSearchForm()\" ><i class=\"fa fa-plus\"></i></a>" +
            "</div>" +
            "</div>";

        $("#datagrid_toolbar").prepend(html);
    }
    $("#data_table").datagrid("reload");
}


//设置数据表格
function settingDatagrid() {
    var html = "";
    var $tds = $(".datagrid-header-row td");
    for (var i = 0; i < $tds.length; i++) {
        var $td = $($tds[i]);
        var field = $td.attr("field");
        var name = $td.text();
        var display = $td.css("display");

        if (name != '') {
            html += "<div class='cloumn' field='" + field + "'  >";
            if (display == 'none') {
                html += "<input class='datagrid-setting-checkbox' type='checkbox'  ></input>" + name;
            } else {
                html += "<input class='datagrid-setting-checkbox' type='checkbox'  checked='checked' ></input>" + name;
            }
            html += "</div>";
        }

    }


    bootbox.dialog({
        message: html,
        title: "配置数据表格",
        className: "",
        buttons: {
            success: {
                label: "保存配置",
                className: "btn-success",
                callback: function () {
                    var $cloums = $(".bootbox .bootbox-body").find(".cloumn");
                    for (var i = 0; i < $cloums.length; i++) {
                        var $cloum = $($cloums[i]);
                        if ($cloum.find(".datagrid-setting-checkbox").prop("checked")) {//checked
                            $("#data_table").datagrid('showColumn', $cloum.attr("field"));
                        } else {
                            $("#data_table").datagrid('hideColumn', $cloum.attr("field"));
                        }

                    }
                }
            },
            "取消配置": {
                className: "btn-danger",
                callback: function () {
                }
            }
        }
    });

}

//设置数据表格
function jmdaSettingDatagrid() {
    var html = "";
    var aaa=WeJson.toString(fieldNameMap);
    console.log(aaa);
    for (field in fieldNameMap) {
        var name = fieldNameMap[field];
        if (name != '') {
            html += "<div class='cloumn' field='" + field + "'  >";
            html += "<input class='datagrid-setting-checkbox' type='checkbox'  checked='checked' ></input>" + name;
            html += "</div>";
        }
    }
    bootbox.dialog({
        message: html,
        title: "配置数据表格",
        className: "",
        buttons: {
            success: {
                label: "保存配置",
                className: "btn-success",
                callback: function () {
                    var $cloums = $(".bootbox .bootbox-body").find(".cloumn");
                    for (var i = 0; i < $cloums.length; i++) {
                        var $cloum = $($cloums[i]);
                        if ($cloum.find(".datagrid-setting-checkbox").prop("checked")) {//checked
                            $("#data_table").datagrid('showColumn', $cloum.attr("field").trim());
                        } else {
                            $("#data_table").datagrid('hideColumn', $cloum.attr("field").trim());
                        }

                    }
                }
            },
            "取消配置": {
                className: "btn-danger",
                callback: function () {
                }
            }
        }
    });

}
//重新加载easyui样式
function resizeEasyuiDom() {
    var $textboxs = $(".textbox");
    for (var i = 0; i < $textboxs.length; i++) {
        var $textbox = $($textboxs[i]);
        $textbox.css("width", $textbox.parent().width());
        var $text = $textbox.find(".textbox-text");
        $text.css("width", $text.parent().width() - 18);
    }
}
//重新设置弹出窗口按钮
function resetWinPageButs() {
    var $butsrow = $(".win .buts-row-bottom");
    var top = $(window).height() - 45;
    $butsrow.css("top", top);

}
//打开菜单
function openMenu(id, label, url) {
	id = Math.random() + id;
    MainTab.createTab(id, label, url);
    $(".sidebar-menu li").removeClass("active");
    if (id == 'home') {
        $lis = $(".sidebar-menu").children();
        for (var i = 0; i < $lis.length - 1; i++) {
            var $li = $($lis[i]);
            if (!$li.hasClass("hidden")) {
                $li.addClass("active");
                break;
            }
        }
    } else {
        $("#menu_" + id).addClass("active");
    }
}
//打开菜单
function openMenu1(id, label, url) {
    MainTab.createTab(id, label, url);
    $(".sidebar-menu li").removeClass("active");
    if (id == 'home') {
        $lis = $(".sidebar-menu").children();
        for (var i = 0; i < $lis.length - 1; i++) {
            var $li = $($lis[i]);
            if (!$li.hasClass("hidden")) {
                $li.addClass("active");
                break;
            }
        }
    } else {
        $("#menu_" + id).addClass("active");
    }
}

function getSkinColor(skinName) {
    switch (skinName) {
        case "blue":
            return "#5DB2FF";
            break;
        case "azure":
            return "#2dc3e8";
            break;
        case "teal":
            return "#03B3B2";
            break;
        case "green":
            return "#53a93f";
            break;
        case "orange":
            return "#FF8F32";
            break;
        case "pink":
            return "#cc324b";
            break;
        case "darkred":
            return "#AC193D";
            break;
        case "purple":
            return "#8C0095";
            break;
        case "darkblue":
            return "#0072C6";
            break;
        case "gray":
            return "#585858";
            break;
        case "black":
            return "#474544";
            break;
        case "deepblue":
            return "#001940";
            break;
        default:
            return "#5DB2FF"
    }

}




