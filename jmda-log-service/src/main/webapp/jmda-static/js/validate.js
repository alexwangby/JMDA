//判断一个串实际长度（1个中文2个单位）
function length2(txtValue) {
    var cArr = txtValue.match(/[^\x00-\xff]/ig);
    return txtValue.length + (cArr == null ? 0 : cArr.length);
}


/*是否包含空格*/
function isIncSpace(ui) {
    var valid = /\s/;
    return (valid.test(ui));
}

/*是否包含系统禁用的字符*/
function isIncSym(ui) {
    var valid = /[\|\'\"\.\。\、\，\+\*\/\%\^\=\\\!\&\:\;\~\`\#\$]+/;
    return (valid.test(ui));
}
function isIncSym2(ui) {
    var valid = /[\|\'\"\.\。\、\，\+\*\/\%\^\=\\\!\&\:\;\~\`\#\$\@]+/;
    return (valid.test(ui));
}
function isIncSym3(ui) {
    var valid2 = /\s/;
    var valid = /[\|\'\"\.\。\、\，\+\*\/\%\^\=\\\!\&\:\;\~\`\#\$\@]+/;
    return (valid.test(ui) || valid2.test(ui));
}
function trim(str) {  //删除左右两端的空格
    return str.replace(/(^\s*)|(\s*$)/g, "");
}
function ltrim(str) {  //删除左边的空格
    return str.replace(/(^\s*)/g, "");
}
function rtrim(str) {  //删除右边的空格
    return str.replace(/(\s*$)/g, "");
}


/**
 IsNumber: 用于判断一个数字型字符串是否为数值型，
 还可判断是否是正数或负数，返回值为true或false
 string: 需要判断的字符串
 sign: 若要判断是正负数是使用，是正用'+'，负'-'，不用则表示不作判断
 */
function IsNumber(string, sign) {
    var number;
    if (string == null) return false;
    if ((sign != null) && (sign != '-') && (sign != '+')) {
        alert('IsNumber(string,sign)的参数出错：\nsign为null或"-"或"+"');
        return false;
    }
    number = new Number(string);
    if (isNaN(number)) {
        return false;
    }
    else if ((sign == null) || (sign == '-' && number < 0) || (sign == '+' && number > 0)) {
        return true;
    }
    else
        return false;
}

/**
 IsNumber: 用于判断一个数字型字符串是否为数值型，
 还可判断是否是非负数或非正数，返回值为true或false
 string: 需要判断的字符串
 sign: 若要判断是正负数是使用，是正用'+'，负'-'，不用则表示不作判断
 */
function isNumber2(string, sign) {
    var number;
    if (string == null) return false;
    if ((sign != null) && (sign != '-') && (sign != '+')) {
        alert('IsNumber(string,sign)的参数出错：\nsign为null或"-"或"+"');
        return false;
    }
    number = new Number(string);
    if (isNaN(number)) {
        return false;
    }
    else if ((sign == null) || (sign == '-' && number <= 0) || (sign == '+' && number >= 0)) {
        return true;
    }
    else
        return false;
}
/**
 * 非负整数校验
 * @constructor
 */
function nonNegativeInteger(num) {
    var r = "^\\d+$";
    return r.test(num);
}


/**
 IsDate: 用于判断一个字符串是否是日期格式的字符串

 返回值：
 true或false

 参数：
 DateString： 需要判断的字符串
 Dilimeter ： 日期的分隔符，缺省值为'-'
 Author: PPDJ
 sample:
 var date = '1999-1-2';
 if (IsDate(date))
 {
 alert('You see, the default separator is "-");
 }
 date = '1999/1/2";
 if (IsDate(date,'/'))
 {
 alert('The date\'s separator is "/");
 }
 */
//IsDate('2003-09-09');
function IsDate(DateString, Dilimeter) {
    if (DateString == null || DateString == '') return true;
    if (Dilimeter == '' || Dilimeter == null)
        Dilimeter = '-';
    var tempy = '';
    var tempm = '';
    var tempd = '';
    var tempArray;
    if (DateString.length < 8 && DateString.length > 10)
        return false;
    tempArray = DateString.split(Dilimeter);
    if (tempArray.length != 3)
        return false;
    if (tempArray[0].length == 4) {
        tempy = tempArray[0];
        tempd = tempArray[2];
    }
    else {
        tempy = tempArray[2];
        tempd = tempArray[1];
    }
    tempm = tempArray[1];
    var tDateString = tempy + '/' + tempm + '/' + tempd + ' 8:0:0';//加八小时是因为我们处于东八区
    var tempDate = new Date(tDateString);
    if (tempd.substring(0, 1) == '0') {
        tempd = tempd.substring(1);
    }
    if (tempm.substring(0, 1) == '0') {
        tempm = tempm.substring(1);
    }

    if (isNaN(tempDate))
        return false;
    if (((tempDate.getUTCFullYear()).toString() == tempy) && (tempDate.getMonth() == parseInt(tempm) - 1) && (tempDate.getDate() == parseInt(tempd))) {
        return true;
    }
    else {
        return false;
    }
}

/**
 *校验两个日期的先后
 *返回值：
 *如果其中有一个日期为空，校验通过,          返回true
 *如果起始日期早于等于终止日期，校验通过，   返回true
 *如果起始日期晚于终止日期，                 返回false    参考提示信息： 起始日期不能晚于结束日期。
 */
function checkDateEarlier(strStart, strEnd) {
    //如果有一个输入为空，则通过检验
    if (( strStart == "" ) || ( strEnd == "" ))
        return true;
    var arr1 = strStart.split("-");
    var arr2 = strEnd.split("-");
    var date1 = new Date(arr1[0], parseInt(arr1[1].replace(/^0/, ""), 10) - 1, arr1[2]);
    var date2 = new Date(arr2[0], parseInt(arr2[1].replace(/^0/, ""), 10) - 1, arr2[2]);
    if (arr1[1].length == 1)
        arr1[1] = "0" + arr1[1];
    if (arr1[2].length == 1)
        arr1[2] = "0" + arr1[2];
    if (arr2[1].length == 1)
        arr2[1] = "0" + arr2[1];
    if (arr2[2].length == 1)
        arr2[2] = "0" + arr2[2];
    var d1 = arr1[0] + arr1[1] + arr1[2];
    var d2 = arr2[0] + arr2[1] + arr2[2];
    if (parseInt(d1, 10) > parseInt(d2, 10)) {
        return false;
    } else {
        return true;
    }
}

function isMailAddress(name) // E-mail值检测
{
    if (name == '')return true;
    var i = name.indexOf("@");
    var j = name.lastIndexOf("@");
    if (i == -1)
        return false;
    if (i != j)
        return false;
    if (i == name.length)
        return false;
    return true;
}


//将数值型输入转换成货币格式

function number2MoneyOfMoneyFormUIComponent(moneyType, numberText) {
    if (moneyType == '$') {
        numberText.value = numberText.value.replace('\$', '');
    } else {
        numberText.value = numberText.value.replace(moneyType, '');
    }
    var stmp = "";
    if (numberText.value == stmp) return;
    var ms2 = numberText.value;
    var ms3 = '';
    if (ms2.indexOf('-') == 0 && ms2.length > 1) {
        ms3 = '-';
    }
    var ms = numberText.value.replace(/[^\d\.]/g, "").replace(/(\.\d{2}).+$/, "$1").replace(/^0+([1-9])/, "$1").replace(/^0+$/, "0");
    var txt = ms.split(".");
    while (/\d{4}(,|$)/.test(txt[0]))
        txt[0] = txt[0].replace(/(\d)(\d{3}(,|$))/, "$1,$2");
    numberText.value = moneyType + ms3 + txt[0] + (txt.length > 1 ? "." + txt[1] : "");
    //alert(txt[0].length);
    if (txt[0].length > 16) {
        alert("数值超出范围！支持的最大数值为 999999999999.99");
    }
}
//将数值型输入转换成货币格式

function number2MoneyOfMoneyGridFormUIComponent(moneyType, numberText) {
    var stmp = "";
    if (numberText == stmp || numberText == null) return;
    var ms2 = numberText;
    var ms3 = '';
    if (ms2.indexOf('-') == 0) {
        ms3 = '';
    }
    var ms = numberText.replace(/[^\d\.]/g, "").replace(/(\.\d{2}).+$/, "$1").replace(/^0+([1-9])/, "$1").replace(/^0+$/, "0");
    var txt = ms.split(".");
    while (/\d{4}(,|$)/.test(txt[0]))
        txt[0] = txt[0].replace(/(\d)(\d{3}(,|$))/, "$1,$2");
    if (txt[0].length > 16) {
        alert("数值超出范围！支持的最大数值为 999999999999.99");
    }
    return moneyType + ms3 + txt[0] + (txt.length > 1 ? "." + txt[1] : ".00");
}
//字符串全部替换 
// modify by wangwq  解决了报表脚本错误
function replaceAll2(streng, soeg, erstat) {
    var st = streng;
    if (soeg.length == 0)
        return st;
    var idx = st.indexOf(soeg);
    while (idx >= 0) {
        st = st.substring(0, idx) + erstat + st.substr(idx + soeg.length);
        idx = st.indexOf(soeg);
    }
    return st;
}
/*


 /*********************************************************/
/**
 *
 * @param {} text   文本
 * @param {} value  值
 * @param {} length 长度（number）
 * @param {} isInSpace 是否允许空格 false不允许
 * @param {} isNum 只允许数字-true
 * @return {Boolean}
 */

function validateLengthAndSpace(text, value, length, isInSpace, isNum) {
    //判读长度
    if (arguments.length == 3) {
        if ((length2(value) > length)) {
            Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]长度不能超过" + length + "个字节!");
            return false;
        }
        return true;
    }
    //判读是否为空
    if (arguments.length == 2) {
        if (value.replace(/\s/g, "") == "") {
            Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]不允许为空!");
            return false;
        }
        return true;
    }
    if (arguments.length == 5) {
        if (!isInSpace) {
            if (isIncSpace(value)) {
                Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]不允许包含空格!");
                return false;
            }
        }
        if ((length2(value) > length)) {
            Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]长度不能超过" + length + "个字节!");
            return false;
        }
        //if(isIncSym(value)){
        //	Ext.MessageBox.alert("警告!","数据未通过合法性校验，["+text+"]不允许包含非法字符!");
        ///	return false;
        //}
        if (isNum) {
            var pattern = /^(\d*,\d*$)|(^\d*$)/;
            if (!pattern.test(value)) {
                Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]必须是数字类型!");
                return false;
            }
        }
        return true;
    }

}


/*********************************************************/
function validateEmail(text, value) {
    var re = new RegExp(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/g);
    //var re=/^w+([-+.]w+)*@w+([-.]w+)*.w+([-.]w+)*$/g;
    if (!re.test(value)) {
        Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]格式错误!");
        return false;
    }
    return true;
}

function validateIP(text, value) {
    var re = new RegExp(/^([1-9]|[1-9]\d|1\d{2}|2[0-1]\d|22[0-3])(\.(\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])){3}$/g);
    if (!re.test(value)) {
        Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]格式错误!");
        return false;
    }
    return true;
}
function validateIps(text, value) {
    var pattern1 = /^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.$/;
    var pattern2 = /^([1-9]|[1-9]\d|1\d{2}|2[0-1]\d|22[0-3])(\.(\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])){3}$/;
    if (pattern1.test(value) || pattern2.test(value)) {
        return true;
    }
    Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]格式错误!");
    return false;

}
//是否是手机号码
function validateMobilePhone(text, value) {
    //if(! (/^(?:13\d|15[0689])-?\d{5}(\d{3}|\*{3})$/.test(value)) ){
    if (value.charAt(0) == '0' && value.length > 12) {
        Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]格式错误!");
        return false;
    } else if (value.charAt(0) != '0' && value.length > 11) {
        Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]格式错误!");
        return false;
    }
    ;
    if (!(/(^0{0,1}13\d{9}$)|(^0{0,1}15\d{9}$)|(^0{0,1}18\d{9}$)/.test(value))) {
        Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]格式错误!");
        return false;
    }
    return true;
}
//是否是电话号码
function validatePhone(text, value) {
    if (!(/(^[0-9]{3,4}\-[0-9]{7,8}$)|(^[0-9]{7,8}$)|(^\([0-9]{3,4}\)[0-9]{7,8}$)|(^[0-9]{3,4}\-[0-9]{7,8}\-[0-9]{1,9}$)|(^[0-9]{7,8}\-[0-9]{1,9}$)|(^\([0-9]{3,4}\)[0-9]{7,8}\-[0-9]{1,9}|(^\+[0-9]{2,4}\-[0-9]{2,4}\-[0-9]{6,8}$)|(^\+\d{2,4}\d{2,4}\d{6,8}$))/ig.test(value))) {
        if (value.indexOf('+') > -1) {
            Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]格式错误!<br>国际电话格式,如：+86-10-12345678");
            return false;
        }
        Ext.MessageBox.alert("警告!", "数据未通过合法性校验，[" + text + "]格式错误!<br>国内电话格式，如：010-12345678或0312-12345678或12345678");
        return false;
    }

    return true;
}


function validateMobilePhone2(text, value) {
    if (value != '') {
        //if(! (/^(?:13\d|15[0689])-?\d{5}(\d{3}|\*{3})$/.test(value)) ){
        if (!(/^\d{11}$/.test(value))) {
            alert("数据未通过合法性校验，[" + text + "]格式错误!");
            return false;
        }
    }
    return true;
}
function validatePhone2(text, value) {
    if (value != '') {
        if (!(/^(([+\?]\d{2,3}-)?([0\+]\d{2,3}-)?(0\d{2,3})-)?(\d{7,8})(-(\d{3,}))?$/.test(value))) {
            if (value.indexOf('+') > -1) {
                alert("数据未通过合法性校验，[" + text + "]格式错误!<br>国际电话格式,如：+86-10-12345678");
                return false;
            }
            alert("数据未通过合法性校验，[" + text + "]格式错误!<br>国内电话格式，如：010-12345678或0312-12345678或12345678");
            return false;
        }
    }
    return true;
}


/**
 * 获取radio的值
 * @param {} radioID
 * @return {}
 */
function getValueFromRadios(radioID) {
    var radios = document.getElementsByName(radioID);
    for (var i = 0; i < radios.length; i++) {
        if (radios[i].checked) {
            return radios[i].value;
        }
    }
}
/**
 * 获取select的值
 * @param {} radioID
 * @return {}
 */
function getValueFromSelect(selectID) {
    var select = $(selectID);
    for (var i = 0; i < select.options.length; i++) {
        if (select.options[i].selected) {
            return select.options[i].value;
        }
    }
}
/**
 * 获取checkbox的值
 * @param {} radioID
 * @return {}
 */
function getValueFromCheckbox(checkboxID) {
    return $(checkboxID).checked ? '1' : '0';
}
function openNavLink(srcURL, targetFrame) {
    srcURL = encodeURI(srcURL);
    window.open(srcURL, targetFrame, "height=100%,width=100%");
}

function addRow() {
    alert(1);
}
function RemoveGrid() {
    alert(2);
}
function saveGrid() {
    alert(3);
}
function UndoGrid() {
}
function RefreshGrid() {
    alert(4);
}
/**/
function validateNumber(id) {
    var value = $('#' + id).val();
    var isNonNegativeInteger = isNumber2(value, '+');
    console.log(value + isNonNegativeInteger);
    if (!isNonNegativeInteger) {
        $.messager.alert('提示', "请输入非负整数", 'info', function () {
            $('#' + id).focus();
            $('#' + id).select();
        });
    } else {
        $('#' + id).val(parseInt(value));
    }
}