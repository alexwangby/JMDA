﻿var tree;
var curNode;
var curNodeType;
var root;
var FORMExpensesViewTabs;
var FROMExpensesViewWiewPort;
var currentNode;
var uiWindow;
Ext.onReady(function() {
	Ext.QuickTips.init();
	Ext.BLANK_IMAGE_URL = application_STATIC_SERVER + 'jmda-static/js/extjs3/images/default/s.gif';
	FORMExpensesViewTabs = new Ext.TabPanel({
		id : 'rightPanel',
		region : 'center',
		margins : '0 0 0 0',
		activeTab : 0,
		defaults : {
			autoScroll : true
		},
		layoutConfig : {
			animate : true
		},

		enableTabScroll : true,
		margins : '0 0 10 0',
		items : [ {
			title : form_title,
			id : 'bd',
			listeners : {
				activate : handleActivate
			},
			html : '<iframe name="PageFrame" id="PageFrame" src=""  frameborder=0 width=100% height=100% ></iframe>',
			autoScroll : true
		}, {
			title : '工具条',
			id : 'gjt',
			listeners : {
				activate : handleActivate
			},
			disabled:false,
			metadataId : '',
			html : '<iframe name="ButtonsFrame" id="ButtonsFrame" src=""  frameborder=0 width=100% height=100% ></iframe>',
			autoScroll : true
		} ]
	});
	FROMExpensesViewWiewPort = new Ext.Viewport({
		layout : 'border',
		items : [ {
			region : 'west',
			id : 'west-panel',
			title : '',
			contentEl : 'formList_Tree',
			width : 210,
			collapsible : true,
			margins : '0 0 0 0',
			// layout: 'accordion',
			layout : 'fit',
			layoutConfig : {
				animate : true
			},
			tbar : [ {
				id : 'editsubTable',
				tooltip : '编辑数据集',
				text : '编辑',
				icon : application_STATIC_SERVER + 'jmda-static/img/repository/database.png',
				handler : function() {
					var nodeId = currentNode.attributes.id;
					var nodeText = currentNode.attributes.text;
					var nodeType = currentNode.attributes.type;
					var tabFormUI = Ext.getCmp('rightPanel').getItem(0);
					if (nodeType != 'dataRoot') {
						var formMetadataId = currentNode.attributes.id.substring(2);
						openDatasetWin(nodeText, formMetadataId)
					}
				},
				iconCls : 'x-btn-text-icon'
			}, {
				id : 'addsubTable',
				tooltip : '添加表格',
				text : '添加',
				icon : application_STATIC_SERVER + 'jmda-static/img/repository/database_add.png',
				handler : function() {
					openDatasetWin('添加表格', '');
				},
				iconCls : 'x-btn-text-icon'
			}, '-', {
				id : "deleteC",
				disabled : true,
				tooltip : '删除表格',
				text : '删除',
				icon : application_STATIC_SERVER + 'jmda-static/img/repository/database_delete.png',
				handler : function() {
					// root.reload();
					if (currentNode != undefined) {
						if (currentNode.id.indexOf('C_') != -1) {
							var datasetUrl = basePath + 'repository/form/metadata/openFormMetadaDataSetPage?form_id=' + form_id + '&formMetadataId=' + formMetadataId;
							var formMetadataId = currentNode.id.substring(2);
							Ext.MessageBox.confirm('提示', '确认要删除您选中的【' + currentNode.text + '】数据集？', function(btn) {
								if (btn == 'yes') {
									Ext.Ajax.request({
										url : basePath + 'repository/form/metadata/removeFormMetadaDataSet',
										method : 'POST',
										params : {
											form_id : form_id,
											formMetadataId : formMetadataId
										},
										failure : function(response, options) {
											Ext.MessageBox.alert('操作失败', '操作失败！可能是网络超时！');
											return false;
										},
										success : function(response, options) {
											if (response.responseText == 'ok') {
												root.reload();
												setTimeout(selectRoot, 500);
												Ext.MessageBox.alert('操作提示', '表单模型数据集删除成功!');
												return false;
											} else {
												Ext.MessageBox.alert('操作失败', '表单模型数据集删除失败！');
												return false;
											}

										}
									});
								}
							});
						} else {
							Ext.MessageBox.alert('操作提示', '请选择要删除的数据集！');
							return false;
						}
					} else {
						Ext.MessageBox.alert('操作提示', '请选择要删除的数据集！');
						return false;
					}
				},
				iconCls : 'x-btn-text-icon'
			} ]
		}, FORMExpensesViewTabs ]
	});
	var Tree = Ext.tree;
	tree = new Tree.TreePanel({
		el : 'formList_Tree',
		animate : true,
		width : 208,
		autoScroll : true,
		border : false,
		containerScroll : true,
		enableDD : false,
		rootVisible : true,
		dropConfig : {
			appendOnly : true
		}
	});
	root = new Tree.AsyncTreeNode({
		text : '数据集',
		draggable : false,
		cls : 'treeBMFormSJY',
		id : 'modelroot',
		type : 'dataRoot',
		expanded : true,
		loader : new Ext.tree.TreeLoader({
			dataUrl : basePath + 'repository/form/metadata/getTreeJson',
			baseParams : {
				requestType : 'root',
				formId : form_id,
				param1 : ''
			}
		}),
		leaf : false
	});
	tree.on('click', function(node) {
		currentNode = node;
		var nodeId = node.attributes.id;
		var nodeType = node.attributes.type;
		var tabFormUI = Ext.getCmp('rightPanel').getItem(0);
		if (nodeType != 'dataRoot') {
			var formMetadataId = node.attributes.id.substring(2);
			PageFrame.location = encodeURI(basePath + 'repository/form/metadatamap/openFormMeatadataPage?form_id=' + form_id + "&formMetadataId=" + formMetadataId);
			tabFormUI.setTitle(node.text);
			Ext.getCmp('rightPanel').getItem(0).show();
		}
		if (nodeType == 'child') {
			Ext.getCmp("deleteC").setDisabled(false);
			//Ext.getCmp("gjt").setDisabled(false);
		} else {
			Ext.getCmp("deleteC").setDisabled(true);
			//Ext.getCmp("gjt").setDisabled(true);
		}
	});
	tree.on("beforeload", function(node) {
		var nodeLoader = node.attributes.loader;
		if (node.attributes.type == 'dataRoot') {
			nodeLoader.baseParams.requestType = 'root';
			nodeLoader.baseParams.formId = form_id;
		} else if (node.attributes.type == 'father') {
			var nodeId = node.attributes.id.substring(2);
			nodeLoader.baseParams.requestType = 'father';
			nodeLoader.baseParams.param1 = nodeId;
			nodeLoader.baseParams.formId = form_id;
		}
	}, this);
	tree.setRootNode(root);
	tree.render();
	setTimeout(selectRoot, 500);
});
function selectRoot() {
	if (root != null && root.findChild('type', 'father') != undefined) {
		Ext.getCmp("deleteC").setDisabled(true);
		var tabFormUI = Ext.getCmp('rightPanel').getItem(0);
		var fatherNode = root.findChild('type', 'father');
		currentNode = fatherNode;
		fatherNode.select();
		var formMetadataId = fatherNode.attributes.id.substring(2);
		PageFrame.location = encodeURI(basePath + 'repository/form/metadatamap/openFormMeatadataPage?form_id=' + form_id + "&formMetadataId=" + formMetadataId);
		// ButtonsFrame.location = encodeURI(basePath +
		// 'repository/form/buttons/openFormMeatadataButtonsPage?form_id=' +
		// form_id + "&formMetadataId=" + formMetadataId);
		tabFormUI.setTitle(form_title);
		Ext.getCmp('rightPanel').getItem(0).show();
	}
}
function openDatasetWin(formMetadataName, formMetadataId) {
	var datasetUrl = basePath + 'repository/form/metadata/openFormMetadaDataSetPage?form_id=' + form_id + '&formMetadataId=' + formMetadataId;
	openExtWindow(formMetadataName, 750, 380, false, false, datasetUrl);
	var btnOk = new Ext.Button({
		text : '保 存',
		tooltip : '保存当前页面数据',
		handler : function() {
			Operate_DlgPage.save();
		},
		minWidth : 75
	});
	OperateWinObj.addButton(btnOk);
	OperateWinObj.addButton({
		text : '关 闭',
		tooltip : '关闭向导',
		handler : function() {
			OperateWinObj.hide();
		},
		minWidth : 75
	});
	OperateWinObj.show();
}

function openFormMetadatMapWin(formMetadataName, formMetadataId) {
	var datasetUrl = basePath + 'repository/form/metadatamap/openFormMetadataMapSelectPage?form_id=' + form_id + '&formMetadataId=' + formMetadataId;
	openExtWindow(formMetadataName, 750, 380, false, false, datasetUrl);
	var btnOk = new Ext.Button({
		text : '确定',
		tooltip : '添加到数据集',
		handler : function() {
			var selectJson = Operate_DlgPage.getSelectVavlue();
			Ext.Ajax.request({
				url : basePath + 'repository/form/metadatamap/addFormMetadataMap',
				method : 'POST',
				params : {
					form_id : form_id,
					formMetadataId : formMetadataId,
					selectJson : selectJson
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('操作失败', '操作失败！可能是网络超时！');
					return false;
				},
				success : function(response, options) {
					// TODO 刷新右侧表格
					OperateWinObj.hide();
					PageFrame.reflashGrid();

				}
			});
		},
		minWidth : 75
	});
	OperateWinObj.addButton(btnOk);
	OperateWinObj.addButton({
		text : '关 闭',
		tooltip : '关闭向导',
		handler : function() {
			OperateWinObj.hide();
		},
		minWidth : 75
	});
	OperateWinObj.show();
}

function refreshTree() {
	root.reload();
}

function handleActivate(tab) {
	if (tab.id == 'gjt') {
		var formMetadataId = currentNode.attributes.id.substring(2);
		if (formMetadataId != tab.metadataId) {
			tab.metadataId = formMetadataId;
			ButtonsFrame.location = encodeURI(basePath + 'repository/form/buttons/openFormMeatadataButtonsPage?form_id=' + form_id + "&formMetadataId=" + formMetadataId);
		}
	}
}

var uiWindow;
function changeUi(uiRowIndex) {

	var uiTabs = new Ext.TabPanel({
		region : 'center',
		deferredRender : false,
		activeTab : 0,
		border : true
	});
	var uiRecord = PageFrame.grid.getStore().getAt(uiRowIndex);
	var id = uiRecord.data.id;
	var uiName = uiRecord.data.ui_type;
	var ui_param = uiRecord.data.ui_param;
	var columnTitle = uiRecord.data.column_title;
	var clumnLabel = uiRecord.data.column_lable;
	var curUiType = uiName;
	createViewTab("ui", uiName, "", "", true);
	var uiroot = new Ext.tree.TreeNode({
		id : 'uiRoot',
		text : 'ui',
		expanded : true
	});
	var nullUi = new Ext.tree.AsyncTreeNode({
		id : 'nullUi',
		text : '无',
		cls : 'formUIComponent_text',
		leaf : true
	});
	var danhang = new Ext.tree.AsyncTreeNode({
		id : 'danhang',
		text : '单行',
		cls : 'formUIComponent_text',
		leaf : true
	});
	var shuzhi = new Ext.tree.AsyncTreeNode({
		text : "数值",
		leaf : true,
		cls : 'formUIComponent_number',
		id : 'shuzhi'
	});
	var data = new Ext.tree.AsyncTreeNode({
		text : "日期",
		leaf : true,
		cls : 'formUIComponent_date',
		id : 'data'
	});
	var select = new Ext.tree.AsyncTreeNode({
		text : "列表",
		cls : 'formUIComponent_combox',
		leaf : true,
		id : 'select'
	});
	var refLableRedis = new Ext.tree.AsyncTreeNode({
		text : "REDIS关联显示",
		cls : 'formUIComponent_text',
		leaf : true,
		id : 'refLableRedis'
	});
	var refLableSQL = new Ext.tree.AsyncTreeNode({
		text : "SQL关联显示",
		cls : 'formUIComponent_text',
		leaf : true,
		id : 'refLableSQL'
	});
	var duohang = new Ext.tree.AsyncTreeNode({
		text : "多行",
		cls : 'formUIComponent_textArea',
		leaf : true,
		id : 'duohang'
	});
	var datadic = new Ext.tree.AsyncTreeNode({
		text : "数据选择器",
		leaf : true,
		cls : 'formUIComponent_text',
		id : 'datadic'
	});
	var ycy = new Ext.tree.AsyncTreeNode({
		text : "隐藏域",
		leaf : true,
		cls : 'formUIComponent_text',
		id : 'ycy'
	});
	var radio = new Ext.tree.AsyncTreeNode({
		text : "单选按钮",
		leaf : true,
		cls : 'formUIComponent_text',
		id : 'radio'
	});
	var radio = new Ext.tree.AsyncTreeNode({
		text : "Label",
		leaf : true,
		cls : 'formUIComponent_text',
		id : 'label'
	});
	uiroot.appendChild(nullUi);
	uiroot.appendChild(danhang);
	uiroot.appendChild(duohang);
	uiroot.appendChild(shuzhi);
	uiroot.appendChild(data);
	// uiroot.appendChild(checkbox);
	uiroot.appendChild(select);
	uiroot.appendChild(refLableRedis);
	uiroot.appendChild(refLableSQL);
	uiroot.appendChild(datadic);
	uiroot.appendChild(ycy);
	uiroot.appendChild(radio);
	var uiTree = new Ext.tree.TreePanel({
		title : "表单UI",
		id : "uiTree",
		region : 'west',
		width : 150,
		border : true, // 面板边框
		useArrows : false, // 箭头节点图标
		root : uiroot, // 根节点
		autoScroll : true, // 内容溢出时产生滚动条
		animate : false,
		rootVisible : false
	});
	uiTree.on('click', function(node) {
		var text = node.attributes.text;
		curUiType = text;
		createViewTab("ui", text, "", "", true);
	});
	uiWindow = new Ext.Window({
		title : '' + columnTitle + '(' + clumnLabel + ')',
		id : "uiwindowid",
		width : 800,
		height : 450,
		modal : true,
		constrainHeader : true,
		plain : true,
		layout : 'border',
		items : [ uiTree, uiTabs ]
	});
	uiWindow.show();
	function createViewTab(tabId, tabTitle, iconClassName, htmlSrc, isRefresh) {
		tabId = 'tabs_' + tabId;
		var html = "<iframe id='Ui_Portal_Tab_Frame' name='Ui_Portal_Tab_Frame' src='' frameborder='0' scrolling=\"false\" width='100%' height='100%'  ></iframe>";
		var tab = uiTabs.getItem(tabId);
		if (tab == null) {
			tab = uiTabs.add({
				id : tabId,
				title : tabTitle,
				tabTip : tabTitle,
				closable : false,
				autoScroll : true,
				iconCls : iconClassName,
				tbar : [ {
					text : '确定',
					iconCls : 'iconSave',
					handler : function() {
						var curParams = Ui_Portal_Tab_Frame.getSettingParams();
						PageFrame.settingUiComponentParams(uiRowIndex, curUiType, curParams);
					}
				} ],
				html : html
			}).show();
			tab.addListener('show', function() {
				setTimeout(function() {
					document.getElementById('formForm').action = basePath + "repository/form/metadatamap/ui/getSettingPage";
					document.getElementById('uiType').value = curUiType;
					document.getElementById('masterDataset').value = PageFrame.is_master_dataset;
					var curParams = curUiType == uiName ? ui_param : '';
					document.getElementById('uiParams').value = curParams;
					document.getElementById('formForm').target = 'Ui_Portal_Tab_Frame';
					document.getElementById('formForm').submit();
				}, 500);
			});
		} else {
			if (isRefresh) {
				tab.setTitle(tabTitle);
			}
			tab.show();
		}

	}
}
