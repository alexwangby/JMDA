var WeJson = (function () {
    function toString(jsonObject) {
        if (typeof(jsonObject) == "string") {
            return jsonObject;
        } else {
            try {
                return JSON.stringify(jsonObject);
            }
            catch (e) {
                alert("请检查对象的格式是否正确")
            }
        }
    }

    function toObject(jsonString) {
        if (typeof(jsonString) == "object") {
            return jsonString;
        } else {
            try {
                return JSON.parse(jsonString);
            }
            catch (e) {
                alert("请检查字符串格式是否正确")
            }

        }
    }

    return {
        toString: toString,
        toObject: toObject
    };
})();