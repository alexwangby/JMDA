﻿<!DOCTYPE html>
<head>
    <title>日志记录</title>
<#include "../../comm/eformMeta.ftl" />
</head>
<!-- /Head -->
<!-- Body -->
<body >
	<div id="cc1" class="easyui-layout" data-options="fit:true">
	    <div data-options="region:'west',title:'日志记录列表'" style="width:302px;">
	    <!--
	    	<div class="buttons-preview" style="padding:10px 10px 0px 10px;background:#eee" >
                <a class="btn btn-default  btn-sm" href="javascript:addCompany()"><i class="fa fa-plus"></i>添加子公司</a>
                <a class="btn btn-default  btn-sm" href="javascript:addDepartment()"><i class="fa fa-plus"></i>添加子部门</a>
                <a class="btn btn-danger btn-sm" href="javascript:remove()"><i class="fa fa-times"></i>删除</a>
	        </div>
	        -->
	    	<ul class="easyui-tree" id="worklist_west_tree"
        			data-options="url:'${basePath}/log/getTree',
					method:'get',
					lines:true">
			</ul>
	    </div>
	    <div data-options="region:'center'" style="background: rgba(0, 0, 0, 0) no-repeat scroll 0 0;">
	    	<iframe id="frame" class="tab-iframe" width="100%" height="100%" frameborder="no" allowtransparency="no" scrolling="auto"
	    			marginheight="0" marginwidth="0" border="0">

	    	</iframe>
	    </div>
	</div>


<script src="${Application.STATIC_SERVER}/jmda-static/assets/js/beyond.js"></script>
<script src="${Application.STATIC_SERVER}/jmda-static/assets/js/toastr/toastr.js"></script>
<script src="${Application.STATIC_SERVER}/jmda-static/assets/js/bootbox/bootbox.js"></script>

<!--easyui -->
<link rel="stylesheet" type="text/css" href="${Application.STATIC_SERVER}/jmda-static/assets/js/jquery-easyui-1.4.3/themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="${Application.STATIC_SERVER}/jmda-static/assets/js/jquery-easyui-1.4.3/themes/icon.css">
<script type="text/javascript" src="${Application.STATIC_SERVER}/jmda-static/assets/js/jquery-easyui-1.4.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${Application.STATIC_SERVER}/jmda-static/assets/js/jquery-easyui-1.4.3/locale/easyui-lang-zh_CN.js"></script>
<!--
<script type="text/javascript" src="/hbdiy-sys/assets/js/jquery-validation/dist/jquery.validate.js"></script>
<script type="text/javascript" src="/hbdiy-sys/assets/js/jquery-validation/dist/localization/messages_zh.js"></script>
-->
<script src="${Application.STATIC_SERVER}/jmda-static/assets/js/init.js"></script>
<script type="text/javascript" >
	//存放id+类别
	var str;
	var Node;
	$('#worklist_west_tree').tree({
		onClick: function(node){
			//拆分节点id值
			Node=node;
			str = node.id.split("@");
			getForm();
		}
	});

	//给iframe添加url
	function getForm(){
		if(str[1] == "org"){
		   document.getElementById("frame").src="${basePath}/log/getData?id="+str[0];
		}
	};

</script>
</body>
<!--  /Body -->
</html>
