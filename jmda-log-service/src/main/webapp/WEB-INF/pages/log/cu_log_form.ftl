<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <title>log form</title>
<#include "../../comm/eformMeta.ftl" />
<script type="text/javascript" src="${basePath}/static/js/log/log.js"></script>
</head>
<body>
	<form id="search_form" class="search-form" role="form">
		<div class="row">
			<div class="col-sm-4">
			 	<div class="form-group">
			 		<label >请输入json串</label>
					<input type="text" class="form-control" name="contents" id="contents" value="${contents}" />
				</div>
			</div>
			<div class="col-sm-4 form-buttons float-right margin-top-20" >
	            	<a class="btn btn-default btn-xs shiny icon-only  float-right blue btn-min" href="javascript:hidshowSearchForm();">
	                	<i class="fa fa-minus"></i></a>
	                <a class="btn btn-labeled btn-blue float-right "  onClick="workListSearch('${id}');">
	                    <i class="btn-label fa  fa-search "></i>查 询
	                </a>
	            </div>
		</div>
	</form>


     <form id="data_form" method="post" action="" class="page-form">

        <div class="form-title">
           	 日志查看<br/>
		<a class="btn btn-primary btn-sm" onClick="downloadData();">下载日志</a>
		<input id="ids" type="hidden" value="${id}" />
        </div>
		<div id="record" name="record">
		<#if records?exists>
		<table>
			<#list records as logsContentVO>
			  <td>
			   <pre>${logsContentVO.content}</pre>
			  </td>
			 </tr>
			</#list>
		</table>
		</#if>
		</div>
		<div>
		</div>
    </form>

</body>

